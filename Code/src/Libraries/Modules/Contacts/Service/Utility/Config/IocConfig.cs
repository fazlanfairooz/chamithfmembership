﻿using Abp.Dependency;
using Inx.Core.Base;
using Inx.Module.Contacts.Core.Service.Contacts;
using Inx.Module.Contacts.Core.Service.Contacts.interfaces;
using Inx.Module.Contacts.Core.Service.Utility;
using Inx.Module.Contacts.Core.Service.Utility.Interface;
using Inx.Module.Contacts.Data.DbContext;
using Inx.Service.Base.Controllers;
using Inx.Service.Base.Utility.Files;
using Inx.Utility.Cache.Runtime;
namespace Inx.Module.Contacts.Service.Utility.Config
{
    public class IocConfig
    {
        public static void Register(IIocManager iocManager)
        {
            iocManager.RegisterIfNot<IContactsDbSet, ContactsDbContext>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<IUnitOfWokContacts, UnitOfWork>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<IContactCategoryService, ContactCategoryService>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<IContactService, ContactService>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<ITableTemplateService, TableTemplateService>();
            iocManager.RegisterIfNot<IContactFileService, ContactFileService>(DependencyLifeStyle.Transient);

            #region genaral
            iocManager.RegisterIfNot<IBlobStorage, Blob>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<IMemoryCacheManager, MemoryCacheManager>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<IBlobStorage, Blob>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<IBaseInjector, BaseInjector>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<ICoreInjector, CoreInjector>();
            #endregion
        }
    }
}
