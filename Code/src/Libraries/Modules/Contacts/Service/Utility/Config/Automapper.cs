﻿using Abp.AutoMapper;
using Abp.Configuration.Startup;
using AutoMapper;
using Inx.Module.Contacts.Core.Bo.Contacts;
using Inx.Module.Contacts.Core.Bo.User;
using Inx.Module.Contacts.Core.Bo.Utlity;
using Inx.Module.Contacts.Data.Entity.User;
using Inx.Module.Contacts.Data.Entity.Utility;
using Inx.Module.Contacts.Service.Models.Contacts;
using Inx.Module.Contacts.Service.Models.Utility;
using Inx.Utility.Extentions;
using Inx.Utility.Models;
using Inx.Utility.Models.TableStorage;

namespace Inx.Module.Contacts.Service.Utility.Config
{
    public class Automapper
    {
        public static void Register(IAbpStartupConfiguration Configuration)
        {
            Configuration.Modules.AbpAutoMapper().Configurators.Add(cfg =>
            {
                cfg.CreateMap<ContactInfoViewModel, ContactInfoBo>()
                    .ForMember(des => des.Email, opt => opt.MapFrom(src => src.Email.TrimAndToLower()))
                    .ForMember(des => des.Phone, opt => opt.MapFrom(src => src.Phone.TrimAndToLower()))
                    .ReverseMap();
                cfg.CreateMap<ContactInfoBo, ContactInfo>()
                    .ForMember(des => des.Email, opt => opt.MapFrom(src => src.Email.TrimAndToLower()))
                    .ForMember(des => des.Phone, opt => opt.MapFrom(src => src.Phone.TrimAndToLower()))
                    .ReverseMap();
                cfg.CreateMap<Contactfile, ContactfileBo>().ReverseMap();
                cfg.CreateMap<ContactfileViewModel, ContactfileBo>().ReverseMap();

                cfg.CreateMap<PersonInfoViewModel, PersonInfoBo>().ReverseMap();
                cfg.CreateMap<PersonInfoBo, PersonInfo>().ReverseMap();

                cfg.CreateMap<WorkingInfoViewModel, WorkingInfoBo>().ReverseMap();
                cfg.CreateMap<WorkingInfoBo, WorkingInfo>().ReverseMap();

                cfg.CreateMap<ContactViewModel, ContactBo>().ReverseMap();

                cfg.CreateMap<ContactCategoryViewModel, ContactCategoryBo>()
                    .ForMember(des => des.Name, opt => opt.MapFrom(src => src.Name.Trim())).ReverseMap();
                cfg.CreateMap<ContactCategory, ContactCategoryBo>().ReverseMap();

                cfg.CreateMap<KeyValueListItemTableStorage<int>, KeyValueListItem<int>>().ReverseMap();
                cfg.CreateMap<KeyValueListItemTableStorage<string>, KeyValueListItem<string>>().ReverseMap();

                cfg.CreateMap<TableTemplate, TableTemplateBo>().ReverseMap();
                cfg.CreateMap<TableTemplateViewModel, TableTemplateBo>()
                    .ForMember(des => des.Keys, opt => opt.MapFrom(src => src.Keys.ToJsonString()))
                    .ReverseMap();
            });
        }

        public static MapperConfiguration Mappers()
        {
            return new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ContactInfoViewModel, ContactInfoBo>()
                    .ForMember(des => des.Email, opt => opt.MapFrom(src => src.Email.TrimAndToLower()))
                    .ForMember(des => des.Phone, opt => opt.MapFrom(src => src.Phone.TrimAndToLower()))
                    .ReverseMap();
                cfg.CreateMap<ContactInfoBo, ContactInfo>()
                    .ForMember(des => des.Email, opt => opt.MapFrom(src => src.Email.TrimAndToLower()))
                    .ForMember(des => des.Phone, opt => opt.MapFrom(src => src.Phone.TrimAndToLower()))
                    .ReverseMap();
                cfg.CreateMap<Contactfile, ContactfileBo>().ReverseMap();
                cfg.CreateMap<ContactfileViewModel, ContactfileBo>().ReverseMap();

                cfg.CreateMap<PersonInfoViewModel, PersonInfoBo>().ReverseMap();
                cfg.CreateMap<PersonInfoBo, PersonInfo>().ReverseMap();

                cfg.CreateMap<WorkingInfoViewModel, WorkingInfoBo>().ReverseMap();
                cfg.CreateMap<WorkingInfoBo, WorkingInfo>().ReverseMap();

                cfg.CreateMap<ContactViewModel, ContactBo>().ReverseMap();

                cfg.CreateMap<ContactCategoryViewModel, ContactCategoryBo>()
                    .ForMember(des => des.Name, opt => opt.MapFrom(src => src.Name.Trim())).ReverseMap();
                cfg.CreateMap<ContactCategory, ContactCategoryBo>().ReverseMap();

                cfg.CreateMap<KeyValueListItemTableStorage<int>, KeyValueListItem<int>>().ReverseMap();
                cfg.CreateMap<KeyValueListItemTableStorage<string>, KeyValueListItem<string>>().ReverseMap();

                cfg.CreateMap<TableTemplate, TableTemplateBo>().ReverseMap();
                cfg.CreateMap<TableTemplateViewModel, TableTemplateBo>()
                    .ForMember(des => des.Keys, opt => opt.MapFrom(src => src.Keys.ToJsonString()))
                    .ReverseMap();
            });
        }
    }
}
