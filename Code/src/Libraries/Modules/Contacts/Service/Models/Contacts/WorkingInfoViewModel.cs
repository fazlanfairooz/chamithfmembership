﻿using System.ComponentModel.DataAnnotations;
using Inx.Data.Base.Utility;
using Inx.Service.Base.Models;
using Inx.Utility.Attributes;

namespace Inx.Module.Contacts.Service.Models.Contacts
{
    public class WorkingInfoViewModel : BaseViewModel
    {
        public int? Country { get; set; }
        [Required, StringLength(DbConstraints.NameLength)]
        public string Designation { get; set; }
        [Required, StringLength(DbConstraints.NameLength)]
        public string Company { get; set; }
        [StringLength(DbConstraints.AddressLength)]
        public string Address { get; set; } 
        [StringLength(DbConstraints.PhoneLength)]
        public string Phone { get; set; }
        [StringLength(DbConstraints.PhoneLength)]
        public string Telephone { get; set; }
        public int PersonId { get; set; }
    }
}
