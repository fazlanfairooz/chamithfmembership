﻿using System;
using System.ComponentModel.DataAnnotations;
using Inx.Data.Base.Utility;
using Inx.Service.Base.Models;
using Inx.Utility.Attributes;

namespace Inx.Module.Contacts.Service.Models.Contacts
{
    public class ContactInfoViewModel : BaseViewModel
    {
        [NumberNotZero]
        public int Country { get; set; }
        [Required,StringLength(DbConstraints.NameLength)]
        public string City { get; set; }
        public string Zip { get; set; }
        [Required,StringLength(DbConstraints.AddressLength)]
        public string Address { get; set; }
        [Required,StringLength(DbConstraints.EmailLength)]
        public string Email { get; set; }
        [Required, StringLength(DbConstraints.PhoneLength)]
        public string Phone { get; set; }
        [StringLength(DbConstraints.PhoneLength)]
        public string Telephone { get; set; }
        [Obsolete]
        public new string Name { get; set; }
        public int PersonId { get; set; }
    }
}
