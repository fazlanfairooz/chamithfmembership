﻿using Inx.Utility.Attributes;
namespace Inx.Module.Contacts.Service.Models.Contacts
{
    public class ContactViewModel
    {
        [ObjectNotNull]
        public PersonInfoViewModel PersonInfo { get; set; }
        [ObjectNotNull]
        public ContactInfoViewModel ContactInfo { get; set; }
        public WorkingInfoViewModel WorkingInfo { get; set; }
        public int Id { get; set; }

        public void SetProfileImage(string profileimage)
        {
            PersonInfo.ProfileImage = profileimage;
        }
        public ContactViewModel()
        {
            PersonInfo = new PersonInfoViewModel();
            ContactInfo = new ContactInfoViewModel();
            WorkingInfo = new WorkingInfoViewModel();
        }
    }
}
