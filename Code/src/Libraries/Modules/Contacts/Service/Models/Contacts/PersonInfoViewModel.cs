﻿using System;
using System.ComponentModel.DataAnnotations;
using Inx.Data.Base.Utility;
using Inx.Module.Contacts.Data;
using Inx.Service.Base.Models;
using Inx.Utility.Utility;

namespace Inx.Module.Contacts.Service.Models.Contacts
{
    public class PersonInfoViewModel: BaseViewModel
    {
        [Required, StringLength(DbConstraints.NameLength)]
        public string FirstName { get; set; }
        [Required, StringLength(DbConstraints.NameLength)]
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public Enums.MartialStatus MartialStatus { get; set; }
        [Required, StringLength(Constraints.IdentityLength)]
        public string Identity { get; set; }
        public bool IsProfileImageChanged { get; set; } = false;
        public string ProfileImage { get; set; } = Constants.ProfileNoImage;
        public int ContactCategoryId { get; set; }
        [Obsolete("first name and last name included")]
        public new string Name { get; set; }
    }
}
