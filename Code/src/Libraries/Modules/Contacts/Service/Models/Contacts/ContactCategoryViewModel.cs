﻿using System.ComponentModel.DataAnnotations;
using Inx.Data.Base.Utility;
using Inx.Service.Base.Models;

namespace Inx.Module.Contacts.Service.Models.Contacts
{
    public class ContactCategoryViewModel : BaseViewModel
    {
        [Required,StringLength(DbConstraints.NameLength)]
        public new string Name { get; set; }
    }
}
