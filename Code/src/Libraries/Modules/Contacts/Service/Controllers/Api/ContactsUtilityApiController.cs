﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Inx.Module.Contacts.Core.Bo.Utlity;
using Inx.Module.Contacts.Core.Service.Utility.Interface;
using Inx.Module.Contacts.Service.Models.Utility;
using Inx.Module.Contacts.Service.Utility;
using Inx.Service.Base.Attributes;
using Inx.Service.Base.Controllers;
using Inx.Service.Base.Models;
using Inx.Utility.Utility;
using Microsoft.AspNetCore.Mvc;
using static Inx.Utility.Utility.Enums;

namespace Inx.Module.Contacts.Service.Controllers.Api
{
    [Route(Constraints.ApiPrefix), AuthorizeRoles(Enums.Roles.SuperAdmin, Enums.Roles.TenantAdmin)]
    public class ContactsUtilityApiController : ContactsBaseApiController
    {
        private readonly ITableTemplateService service;
        public ContactsUtilityApiController(ITableTemplateService _service, IBaseInjector baseinject) : base(baseinject)
        {
            service = _service;
        }

        #region table templates
        [HttpGet, Route("utility/tabletemplate/{type:int}")]
        public async Task<IActionResult> Read(int type)
        {
            try
            {
                var stopwatch = Stopwatch.StartNew();
                var result = await service.Read(Request(type));
                stopwatch.Stop();
                return Ok(new Response<List<string>>
                {
                    Item = result,
                    TimeTaken = stopwatch.ElapsedMilliseconds
                });
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }
        [HttpGet, Route("utility/tabletemplate/headers/{type:int}")]
        public async Task<IActionResult> Read(TableTemplateTypes type)
        {
            try
            {
                var stopwatch = Stopwatch.StartNew();
                var result = service.GetColomHeaders(type);
                stopwatch.Stop();
                return Ok(new Response<List<string>>
                {
                    Item = result,
                    TimeTaken = stopwatch.ElapsedMilliseconds
                });
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }
        [HttpPut, Route("utility/tabletemplate"), ModelValidation]
        public async Task<IActionResult> Update([FromBody]  TableTemplateViewModel item)
        {
            return await Update<TableTemplateViewModel, TableTemplateBo, ITableTemplateService>(item, this.service);
        }
        #endregion

    }
}