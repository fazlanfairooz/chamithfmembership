﻿using System.Threading.Tasks;
using Inx.Module.Contacts.Core.Bo.Contacts;
using Inx.Module.Contacts.Core.Service.Contacts.interfaces;
using Inx.Module.Contacts.Service.Models.Contacts;
using Inx.Module.Contacts.Service.Utility;
using Inx.Service.Base.Controllers;
using Microsoft.AspNetCore.Mvc;
using Inx.Service.Base.Attributes;
using Inx.Utility.Utility;
namespace Inx.Module.Contacts.Service.Controllers.Api
{
    [Route(Constraints.ApiPrefix), AuthorizeRoles(Enums.Roles.SuperAdmin,Enums.Roles.TenantAdmin)]
    public class ContactCategoryApiController : ContactsBaseApiController, IBaseApi<ContactCategoryViewModel, int>
    {
        private readonly IContactCategoryService service;
        public ContactCategoryApiController(IContactCategoryService _service, IBaseInjector baseinject) : base(baseinject)
        {
            service = _service;
        }
        [HttpPost, Route("contactcategories")]
        public async Task<IActionResult> Create([FromBody] ContactCategoryViewModel item)
        {
            return await Create<ContactCategoryViewModel, ContactCategoryBo, IContactCategoryService>(item, service);
        }
        [HttpDelete, Route("contactcategories/{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            return await Delete<ContactCategoryBo, IContactCategoryService>(id, service);
        }
        [HttpGet, Route("contactcategories/{id:int}")]
        public async Task<IActionResult> Read(int id)
        {
            return await Read<ContactCategoryViewModel, ContactCategoryBo, IContactCategoryService>(id, service);
        }
        [HttpGet, Route("contactcategories")]
        public async Task<IActionResult> Read(int skip = 0, int take = 0, string search = null, string orderby = null)
        {
            return await Read<ContactCategoryViewModel, ContactCategoryBo, IContactCategoryService>(SearchRequest(skip,
                take, searchTerm: search, orderByTerm: orderby), service);
        }
        [HttpPut, Route("contactcategories")]
        public async Task<IActionResult> Update([FromBody] ContactCategoryViewModel item)
        {
            return await Update<ContactCategoryViewModel, ContactCategoryBo, IContactCategoryService>(item, service);
        }
        [HttpGet, Route("contactcategories/keyvalue")]
        public async Task<IActionResult> ReadKeyValue()
        {
            return await ReadKeyValue<ContactCategoryBo, IContactCategoryService>(service);
        }
    }
}
