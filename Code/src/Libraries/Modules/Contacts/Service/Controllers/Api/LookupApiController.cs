﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Inx.Service.Base.Controllers;
using Inx.Utility.Models;
using Inx.Utility.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Inx.Utility.Cache.TableStorage;
using System.Diagnostics;
using Inx.Service.Base.Models;
using Inx.Utility.Models.TableStorage;

namespace Inx.Module.Contacts.Service.Controllers.Api
{
    [Route("api/contacts/v1/lookups")]
    public class LookupApiController: ContactsBaseApiController
    {
        public LookupApiController(IBaseInjector baseinject) : base(baseinject)
        {
        }
        [HttpGet, AllowAnonymous, Route("martialstatus")]
        public async Task<IActionResult> MartialStatus()
        {
            try
            {
                var result = new List<KeyValueListItem<byte>>();
                foreach (byte value in Enum.GetValues(typeof(Enums.MartialStatus)))
                {
                    result.Add(new KeyValueListItem<byte>()
                    {
                        Value = value,
                        Text = ((Enums.MartialStatus)value).ToString()
                    });
                }
                return Json(result);
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }
        [HttpGet, AllowAnonymous, Route("countries")]
        public async Task<IActionResult> Country()
        {
            try
            {
                var stopwatch = Stopwatch.StartNew();
                var result = await new TableStorage<CountriesTableStorage>("countries",cache, "countries").ReadArrayList<CountriesTableStorage>();
                stopwatch.Stop();
                return Ok(new Response<List<CountriesTableStorage>>()
                {
                    Item = result,
                    TimeTaken = stopwatch.ElapsedMilliseconds
                });
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }
        [HttpGet, AllowAnonymous, Route("languages")]
        public async Task<IActionResult> Languages()
        {
            try
            {
                var result = new List<KeyValueListItem<byte>>();
                foreach (byte value in Enum.GetValues(typeof(Enums.Languages)))
                {
                    result.Add(new KeyValueListItem<byte>()
                    {
                        Value = value,
                        Text = ((Enums.Languages)value).ToString()
                    });
                }
                return Json(result);
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }

        [HttpGet, AllowAnonymous, Route("currency")]
        public async Task<IActionResult> Currency()
        {
            try
            {
                var result = new List<KeyValueListItem<byte>>();
                foreach (byte value in Enum.GetValues(typeof(Enums.Currency)))
                {
                    result.Add(new KeyValueListItem<byte>()
                    {
                        Value = value,
                        Text = ((Enums.Currency)value).ToString()
                    });
                }
                return Json(result);
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }
    }
}
