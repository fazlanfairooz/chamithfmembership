﻿using Abp.AspNetCore;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Inx.Module.Contacts.Service.Utility.Config;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
namespace Inx.Module.Contacts.Service
{
    [DependsOn(typeof(AbpAspNetCoreModule))]
    public class ContactsServiceModule : AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public ContactsServiceModule(IHostingEnvironment env)
        {
        }
        public override void PreInitialize()
        {
            IocConfig.Register(IocManager);
        }
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(ContactsServiceModule).GetAssembly());
            Automapper.Register(Configuration);
        }
    }
}
