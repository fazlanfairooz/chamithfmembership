﻿using Inx.Utility.Models;
using System;

namespace Inx.Module.Contacts.Core.Bo.Contacts
{
    public class ContactfileBo : BaseBo
    {
        public int ContactId { get; set; }
        public string FileName { get; set; }
        public string FileSaveName { get; set; }
        public decimal Size { get; set; }
        public DateTime CreatedOn { get; set; }
        public string DownloadPath { get; set; }
        [Obsolete]
        public new string Name { get; set; }
    }
}
