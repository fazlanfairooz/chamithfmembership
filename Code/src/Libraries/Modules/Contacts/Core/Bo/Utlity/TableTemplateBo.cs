﻿using System;
using Inx.Utility.Models;
namespace Inx.Module.Contacts.Core.Bo.Utlity
{
    public class TableTemplateBo : BaseBo
    {
        public  Enums.TableTemplate TemplateType { get; set; }
        public string Keys { get; set; } = "[]";
        [Obsolete]
        public new string Name { get; set; }
    }
}
