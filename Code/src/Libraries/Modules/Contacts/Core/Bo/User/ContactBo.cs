﻿namespace Inx.Module.Contacts.Core.Bo.User
{
    public class ContactBo
    {
        public PersonInfoBo PersonInfo { get; set; }
        public ContactInfoBo ContactInfo { get; set; }
        public WorkingInfoBo WorkingInfo { get; set; }
        public ContactBo()
        {
            PersonInfo = new PersonInfoBo();
            ContactInfo = new ContactInfoBo();
            WorkingInfo = new WorkingInfoBo();
        }
    }
}
