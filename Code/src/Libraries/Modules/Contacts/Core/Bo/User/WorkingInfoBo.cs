﻿using System;
using Inx.Utility.Models;
namespace Inx.Module.Contacts.Core.Bo.User
{
    public class WorkingInfoBo : BaseBo
    {
        public int PersonId { get; set; }
        public int Country { get; set; }
        public string Designation { get; set; }
        public string Company { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        [Obsolete]
        public new string Name { get; set; }
        public string Telephone { get; set; }
    }
}
