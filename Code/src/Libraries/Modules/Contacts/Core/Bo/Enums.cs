﻿namespace Inx.Module.Contacts.Core.Bo
{
    public class Enums
    {
        public enum ContactInfoUpdateType : byte
        {
            All = 1,
            Person = 2,
            Contacts = 3,
            Workings = 4
        }

        public enum CacheKey : int
        {
            ContactCategory = 1,
            ContactCategoryKeyValue = 2,
            ContactTableTemplate = 3,
            Contact = 4
        }
        public enum TableTemplate
        {
            None = 0,
            ContactView = 1
        }
    }
}
