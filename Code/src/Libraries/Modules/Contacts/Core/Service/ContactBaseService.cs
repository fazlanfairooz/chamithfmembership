﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Inx.Core.Base;
using Inx.Module.Contacts.Core.Bo;
using Inx.Utility.Cache.Runtime;
namespace Inx.Module.Contacts.Core.Service
{
    public class ContactBaseService : BaseService
    {
        protected IMemoryCacheManager cache { get; set; } = null;
        public ContactBaseService()
        {
        }
        public ContactBaseService(ICoreInjector inject)
        {
            try
            {
                cache = inject.Cache;
            }
            catch (Exception e)
            {
                cache = new MemoryCacheManager(null);
            }
          
        }
        protected async Task RemoveContactCacheAsync(int tenantid)
        {
            await cache.RemoveByStartwith(CacheKeyGen(new[] {tenantid.ToString(), Enums.CacheKey.Contact.ToString()}.ToList()));
        }
    }
}
