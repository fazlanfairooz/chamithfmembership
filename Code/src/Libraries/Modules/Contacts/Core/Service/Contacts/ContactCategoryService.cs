﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using Inx.Core.Base;
using Inx.Module.Contacts.Core.Bo;
using Inx.Module.Contacts.Core.Bo.Contacts;
using Inx.Module.Contacts.Core.Service.Contacts.interfaces;
using Inx.Module.Contacts.Data.DbContext;
using Inx.Module.Contacts.Data.Entity.User;
using Inx.Utility.Cache.Runtime;
using Inx.Utility.Extentions;
using Inx.Utility.Models;
namespace Inx.Module.Contacts.Core.Service.Contacts
{
    public class ContactCategoryService : ContactBaseService, IContactCategoryService
    {
        private readonly IUnitOfWokContacts uow;
        public ContactCategoryService(IUnitOfWokContacts _uow, ICoreInjector inject):base(inject)
        {
            uow = (IUnitOfWokContacts)_uow;
        }

        public async Task<ContactCategoryBo> Create(Request<ContactCategoryBo> req)
        {
            try
            {
                var item = await uow.ContactCategoryRepository.CreateAndSave(
                    req.MapRequestObject<ContactCategoryBo, ContactCategory>());
                await RemoveCache(req.TenantId);
                return item.MapObject<ContactCategory, ContactCategoryBo>();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task Delete(Request<int> req)
        {
            try
            {
                await uow.ContactCategoryRepository.DeletePermanentAndSave(req);
                await RemoveCache(req.TenantId);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        #region read
        [Description("search")]
        public async Task<PageList<ContactCategoryBo>> Read(Search search)
        {
            try
            {
                var ckey = CacheKeyGen(new List<string> { Enums.CacheKey.ContactCategory.ToString(),search.TenantId.ToString() });
                var result = await cache.GetAsync<PageList<ContactCategoryBo>>(ckey);
                return result.IsNull()
                    ? (await uow.ContactCategoryRepository.Read<ContactCategory>(search))
                    .MapPageObject<ContactCategory, ContactCategoryBo>()
                    .CacheSetAndGet(cache, ckey)
                    : result;
            }
            catch (Exception e) 
            {
                throw e.HandleException();
            }
        }

        public async Task<ContactCategoryBo> Read(Request<int> req)
        {
            try
            {
                return (await uow.ContactCategoryRepository.Read(req))
                    .MapObject<ContactCategory, ContactCategoryBo>();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        [Description("read key value pair")]
        public async Task<List<KeyValueListItem<int>>> ReadKeyValue(Search req)
        {
            try
            {
                var ckey = CacheKeyGen(new List<string> { Enums.CacheKey.ContactCategoryKeyValue.ToString(), req.TenantId.ToString() });
                var result = await cache.GetAsync<List<KeyValueListItem<int>>>(ckey);
                return result.IsNull()
                    ? (await uow.ContactCategoryRepository.ReadKeyValue<ContactCategory>(req)).CacheSetAndGet(cache,
                        ckey)
                    : result;
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        #endregion 

        public async Task Update(Request<ContactCategoryBo> req)
        {
            try
            {
                await uow.ContactCategoryRepository
                    .UpdateAndSave(req.MapRequestObject<ContactCategoryBo, ContactCategory>(req.Item.Id));
                await RemoveCache(req.TenantId);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        private async Task RemoveCache(int tenantid)
        {
            await cache.Remove(CacheKeyGen(new List<string>{Enums.CacheKey.ContactCategory.ToString(),tenantid.ToString()}));
            await cache.Remove(CacheKeyGen(new List<string> {Enums.CacheKey.ContactCategoryKeyValue.ToString(), tenantid.ToString()}));
            await RemoveContactCacheAsync(tenantid);
        }
    }
}
