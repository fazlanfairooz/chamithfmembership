﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Inx.Core.Base;
using Inx.Module.Contacts.Core.Bo.Contacts;
using Inx.Utility.Models;

namespace Inx.Module.Contacts.Core.Service.Contacts.interfaces
{
    public interface IContactCategoryService: IBaseService<ContactCategoryBo>
    {
        Task<List<KeyValueListItem<int>>> ReadKeyValue(Search req);
    }
}
