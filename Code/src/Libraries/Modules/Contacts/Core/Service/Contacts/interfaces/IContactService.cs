﻿using System.Threading.Tasks;
using Inx.Core.Base;
using Inx.Module.Contacts.Core.Bo.User;
using Inx.Utility.Models;

namespace Inx.Module.Contacts.Core.Service.Contacts.interfaces
{
    public interface IContactService: IBaseService<ContactBo>,ISearchService<ContactsViewResult>
    {
        Task Update(Request<PersonInfoBo> item);
        Task Update(Request<ContactInfoBo> item);
        Task Update(Request<WorkingInfoBo> item);
        Task Restore(Request<int> item);
        Task<PageList<ContactsViewResult>> SearchArchive(Search request);

    }
}
