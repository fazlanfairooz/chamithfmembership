﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Inx.Core.Base;
using Inx.Module.Contacts.Core.Bo.Contacts;
using Inx.Module.Contacts.Core.Service.Contacts.interfaces;
using Inx.Module.Contacts.Data.DbContext;
using Inx.Module.Contacts.Data.Entity.User;
using Inx.Utility;
using Inx.Utility.Extentions;
using Inx.Utility.Models;
using Inx.Utility.Utility;

namespace Inx.Module.Contacts.Core.Service.Contacts
{
    public class ContactFileService : ContactBaseService, IContactFileService
    {
        private readonly IUnitOfWokContacts uow;
        public ContactFileService(IUnitOfWokContacts _uow, ICoreInjector inject) : base(inject)
        {
            uow = (IUnitOfWokContacts)_uow;
        }
        public async Task<ContactfileBo> Create(Request<ContactfileBo> req)
        {
            try
            {
                var result = (await uow.ContactfileRepository.Create(req.MapRequestObject<ContactfileBo, Contactfile>()));
                await uow.SaveAsync();
                return result.MapObject<Contactfile, ContactfileBo>();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public async Task<string> Delete(Request<int> req)
        {
            try
            {
                var result = await uow.ContactfileRepository.Read(req);
                await uow.ContactfileRepository.DeletePermanentAndSave(req);
                return result.FileSaveName;
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public async Task<PageList<ContactfileBo>> Read(Search search)
        {
            try
            {
                var result = (await uow.ContactfileRepository.Read<Contactfile>(search))
                    .MapPageObject<Contactfile, ContactfileBo>();
                foreach (var item in result.Items)
                {
                    item.DownloadPath = $"{GlobleConfig.BlobPath}/{Enums.FileType.ContactsFiles.ToString()}/{item.FileSaveName}";
                }
                return result;
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public async Task<ContactfileBo> Read(Request<int> req)
        {
            try
            {
                return (await uow.ContactfileRepository.Read(req))
                   .MapObject<Contactfile, ContactfileBo>();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public async Task<List<KeyValueListItem<int>>> ReadKeyValue(Search req)
        {
            try
            {
                return await uow.ContactfileRepository.ReadKeyValue<Contactfile>(req);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public async Task Update(Request<ContactfileBo> req)
        {
            try
            {
                await uow.ContactfileRepository.UpdateAndSave(req.MapRequestObject<ContactfileBo, Contactfile>(req.Item.Id));
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        #region not implmented
        Task IBaseService<ContactfileBo>.Delete(Request<int> req)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}