﻿using Inx.Core.Base;
using Inx.Module.Contacts.Core.Bo.Contacts;
using Inx.Utility.Models;
using System.Threading.Tasks;

namespace Inx.Module.Contacts.Core.Service.Contacts.interfaces
{
    public interface IContactFileService : IBaseService<ContactfileBo>
    {
        new Task<string> Delete(Request<int> req);
    }
}
