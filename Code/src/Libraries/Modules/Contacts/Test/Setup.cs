using System;
using AutoMapper;
using Inx.Module.Contacts.Core.Bo.Contacts;
using Inx.Module.Contacts.Data.Entity.User;
using Inx.Module.Contacts.Test.Seed;
using Inx.Module.Contacts.Test.Utility;
using Inx.Module.Identity.Data.Entity.Security;
using Inx.Module.Identity.Data.Entity.Tenant;
using Inx.Utility.Utility;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Inx.Module.Contacts.Test
{
    [TestClass]
    public class Setup
    {
        [AssemblyInitialize]
        public static void AssemblyInit(TestContext context)
        {
            new Comman().Context.Database.EnsureDeleted();
            try
            {
                new Comman().Context.Database.EnsureCreated();
            }
            catch (Exception e)
            {
            }
            new Comman().IdentityContext.Database.Migrate();
            new Comman().Context.Database.Migrate();
            new SeedData().Seed();
            //automapper 
            Mapper.Initialize(config =>
            {
                config.CreateMap<ContactCategory, ContactCategoryBo>().ReverseMap();
            });
            IdentitySeed();
        }

        static void IdentitySeed()
        {
            using (var c = new Comman().IdentityContext)
            {
                c.Tenants.Add(new Tenant
                {
                    Host = "http://test.com",
                    Name = "test app",
                    TenantTypeId = 2,
                    OrganizationLogo = "noimage.png",
                });
                c.SaveChanges();
                c.Users.Add(new User
                {
                    CreatedById = 0,CreatedOn = DateTime.Today,
                    Email = "test@gmail.com",Password = "test@gmail.com",
                    EmailConfirmCode = Guid.Empty,
                    IsEmailConfirmed = true,
                    RecordStatus = Enums.UserRecordStatus.Active,
                    PasswordResetCode = Guid.Empty,
                    
                });
                c.SaveChanges();
            }
          

        }
    }
}
