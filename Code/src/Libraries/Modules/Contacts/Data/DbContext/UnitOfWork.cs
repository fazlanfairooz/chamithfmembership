﻿using System;
using System.Threading.Tasks;
using Inx.Data.Base.Repository;
using Inx.Module.Contacts.Data.Entity.User;
using Inx.Module.Contacts.Data.Entity.Utility;
using Microsoft.EntityFrameworkCore;

namespace Inx.Module.Contacts.Data.DbContext
{
    public partial class UnitOfWork : IUnitOfWokContacts
    {
        #region fields

        private bool disposed;
        public ContactsDbContext Context { get; }

        #region contact

        private GenericRepository<ContactInfo> contactInfoRepository;
        private GenericRepository<PersonInfo> personInfoRepository;
        private GenericRepository<WorkingInfo> workingInfoRepository;
        private GenericRepository<ContactCategory> contactCategoryRepository;
        private GenericRepository<Contactfile> contactfileRepository;
        private GenericRepository<TableTemplate> tableTemplateRepository;
        #endregion

        public string ConnectionString => Context.Database.GetDbConnection().ConnectionString;

        #endregion

        #region ctor

        public UnitOfWork(IContactsDbSet dbcontext)
        {
            Context = (ContactsDbContext) dbcontext;
        }

        #endregion

        #region repositories

        public GenericRepository<ContactInfo> ContactInfoRepository =>
            contactInfoRepository ?? (contactInfoRepository = new GenericRepository<ContactInfo>(Context));

        public GenericRepository<PersonInfo> PersonInfoRepository =>
            personInfoRepository ?? (personInfoRepository = new GenericRepository<PersonInfo>(Context));

        public GenericRepository<WorkingInfo> WorkingInfoRepository =>
            workingInfoRepository ?? (workingInfoRepository = new GenericRepository<WorkingInfo>(Context));

        public GenericRepository<ContactCategory> ContactCategoryRepository =>
            contactCategoryRepository ?? (contactCategoryRepository = new GenericRepository<ContactCategory>(Context));

        public GenericRepository<TableTemplate> TableTemplateRepository =>
            tableTemplateRepository ?? (tableTemplateRepository = new GenericRepository<TableTemplate>(Context));

        public GenericRepository<Contactfile> ContactfileRepository =>
                contactfileRepository ?? (contactfileRepository = new GenericRepository<Contactfile>(Context));

        Microsoft.EntityFrameworkCore.DbContext IUnitOfWork.Context => throw new NotImplementedException();

        #endregion

        #region methods

        public void Save()
        {
            Context.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await Context.SaveChangesAsync();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    Context.Dispose();
                }
            }
            disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
