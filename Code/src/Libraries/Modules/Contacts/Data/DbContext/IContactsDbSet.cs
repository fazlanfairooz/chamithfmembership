﻿using Inx.Module.Contacts.Data.Entity.User;
using Inx.Module.Contacts.Data.Entity.Utility;
using Microsoft.EntityFrameworkCore;

namespace Inx.Module.Contacts.Data.DbContext
{
    public interface IContactsDbSet
    {
        DbSet<PersonInfo> PersonInfo { get; set; }
        DbSet<WorkingInfo> WorkingInfo { get; set; }
        DbSet<ContactInfo> ContactInfo { get; set; }
        DbSet<TableTemplate> TableTemplate { get; set; }
    }
}
