﻿using Inx.Data.Base.Repository;
using Inx.Module.Contacts.Data.Entity.User;
using Inx.Module.Contacts.Data.Entity.Utility;

namespace Inx.Module.Contacts.Data.DbContext
{
    public interface IUnitOfWokContacts : IUnitOfWork
    {
        GenericRepository<ContactInfo> ContactInfoRepository { get; }
        GenericRepository<PersonInfo> PersonInfoRepository { get; }
        GenericRepository<WorkingInfo> WorkingInfoRepository { get; }
        GenericRepository<ContactCategory> ContactCategoryRepository { get; }
        GenericRepository<Contactfile> ContactfileRepository { get; }
        GenericRepository<TableTemplate> TableTemplateRepository { get; }
    }
}
