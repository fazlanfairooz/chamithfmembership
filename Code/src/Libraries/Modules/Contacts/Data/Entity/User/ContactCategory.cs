﻿using System.ComponentModel.DataAnnotations.Schema;
using Inx.Module.Identity.Data.Entity.Application;

namespace Inx.Module.Contacts.Data.Entity.User
{
    [Table("ContactCategories", Schema = Constraints.Schema)]
    public class ContactCategory : AuditableEntity
    {
    }
}
