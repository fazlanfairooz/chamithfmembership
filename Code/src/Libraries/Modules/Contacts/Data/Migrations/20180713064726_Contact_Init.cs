﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Inx.Module.Contacts.Data.Migrations
{
    public partial class Contact_Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Inexis.Contacts");

            migrationBuilder.CreateTable(
                name: "ContactCategories",
                schema: "Inexis.Contacts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    EditedById = table.Column<int>(nullable: true),
                    EditedOn = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    RecordState = table.Column<byte>(nullable: false),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContactCategories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ContactCategories_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ContactCategories_Users_EditedById",
                        column: x => x.EditedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ContactCategories_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalSchema: "Inexis.Security",
                        principalTable: "Tenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TableTemplates",
                schema: "Inexis.Contacts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    EditedById = table.Column<int>(nullable: true),
                    EditedOn = table.Column<DateTime>(nullable: true),
                    Keys = table.Column<string>(nullable: true),
                    RecordState = table.Column<byte>(nullable: false),
                    TemplateType = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TableTemplates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TableTemplates_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TableTemplates_Users_EditedById",
                        column: x => x.EditedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TableTemplates_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalSchema: "Inexis.Security",
                        principalTable: "Tenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PersonInfo",
                schema: "Inexis.Contacts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ContactCategoryId = table.Column<int>(nullable: false),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    DOB = table.Column<DateTime>(type: "Date", nullable: false),
                    EditedById = table.Column<int>(nullable: true),
                    EditedOn = table.Column<DateTime>(nullable: true),
                    FirstName = table.Column<string>(maxLength: 50, nullable: false),
                    Identity = table.Column<string>(maxLength: 50, nullable: false),
                    LastName = table.Column<string>(maxLength: 50, nullable: false),
                    MartialStatus = table.Column<byte>(nullable: false),
                    ProfileImage = table.Column<string>(maxLength: 100, nullable: false),
                    RecordState = table.Column<byte>(nullable: false),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PersonInfo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PersonInfo_ContactCategories_ContactCategoryId",
                        column: x => x.ContactCategoryId,
                        principalSchema: "Inexis.Contacts",
                        principalTable: "ContactCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PersonInfo_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PersonInfo_Users_EditedById",
                        column: x => x.EditedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PersonInfo_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalSchema: "Inexis.Security",
                        principalTable: "Tenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Contactfiles",
                schema: "Inexis.Contacts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ContactId = table.Column<int>(nullable: false),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    EditedById = table.Column<int>(nullable: true),
                    EditedOn = table.Column<DateTime>(nullable: true),
                    FileName = table.Column<string>(maxLength: 100, nullable: true),
                    FileSaveName = table.Column<string>(maxLength: 100, nullable: true),
                    RecordState = table.Column<byte>(nullable: false),
                    Size = table.Column<decimal>(nullable: false),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contactfiles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Contactfiles_PersonInfo_ContactId",
                        column: x => x.ContactId,
                        principalSchema: "Inexis.Contacts",
                        principalTable: "PersonInfo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Contactfiles_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Contactfiles_Users_EditedById",
                        column: x => x.EditedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Contactfiles_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalSchema: "Inexis.Security",
                        principalTable: "Tenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContactInfo",
                schema: "Inexis.Contacts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Address = table.Column<string>(maxLength: 200, nullable: false),
                    City = table.Column<string>(maxLength: 50, nullable: false),
                    Country = table.Column<int>(nullable: false),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    EditedById = table.Column<int>(nullable: true),
                    EditedOn = table.Column<DateTime>(nullable: true),
                    Email = table.Column<string>(maxLength: 50, nullable: false),
                    PersonId = table.Column<int>(nullable: false),
                    Phone = table.Column<string>(maxLength: 20, nullable: false),
                    RecordState = table.Column<byte>(nullable: false),
                    Telephone = table.Column<string>(maxLength: 20, nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Zip = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContactInfo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ContactInfo_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ContactInfo_Users_EditedById",
                        column: x => x.EditedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ContactInfo_PersonInfo_PersonId",
                        column: x => x.PersonId,
                        principalSchema: "Inexis.Contacts",
                        principalTable: "PersonInfo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ContactInfo_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalSchema: "Inexis.Security",
                        principalTable: "Tenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WorkingInfo",
                schema: "Inexis.Contacts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Address = table.Column<string>(maxLength: 200, nullable: true),
                    Company = table.Column<string>(maxLength: 50, nullable: false),
                    Country = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    Designation = table.Column<string>(maxLength: 50, nullable: false),
                    EditedById = table.Column<int>(nullable: true),
                    EditedOn = table.Column<DateTime>(nullable: true),
                    PersonId = table.Column<int>(nullable: false),
                    Phone = table.Column<string>(maxLength: 50, nullable: true),
                    RecordState = table.Column<byte>(nullable: false),
                    Telephone = table.Column<string>(maxLength: 20, nullable: true),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkingInfo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WorkingInfo_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WorkingInfo_Users_EditedById",
                        column: x => x.EditedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WorkingInfo_PersonInfo_PersonId",
                        column: x => x.PersonId,
                        principalSchema: "Inexis.Contacts",
                        principalTable: "PersonInfo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WorkingInfo_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalSchema: "Inexis.Security",
                        principalTable: "Tenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ContactCategories_CreatedById",
                schema: "Inexis.Contacts",
                table: "ContactCategories",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_ContactCategories_EditedById",
                schema: "Inexis.Contacts",
                table: "ContactCategories",
                column: "EditedById");

            migrationBuilder.CreateIndex(
                name: "IX_ContactCategories_TenantId",
                schema: "Inexis.Contacts",
                table: "ContactCategories",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ContactCategories_Name_TenantId",
                schema: "Inexis.Contacts",
                table: "ContactCategories",
                columns: new[] { "Name", "TenantId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Contactfiles_ContactId",
                schema: "Inexis.Contacts",
                table: "Contactfiles",
                column: "ContactId");

            migrationBuilder.CreateIndex(
                name: "IX_Contactfiles_CreatedById",
                schema: "Inexis.Contacts",
                table: "Contactfiles",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Contactfiles_EditedById",
                schema: "Inexis.Contacts",
                table: "Contactfiles",
                column: "EditedById");

            migrationBuilder.CreateIndex(
                name: "IX_Contactfiles_TenantId",
                schema: "Inexis.Contacts",
                table: "Contactfiles",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ContactInfo_CreatedById",
                schema: "Inexis.Contacts",
                table: "ContactInfo",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_ContactInfo_EditedById",
                schema: "Inexis.Contacts",
                table: "ContactInfo",
                column: "EditedById");

            migrationBuilder.CreateIndex(
                name: "IX_ContactInfo_PersonId",
                schema: "Inexis.Contacts",
                table: "ContactInfo",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_ContactInfo_TenantId",
                schema: "Inexis.Contacts",
                table: "ContactInfo",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ContactInfo_Phone_TenantId",
                schema: "Inexis.Contacts",
                table: "ContactInfo",
                columns: new[] { "Phone", "TenantId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PersonInfo_ContactCategoryId",
                schema: "Inexis.Contacts",
                table: "PersonInfo",
                column: "ContactCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_PersonInfo_CreatedById",
                schema: "Inexis.Contacts",
                table: "PersonInfo",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_PersonInfo_EditedById",
                schema: "Inexis.Contacts",
                table: "PersonInfo",
                column: "EditedById");

            migrationBuilder.CreateIndex(
                name: "IX_PersonInfo_TenantId",
                schema: "Inexis.Contacts",
                table: "PersonInfo",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_TableTemplates_CreatedById",
                schema: "Inexis.Contacts",
                table: "TableTemplates",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_TableTemplates_EditedById",
                schema: "Inexis.Contacts",
                table: "TableTemplates",
                column: "EditedById");

            migrationBuilder.CreateIndex(
                name: "IX_TableTemplates_TenantId",
                schema: "Inexis.Contacts",
                table: "TableTemplates",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkingInfo_CreatedById",
                schema: "Inexis.Contacts",
                table: "WorkingInfo",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_WorkingInfo_EditedById",
                schema: "Inexis.Contacts",
                table: "WorkingInfo",
                column: "EditedById");

            migrationBuilder.CreateIndex(
                name: "IX_WorkingInfo_PersonId",
                schema: "Inexis.Contacts",
                table: "WorkingInfo",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkingInfo_TenantId",
                schema: "Inexis.Contacts",
                table: "WorkingInfo",
                column: "TenantId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Contactfiles",
                schema: "Inexis.Contacts");

            migrationBuilder.DropTable(
                name: "ContactInfo",
                schema: "Inexis.Contacts");

            migrationBuilder.DropTable(
                name: "TableTemplates",
                schema: "Inexis.Contacts");

            migrationBuilder.DropTable(
                name: "WorkingInfo",
                schema: "Inexis.Contacts");

            migrationBuilder.DropTable(
                name: "PersonInfo",
                schema: "Inexis.Contacts");

            migrationBuilder.DropTable(
                name: "ContactCategories",
                schema: "Inexis.Contacts");

            migrationBuilder.DropTable(
                name: "Users",
                schema: "Inexis.Security");

            migrationBuilder.DropTable(
                name: "Tenants",
                schema: "Inexis.Security");

            migrationBuilder.DropTable(
                name: "TenantTypes",
                schema: "Inexis.Security");
        }
    }
}
