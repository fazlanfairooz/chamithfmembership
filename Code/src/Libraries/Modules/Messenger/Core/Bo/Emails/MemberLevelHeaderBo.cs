﻿using System;
using System.Collections.Generic;
using System.Linq;
using static Inx.Utility.Utility.Enums;

namespace Inx.Module.Messenger.Core.Bo.Emails
{
    public class MemberLevelHeaderBo
    {
        public int Id { get; set; }
        public decimal Price { get; set; }
        public string LevelName { get; set; }
        public int Capacity { get; set; }
        public DateTime JoinDate { get; set; }
        public DateTime RenewDate { get; set; }
        public int MemberId { get; set; }
        public int MemberLevelId { get; set; }
        public MembershipStatus MemberStatusId { get; set; }


       
        public List<string> GetProperties(List<string> exclude)
        {
            var foo = this;
            var lst = foo.GetType().GetProperties().Select(prop => prop.Name).ToList();
            var result = new List<string>();
            foreach (var item in lst)
            {
                if (!exclude.Contains(item))
                {
                    result.Add(item);
                }
            }
            return result;
        }
    }
}
