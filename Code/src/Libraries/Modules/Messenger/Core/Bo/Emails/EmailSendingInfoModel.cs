﻿using Inx.Utility.Models;
using System.Collections.Generic;
using System.IO;
using static Inx.Utility.Utility.Enums;

namespace Inx.Module.Messenger.Core.Bo.Emails
{
    public class EmailSendingInfoModel : BaseBo
    {

        public string EmailBody { get; set; }
        public string Subject { get; set; }
        public string ToAddress { get; set; }
        public List<Attachment> Attachment { get; set; }
    }
    public class Attachment
    {
        public Stream File { get; set; }
        public string FileName { get; set; }
        public string Extention { get; set; }
        public string File64String { get; set; }
    }
}
