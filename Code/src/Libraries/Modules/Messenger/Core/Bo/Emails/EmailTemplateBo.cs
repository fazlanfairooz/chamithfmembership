﻿using Inx.Utility.Models;
using static Inx.Utility.Utility.Enums;

namespace Inx.Module.Messenger.Core.Bo.Emails
{
    public class EmailTemplateBo:BaseBo
    {
        public string EmailBody { get; set; }
        public EmailTemplateTypes EmailTemplateTypes { get; set; }
    }
}
