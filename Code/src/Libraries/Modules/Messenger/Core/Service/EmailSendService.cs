﻿
using Inx.Module.Messenger.Core.Bo.Emails;
using Inx.Module.Messenger.Core.Service.Interfaces;
using System;
using System.Threading.Tasks;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Collections.Generic;
using System.IO;

namespace Inx.Module.Messenger.Core.Service
{
    public class EmailSendService : IEmailSendService
    {
        public async Task SendEmail(EmailSendingInfoModel model)
        {
          
            var apiKey = Environment.GetEnvironmentVariable("membership_email_api_key");
            var client = new SendGridClient("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");
            var from = new EmailAddress("infoassocifyportal@gmail.com", "Assocify");
            var subject = model.Subject;
            var to = new EmailAddress(model.ToAddress, "Example User");

            var htmlContent = model.EmailBody;
            var msg = MailHelper.CreateSingleEmail(from, to, subject, "", htmlContent);
            if (model.Attachment.Count != 0)
            {
                List<SendGrid.Helpers.Mail.Attachment> FilesList = new List<SendGrid.Helpers.Mail.Attachment>();
                foreach (var item in model.Attachment)
                {
                    using (MemoryStream output = new MemoryStream())
                    {
                        byte[] buffer = new byte[16 * 1024];
                        int read;
                        while ((read = item.File.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            output.Write(buffer, 0, read);
                        }
                        item.File64String = Convert.ToBase64String(output.ToArray());
                        FilesList.Add(new SendGrid.Helpers.Mail.Attachment
                        {
                            Content = item.File64String,
                            Filename = item.FileName,
                            Type = item.Extention
                        });
                    }
                };
                msg.AddAttachments(FilesList);
            }
            var response = await client.SendEmailAsync(msg);
        }
    }
}
