﻿using Inx.Core.Base;
using Inx.Module.Messenger.Core.Bo.Emails;
using Inx.Utility.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using static Inx.Utility.Utility.Enums;

namespace Inx.Module.Messenger.Core.Service.Interfaces
{
    public interface IEmailTemplateService:IBaseService<EmailTemplateBo>
    {
        Task<List<KeyValueListItem<int>>> ReadKeyValue(Search req);
        EmailHeaderBo GetColomHeaders(EmailTemplateTypes request);
    }
}
