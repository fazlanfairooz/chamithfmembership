﻿using Inx.Module.Messenger.Core.Bo.Emails;
using System.Threading.Tasks;

namespace Inx.Module.Messenger.Core.Service.Interfaces
{
    public interface IEmailSendService
    {
        Task SendEmail(EmailSendingInfoModel emailModel);
    }
}
