﻿using Inx.Module.Messenger.Core.Bo.Emails;
using Inx.Module.Messenger.Core.Service.Interfaces;
using Inx.Module.Messenger.Data.DbContext;
using Inx.Module.Messenger.Data.Entity;
using Inx.Utility.Cache.Runtime;
using Inx.Utility.Extentions;
using Inx.Utility.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static Inx.Utility.Utility.Enums;
using System.Linq;
using Inx.Utility.Exceptions;

namespace Inx.Module.Messenger.Core.Service
{
    public class EmailTemplateService : IEmailTemplateService
    {
        private readonly IUnitOfWorkMassengers uow;
        private readonly IMemoryCacheManager cache;
        public EmailTemplateService(IUnitOfWorkMassengers _uow, IMemoryCacheManager _cache)
        {
            uow = (IUnitOfWorkMassengers)_uow;
            cache = _cache;
        }
        public async Task<EmailTemplateBo> Create(Request<EmailTemplateBo> req)
        {
            try
            {
                var isTemplateExist = uow.EmailTemplateRepository.TableAsNoTracking.Any(a => a.Name == req.Item.Name);

                if (isTemplateExist)
                    throw new SameNameException("Template name already exists");

                return (await uow.EmailTemplateRepository.CreateAndSave(req.MapRequestObject<EmailTemplateBo, EmailTemplate>())).MapObject<EmailTemplate, EmailTemplateBo>();

                // IF THERE IS CACHE
                // var item = await uow.EmailTemplateRepository.CreateAndSave(
                //         req.MapRequestObject<EmailTemplateBo, EmailTemplate>());
                //     await RemoveCache(req.TenantId);
                //     return item.MapObject<EmailTemplate, EmailTemplateBo>();		

            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public async Task Delete(Request<int> req)
        {
            try
            {
                await uow.EmailTemplateRepository.DeleteAndSave(req);
                //IF THERE CACHE
                //await RemoveCache(req.TenantId);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public async Task<PageList<EmailTemplateBo>> Read(Search search)
        {
            try
            {
                return (await uow.EmailTemplateRepository.Read<EmailTemplate>(search))
                    .MapPageObject<EmailTemplate, EmailTemplateBo>();

                // IF THERE IS CACHE
                /*var ckey = CacheKeyGen(new List<string> {"YOUR CACHE KEY",search.TenantId.ToString() });
                  var result = await cache.GetAsync<PageList<EmailTemplateBo>>(ckey);
                  return result.IsNull()
                      ? (await uow.EmailTemplateRepository.Read<EmailTemplate>(search))
                      .MapPageObject<EmailTemplate, EmailTemplateBo>()
                      .CacheSetAndGet(cache, ckey)
                      : result;*/
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public async Task<EmailTemplateBo> Read(Request<int> req)
        {
            try
            {
                return (await uow.EmailTemplateRepository.Read(req))
                   .MapObject<EmailTemplate, EmailTemplateBo>();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public async Task<List<KeyValueListItem<int>>> ReadKeyValue(Search req)
        {
            try
            {
                return await uow.EmailTemplateRepository.ReadKeyValue<EmailTemplate>(req);

                //IF THERE IS CACHE
                /*var ckey = CacheKeyGen(new List<string> { "cachekey".ToString(), req.TenantId.ToString() });
                var result = await cache.GetAsync<List<KeyValueListItem<int>>>(ckey);
                return result.IsNull()
                    ? (await uow..EmailTemplateRepository.ReadKeyValue<EmailTemplate>(req)).CacheSetAndGet(cache,
                        ckey)
                    : result;*/
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public async Task Update(Request<EmailTemplateBo> req)
        {
            try
            {
                await uow.EmailTemplateRepository.UpdateAndSave(req.MapRequestObject<EmailTemplateBo, EmailTemplate>(req.Item.Id));
                //IF THERE CACHE
                //await RemoveCache(req.TenantId);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public EmailHeaderBo GetColomHeaders(EmailTemplateTypes request)
        {
            var headers = new EmailHeaderBo();
            headers.MemberHeaders = new MemberHeaderBo().GetProperties(new List<string> {"Id" });
            switch (request)
            {
                case EmailTemplateTypes.MemberLevel:
                    headers.CustomHeaders = new MemberLevelHeaderBo().GetProperties(new List<string> { "MemberLevelId", "MemberStatusId","Id" });
                    break;

                default:
                    throw new ArgumentException();
            }
            return headers;
        }


        private async Task RemoveCache(int tenantid)
        {
            // await cache.Remove(CacheKeyGen(new List<string>{Enums.CacheKey.ContactCategory.ToString(),tenantid.ToString()}));
            //  await cache.Remove(
            //  CacheKeyGen(new List<string> {Enums.CacheKey.ContactCategoryKeyValue.ToString(), tenantid.ToString()}));
        }
    }
}
