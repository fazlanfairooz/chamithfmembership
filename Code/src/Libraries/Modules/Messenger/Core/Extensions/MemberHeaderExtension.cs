﻿using Inx.Module.Messenger.Core.Bo.Emails;
using Inx.Utility.Utility;
using System.Linq;
using static Inx.Utility.Utility.Enums;

namespace Inx.Module.Messenger.Core.Extensions
{
    public static class MemberHeaderExtension
    {
        public static IQueryable<MemberHeaderBo> FilterMembersById(this IQueryable<MemberHeaderBo> members, EmailSendingInfoModel emailInfoModel, int tenantId)
        {
            //filter list of members
            if (emailInfoModel.EmailSendType == EmailSendTypes.Individuals)
                 members = members.Where(item => emailInfoModel.MemberIds.Any(m => m.Equals(item.Id)));

            if (emailInfoModel.EmailSendType == EmailSendTypes.All)
                members = members.Where(a => a.TenantId == tenantId);
            if (emailInfoModel.EmailSendType == EmailSendTypes.Multiple)
            {
                if (emailInfoModel.MemberStatusId == MembershipStatus.Active)
                {
                    members = members.Where(a => a.TenantId == tenantId && a.MembershipStatus == Enums.MembershipStatus.Active && a.RecordStatus == Enums.RecordStatus.Active);
                }
                if (emailInfoModel.MemberStatusId == MembershipStatus.Inactive)
                {
                    members = members.Where(a => a.TenantId == tenantId && a.MembershipStatus == Enums.MembershipStatus.Inactive && a.RecordStatus == Enums.RecordStatus.Active);
                }
                if (emailInfoModel.MemberStatusId == MembershipStatus.Pending)
                {
                    members = members.Where(a => a.TenantId == tenantId && a.MembershipStatus == Enums.MembershipStatus.Pending && a.RecordStatus == Enums.RecordStatus.Active);
                }
                if (emailInfoModel.MemberStatusId == MembershipStatus.Terminated)
                {
                    members = members.Where(a => a.TenantId == tenantId && a.MembershipStatus == Enums.MembershipStatus.Terminated && a.RecordStatus == Enums.RecordStatus.Active);
                }
                if (emailInfoModel.MemberStatusId == MembershipStatus.All)
                {
                    members = members.Where(a => a.TenantId == tenantId);
                }
            }
            //filter specific member
            //this kind of email send through member section targeting speific member
            if (emailInfoModel.MemberId != null)
            {
                members = members.Where(a => a.Id == emailInfoModel.MemberId);
            }
            return members;
        }


        public static IQueryable<MemberLevelHeaderBo> FilterMembersByLevel(this IQueryable<MemberLevelHeaderBo> memberLevels, EmailSendingInfoModel emailInfoModel)
        {
            if (emailInfoModel.EmailSendType == EmailSendTypes.Multiple && emailInfoModel.MemberId != null)
            {
                memberLevels = memberLevels.Where(a => a.MemberId == emailInfoModel.MemberId);
            }

            if (emailInfoModel.EmailSendType == EmailSendTypes.Multiple && emailInfoModel.MemberLevelId != null && emailInfoModel.MemberStatusId != null)
            {
                memberLevels = memberLevels.Where(a => a.MemberStatusId == emailInfoModel.MemberStatusId
                && a.MemberLevelId == emailInfoModel.MemberLevelId);
            }

            return memberLevels;
        }
    }
}
