﻿using System.Threading;
using System.Threading.Tasks;
using Inx.Module.Messenger.Data.Entity;
using Inx.Utility;
using Microsoft.EntityFrameworkCore;

namespace Inx.Module.Messenger.Data.DbContext
{
    public class MessengerDbContext : Microsoft.EntityFrameworkCore.DbContext, IMessengerDbSet

    {
        public DbSet<EmailTemplate> EmailTemplate { get; set; }

        private readonly string connectionString;


        public MessengerDbContext()
        {
            connectionString = GlobleConfig.ConnectionString;
        }

        public MessengerDbContext(string _connectionstring)
        {
            connectionString = _connectionstring;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(connectionString);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }
        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            return await base.SaveChangesAsync(cancellationToken);
        }
    }
}
