﻿using Inx.Module.Identity.Data.Entity.Application;
using System.ComponentModel.DataAnnotations.Schema;
using static Inx.Utility.Utility.Enums;

namespace Inx.Module.Messenger.Data.Entity
{
    [Table("EmailTemplate", Schema = Constraints.Schema)]
    public class EmailTemplate: AuditableEntity
    {
        public string EmailBody { get; set; }
        public EmailTemplateTypes EmailTemplateTypes { get; set; }
    }
}
