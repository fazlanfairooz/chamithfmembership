﻿using Inx.Data.Base.Utility;

namespace Inx.Module.Messenger.Data
{
    public class Constraints : DbConstraints
    {
        public new const string Schema = "Inexis.Messenger";
        public const int IdentityLength = 50;
    }
}
