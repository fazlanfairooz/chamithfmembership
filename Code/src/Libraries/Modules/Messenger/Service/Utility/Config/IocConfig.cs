﻿using Abp.Dependency;
using Inx.Core.Base;
using Inx.Module.Messenger.Core.Service;
using Inx.Module.Messenger.Core.Service.Interfaces;
using Inx.Module.Messenger.Data.DbContext;
using Inx.Service.Base.Controllers;
using Inx.Utility.Cache.Runtime;

namespace Inx.Module.Messenger.Service.Utility.Config
{
    public class IocConfig
    {
        public static void Register(IIocManager iocManager)
        {
            iocManager.RegisterIfNot<IMessengerDbSet, MessengerDbContext>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<IUnitOfWorkMassengers, UnitOfWork>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<IEmailTemplateService, EmailTemplateService>();
            iocManager.RegisterIfNot<IEmailSendService, EmailSendService>();
            #region genaral
            iocManager.RegisterIfNot<IMemoryCacheManager, MemoryCacheManager>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<IBaseInjector, BaseInjector>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<ICoreInjector, CoreInjector>();
            #endregion
        }
    }
}
