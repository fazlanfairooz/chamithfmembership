﻿using Inx.Module.Messenger.Core.Service.Interfaces;
using Inx.Module.Messenger.Service.Utility;
using Inx.Service.Base.Attributes;
using Inx.Service.Base.Controllers;
using Microsoft.AspNetCore.Mvc;
using Inx.Utility.Utility;

namespace Inx.Module.Messenger.Service.Controllers.Api
{
    [Route(Constraints.ApiPrefix), AuthorizeRoles(Enums.Roles.SuperAdmin, Enums.Roles.TenantAdmin)]
    public class SendEmailApiController:MessengerBaseApiController
    {
        private readonly IEmailSendService service;
        public SendEmailApiController(IEmailSendService _service, IBaseInjector baseinject) : base(baseinject)
        {
            service = _service;
        }

    }
}
