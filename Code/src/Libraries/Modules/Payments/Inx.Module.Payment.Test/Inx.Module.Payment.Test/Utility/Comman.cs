﻿using Inx.Module.Identity.Data.DbContext;
using Inx.Module.Membership.Data.DbContext;
using Inx.Module.Messenger.Data.DbContext;
using Inx.Module.Payment.Data.DbContext;
using Inx.Utility;


namespace Inx.Module.Membership.Test.Utility
{
    public class Comman
    {
        public IdentityDbContext IdentityContext { get; set; } = new IdentityDbContext(GlobleConfig.ConnectionStringTest);
        public MembershipDbContext MemberContext { get; set; } = new MembershipDbContext(GlobleConfig.ConnectionStringTest);
        public PaymentDbContext PaymentContext { get; set; } = new PaymentDbContext(GlobleConfig.ConnectionStringTest);
        public MessengerDbContext MessengerContext { get; set; } = new MessengerDbContext(GlobleConfig.ConnectionStringTest);

        public IUnitOfWokMembership IUnitOfWokMembership { get; set; } = new Inx.Module.Membership.Data.DbContext.UnitOfWork(new MembershipDbContext(GlobleConfig.ConnectionStringTest));
        public IUnitOfWorkPayment IUnitOfWorkPayment { get; set; } = new Inx.Module.Payment.Data.DbContext.UnitOfWork(new PaymentDbContext(GlobleConfig.ConnectionStringTest));
        public IUnitOfWorkMassengers IUnitOfWorkMassengers { get; set; } = new Inx.Module.Messenger.Data.DbContext.UnitOfWork(new MessengerDbContext(GlobleConfig.ConnectionStringTest));
    }
}
