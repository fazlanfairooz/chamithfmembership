using AutoMapper;
using Inx.Module.Identity.Core.Bo;
using Inx.Module.Identity.Data.Entity.Security;
using Inx.Module.Identity.Test.Seed;
using Inx.Module.Membership.Test.Utility;
using Inx.Module.Payment.Core.Bo;
using Inx.Module.Payment.Data.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Inx.Module.Payment.Test
{
    [TestClass]
    public class Setup
    {
        [AssemblyInitialize]
        public static void AssemblyInit(TestContext context)
        {
            new Comman().PaymentContext.Database.EnsureDeleted();
            try
            {
                new Comman().PaymentContext.Database.EnsureCreated();
            }
            catch (Exception e)
            {
            }
            new Comman().IdentityContext.Database.Migrate();
            new Comman().PaymentContext.Database.Migrate();
            new SeedData().Seed();
            //automapper 
            Mapper.Initialize(config =>
            {
                config.CreateMap<PaymentMaster, PaymentBo>().ReverseMap();
            });
            IdentitySeed();
            MemberPaymentSeed();
        }

        static void IdentitySeed()
        {
            using (var c = new Comman().IdentityContext)
            {

                c.Users.Add(new User
                {
                    CreatedById = 0,
                    CreatedOn = DateTime.Today,
                    Email = "test@gmail.com",
                    Password = "test@gmail.com",
                    EmailConfirmCode = Guid.Empty,
                    IsEmailConfirmed = true,
                    RecordStatus = Enums.UserRecordStatus.Active,
                    PasswordResetCode = Guid.Empty,

                });
                c.SaveChanges();
            }
        }

        static void MemberPaymentSeed()
        {
            using (var c = new Comman().PaymentContext)
            {
                c.PaymentMaster.Add(new PaymentMaster
                {
                    MemberId = 1,
                    Amount = 1800,
                    Description = "Payment for level payment",
                    MemberSubscriptionId = 1,
                    PaymentState = Enums.PaymentState.PartialyPaid,
                    RecordState = Enums.RecordStatus.Active,
                    CreatedById = 1,
                    CreatedOn = DateTime.Now,
                });
                c.PaymentMaster.Add(new PaymentMaster
                {
                    MemberId = 2,
                    Amount = 100,
                    Description = "Payment for level payment",
                    MemberSubscriptionId = 1,
                    PaymentState = Enums.PaymentState.Paid,
                    RecordState = Enums.RecordStatus.Active,
                    CreatedById = 1,
                    CreatedOn = DateTime.Now,
                });
                c.SaveChanges();
            }
        }
    }
}
