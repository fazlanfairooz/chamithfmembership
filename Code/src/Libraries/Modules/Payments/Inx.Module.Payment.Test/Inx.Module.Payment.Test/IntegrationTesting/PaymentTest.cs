﻿using Inx.Module.Identity.Core.Bo;
using Inx.Module.Membership.Test.Utility;
using Inx.Module.Payment.Core.Bo;
using Inx.Module.Payment.Core.Services;
using Inx.Module.Payment.Core.Services.Interfaces;
using Inx.Test.Base.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace Inx.Module.Payment.Test.IntegrationTesting
{
    public class PaymentTest : CoreTestBase, ICoreTestBase
    {
        private IPaymentService service;

        //[TestInitialize]
        public void Initialize()
        {
            service = new PaymentService(new Comman().IUnitOfWorkPayment, null,new Comman().IUnitOfWokMembership);
        }

        [DataTestMethod]
        [DataRow(1,0,1,100,Enums.TransActionType.MemberApplicant,Enums.PaymentTypes.Card,Enums.PaymentState.Paid,"Payment Test","")]
        [DataRow(1,1,1,500,Enums.TransActionType.MemberApplicant,Enums.PaymentTypes.Card,Enums.PaymentState.Paid,"Payment Test","")]
        [DataRow(1,0,1,666,Enums.TransActionType.MemberApplicant,Enums.PaymentTypes.Card,Enums.PaymentState.Paid,"Test","")]
        [DataRow(1,0,1,679,2,Enums.PaymentTypes.Card,Enums.PaymentState.Paid,"","")]
        [DataRow(500,0,1,4534,Enums.TransActionType.MemberApplicant,Enums.PaymentTypes.Card,Enums.PaymentState.Paid,"Payment Test","")]
        public async Task Create()
        {
            object[] data = null;
            var bo = new PaymentBo
            {
               MemberSubscriptionId =  Convert.ToInt32(data[1]),
               PaymentMasterId = Convert.ToInt32(data[2]),
               MemberId = Convert.ToInt32(data[3]),
               Amount = Convert.ToInt32(data[4]),
               TransActionType = (Enums.TransActionType)(data[5]),
               PaymentType = (Enums.PaymentTypes)(data[6]),
               PaymentState = (Enums.PaymentState)(data[7]),
               Description = Convert.ToString(data[8]),
            };
            try
            {
                await base.Create(bo, service);
                True("PaymentTest=>Create");
            }
            catch (Exception e)
            {
                Fail(e.Message);
            }
        }

        [DataTestMethod]
        [DataRow(1)]
        [DataRow(100)]
        [ExpectedException(typeof(NullReferenceException))]
        public async Task Delete(int id)
        {
            try
            {
                await base.Delete<PaymentBo, IPaymentService>(Convert.ToInt32(id), service);
                True("PaymentTest=>Delete");
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [DataTestMethod]
        [DataRow(0, 25, "searchTerm")]
        [ExpectedException(typeof(NullReferenceException))]
        public async Task Read(int skip, int take, string search)
        {
            try
            {
                await base.Read<PaymentBo, IPaymentService>(
                    base.TestSearchRequest(skip: skip, take: take
                        , searchTerm: search, fk: 0, orderByTerm: string.Empty), service);
                True("PaymentTest=>Read");
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [DataTestMethod]
        [DataRow(1)]
        public async Task ReadKeyValue()
        {
            try
            {
                await base.ReadKeyValue<PaymentBo, IPaymentService>(service);
                True("PaymentTest=>ReadKeyValue");
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        public Task ReadById(int id)
        {
            throw new NotImplementedException();
        }

        public Task Search(int skip, int take, string search)
        {
            throw new NotImplementedException();
        }

        public new Task Update()
        {
            throw new NotImplementedException();
        }
    }
}
