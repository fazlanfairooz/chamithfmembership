﻿using Abp.AspNetCore;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Inx.Module.Payment.Service.Utility.Config;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace Inx.Module.Payment.Service
{
  
    [DependsOn(typeof(AbpAspNetCoreModule))]
    public class PaymentServiceModule : AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public PaymentServiceModule(IHostingEnvironment env)
        {

        }

        public override void PreInitialize()
        {
            IocConfig.Register(IocManager);
        }
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(PaymentServiceModule).GetAssembly());
            Automapper.Register(Configuration);

        }
    }
}
