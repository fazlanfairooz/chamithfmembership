﻿using Abp.AutoMapper;
using Abp.Configuration.Startup;
using Inx.Module.Payment.Core.Bo;
using Inx.Module.Payment.Data.Entity;
using Inx.Module.Payment.Service.Models;

namespace Inx.Module.Payment.Service.Utility.Config
{
    public class Automapper
    {
        public static void Register(IAbpStartupConfiguration Configuration)
        {
            Configuration.Modules.AbpAutoMapper().Configurators.Add(cfg =>
            {
                cfg.CreateMap<PaymentViewModel, PaymentBo>().ReverseMap();
                cfg.CreateMap<PaymentBo, PaymentMaster>().ReverseMap()
                .ForMember(des => des.PaymentDate, opt => opt.MapFrom(src => src.CreatedOn))
                .ForMember(des => des.PaymentMasterId, opt => opt.MapFrom(src => src.Id));

                cfg.CreateMap<PaymentBo, PaymentInfo>().ReverseMap();

                cfg.CreateMap<PaymentBo, PaymentViewResult>()
                    .ForMember(des => des.PaymentState, opt => opt.MapFrom(src => src.PaymentState.ToString()))
                    .ForMember(des => des.TransActionType, opt => opt.MapFrom(src => src.TransActionType.ToString()))
                    .ReverseMap();
            });
        }
    }
}
