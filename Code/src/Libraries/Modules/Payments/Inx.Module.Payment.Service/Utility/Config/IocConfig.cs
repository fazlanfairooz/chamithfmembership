﻿using Abp.Dependency;
using Inx.Module.Payment.Core.Services;
using Inx.Module.Payment.Core.Services.Interfaces;
using Inx.Module.Payment.Data.DbContext;
using Inx.Service.Base.Utility.Files;

namespace Inx.Module.Payment.Service.Utility.Config
{
    public class IocConfig
    {
        public static void Register(IIocManager iocManager)
        {
            iocManager.RegisterIfNot<IPaymentDbSet, PaymentDbContext>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<IUnitOfWorkPayment, UnitOfWork>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<IPaymentService, PaymentService>(DependencyLifeStyle.Transient);
        }
    }
}
