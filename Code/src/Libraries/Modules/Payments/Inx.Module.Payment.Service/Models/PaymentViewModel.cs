﻿using Inx.Service.Base.Models;
using static Inx.Utility.Utility.Enums;

namespace Inx.Module.Payment.Service.Models
{
    public class PaymentViewModel: BaseViewModel
    {
        public int MemberSubscriptionId { get; set; }
        public int MemberId { get; set; }
        public int PaymentMasterId { get; set; }
        public decimal Amount { get; set; }
        public decimal SubscriptionPrice { get; set; }
        public decimal Remaining { get; set; }
        public PaymentTypes PaymentType { get; set; }
        public TransActionType TransActionType { get; set; }
        public PaymentState PaymentState { get; set; }
        public string Description { get; set; }

       
    }
}
