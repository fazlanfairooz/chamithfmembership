﻿using Inx.Data.Base.Repository;
using Inx.Module.Payment.Data.Entity;
using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Inx.Module.Payment.Data.DbContext
{
    public class UnitOfWork: IUnitOfWorkPayment
    {
        #region fields

        private bool disposed;
        public Microsoft.EntityFrameworkCore.DbContext Context { get; }

        #region contact

        private GenericRepository<PaymentMaster> paymentMasterRepository;
        private GenericRepository<PaymentInfo> paymentInfoRepository;

        #endregion

        public string ConnectionString => Context.Database.GetDbConnection().ConnectionString;

        #endregion

        #region ctor

        public UnitOfWork(IPaymentDbSet dbcontext)
        {
            Context = (PaymentDbContext)dbcontext;
        }

        #endregion

        #region repositories

        public GenericRepository<PaymentMaster> PaymentMasterRepository =>
            paymentMasterRepository ?? (paymentMasterRepository = new GenericRepository<PaymentMaster>(Context));

        public GenericRepository<PaymentInfo> PaymentInfoRepository =>
           paymentInfoRepository ?? (paymentInfoRepository = new GenericRepository<PaymentInfo>(Context));

        #endregion

        #region methods

        public void Save()
        {
            Context.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await Context.SaveChangesAsync();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    Context.Dispose();
                }
            }
            disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
