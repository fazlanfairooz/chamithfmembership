﻿using Inx.Data.Base.Repository;
using Inx.Module.Payment.Data.Entity;

namespace Inx.Module.Payment.Data.DbContext
{
    public interface IUnitOfWorkPayment: IUnitOfWork
    {
        GenericRepository<PaymentMaster> PaymentMasterRepository { get; }
        GenericRepository<PaymentInfo> PaymentInfoRepository { get; }
    }
}
