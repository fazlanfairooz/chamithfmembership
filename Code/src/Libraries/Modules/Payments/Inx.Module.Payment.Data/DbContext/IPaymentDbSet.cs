﻿using Inx.Module.Payment.Data.Entity;
using Microsoft.EntityFrameworkCore;

namespace Inx.Module.Payment.Data.DbContext
{
    public interface IPaymentDbSet
    {
        DbSet<PaymentMaster> PaymentMaster { get; set; }
        DbSet<PaymentInfo> PaymentInfo { get; set; }

    }
}
