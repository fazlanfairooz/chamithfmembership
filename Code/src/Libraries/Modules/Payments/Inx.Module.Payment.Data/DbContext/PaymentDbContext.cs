﻿using System.Threading;
using System.Threading.Tasks;
using Inx.Module.Payment.Data.Entity;
using Inx.Utility;
using Microsoft.EntityFrameworkCore;

namespace Inx.Module.Payment.Data.DbContext
{
    public class PaymentDbContext : Microsoft.EntityFrameworkCore.DbContext, IPaymentDbSet

    {
        public DbSet<PaymentMaster> PaymentMaster { get; set; }
        public DbSet<PaymentInfo> PaymentInfo { get; set; }

        private readonly string connectionString;


        public PaymentDbContext()
        {
            connectionString = GlobleConfig.ConnectionString;
        }

        public PaymentDbContext(string _connectionstring)
        {
            connectionString = _connectionstring;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(connectionString);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }
        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            return await base.SaveChangesAsync(cancellationToken);
        }
    }
}
