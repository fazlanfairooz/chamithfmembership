﻿using Inx.Data.Base.Utility;

namespace Inx.Module.Payment.Data
{
    public class Constraints : DbConstraints
    {
        public new const string Schema = "Inexis.Payments";
        public const int IdentityLength = 50;
    }
}
