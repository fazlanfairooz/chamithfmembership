﻿using Inx.Module.Payment.Core.Bo;
using Inx.Module.Payment.Core.Services.Interfaces;
using Inx.Module.Payment.Data.DbContext;
using Inx.Module.Payment.Data.Entity;
using Inx.Utility.Cache.Runtime;
using Inx.Utility.Extentions;
using Inx.Utility.Models;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using static Inx.Utility.Utility.Enums;
using Microsoft.EntityFrameworkCore;
using Inx.Module.Membership.Data.DbContext;
using Inx.Data.Base.DbaContext;
using Dapper;
using Inx.Module.Identity.Core.Bo;
using Inx.Core.Base;

namespace Inx.Module.Payment.Core.Services
{
    public class PaymentService :BaseService, IPaymentService
    {
        private readonly IUnitOfWorkPayment uow;

        private readonly IUnitOfWokMembership uowm;
        private readonly IMemoryCacheManager cache;
        public PaymentService(IUnitOfWorkPayment _uow, IMemoryCacheManager _cache, IUnitOfWokMembership _uowm)
        {
            uow = (IUnitOfWorkPayment)_uow;
            cache = _cache;
            uowm = _uowm;
        }
        public async Task<PaymentBo> Create(Request<PaymentBo> req)
        {
            try
            {
                if (req.Item.PaymentState == PaymentState.Pending && req.Item.PaymentMasterId == 0)
                {
                  return (await uow.PaymentMasterRepository.CreateAndSave(req.MapRequestObject<PaymentBo, PaymentMaster>())).MapObject<PaymentMaster, PaymentBo>();
                }
                else if ((req.Item.PaymentState == PaymentState.Paid || req.Item.PaymentState == PaymentState.PartialyPaid) && req.Item.PaymentMasterId == 0)
                {

                    var paymentMaster = req.MapRequestObject<PaymentBo, PaymentMaster>();
                    paymentMaster.Item.PaymentInfoes.Add(new PaymentInfo
                    {
                        PaymentDate = DateTimeOffset.Now,
                        Amount = req.Item.Amount,
                        PaymentMasterId = paymentMaster.Id,
                        PaymentType = req.Item.PaymentType,
                        Name = string.Empty,
                        TenantId = req.TenantId,
                        CreatedById = req.UserId,
                    });

                    return (await uow.PaymentMasterRepository.CreateAndSave(paymentMaster)).MapObject<PaymentMaster, PaymentBo>();
                }
                else if (req.Item.PaymentMasterId != 0)
                {

                    (await uow.PaymentInfoRepository.CreateAndSave(req.MapRequestObject<PaymentBo, PaymentInfo>())).MapObject<PaymentInfo, PaymentBo>();
                    var paymentMaster = await uow.PaymentMasterRepository.ReadAsTracking(new Request<int>() { TenantId = req.TenantId, UserId = req.UserId, Item = req.Item.PaymentMasterId });
                    paymentMaster.PaymentState = PaymentState.Paid;
                    paymentMaster.EditedOn = DateTime.Now;
                    await uow.SaveAsync();
                    return paymentMaster.MapObject<PaymentMaster, PaymentBo>();

                }
                else
                {
                    throw new NotImplementedException();
                }
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public async Task Delete(Request<int> req)
        {
            try
            {
                await uow.PaymentMasterRepository.DeleteAndSave(req);

            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task<PageList<PaymentViewResult>> ReadPayment(Search request)
        {
            try
            {
                List<PaymentViewResult> queryResult = new List<PaymentViewResult>();
                var search = request.SearchTerm.ToJsonObject<PaymentSearchBo>();
                if (search.IsNull())
                {
                    search = new PaymentSearchBo();
                }

                var term = request.OrderByTerm != "" ? request.OrderByTerm.Split(':')[0].TrimAndToLower() : "payment.Id";
                var order = request.OrderByTerm != "" ? request.OrderByTerm.Split(':')[1] : "ASC";

                var searchresult = search.Search;
                var skip = request.Skip;
                var take = request.Take;
 
                var query = $@"select member.Id,member.Name, member.ProfileImage, member.Email, payment.PaymentState,payment.MemberId, payment.Amount, payment.Id, payment.TransActionType, payment.CreatedOn,payment.EditedOn
                            from [Inexis.Membership].[Member] member 
                            left join [Inexis.Membership].[MemberSubscription]  membersubscription on member.id = membersubscription.MemberId
                            left join [Inexis.Payments].[PaymentMaster]  payment on member.Id = payment.MemberId
                            where (member.FirstName like '{searchresult}%' or
                            payment.Id like '{searchresult}%' or
                            member.Email like '{searchresult}%' or
                            member.Phone like '{searchresult}%')
                            and member.TenantId = @tenantId
                            and member.RecordState = @recordState
                            and payment.CreatedOn BETWEEN @fromDate AND @toDate
                            order by {term} {order}
                            OFFSET {skip} ROWS 
                            FETCH NEXT {take} ROWS ONLY";


                using (var conn = new DatabaseInfo().Connection())
                {
                    queryResult = conn.Query<PaymentViewResult>(query, new
                    {
                        tenantId = request.TenantId,
                        recordState = Enums.RecordStatus.Active,
                    fromDate = search.FromDate.ToUniversalTime(),
                        toDate= search.ToDate.ToUniversalTime()
                }).ToList();
                }

                foreach (var item in queryResult)
                {
                    item.ProfileImage = ImagePath(FileType.ProfileImageDefault, item.ProfileImage);
                }

                var itemcount = uow.PaymentMasterRepository.TableAsNoTracking.Where(mem => mem.TenantId == request.TenantId).Count();

                var pageresult = new PageList<PaymentViewResult>
                {
                    Skip = request.Skip,
                    Take = request.Take,
                    Items = queryResult,
                    TotalRecodeCount = itemcount
                };
                return pageresult;
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }


        public async Task<PaymentViewResult> ReadById(Request<int> req)
        {
            try
            {

                PaymentViewResult queryResult = new PaymentViewResult();
                var query = $@"select member.Id, member.Name as MemberName, member.ProfileImage, subscriptionType.Name, member.Email, payment.PaymentState,payment.MemberId, payment.Amount, payment.Id, payment.TransActionType, payment.CreatedOn,payment.EditedOn
                            from [Inexis.Membership].[Member] member 
                            left join [Inexis.Membership].[MemberSubscription]  membersubscription on member.id = membersubscription.MemberId
                            left join [Inexis.Payments].[PaymentMaster]  payment on membersubscription.Id = payment.MemberSubscriptionId
                            left join [Inexis.Membership].[SubscriptionType] subscriptionType on membersubscription.SubscriptinId = subscriptionType.Id
                            where member.TenantId = @tenantId
                            and payment.Id = @paymentId
                            and member.RecordState = @recordState";
                using (var conn = new DatabaseInfo().Connection())
                {
                    queryResult = conn.Query<PaymentViewResult>(query, new
                    {
                        tenantId = req.TenantId,
                        paymentId = req.Item,
                        recordState = Enums.RecordStatus.Active,
                    }).FirstOrDefault();
                }
                return queryResult;
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task<PaymentBo> ReadMemberPayment(Request<int> req)
        {
            try
            {
                return (await uow.PaymentMasterRepository.TableAsNoTracking.FirstOrDefaultAsync(a => a.MemberId == req.Item))
                   .MapObject<PaymentMaster, PaymentBo>();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task<PaymentBo> ReadMemberPaymentBySubscription(Request<int> req)
        {
            try
            {
                return (await uow.PaymentMasterRepository.TableAsNoTracking.FirstOrDefaultAsync(a => a.MemberSubscriptionId == req.Item))
                   .MapObject<PaymentMaster, PaymentBo>();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }


        public async Task<List<KeyValueListItem<int>>> ReadKeyValue(Search req)
        {
            try
            {
                return await uow.PaymentMasterRepository.ReadKeyValue<PaymentMaster>(req);

            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public async Task Update(Request<PaymentBo> req)
        {
            try
            {
                await uow.PaymentMasterRepository.UpdateAndSave(req.MapRequestObject<PaymentBo, PaymentMaster>(req.Item.Id));

            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        Task<PageList<PaymentBo>> IBaseService<PaymentBo>.Read(Search search)
        {
            throw new NotImplementedException();
        }

        public Task<PaymentBo> Read(Request<int> request)
        {
            throw new NotImplementedException();
        }
    }
}
