﻿using Inx.Core.Base;
using Inx.Module.Payment.Core.Bo;
using Inx.Utility.Models;
using System.Threading.Tasks;

namespace Inx.Module.Payment.Core.Services.Interfaces
{
    public interface IPaymentService : IBaseService<PaymentBo>
    {
        Task<PaymentBo> ReadMemberPayment(Request<int> req);
        Task<PageList<PaymentViewResult>> ReadPayment(Search request);
        Task<PaymentViewResult> ReadById(Request<int> req);
        Task<PaymentBo> ReadMemberPaymentBySubscription(Request<int> req);
    }
}
