﻿using System;

namespace Inx.Module.Payment.Core.Bo
{
    public class PaymentSearchBo
    {
        public string Search { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}
