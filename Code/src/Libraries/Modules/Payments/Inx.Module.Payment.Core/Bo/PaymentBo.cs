﻿using Inx.Utility.Models;
using System;
using static Inx.Utility.Utility.Enums;

namespace Inx.Module.Payment.Core.Bo
{
    public class PaymentBo : BaseBo
    {
        public int MemberSubscriptionId { get; set; }
        public int MemberId { get; set; }
        public int PaymentMasterId { get; set; }
        public decimal Amount { get; set; }
        public decimal SubscriptionPrice { get; set; }
        public decimal Remaining { get; set; }
        public PaymentTypes PaymentType { get; set; }
        public TransActionType TransActionType { get; set; }
        public PaymentState PaymentState { get; set; }
        public string Description { get; set; }
        public DateTimeOffset PaymentDate { get; set; }

        public DateTime CreatedOn { get; set; }
        public DateTime? EditedOn { get; set; }
        public PaymentBo()
        {
            Remaining = Amount - SubscriptionPrice;
            Name = this.TransActionType.ToString() + Name;
        }

    }
}
