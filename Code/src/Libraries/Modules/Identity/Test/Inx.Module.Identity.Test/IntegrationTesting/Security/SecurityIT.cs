﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Inx.Module.Identity.Core.Bo.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Inx.Module.Identity.Core.Services.Security;
using Inx.Module.Identity.Test.Utility;
using Inx.Utility.Exceptions;
using Inx.Utility.Models;
using Inx.Utility.Utility;
using Inx.Test.Base.Core;

namespace Inx.Module.Identity.Test.IntegrationTesting.Security
{
    [TestClass]
    public class SecurityIT: CoreTestBase
    {
        private ISecurityService service = null;
        private Guid token;
        [TestInitialize]
        public void Initialize()
        {
            service = new SecurityService(new Comman().IUnitOfWorkIdentity);
        }

        [DataTestMethod]
        [DataRow("test@gmail.com", "123")] //true
        [DataRow("test123@gmail.com", "123")] //false
        [DataRow("test@gmail.com", "1234")] //false
        public async Task Authentication(string uname, string password)
        {
            try
            {
                await service.Authentication(new AuthenticateBo
                {
                    Email = uname,
                    Password = password.Encrypt()
                });
                Assert.IsTrue(true, "loged");
            }
            catch (UserSecurityException e)
            {
                if (uname == "test123@gmail.com" || password == "1234")
                {
                    Assert.IsTrue(true, "catch invalid username and password");
                }
                else
                {
                    throw e;
                }
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [DataTestMethod]
        [DataRow("test@gmail.com", "123")] //false
        [DataRow("test123@gmail.com", "123")] //true
        public async Task Register(string email, string password)
        {
            try
            {
                await service.Register(new Request<RegisterBo>()
                {
                    Item = new RegisterBo()
                    {
                        Email = email,
                        Password = password
                    },
                    TenantId = 0,
                    Id = 0,
                    UserId = 0
                });
            }
            catch (Exception e)
            {
                if (e is UserSecurityException && email == "test@gmail.com")
                {
                    if (e.Message == "{\"State\":3,\"Message\":\"AlreadyRegisted\",\"Description\":null}")
                    {
                        Assert.IsTrue(true);
                    }
                }

                Assert.Fail(e.Message);
            }
        }

        [DataTestMethod]
        [DataRow("chamith@inexisconsulting.com")] //false
        [DataRow("faketest123@gmail.com")] //true
        public async Task ResendRegisterValidateEmail(string email)
        {
            try
            {
                token = await service.ResendRegisterValidateEmail(new Request<string>()
                {
                    Item = email
                });
                Assert.IsTrue(true);
            }
            catch (Exception e)
            {
                if (e is RecordNotFoundException && email == "faketest123@gmail.com")
                {
                    Assert.IsTrue(true);
                }

                Assert.Fail(e.Message);
            }
        }
        [DataTestMethod]
        [DataRow("chamith@inexisconsulting.com")] //false
        [DataRow("faketest123@gmail.com")] //true
        public async Task ValidateEmailAddress(Request<Dictionary<string, string>> request)
        {

            try
            {
                Assert.IsTrue(true);
            }
            catch (Exception e)
            {
                
            }
        }

        [DataTestMethod]
        [DataRow(1,"123", "xxx")]  
        [DataRow(1,"1234", "xxx")]
        [DataRow(2, "123", "xxx")]
        [DataRow(2, "1234","xxx")]
        public async Task ChangePassword(int userid,string currentpassword,string newpassword)
        {
            try
            {
                await service.ChangePassword(new Request<ChangePasswordBo>()
                {
                    Item = new ChangePasswordBo()
                    {
                        CurrentPassword = "",
                        NewPassword = ""
                    }
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [DataTestMethod]
        [DataRow("chamith@inexisconsulting.com")]
        [DataRow("faketest123@gmail.com")]
        public async Task ForgetPasswordRequest(string email)
        {
            try
            {
                await service.ForgetPasswordRequest(new Request<string>() {Item = email});
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        [DataTestMethod]
        [DataRow("chamith@inexisconsulting.com")]
        [DataRow("faketest123@gmail.com")]
        public async Task ForgetPasswordValidateToken(string email)
        {

        }

        public async Task ForgetPasswordChangePassword()
        {

        }
    }
}
