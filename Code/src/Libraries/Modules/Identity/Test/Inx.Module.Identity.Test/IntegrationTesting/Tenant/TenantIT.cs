﻿using System.Threading.Tasks;
using Inx.Module.Identity.Core.Services.Tenant;
using Inx.Module.Identity.Core.Services.Tenant.Interface;
using Inx.Module.Identity.Test.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Inx.Module.Identity.Test.IntegrationTesting.Tenant
{
    public class TenantId
    {
        private ITenantService service = null;
        [TestInitialize]
        public void Initialize()
        {
            service = new TenantService(new Comman().IUnitOfWorkIdentity);
        }

        [DataTestMethod]
        [DataRow("chamith@inexisconsulting.com")]
        [DataRow("faketest123@gmail.com")]
        public async Task Create()
        {

        }
        [DataTestMethod]
        [DataRow("chamith@inexisconsulting.com")]
        [DataRow("faketest123@gmail.com")]
        public async Task CheckDomain(string domain)
        {

        }
        [DataTestMethod]
        [DataRow("chamith@inexisconsulting.com")]
        [DataRow("faketest123@gmail.com")]
        public async Task Read()
        {
        }
        [DataTestMethod]
        [DataRow("chamith@inexisconsulting.com")]
        [DataRow("faketest123@gmail.com")]
        public async Task Update()
        {

        }
    }
}
