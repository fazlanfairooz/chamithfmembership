﻿using Inx.Module.Identity.Service.Models.Tenant;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Inx.Module.Identity.Test.ModelValidation
{
    public class TenantModelValidation
    {
        [DataTestMethod]
        [DataRow("123", "1234")]
        [DataRow("", "")]
        public void HostNameViewModel(string host)
        {
            var model = new HostNameViewModel()
            {
                Host = host
            };
        }

        [DataTestMethod]
        [DataRow("123", "1234")]
        [DataRow("", "")]
        public void TenantViewModel(string email, string password)
        {
            var model = new TenantViewModel()
            {
               
            };
        }
        
    }
}
