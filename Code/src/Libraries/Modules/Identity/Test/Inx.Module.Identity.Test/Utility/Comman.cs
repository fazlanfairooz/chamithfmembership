﻿using Inx.Module.Identity.Data.DbContext;
using Inx.Utility;

namespace Inx.Module.Identity.Test.Utility
{
    public class Comman
    {
        public IdentityDbContext Context { get; set; } = new IdentityDbContext(GlobleConfig.ConnectionStringTest);
        public IUnitOfWorkIdentity IUnitOfWorkIdentity { get; set; } = new UnitOfWork(new IdentityDbContext(GlobleConfig.ConnectionStringTest));
    }
}
