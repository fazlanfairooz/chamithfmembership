﻿using Inx.Module.Identity.Core.Bo.Security;
using Inx.Module.Identity.Service.Controllers.Api.Security.Utility.Jwt;
using Inx.Service.Base.Models;
using Inx.Utility.Extentions;
using Inx.Utility.Models;
using Inx.Utility.Utility;

namespace Inx.Module.Identity.Service.Utility
{
    public class Token
    {
        public static Response<dynamic> TokenGenarator(AuthResponse result, long timetaken = 0,Enums.UserRecordStatus userstate=Enums.UserRecordStatus.Active)
        {

            var token = new JwtTokenBuilder()
                .AddSecurityKey(JwtSecurityKey.Create(JwtConfig.SecurityKey))
                .AddSubject(JwtConfig.Subject)
                .AddIssuer(JwtConfig.Issuer)
                .AddAudience(JwtConfig.Audience)
                .AddExpiry(JwtConfig.Expiry * 24 * 60)
                .AddClaim("userid", result.Id.ToString())
                .AddClaim("tenant", result.TenantId.ToString())
                .AddClaim("userroles", result.Roles.ToJsonString())
                .AddClaim("userinfo", result.MapObject<AuthResponse, UserInfo>().ToJsonString())
                .AddClaim("userstate", result.UserState.ToString())
                .Build();

            return new Response<dynamic>
            {
                Item = new
                {
                    token = token.Value,
                    userInfo = result.ToJsonString(),
                    userState = (int)userstate
                },
                TimeTaken = timetaken
            };
        }
    }
}
