﻿using Abp.AutoMapper;
using Abp.Configuration.Startup;
using Inx.Module.Identity.Core.Bo.Security;
using Inx.Module.Identity.Core.Bo.Security.User;
using Inx.Module.Identity.Core.Bo.Tenant;
using Inx.Module.Identity.Core.Bo.User;
using Inx.Module.Identity.Data.Entity.Tenant;
using Inx.Module.Identity.Service.Models;
using Inx.Module.Identity.Service.Models.Security.ChangePassword;
using Inx.Module.Identity.Service.Models.Security.Login;
using Inx.Module.Identity.Service.Models.Security.Register;
using Inx.Module.Identity.Service.Models.Security.User;
using Inx.Module.Identity.Service.Models.Tenant;
using Inx.Service.Base.Models;
using Inx.Utility.Extentions;
using Inx.Utility.Utility;
using UserBo = Inx.Module.Identity.Core.Bo.Security.User.UserBo;
using UserViewModel = Inx.Module.Identity.Service.Models.Security.User.UserViewModel;

namespace Inx.Module.Identity.Service.Utility.Config
{
    public class Automapper
    {
        public static void Register(IAbpStartupConfiguration Configuration)
        {
            Configuration.Modules.AbpAutoMapper().Configurators.Add(cfg =>
            {
                cfg.CreateMap<RegisterViewModel, RegisterBo>()
                    .ForMember(des => des.Email, opt => opt.MapFrom(src => src.Email.TrimAndToLower()))
                    .ForMember(des => des.Password, opt => opt.MapFrom(src => src.Password.Encrypt()))
                    .ReverseMap();
                cfg.CreateMap<AuthenticateViewModel, AuthenticateBo>()
                    .ForMember(des => des.Email, opt => opt.MapFrom(src => src.Email.TrimAndToLower()))
                    .ForMember(des => des.Password, opt => opt.MapFrom(src => src.Password.Encrypt()))
                    .ReverseMap();
                cfg.CreateMap<AuthResponse, UserInfo>().ReverseMap();
                cfg.CreateMap<TenantViewModel, TenantBo>()
                    .ForMember(des => des.Host, opt => opt.MapFrom(src => src.Host.TrimAndToLower()))
                    .ReverseMap();
                cfg.CreateMap<TenantBo, Tenant>().ReverseMap();
                cfg.CreateMap<ChangePasswordViewModel, ChangePasswordBo>().ReverseMap();
                cfg.CreateMap<Core.Bo.User.UserBo, Models.UserViewModel>().ReverseMap();
                cfg.CreateMap<TenantInfoBo, TenantInfo>().ReverseMap();
                cfg.CreateMap<TenantInfoViewModel, TenantInfoBo>().ReverseMap();
                cfg.CreateMap<UserViewModel, UserBo>()
                   .ForMember(des => des.Email, opt => opt.MapFrom(src => src.Email.TrimAndToLower()))
                  // .ForMember(des => des.Password, opt => opt.MapFrom(src => src.Password.Encrypt()))
                   .ReverseMap();
                cfg.CreateMap<PermissionUserBo, PermissionUserViewModel>().ReverseMap();
                cfg.CreateMap<ChangePermissionPasswordViewModel, ChangePermissionPasswordBo>().ReverseMap();
            });     
        }
    }
}
