﻿using Abp.Dependency;
using Inx.Core.Base;
using Inx.Module.Identity.Core.Services.Security;
using Inx.Module.Identity.Core.Services.Tenant;
using Inx.Module.Identity.Core.Services.Tenant.Interface;
using Inx.Module.Identity.Data.DbContext;
using Inx.Service.Base.Controllers;
using Inx.Utility.Cache.Runtime;

namespace Inx.Module.Identity.Service.Utility.Config
{
    public class IocConfig
    {
        public  static void Register(IIocManager iocManager)
        {
            iocManager.RegisterIfNot<IIdentityDbSet, IdentityDbContext>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<IUnitOfWorkIdentity, UnitOfWork>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<ISecurityService, SecurityService>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<ITenantService, TenantService>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<ITenantTypeService, TenantTypeService>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<ITenantInfoService, TenantInfoService>(DependencyLifeStyle.Transient);
  
            #region genaral
            iocManager.RegisterIfNot<IMemoryCacheManager, MemoryCacheManager>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<IBaseInjector, BaseInjector>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<ICoreInjector, CoreInjector>();
            #endregion

        }
    }
}
