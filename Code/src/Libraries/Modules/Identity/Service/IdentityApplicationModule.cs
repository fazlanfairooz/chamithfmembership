﻿using Abp.AspNetCore;
using Abp.AutoMapper;
using Abp.Hangfire;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Inx.Module.Identity.Service.Utility.Config;
namespace Inx.Module.Identity.Service
{
    [DependsOn( typeof(AbpAspNetCoreModule)),
     DependsOn(typeof(AbpAutoMapperModule)),
     DependsOn(typeof(AbpHangfireAspNetCoreModule))]
    public class IdentityApplicationModule : AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public IdentityApplicationModule(IHostingEnvironment env)
        {
        }

        public override void PreInitialize()
        {
            IocConfig.Register(IocManager);
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(IdentityApplicationModule).GetAssembly());
            Automapper.Register(Configuration);
        }
    }
}
