﻿using Inx.Service.Base.Controllers;
using System;
using Inx.Module.Identity.Service.Models.Roles;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Inx.Module.Identity.Service.Controllers.Api.Roles
{
    [Route("api/identity/v1")]
    public class UserRolesApiController : IdentityBaseApiController, IBaseApi<UserRoleViewModel, int>
    {
        public UserRolesApiController(IBaseInjector baseinject):base(baseinject)
        {
        }
        #region not implemented
        [ApiExplorerSettings(IgnoreApi = true)]
        public Task<IActionResult> Create(UserRoleViewModel item)
        {
            throw new NotImplementedException();
        }
        [ApiExplorerSettings(IgnoreApi = true)]
        public Task<IActionResult> Delete(int id)
        {
            throw new NotImplementedException();
        }
        [ApiExplorerSettings(IgnoreApi = true)]
        public Task<IActionResult> Read(int id)
        {
            throw new NotImplementedException();
        }
        [ApiExplorerSettings(IgnoreApi = true)]
        public Task<IActionResult> Read(int skip = 0, int take = 0, string search = null, string orderby = null)
        {
            throw new NotImplementedException();
        }
        [ApiExplorerSettings(IgnoreApi = true)]
        public Task<IActionResult> ReadKeyValue()
        {
            throw new NotImplementedException();
        }
        [ApiExplorerSettings(IgnoreApi = true)]
        public Task<IActionResult> Update(UserRoleViewModel item)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
