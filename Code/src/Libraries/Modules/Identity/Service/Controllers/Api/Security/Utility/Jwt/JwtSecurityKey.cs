﻿using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace Inx.Module.Identity.Service.Controllers.Api.Security.Utility.Jwt
{
    public static class JwtSecurityKey
    {
        public static SymmetricSecurityKey Create(string secret)
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secret));
        }
    }
}
