﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Inx.Module.Identity.Core.Bo.User;
using Inx.Module.Identity.Service.Models;
using Inx.Service.Base.Attributes;
using Inx.Service.Base.Models;
using Inx.Utility;
using Inx.Utility.Extentions;
using Inx.Utility.Messenger.Email.Modules.Email;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using UserBo = Inx.Module.Identity.Core.Bo.Security.User.UserBo;
using UserViewModel = Inx.Module.Identity.Service.Models.Security.User.UserViewModel;

namespace Inx.Module.Identity.Service.Controllers.Api.Security
{
    public partial class SecurityApiController
    {
        [HttpPost, Route("users"), AllowAnonymous, ModelValidation]
        public async Task<IActionResult> UserCreate([FromBody]UserViewModel request)
        {
            try
            {
                var stopwatch = Stopwatch.StartNew();

                var res = await serviceSecurity.CreateUser(Request<UserViewModel, UserBo>(request));
                //send confirmation email
                await SendEmailtoInviter(request.Email, res.Token);
                //BackgroundJob.Enqueue(() => SendEmailValidationEmail(request.Email, res.Token));
                stopwatch.Stop();
                return Ok(new Response<bool>()
                {
                    Item = true,
                    TimeTaken = stopwatch.ElapsedMilliseconds
                });
            }
            catch (Exception ex)
            {
                return await HandleException(ex);
            }
        }
        [HttpGet, Route("users")]
        public async Task<IActionResult> ReadUser()
        {
            try
            {
                return Ok((await serviceSecurity.ReadUser(Request(string.Empty))).MapObject<Core.Bo.User.UserBo, Models.UserViewModel>());
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }

        [HttpGet, Route("getpermissionusers")]
        public async Task<IActionResult> GetPermissionUserInfo()
        {
            try
            {
                return Ok((await serviceSecurity.ReadUsers(Request(string.Empty))).MapListObject<PermissionUserBo, PermissionUserViewModel>());
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }
        #region email
        [ApiExplorerSettings(IgnoreApi = true)]
        private async Task SendEmailtoInviter(string email, Guid token)
        {
            await Email.Generate(await Lang("Welcome to  Assocify!", HttpContext),
                    await Lang("Click following button to set the password and login to the system.", HttpContext))
                .Sender(await Lang("Welcome to  Assocify!", HttpContext), $"Hi {email},",
                    new Inx.Utility.Messenger.Email.Modules.Application.Tenant
                    {
                        Name = "Membership application",
                        Host = "sa",
                        LogoFileName = ""
                    }, new List<string>() { email })
                .AddButton(new Button
                {
                    Title = await Lang("Accept", HttpContext),
                    Type = ButtonType.Apply,
                    Url = $"{GlobleConfig.ApiUrl}/#/permissionpassword?email={email}&token={token.ToString()}"

                }).Send();
        }


        #endregion
    }
}
