﻿using System.Threading.Tasks;
using Inx.Module.Identity.Service.Models.Security.ChangePassword;
using Inx.Module.Identity.Service.Models.Security.Login;
using Inx.Module.Identity.Service.Models.Security.Register;
using Inx.Module.Identity.Service.Models.Security.User;
using Inx.Service.Base.Models;
using Microsoft.AspNetCore.Mvc;

namespace Inx.Module.Identity.Service.Controllers.Api.Security
{
    internal interface ISecurityApiController
    {
        Task<IActionResult> Register(RegisterViewModel request);
        Task<IActionResult> ForgetPasswordRequest(PostValue<string> email);
        Task<IActionResult> ForgetPasswordValidateToken(string email, string token);
        Task<IActionResult> ForgetPasswordSetNewPassword(PostValue<string> password);
        Task<IActionResult> ChangePassword(ChangePasswordViewModel request);
        Task<IActionResult> ValidateEmailAddress(string email,string token);
        Task<IActionResult> Authentication(AuthenticateViewModel request);
        Task<IActionResult> ResendRegisterValidateEmail(PostValue<string> email);

        //new user
        Task<IActionResult> ReadUser();
        Task<IActionResult> UserCreate([FromBody] UserViewModel request);
      
      
    }
}
