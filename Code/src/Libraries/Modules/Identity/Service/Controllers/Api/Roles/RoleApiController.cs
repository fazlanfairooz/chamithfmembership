﻿using System;
using System.Threading.Tasks;
using Inx.Module.Identity.Service.Models.Roles;
using Inx.Service.Base.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace Inx.Module.Identity.Service.Controllers.Api.Roles
{
    [Route("api/identity/v1")]
    public class RolesApiController : IdentityBaseApiController, IBaseApi<RoleViewModel, int>
    {
        public RolesApiController(IBaseInjector baseinject):base(baseinject)
        {
            
        }
        [ApiExplorerSettings(IgnoreApi = true)]
        public Task<IActionResult> Create(RoleViewModel item)
        {
            throw new NotImplementedException();
        }
        [ApiExplorerSettings(IgnoreApi = true)]
        public Task<IActionResult> Delete(int id)
        {
            throw new NotImplementedException();
        }
        [ApiExplorerSettings(IgnoreApi = true)]
        public Task<IActionResult> Read(int id)
        {
            throw new NotImplementedException();
        }
        [ApiExplorerSettings(IgnoreApi = true)]
        public Task<IActionResult> Read(int skip = 0, int take = 0, string search = null, string orderby = null)
        {
            throw new NotImplementedException();
        }
        [ApiExplorerSettings(IgnoreApi = true)]
        public Task<IActionResult> ReadKeyValue()
        {
            throw new NotImplementedException();
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public Task<IActionResult> Update(RoleViewModel item)
        {
            throw new NotImplementedException();
        }
    }
}
