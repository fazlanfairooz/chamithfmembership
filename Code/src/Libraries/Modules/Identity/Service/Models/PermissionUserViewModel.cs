﻿using Inx.Service.Base.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Inx.Module.Identity.Service.Models
{
    public class PermissionUserViewModel : BaseViewModel
    {
        public string Email { get; set; }
        public string RoleName { get; set; }
        public string UserStatus { get; set; }
        public Inx.Utility.Utility.Enums.UserRecordStatus RecordStatus { get; set; }
        public int TenantId { get; set; }
        public Inx.Utility.Utility.Enums.Roles Role { get; set; }
        public int updateRecordStatus { get; set; }
    }
}
