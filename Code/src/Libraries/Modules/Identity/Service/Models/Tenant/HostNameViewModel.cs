﻿using System.ComponentModel.DataAnnotations;
using Inx.Utility;

namespace Inx.Module.Identity.Service.Models.Tenant
{
    public class HostNameViewModel
    {
        [Required,RegularExpression(RegularExpression.Name),MaxLength(15),MinLength(3)]
        public string Host { get; set; }
    }
}
