﻿using Inx.Data.Base.Utility;
using Inx.Service.Base.Models;
using System.ComponentModel.DataAnnotations;

namespace Inx.Module.Identity.Service.Models.Tenant
{
    public class TenantInfoViewModel: BaseViewModel
    {
        [EmailAddress, StringLength(DbConstraints.EmailLength)]
        public string Email { get; set; }
        [StringLength(DbConstraints.AddressLength)]
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public int? Country { get; set; }
        [StringLength(DbConstraints.PhoneLength)]
        public string Phone { get; set; }
        [StringLength(DbConstraints.FaxLength)]
        public string Fax { get; set; }
        public int Language { get; set; }
        public int Currency { get; set; }
        public string Prefix { get; set; }
    }
}
