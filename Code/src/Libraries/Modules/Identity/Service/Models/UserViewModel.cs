﻿using Inx.Service.Base.Models;

namespace Inx.Module.Identity.Service.Models
{
    public class UserViewModel: BaseViewModel
    {
        public string Email { get; set; }
        public Inx.Utility.Utility.Enums.UserRecordStatus RecordStatus { get; set; }
        public string ProfileImage { get; set; }
        public int TenantId { get; set; }
    }
}
