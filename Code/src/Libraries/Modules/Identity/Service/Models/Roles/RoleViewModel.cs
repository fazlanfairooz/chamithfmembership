﻿using System.ComponentModel.DataAnnotations;
using Inx.Data.Base.Utility;
using Inx.Service.Base.Models;

namespace Inx.Module.Identity.Service.Models.Roles
{
    public class RoleViewModel:BaseViewModel
    {
        [Required, StringLength(DbConstraints.NameLength)]
        public string Name { get; set; }
        [StringLength(DbConstraints.DescriptionLength)]
        public string Description { get; set; }
    }
}
