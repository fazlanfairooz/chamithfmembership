﻿using Inx.Service.Base.Models;
using Inx.Utility.Attributes;

namespace Inx.Module.Identity.Service.Models.Roles
{
    public class UserRoleViewModel:BaseViewModel
    {
        [NumberNotZero]
        public long UserId { get; set; }
        [NumberNotZero]
        public int RoleId { get; set; }
    }
}
