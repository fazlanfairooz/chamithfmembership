﻿using System.ComponentModel.DataAnnotations;
using Inx.Data.Base.Utility;
using Inx.Service.Base.Models;

namespace Inx.Module.Identity.Service.Models.Security.ChangePassword
{
    public class ChangePasswordViewModel: BaseViewModel
    {
        [Required,StringLength(DbConstraints.NameLength)]
        public string CurrentPassword { get; set; }
        [Required, StringLength(DbConstraints.NameLength)]
        public string NewPassword { get; set; }
        public Inx.Utility.Utility.Enums.UserRecordStatus RecordStatus { get; set; }
      
    }
}
