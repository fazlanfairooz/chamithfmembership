﻿namespace Inx.Module.Identity.Service.Models.Security.Login
{
    public class LoginResponseViewModel
    {
        public string AccessToken { get; set; }
        public long UserId { get; set; }
    }
}
