﻿using Inx.Data.Base.Repository;
using Inx.Module.Identity.Data.Entity.Security;
using Inx.Module.Identity.Data.Entity.Tenant;

namespace Inx.Module.Identity.Data.DbContext
{
    public interface IUnitOfWorkIdentity: IUnitOfWork
    {
        GenericRepository<User> UserRepository { get; }
        GenericRepository<UserRole> UserRoleRepository { get; }
        GenericRepository<Role> RoleRepository { get; }
        GenericRepository<Tenant> TenantRepository { get; }
        GenericRepository<TenantType> TenantTypeRepository { get; }
        GenericRepository<TenantInfo> TenantInfoRepository { get; }
    }
}
