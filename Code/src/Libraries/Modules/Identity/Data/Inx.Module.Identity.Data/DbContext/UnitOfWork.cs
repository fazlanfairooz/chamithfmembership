﻿using System;
using System.Threading.Tasks;
using Inx.Data.Base.Repository;
using Inx.Module.Identity.Data.Entity.Security;
using Inx.Module.Identity.Data.Entity.Tenant;
using Microsoft.EntityFrameworkCore;

namespace Inx.Module.Identity.Data.DbContext
{
    public partial class UnitOfWork : IUnitOfWorkIdentity
    {
        #region fields
        private bool disposed;
        public Microsoft.EntityFrameworkCore.DbContext Context { get; }

        #region identity
        private GenericRepository<User> userRepository;
        private GenericRepository<UserRole> userRoleRepository;
        private GenericRepository<Role> roleRepository;
        private GenericRepository<Tenant> tenantRepository;
        private GenericRepository<TenantType> tenantTypeRepository;
        private GenericRepository<TenantInfo> tenantInfoRepository;
        #endregion
        public string ConnectionString => Context.Database.GetDbConnection().ConnectionString;
        #endregion

        #region ctor
        public UnitOfWork(IIdentityDbSet dbcontext)
        {
            Context = (IdentityDbContext)dbcontext;
        }
        #endregion
        #region repositories
        public GenericRepository<User> UserRepository => userRepository ?? (userRepository = new GenericRepository<User>(Context));
        public GenericRepository<UserRole> UserRoleRepository => userRoleRepository ?? (userRoleRepository = new GenericRepository<UserRole>(Context));
        public GenericRepository<Role> RoleRepository => roleRepository ?? (roleRepository = new GenericRepository<Role>(Context));
        public GenericRepository<Tenant> TenantRepository => tenantRepository   ?? (tenantRepository = new GenericRepository<Tenant>(Context));
        public GenericRepository<TenantType> TenantTypeRepository => tenantTypeRepository ?? (tenantTypeRepository = new GenericRepository<TenantType>(Context));
        public GenericRepository<TenantInfo> TenantInfoRepository => tenantInfoRepository ?? (tenantInfoRepository = new GenericRepository<TenantInfo>(Context));
        #endregion

        #region methods
        public void Save()
        {
            Context.SaveChanges();
        }
        public async Task SaveAsync()
        {
            await Context.SaveChangesAsync();
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    Context.Dispose();
                }
            }
            disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
      #endregion
    }
}
