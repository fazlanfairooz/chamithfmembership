﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Inx.Module.Identity.Data.Migrations
{
    public partial class Identity_Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Inexis.Security");

            migrationBuilder.CreateTable(
                name: "Roles",
                schema: "Inexis.Security",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    EditedById = table.Column<int>(nullable: true),
                    EditedOn = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    RecordState = table.Column<byte>(nullable: false),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TenantInfo",
                schema: "Inexis.Security",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Address = table.Column<string>(maxLength: 200, nullable: true),
                    City = table.Column<string>(nullable: true),
                    Country = table.Column<int>(nullable: false),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    Currency = table.Column<int>(nullable: false),
                    EditedById = table.Column<int>(nullable: true),
                    EditedOn = table.Column<DateTime>(nullable: true),
                    Email = table.Column<string>(maxLength: 50, nullable: true),
                    Fax = table.Column<string>(maxLength: 50, nullable: true),
                    Language = table.Column<int>(nullable: false),
                    Phone = table.Column<string>(maxLength: 20, nullable: true),
                    Prefix = table.Column<string>(nullable: true),
                    RecordState = table.Column<byte>(nullable: false),
                    State = table.Column<string>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Zip = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TenantInfo", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TenantTypes",
                schema: "Inexis.Security",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TenantTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                schema: "Inexis.Security",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    EditedById = table.Column<int>(nullable: true),
                    EditedOn = table.Column<DateTime>(nullable: true),
                    Email = table.Column<string>(maxLength: 50, nullable: false),
                    EmailConfirmCode = table.Column<Guid>(nullable: false),
                    IsEmailConfirmed = table.Column<bool>(nullable: false),
                    Password = table.Column<string>(maxLength: 200, nullable: false),
                    PasswordResetCode = table.Column<Guid>(nullable: false),
                    RecordStatus = table.Column<byte>(nullable: false),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Tenants",
                schema: "Inexis.Security",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Host = table.Column<string>(maxLength: 50, nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    OrganizationLogo = table.Column<string>(nullable: true),
                    TenantTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tenants", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tenants_TenantTypes_TenantTypeId",
                        column: x => x.TenantTypeId,
                        principalSchema: "Inexis.Security",
                        principalTable: "TenantTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserRoles",
                schema: "Inexis.Security",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    EditedById = table.Column<int>(nullable: true),
                    EditedOn = table.Column<DateTime>(nullable: true),
                    RecordState = table.Column<byte>(nullable: false),
                    RoleId = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRoles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserRoles_Roles_RoleId",
                        column: x => x.RoleId,
                        principalSchema: "Inexis.Security",
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserRoles_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Tenants_Host",
                schema: "Inexis.Security",
                table: "Tenants",
                column: "Host",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Tenants_TenantTypeId",
                schema: "Inexis.Security",
                table: "Tenants",
                column: "TenantTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRoles_RoleId",
                schema: "Inexis.Security",
                table: "UserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRoles_UserId",
                schema: "Inexis.Security",
                table: "UserRoles",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_Email",
                schema: "Inexis.Security",
                table: "Users",
                column: "Email",
                unique: true);

            migrationBuilder.Sql(@"
                      SET IDENTITY_INSERT [Inexis.Security].TenantTypes ON;  
                 INSERT INTO [Inexis.Security].TenantTypes
                                         (Id, Name)
                VALUES        (0, 'DEFAULT')
                 SET IDENTITY_INSERT [Inexis.Security].TenantTypes OFF;  


               SET IDENTITY_INSERT [Inexis.Security].Tenants ON;  
                INSERT INTO [Inexis.Security].Tenants
                                         (Id, Host, Name,TenantTypeId)
                VALUES        (0, 'DEFAULT', 'DEFAULT',0)
                SET IDENTITY_INSERT [Inexis.Security].Tenants OFF;  

                 SET IDENTITY_INSERT [Inexis.Security].Users ON;  
                INSERT INTO [Inexis.Security].Users
                                         (Id, CreatedById, CreatedOn, Email, TenantId, RecordStatus, Password, IsEmailConfirmed,EmailConfirmCode,PasswordResetCode)
                VALUES        (0, 0,GETUTCDATE(), N'chamith@inexisconsulting.com', 0, 1, N'admin', 0,NEWID (),NEWID ()  );
                SET IDENTITY_INSERT [Inexis.Security].Users OFF;  


                INSERT INTO [Inexis.Security].Roles
                                         (CreatedById, CreatedOn, Description, Name, RecordState, TenantId)
                VALUES        (0, GETUTCDATE(), N'Tenant Creator', N'Super Admin', 1, 0)


                INSERT INTO [Inexis.Security].UserRoles
                                         (CreatedById, CreatedOn, RoleId, TenantId, UserId, RecordState)
                VALUES        (0, GETUTCDATE(), 1, 0, 0, 1)
                    ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TenantInfo",
                schema: "Inexis.Security");

            migrationBuilder.DropTable(
                name: "Tenants",
                schema: "Inexis.Security");

            migrationBuilder.DropTable(
                name: "UserRoles",
                schema: "Inexis.Security");

            migrationBuilder.DropTable(
                name: "TenantTypes",
                schema: "Inexis.Security");

            migrationBuilder.DropTable(
                name: "Roles",
                schema: "Inexis.Security");

            migrationBuilder.DropTable(
                name: "Users",
                schema: "Inexis.Security");
        }
    }
}
