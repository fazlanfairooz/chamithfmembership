﻿using Inx.Data.Base.Utility;

namespace Inx.Module.Identity.Data
{
    public class Constraints:DbConstraints
    {
        public new const string Schema = "Inexis.Security";
    }
}
