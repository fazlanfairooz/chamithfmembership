﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Inx.Data.Base.Utility;
using Inx.Module.Identity.Data.Entity.Application;
using static Inx.Utility.Utility.Enums;
namespace Inx.Module.Identity.Data.Entity.Security
{
    [Table("Users", Schema = Constraints.Schema)]
    public class User : AuditableEntity
    {
        [Required, EmailAddress, StringLength(DbConstraints.EmailLength)]
        public string Email { get; set; }
        [Required, StringLength(200)] public string Password { get; set; }
        public bool IsEmailConfirmed { get; set; } = false;
        public Guid EmailConfirmCode { get; set; } = Guid.Empty;
        public Guid PasswordResetCode { get; set; } = Guid.Empty;
        public UserRecordStatus RecordStatus { get; set; }
        [NotMapped, Obsolete] public new string Name { get; set; }
        [NotMapped,Obsolete] public new RecordStatus RecordState { get; set; }
    }
}
