﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Inx.Module.Identity.Data.Entity.Application;

namespace Inx.Module.Identity.Data.Entity.Security
{
    [Table("Roles", Schema = Constraints.Schema)]
    public class Role :AuditableEntity
    {
        [StringLength(500)]
        public string Description { get; set; }
    }
}
