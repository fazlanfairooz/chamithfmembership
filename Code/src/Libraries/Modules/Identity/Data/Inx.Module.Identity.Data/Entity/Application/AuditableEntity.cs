﻿using System.ComponentModel.DataAnnotations.Schema;
using Inx.Data.Base.Models;
using Inx.Module.Identity.Data.Entity.Security;

namespace Inx.Module.Identity.Data.Entity.Application
{
    public class AuditableEntity : BaseAuditableEntity
    {
        #region relations
        [ForeignKey("CreatedById")]
        public User Creator { get; set; }
        [ForeignKey("EditedById")]
        public User Editor { get; set; }
        [ForeignKey("TenantId")]
        public Tenant.Tenant Tenant { get; set; }
        #endregion
    }
}
