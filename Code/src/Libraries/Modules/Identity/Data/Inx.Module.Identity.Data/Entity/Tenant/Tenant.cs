﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Inx.Data.Base.Models;
using Inx.Data.Base.Utility;
using Inx.Utility.Utility;

namespace Inx.Module.Identity.Data.Entity.Tenant
{
    [Table("Tenants", Schema = Constraints.Schema)]
    public class Tenant: BaseEntity
    {
        [Required,StringLength(DbConstraints.NameLength)]
        public string Host { get; set; }
        [Required,StringLength(DbConstraints.NameLength)]
        public new string Name { get; set; }
        [NotMapped]
        public new int TenantId { get; set; }
        public int TenantTypeId { get; set; }
        [ForeignKey("TenantTypeId")]
        public TenantType TenantType { get; set; }
        public string OrganizationLogo { get; set; } = Constants.OrganizationNoImage;
    }
}
