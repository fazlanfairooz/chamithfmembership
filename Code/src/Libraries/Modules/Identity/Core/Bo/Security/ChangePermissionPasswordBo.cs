﻿using Inx.Utility.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Inx.Module.Identity.Core.Bo.Security
{
    public class ChangePermissionPasswordBo : BaseBo
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
    }
}
