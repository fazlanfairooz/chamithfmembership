﻿using System;
namespace Inx.Module.Identity.Core.Bo.Security
{
    public class RegisterResponse
    {
        public int UserId { get; set; }
        public Guid Token { get; set; }
    }
}
