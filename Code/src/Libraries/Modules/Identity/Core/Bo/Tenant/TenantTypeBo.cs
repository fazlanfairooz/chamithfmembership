﻿namespace Inx.Module.Identity.Core.Bo.Tenant
{
    public class TenantTypeBo
    {
        public int Id { get; set; }
        public string Name { get;set;}
    }
}
