﻿namespace Inx.Module.Identity.Core.Bo.Tenant
{
    public class TenantInfoBo
    {
        public string Email { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public int Country { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public int Language { get; set; }
        public int Currency { get; set; }
        public string Prefix { get; set; }
    }
}
