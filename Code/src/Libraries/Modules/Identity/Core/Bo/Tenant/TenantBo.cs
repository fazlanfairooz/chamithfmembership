﻿using System;
using Inx.Utility.Models;

namespace Inx.Module.Identity.Core.Bo.Tenant
{
    public class TenantBo: BaseBo
    {
        public int TenantTypeId { get; set; }
        public string Host { get; set; }
        [Obsolete]
        public new int TenantId { get; set; }
        public string OrganizationLogo { get; set; }
    }
}   
