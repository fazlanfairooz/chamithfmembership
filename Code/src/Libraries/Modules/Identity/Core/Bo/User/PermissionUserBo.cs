﻿using Inx.Utility.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Inx.Module.Identity.Core.Bo.User
{
    public class PermissionUserBo : BaseBo
    {
        public string Email { get; set; }
        public string RoleName { get; set; }
        public string UserStatus {get;set;}
        public Inx.Utility.Utility.Enums.Roles Role { get; set; }
        public Inx.Utility.Utility.Enums.UserRecordStatus RecordStatus { get; set; }
    }
}
