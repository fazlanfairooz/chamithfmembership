﻿using Inx.Utility.Models;
using Inx.Utility.Utility;

namespace Inx.Module.Identity.Core.Bo.User
{
    public class UserBo:BaseBo
    {
        public string Email { get; set; }
        public Inx.Utility.Utility.Enums.UserRecordStatus RecordStatus { get; set; }
        public string ProfileImage { get; set; } = Constants.ProfileNoImage;
        public new string Name { get; set; } = Constants.DefaultUserDisplayName;
    }
}
