﻿namespace Inx.Module.Identity.Core.Bo
{
    public class Enums : Inx.Utility.Utility.Enums
    {
        public enum CacheKey
        {
            TenantTypes = 1
        }
        public enum SecurityErrorTypes : byte
        {
            EmailSubmitedWithoutValidating = 1,
            EmailRegistedWithoutTenant = 2,
            AlreadyRegisted = 3,
            AlreadyRegstedButInactive = 4,
            InvaliedUsernameOrPassword=5,
            EmailNotFoundedInSystem = 6,
            InvaliedEmailOrToken =7
        }
    }
}
