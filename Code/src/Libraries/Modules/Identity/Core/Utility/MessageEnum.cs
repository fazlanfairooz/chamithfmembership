﻿using System.ComponentModel;

namespace Inx.Module.Identity.Core.Utility
{
    public enum EMessages : int
    {
        /*0-200==> security messages*/
        [Description("Invalied user name or password")]
        InvaliedUserNameOrPassword = 1,

        [Description("You already active on system")]
        YouAlreadyActiveInsystem = 2,

        [Description("Email or token")]
        EmailOrTokenNotFounded = 3,

        [Description("Email address or password validation token not founded")]
        PasswordValidationTokenNotFounded = 4,

        [Description("Current password is invalied")]
        InvaliedCurrentPassword = 5,

        SendEmailValidationMessageHeader = 6,

        SendEmailValidationMessageSubHeader = 7,

        ValidateEmailAddressEmailButtonMessage=8,

        ForgetPasswordTokenMessageHeader = 9,

        ForgetPasswordTokenMessageSubHeader = 10,

        ValidateForgotPasswordEmailButtonMessage = 11
    }
}
