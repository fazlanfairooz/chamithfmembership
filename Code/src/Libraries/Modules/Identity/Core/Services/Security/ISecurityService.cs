﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Inx.Module.Identity.Core.Bo.Security;
using Inx.Module.Identity.Core.Bo.Security.User;
using Inx.Module.Identity.Core.Bo.User;
using Inx.Utility.Models;
using UserBo = Inx.Module.Identity.Core.Bo.Security.User.UserBo;

namespace Inx.Module.Identity.Core.Services.Security
{
    public interface ISecurityService: IApplicationService
    {
        Task<AuthResponse> Authentication(AuthenticateBo request);
        Task ChangePassword(Request<ChangePasswordBo> request);

        #region register
        Task<RegisterResponse> Register(Request<RegisterBo> request);
        Task<Guid> ResendRegisterValidateEmail(Request<string> email);
        Task<int> ValidateEmailAddress(Request<Dictionary<string, string>> request);
        #endregion

        #region forget password
        Task<Guid> ForgetPasswordRequest(Request<string> email);
        Task ForgetPasswordChangePassword(Request<string> request);
        Task<AuthResponse> ForgetPasswordValidateToken(Request<Dictionary<string, string>> request);
        #endregion

        #region user
        Task<Bo.User.UserBo> ReadUser(Request<string> request);
        Task<List<PermissionUserBo>> ReadUsers(Request<string> request);
        Task<RegisterResponse> CreateUser(Request<UserBo> request);
        Task NewUserCreateRequestValidation(Request<Dictionary<string, string>> request);
        Task ChangeUserInitialPassword(Request<ChangePermissionPasswordBo> request);
        #endregion
    }
}
