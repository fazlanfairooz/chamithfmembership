﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Inx.Core.Base;
using Inx.Module.Identity.Core.Bo.Tenant;
using Inx.Module.Identity.Core.Services.Tenant.Interface;
using Inx.Utility.Extentions;
using Inx.Utility.Models;
using Inx.Module.Identity.Data.DbContext;
using Inx.Utility.Utility;
using Microsoft.EntityFrameworkCore;
using Inx.Module.Identity.Core.Bo.Security;
using Microsoft.EntityFrameworkCore.Storage;
using Inx.Utility.Exceptions;

namespace Inx.Module.Identity.Core.Services.Tenant
{
    public class TenantService : BaseService, ITenantService
    {
        private readonly IUnitOfWorkIdentity uow;
        public TenantService(IUnitOfWorkIdentity _uow)
        {
            uow = _uow;
        }
        public async Task<AuthResponse> Create(Request<TenantBo> req)
        {
            IDbContextTransaction transaction = null;
            try
            {
                using (transaction = uow.Context.Database.BeginTransaction())
                {
                    var @tenant = new Data.Entity.Tenant.Tenant
                    {
                        Name = req.Item.Name.TrimAndToLower(),
                        Host = req.Item.Host.TrimAndToLower(),
                        TenantTypeId = req.Item.TenantTypeId,
                        OrganizationLogo = req.Item.OrganizationLogo
                    };
                    await uow.Context.AddAsync(@tenant);
                    await uow.SaveAsync();
                    var user = await uow.UserRepository.Read(p => p.Id == req.UserId);
                    var userroles = await uow.UserRoleRepository.Read(p => p.UserId == req.UserId);
                    user.TenantId = @tenant.Id;
                    user.RecordStatus = Enums.UserRecordStatus.Active;
                    userroles.TenantId = @tenant.Id;
                    await uow.SaveAsync();
                    transaction.Commit();
                    return new AuthResponse
                    {
                        Name = Constants.DefaultUserDisplayName,
                        Id = user.Id,
                        Email = user.Email,
                        TenantId = user.TenantId,
                        ProfileImage = Constants.OrganizationNoImage,
                        UserState = Enums.UserRecordStatus.Active,
                        Roles = new List<string>() { Constants.SuperAdmin }
                    };
                }
            }
            catch (Exception ex)
            {
                transaction?.Rollback();
                throw ex.HandleException();
            }
        }
        public async Task<bool> CheckDomain(string domain)
        {
            try
            {
                domain = domain.TrimAndToLower();
                return await uow.TenantRepository.TableAsNoTracking.AnyAsync(p => p.Host == domain);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public async Task<TenantBo> Read(Request<int> req)
        {
            try
            {
                var res = (await uow.TenantRepository.Read(p => p.Id == req.TenantId)).MapObject<Data.Entity.Tenant.Tenant, TenantBo>();
                res.OrganizationLogo = ImagePath(Enums.FileType.OrginazationLogoDefault, res.OrganizationLogo);
                return res;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task Update(Request<TenantBo> req)
        {
            try
            {
                if (req.Item.IsNull())
                {
                    throw new NullObjectException();
                }
                var result = await uow.TenantRepository.Table.FirstOrDefaultAsync(p => p.Id == req.TenantId);
                if (result.IsNull())
                {
                    throw new RecordNotFoundException();
                }
                result.OrganizationLogo = req.Item.OrganizationLogo;
                await uow.SaveAsync();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }


        #region not implemented
        public Task<PageList<TenantBo>> Read(Search search)
        {
            throw new NotImplementedException();
        }
        public Task Delete(Request<int> req)
        {
            throw new NotImplementedException();
        }
        Task<TenantBo> IBaseService<TenantBo>.Create(Request<TenantBo> req)
        {
            throw new NotImplementedException();
        }
        public Task<List<KeyValueListItem<int>>> ReadKeyValue(Search req)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
