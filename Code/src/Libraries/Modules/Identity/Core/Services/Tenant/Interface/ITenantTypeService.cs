﻿using Inx.Core.Base;
using Inx.Module.Identity.Core.Bo.Tenant;

namespace Inx.Module.Identity.Core.Services.Tenant.Interface
{
    public interface ITenantTypeService:IBaseService<TenantTypeBo>
    {
    }
}
