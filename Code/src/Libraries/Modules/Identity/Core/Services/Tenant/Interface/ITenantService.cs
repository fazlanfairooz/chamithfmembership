﻿using System.Threading.Tasks;
using Inx.Core.Base;
using Inx.Module.Identity.Core.Bo.Security;
using Inx.Module.Identity.Core.Bo.Tenant;
using Inx.Utility.Models;

namespace Inx.Module.Identity.Core.Services.Tenant.Interface
{
    public interface ITenantService : IBaseService<TenantBo>
    {
        Task<bool> CheckDomain(string domain);
        new Task<AuthResponse> Create(Request<TenantBo> req);
    }
}
