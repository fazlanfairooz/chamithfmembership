﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Inx.Core.Base;
using Inx.Module.Identity.Core.Bo.Tenant;
using Inx.Module.Identity.Core.Services.Tenant.Interface;
using Inx.Module.Identity.Data.DbContext;
using Inx.Module.Identity.Data.Entity.Tenant;
using Inx.Utility.Extentions;
using Inx.Utility.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Inx.Module.Identity.Core.Services.Tenant
{
    public class TenantInfoService : BaseService, ITenantInfoService
    {
        private readonly IUnitOfWorkIdentity uow;
        public TenantInfoService(IUnitOfWorkIdentity _uow, ICoreInjector inject) : base()
        {
            uow = _uow;
        }

        public async Task<TenantInfoBo> Create(Request<TenantInfoBo> req)
        {
            try
            {
                return (await uow.TenantInfoRepository.CreateAndSave(req.MapRequestObject<TenantInfoBo, TenantInfo>())).MapObject<TenantInfo, TenantInfoBo>();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task<TenantInfo> GetTenantInfo(Request<int> req)
        {
            try
            {
                return await (from tenantinfo in uow.TenantInfoRepository.TableAsNoTracking
                                  where tenantinfo.TenantId == req.TenantId
                                  select tenantinfo).FirstOrDefaultAsync();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task Update(Request<TenantInfoBo> req)
        {
            try
            {
                var tenantInfo = (from tenantinfo in uow.TenantInfoRepository.Table
                                  where tenantinfo.TenantId == req.TenantId
                                  select tenantinfo).FirstOrDefault();

                tenantInfo.Email = req.Item.Email;
                tenantInfo.Address = req.Item.Address;
                tenantInfo.City = req.Item.City;
                tenantInfo.State = req.Item.State;
                tenantInfo.Zip = req.Item.Zip;
                tenantInfo.Country = req.Item.Country;
                tenantInfo.Phone = req.Item.Phone;
                tenantInfo.Fax = req.Item.Fax;
                tenantInfo.Language = req.Item.Language;
                tenantInfo.Currency = req.Item.Currency;
                tenantInfo.Prefix = req.Item.Prefix;
                await uow.SaveAsync();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        #region not implemented
        public Task Delete(Request<int> req)
        {
            throw new NotImplementedException();
        }

        public Task<PageList<TenantInfoBo>> Read(Search search)
        {
            throw new NotImplementedException();
        }

        public Task<List<KeyValueListItem<int>>> ReadKeyValue(Search req)
        {
            throw new NotImplementedException();
        }

        public Task<TenantInfoBo> Read(Request<int> req)
        {
            throw new NotImplementedException();
        }

        public Task Update(TenantInfoBo item)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
