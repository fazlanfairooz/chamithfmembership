﻿using Inx.Core.Base;
using Inx.Module.Identity.Core.Bo.Tenant;
using Inx.Module.Identity.Data.Entity.Tenant;
using Inx.Utility.Models;
using System.Threading.Tasks;

namespace Inx.Module.Identity.Core.Services.Tenant.Interface
{
    public interface ITenantInfoService : IBaseService<TenantInfoBo>
    {
        Task<TenantInfo> GetTenantInfo(Request<int> item);
        Task Update(TenantInfoBo item);
    }
}
