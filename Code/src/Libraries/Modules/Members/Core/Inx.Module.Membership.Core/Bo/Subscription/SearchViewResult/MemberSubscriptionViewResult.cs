﻿namespace Inx.Module.Membership.Core.Bo.Subscription.SearchViewResult
{
    public class MemberSubscriptionViewResult
    {
        public int MemberId { get; set; }
        public Enums.MembershipStatus MembershipStatus { get; set; }
        public int TotalRecordCount { get; set; }
        public int ActiveCount { get; set; }
        public int InActiveCount { get; set; }
        public int PendingCount { get; set; }
        public int TerminatedCount { get; set; }
    }
}
