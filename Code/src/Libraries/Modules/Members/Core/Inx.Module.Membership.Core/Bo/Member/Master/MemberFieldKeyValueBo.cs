﻿using System;
using Inx.Utility.Models;

namespace Inx.Module.Membership.Core.Bo.Member.Master
{
    public class MemberFieldKeyValueBo:BaseBo
    {
        public Guid Key { get; set; }
        public string Value { get; set; }
    }
}
