﻿using Inx.Utility.Models;

namespace Inx.Module.Membership.Core.Bo.MemberGroup
{
    public class GroupBo : BaseBo
    {
        public string Description { get; set; }
        public int MemberCount { get; set; }
        public int GroupId { get; set; }
    }
}
