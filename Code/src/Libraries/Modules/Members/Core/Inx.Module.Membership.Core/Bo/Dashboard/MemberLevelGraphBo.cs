﻿using System.Collections.Generic;

namespace Inx.Module.Membership.Core.Bo.Dashboard
{
    public class MemberLevelGraphBo
    {
        public List<string> MemberLevels { get; set; }
        public List<int> ActiveMembers { get; set; }
        public List<int> InActiveMembers { get; set; }
        public MemberLevelGraphBo()
        {
            MemberLevels = new List<string>();
            ActiveMembers = new List<int>();
            InActiveMembers = new List<int>();
        }
    }
}
