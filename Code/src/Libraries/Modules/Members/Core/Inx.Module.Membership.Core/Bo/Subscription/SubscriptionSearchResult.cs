﻿using System;
namespace Inx.Module.Membership.Core.Bo.Subscription
{
    public class SubscriptionSearchResult: SubscriptionBo
    {
        public DateTime CreatedOn { get; set; }
        public DateTime? EditedOn { get; set; }
    }
}
