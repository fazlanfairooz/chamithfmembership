﻿using Inx.Utility.Models;

namespace Inx.Module.Membership.Core.Bo.Subscription
{
    public class SubscriptionRenewalBo : BaseBo
    {
        public bool IsRenewable { get; set; }
        public bool IsSendEmail { get; set; }
        public int RenewalDays { get; set; }
        public int EmaillTemplateId { get; set; }
    }
}
