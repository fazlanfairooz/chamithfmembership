﻿using System;
using Inx.Utility.Models;

namespace Inx.Module.Membership.Core.Bo.Settings.CustomFieldDefinition
{
    public class MemberFieldRequiredBo:BaseBo
    {
        public Guid Key { get; set; }
        public Enums.MemberType MemberType { get; set; }
    }
}
