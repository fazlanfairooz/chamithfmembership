﻿using System.Collections.Generic;
using Inx.Utility.Utility;

namespace Inx.Module.Membership.Core.Bo.Member.Master.ViewResult
{
    public class MemberInfoViewResult
    {
        private int item;
        private List<MemberFieldKeyValueBo> keyValue;

        public int MemberId { get; set; }
        public string ProfileImage { get; set; }
        public Enums.MemberType MemberType { get; set; }
        public List<MemberFieldKeyValueBo> MemberFieldKeyValue { get; set; }
        public int? SubscriptionId { get; set; }
        public MemberInfoViewResult(int memberId, string profileImage,  Enums.MemberType memberType, List<MemberFieldKeyValueBo> memberFieldKeyValue, int? subscriptionId)
        {
            MemberId = memberId;
            ProfileImage = profileImage;
            MemberFieldKeyValue = memberFieldKeyValue;
            MemberType = memberType;
            SubscriptionId = subscriptionId;
        }

    }
}
