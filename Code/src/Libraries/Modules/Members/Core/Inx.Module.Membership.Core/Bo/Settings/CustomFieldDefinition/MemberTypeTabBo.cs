﻿using Inx.Utility.Models;

namespace Inx.Module.Membership.Core.Bo.Settings.CustomFieldDefinition
{
    public class MemberTypeTabBo: BaseBo
    {
        public int TabId { get; set; }
        public Enums.MemberType MemberType { get; set; }

        public MemberTypeTabBo()
        {
        }

        public MemberTypeTabBo(int tabId, Enums.MemberType memberType)
        {
            TabId = tabId;
            MemberType = memberType;
        }
    }
}
