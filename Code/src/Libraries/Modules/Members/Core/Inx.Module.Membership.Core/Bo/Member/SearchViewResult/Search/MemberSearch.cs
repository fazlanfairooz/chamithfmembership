﻿namespace Inx.Module.Membership.Core.Bo.Member.SearchViewResult.Search
{
    public class MemberSearch
    { 
        public string Search { get; set; } = string.Empty;
        public Enums.MembershipStatus Status { get; set; } = Enums.MembershipStatus.All;
        public bool IsDefault { get; set; } = false;
    }
}
