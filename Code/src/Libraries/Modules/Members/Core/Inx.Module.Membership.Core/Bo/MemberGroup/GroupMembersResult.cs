﻿namespace Inx.Module.Membership.Core.Bo.MemberGroup
{
    public class GroupMembersResult
    {
         public int Id { get; set; }
         public int GroupId { get; set; }
         public int MemberId { get; set; }
         public string Name { get; set; }
         public string Level { get; set; }
         public string Email { get; set; }
         public string Phone { get; set; }
    }
}
