﻿namespace Inx.Module.Membership.Core.Bo.Subscription
{
    public class TerminationNoteBo
    {
        public int MemberId { get; set; }
        public string Notes { get; set; }
    }
}
