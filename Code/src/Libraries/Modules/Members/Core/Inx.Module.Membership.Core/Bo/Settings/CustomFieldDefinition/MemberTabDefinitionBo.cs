﻿using Inx.Utility.Models;
using System;

namespace Inx.Module.Membership.Core.Bo.Settings.CustomFieldDefinition
{
    public class MemberTabDefinitionBo : BaseBo
    {
        public Guid Key { get; set; }
        public string DisplayName { get; set; }
        public bool IsVisible { get; set; }
        public int Order { get; set; }
        public string TabDescription { get; set; }
        public bool IsSystemTab { get; set; }
    }
}
