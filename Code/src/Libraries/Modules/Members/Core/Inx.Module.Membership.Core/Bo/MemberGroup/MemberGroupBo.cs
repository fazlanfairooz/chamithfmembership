﻿using System.Collections.Generic;
using Inx.Utility.Models;
namespace Inx.Module.Membership.Core.Bo.MemberGroup
{
    public class MemberGroupBo:BaseBo
    {
        public List<int> MemberIds { get; set; }
        public int GroupId { get; set; }
        public new string Name { get; set; }
    }
}
