﻿using System;
using System.Collections.Generic;

namespace Inx.Module.Membership.Core.Bo.Dashboard
{
    public class DashboardCountsBo
    {
        public List<DateTime> MemberSubscriptionDate { get; set; }
        public List<decimal> PaymentDue { get; set; }
        public List<decimal> NetRevenue { get; set; }
        public DateTime FromDate { get; set; }
        public DashboardCountsBo()
        {
            MemberSubscriptionDate = new List<DateTime>();
            PaymentDue = new List<decimal>();
            NetRevenue = new List<decimal>();
            FromDate = new DateTime();

        }
    }
}
