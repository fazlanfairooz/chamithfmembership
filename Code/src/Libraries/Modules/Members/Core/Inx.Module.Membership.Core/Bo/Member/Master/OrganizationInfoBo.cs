﻿using Inx.Utility.Models;
namespace Inx.Module.Membership.Core.Bo.Member.Master
{
    public class OrganizationInfoBo :BaseBo
    {
        public string OrganizationName { get; set; }
    }
}
