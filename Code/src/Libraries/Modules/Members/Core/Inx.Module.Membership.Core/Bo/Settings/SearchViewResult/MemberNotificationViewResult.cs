﻿namespace Inx.Module.Membership.Core.Bo.Settings.SearchViewResult
{
    public class MemberNotificationViewResult
    {
        public string Url { get; set; } = "#";
        public string Message { get; set; }
        public int GroupId { get; set; }
        public bool Read { get; set; }
    }
}
