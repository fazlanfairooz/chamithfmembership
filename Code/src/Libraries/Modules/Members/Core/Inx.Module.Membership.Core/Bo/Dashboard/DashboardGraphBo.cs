﻿namespace Inx.Module.Membership.Core.Bo.Dashboard
{
    public class DashboardGraphBo 
    {
        public MemberActiveGraphBo MemberActiveGraph { get; set; }
        public MemberLevelGraphBo MemberLevelGraph { get; set; }
        public DashboardCountsBo DashboardCounts { get; set; }
        
    }
}
