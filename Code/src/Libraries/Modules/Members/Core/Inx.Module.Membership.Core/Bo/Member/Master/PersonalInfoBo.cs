﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Inx.Data.Base.Utility;
using Inx.Utility.Models;

namespace Inx.Module.Membership.Core.Bo.Member.Master
{
    public class PersonalInfoBo : BaseBo
    {
        [Key]
        public new int Id { get; set; }
        [ForeignKey("Id")]
        public Data.Entity.Members.Master.Member Member { get; set; }
        [StringLength(DbConstraints.NameLength),Required]
        public string FirstName { get; set; }
        [StringLength(DbConstraints.NameLength)]
        public string LastName { get; set; }

        #region not map
        [NotMapped, Obsolete]
        public new string Name { get; set; }
        #endregion

    }
}
