﻿using Inx.Core.Base;
using Inx.Module.Membership.Core.Bo.Settings.CustomFieldDefinition;
using Inx.Utility.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace Inx.Module.Membership.Core.Service.Settings.CustomFieldDefinition.Interface
{
    public interface IMemberFieldDefinitionService : IBaseService<MemberFieldDefinitionBo>
    {
        Task<List<MemberFieldDefinitionBo>> ReadDefault();
        Task<int> Create(Request<MemberFieldDefinitionBo> request);
    }
}
