﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Inx.Core.Base;
using Inx.Module.Membership.Core.Bo.MemberGroup;
using Inx.Utility.Models;

namespace Inx.Module.Membership.Core.Service.MemberGroup.Interface
{
    public interface IGroupService : IBaseService<GroupBo>
    {
        Task<List<GroupBo>> ReadMemberGroupCount(Search search);
    }
}
