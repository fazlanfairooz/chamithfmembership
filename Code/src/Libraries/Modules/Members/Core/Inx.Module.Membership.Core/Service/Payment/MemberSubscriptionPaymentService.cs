﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using Inx.Core.Base;
using Inx.Module.Membership.Core.Service.MemberSubscription.Interface;
using Inx.Module.Membership.Core.Service.Payment.Interface;
using Inx.Module.Membership.Data.DbContext;
using Inx.Module.Payment.Core.Bo;
using Inx.Module.Payment.Data.DbContext;
using Inx.Module.Payment.Data.Entity;
using Inx.Utility.Extentions;
using Inx.Utility.Models;
using Inx.Utility.Utility;
using Microsoft.EntityFrameworkCore;

namespace Inx.Module.Membership.Core.Service.Payment
{
    public class MemberSubscriptionPaymentService: MemberBaseService, IMemberSubscriptionPaymentService
    {
        private readonly IUnitOfWokMembership uowMembership;
        private readonly IUnitOfWorkPayment uowPayment;
        public MemberSubscriptionPaymentService(IUnitOfWokMembership _uowMembership, IUnitOfWorkPayment _uowPayment,ICoreInjector _coreInjector):base(_coreInjector)
        {
            uowMembership = _uowMembership;
            uowPayment = _uowPayment;
        }

        #region payment
        public async Task<PageList<PaymentViewResult>> GetPayments(Search search)
        {

            //var searchTerm = search.SearchTerm.ToJsonObject<PaymentSearchBo>();

            //var payments = await uowPayment.PaymentMasterRepository.TableAsNoTracking.Where(p =>
            //    p.TenantId == search.TenantId && p.CreatedOn.Date >= searchTerm.FromDate.Date
            //                                  && p.CreatedOn.Date <= searchTerm.ToDate.Date)
            //    .Select(a => new
            //{
            //    a.Amount,
            //    a.Id,
            //    a.MemberSubscriptionId,
            //    a.Remaining,
            //    a.PaymentState,
            //    a.TransActionType,
            //    a.CreatedOn,
            //    a.EditedOn,
            //    a.Name
            //}).ToListAsync();

            //var memberPayments = (from memberSubscription in uowMembership.MemberSubscriptionRepository.TableAsNoTracking
            //                      join subscription in uowMembership.SubscriptionRepository.TableAsNoTracking on memberSubscription.SubscriptinId equals subscription.Id
            //                      join member in uowMembership.MemberRepository.TableAsNoTracking on memberSubscription.MemberId equals member.Id
            //                      join contactInfo in uowMembership.ContactInfoRepository.TableAsNoTracking on memberSubscription.MemberId equals contactInfo.MemberId
            //                      join p in payments on memberSubscription.Id equals p.MemberSubscriptionId
            //                      orderby member.Id descending
            //                      select new PaymentViewResult
            //                      {
            //                          MemberSubscriptionId = memberSubscription.Id,
            //                          MemberId = member.Id,
            //                          Amount = p.Amount,
            //                          Remaining = p.Remaining,
            //                          Name = p.Name,
            //                          ProfileImage = ImagePath(Enums.FileType.ProfileImageThumbnail, member.ProfileImage),
            //                          Email = contactInfo.Email,
            //                          MemberName = member.Name,
            //                          PaymentMasterId = p.Id,
            //                          TransActionType = p.TransActionType.GetDescription(),
            //                          PaymentState = p.PaymentState.ToString(),
            //                          PayedDate = (p.PaymentState == Enums.PaymentState.Paid && p.EditedOn == null) ? p.CreatedOn : p.EditedOn,
            //                          CreatedDate = p.CreatedOn

            //                      }).AsQueryable();

            //if (!string.IsNullOrEmpty(searchTerm.Search))
            //    memberPayments = memberPayments.Where(a => a.MemberName.ToLower().Contains(searchTerm.Search)
            // || a.TransActionType.ToLower().Contains(searchTerm.Search)
            // || a.Amount.ToString().ToLower().Contains(searchTerm.Search) || a.PaymentState.ToLower().Contains(searchTerm.Search)).AsQueryable();

            //var filterMemberPayments = await memberPayments.Skip(search.Skip).Take(search.Take).ToListAsync();

            //var pageresult = new PageList<PaymentViewResult>
            //{
            //    Skip = search.Skip,
            //    Take = search.Take,
            //    Items = filterMemberPayments,
            //    TotalRecodeCount = payments.Count()
            //};
            //return pageresult;
            return null;

        }

        public async Task<PaymentBo> GetPayment(Request<int> req)
        {

            var payment = (await uowPayment.PaymentMasterRepository.TableAsNoTracking.FirstOrDefaultAsync(a => a.MemberId == req.Item)).MapObject<PaymentMaster, PaymentBo>();

            return payment;
        }

        #endregion
    }
}
