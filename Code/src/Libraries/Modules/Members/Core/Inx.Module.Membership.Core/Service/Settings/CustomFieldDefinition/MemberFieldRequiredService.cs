﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inx.Core.Base;
using Inx.Module.Membership.Core.Bo.Settings.CustomFieldDefinition;
using Inx.Module.Membership.Core.Service.Settings.CustomFieldDefinition.Interface;
using Inx.Module.Membership.Data.DbContext;
using Inx.Module.Membership.Data.Entity.Settings.CustomFieldDefinition;
using Inx.Utility.Extentions;
using Microsoft.EntityFrameworkCore;

namespace Inx.Module.Membership.Core.Service.Settings.CustomFieldDefinition
{
    public class MemberFieldRequiredService : MemberBaseService, IMemberFieldRequiredService
    {
        private readonly IUnitOfWokMembership unitOfWokMembership;
        public MemberFieldRequiredService(IUnitOfWokMembership _unitOfWokMembership,ICoreInjector icoreInjector):base(icoreInjector)
        {
            unitOfWokMembership = _unitOfWokMembership;
        }
        public async Task<List<MemberFieldRequiredBo>> Read()
        {
            try
            {
                return (await unitOfWokMembership.MemberFieldRequiredRepository.TableAsNoTracking.ToListAsync())
                    .MapListObject<MemberFieldRequired, MemberFieldRequiredBo>().ToList();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
    }
}
