﻿using System;
using System.Threading.Tasks;
using Inx.Module.Identity.Data.Entity.Application;
using Inx.Module.Membership.Core.Service.Members.interfaces;
using Inx.Module.Membership.Data.DbContext;
using Inx.Module.Membership.Data.Entity.Members.Master;
using Inx.Utility.Models;

namespace Inx.Module.Membership.Core.Service.Members.Master
{
    public class MemberPersonService:MemberBaseService, IMemberInfoTypeService
    {
        private readonly IUnitOfWokMembership unitOfWokMembership;
        public MemberPersonService(IUnitOfWokMembership _unitOfWokMembership)
        {
            unitOfWokMembership = _unitOfWokMembership;
        }

        public async Task Create<T>(Request<T> request) where T : AuditableEntity
        {
            var reqItem = request.Item as PersonalInfo;
            await unitOfWokMembership.PersonalInfoRepository.CreateAndSave(Request(reqItem, request.TenantId,
                request.UserId));
        }
    }
}
