﻿using System.Threading.Tasks;
using Inx.Module.Identity.Data.Entity.Application;
using Inx.Module.Membership.Core.Service.Members.interfaces;
using Inx.Module.Membership.Data.DbContext;
using Inx.Module.Membership.Data.Entity.Members.Master;
using Inx.Utility.Models;

namespace Inx.Module.Membership.Core.Service.Members.Master
{
    public class MemberOrganizationService: MemberBaseService, IMemberInfoTypeService
    {
        private readonly IUnitOfWokMembership unitOfWokMembership;
        public MemberOrganizationService(IUnitOfWokMembership _unitOfWokMembership)
        {
            unitOfWokMembership = _unitOfWokMembership;
        }

        public async Task Create<T>(Request<T> request) where T : AuditableEntity
        {
            var reqItem = request.Item as OrganizationInfo;
            await unitOfWokMembership.OrganizationInfoRepository.CreateAndSave(Request(reqItem, request.TenantId,request.UserId));
        }
    }
}
