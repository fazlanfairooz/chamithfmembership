﻿using Inx.Module.Membership.Core.Bo.MemberEmail;
using Inx.Utility.Models;
using System.Threading.Tasks;

namespace Inx.Module.Membership.Core.Service.MemberEmails.Interface
{
    public interface IMemberEmailSendService
    {
        Task SendEmails(Request<EmailSendingInfoModel> emailInfoModel);
    }
}
