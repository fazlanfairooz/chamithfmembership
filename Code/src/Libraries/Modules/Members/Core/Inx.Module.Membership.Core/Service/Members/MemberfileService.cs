﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Inx.Core.Base;
using Inx.Module.Membership.Core.Bo.Member;
using Inx.Module.Membership.Core.Service.Members.interfaces;
using Inx.Module.Membership.Data.DbContext;
using Inx.Module.Membership.Data.Entity.Members;
using Inx.Utility.Exceptions;
using Inx.Utility.Extentions;
using Inx.Utility.Models;
using Inx.Utility.Utility;
using Microsoft.EntityFrameworkCore;

namespace Inx.Module.Membership.Core.Service.Members
{
    public class MemberFileService : MemberBaseService, IMemberFileService
    {
        private readonly IUnitOfWokMembership uow;
        public MemberFileService(IUnitOfWokMembership _uow, ICoreInjector inject) : base(inject)
        {
            uow = (IUnitOfWokMembership)_uow;
        }
        public async Task<MemberFileBo> Create(Request<MemberFileBo> req)
        {
            try
            {
                var result = (await uow.MemberFileRepository.Create(req.MapRequestObject<MemberFileBo, MemberFile>()));
                await uow.SaveAsync();
                return result.MapObject<MemberFile, MemberFileBo>();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public async Task<string> Delete(Request<int> req)
        {
            try
            {
                var result = await uow.MemberFileRepository.Read(req);
                await uow.MemberFileRepository.DeletePermanentAndSave(req);
                return result.FileSaveName;
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public async Task<PageList<MemberFileBo>> Read(Search search)
        {
            try
            {
                var memberid = 0;
                if (!int.TryParse(search.SearchTerm,out memberid))
                {
                    throw new RecordNotFoundException("member not found");
                }

                var result = (await uow.MemberFileRepository.TableAsNoTracking.Where(p =>
                    p.TenantId == search.TenantId && p.MemberId == memberid).ToListAsync());

                var pageitem = new List<MemberFileBo>();
               
                foreach (var item in result)
                {
                    pageitem.Add(new MemberFileBo
                    {
                        CreatedOn = item.CreatedOn,
                        DownloadPath= ImagePath(Enums.FileType.MembersFiles, item.FileSaveName),
                        FileName = item.FileName,
                        FileSaveName = item.FileSaveName,
                        Size = item.Size,
                        Id = item.Id
                    });
                }
                return new PageList<MemberFileBo>
                {
                    Take = search.Take,
                    Skip = search.Skip,
                    Items = pageitem
                };
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public async Task<MemberFileBo> Read(Request<int> req)
        {
            try
            {
                return (await uow.MemberFileRepository.Read(req))
                   .MapObject<MemberFile, MemberFileBo>();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public async Task<List<KeyValueListItem<int>>> ReadKeyValue(Search req)
        {
            try
            {
                return await uow.MemberFileRepository.ReadKeyValue<MemberFile>(req);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public async Task Update(Request<MemberFileBo> req)
        {
            try
            {
                await uow.MemberFileRepository.UpdateAndSave(req.MapRequestObject<MemberFileBo, MemberFile>(req.Item.Id));
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        #region not implmented
        Task IBaseService<MemberFileBo>.Delete(Request<int> req)
        {
            throw new NotImplementedException();
        }
        #endregion

    }

}

