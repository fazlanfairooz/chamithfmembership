﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Inx.Core.Base;
using Inx.Module.Membership.Core.Bo.Dashboard;
using Inx.Module.Membership.Core.Bo.Settings;
using Inx.Module.Membership.Core.Service.Dashboard.Interface;
using Inx.Module.Membership.Data.DbContext;
using Inx.Module.Payment.Data.DbContext;
using Inx.Utility.Extentions;
using Inx.Utility.Models;
using Inx.Utility.Utility;
using Microsoft.EntityFrameworkCore;

namespace Inx.Module.Membership.Core.Service.Dashboard
{
    public class DashboardService : MemberBaseService, IDashboardService
    {
        private readonly IUnitOfWokMembership uowMembership;
        private readonly IUnitOfWorkPayment unitOfWorkPayment;
        public DashboardService(IUnitOfWokMembership _uowMembership, IUnitOfWorkPayment _uowPayment, ICoreInjector _icoreCoreInjector):base(_icoreCoreInjector)
        {
            uowMembership = _uowMembership;
            unitOfWorkPayment = _uowPayment;
        }

        #region dashboardgraphs
        public async Task<DashboardGraphBo> DashboardGraph(Request<Bo.Enums.DateRangeFilter> request)
        {
            try
            {
                var fromDate = DateTime.UtcNow.Date;
                var toDate = DateTime.UtcNow.Date;
                decimal TotalNetRevenue = 0;
                decimal PaymentDue = 0;
                switch (request.Item)
                {
                    case Bo.Enums.DateRangeFilter.ThreeMonths:
                        fromDate = toDate.AddMonths(-3);
                        break;
                    case Bo.Enums.DateRangeFilter.OneYear:
                        fromDate = toDate.AddYears(-1);
                        break;
                    case Bo.Enums.DateRangeFilter.SixMonths:
                        fromDate = toDate.AddMonths(-6);
                        break;
                    case Bo.Enums.DateRangeFilter.Week:
                        fromDate = toDate.AddDays(-7);
                        break;
                }

                var paymentContext = (from payment in unitOfWorkPayment.PaymentMasterRepository.TableAsNoTracking
                                      select payment).ToList();

                var memberAllData = (from membersubscription in uowMembership.MemberSubscriptionRepository.TableAsNoTracking
                                     join member in uowMembership.MemberRepository.TableAsNoTracking on membersubscription.MemberId equals member.Id
                                     join subscriptiontype in uowMembership.SubscriptionTypeRepository.TableAsNoTracking on membersubscription.SubscriptinId equals subscriptiontype.Id
                                     join subscription in uowMembership.SubscriptionRepository.TableAsNoTracking on subscriptiontype.Id equals subscription.Id
                                     join payment in paymentContext on membersubscription.Id equals payment.MemberSubscriptionId
                                     
                                     where (member.TenantId == request.TenantId && membersubscription.CreatedOn.Date >= fromDate.Date && membersubscription.CreatedOn.Date <= toDate.Date)
                                     select new
                                     {
                                         MemberStatus = membersubscription.MembershipStatus,
                                         MemberLevel = subscriptiontype.Name,
                                         MemberSubscriptionDate = membersubscription.CreatedOn,
                                         PaymentStatus = payment.PaymentState,
                                         Amount = payment.Amount
                                     }).AsQueryable();

                await memberAllData.ForEachAsync(ele =>
                {
                    if (ele.PaymentStatus == Enums.PaymentState.Paid)
                        TotalNetRevenue += ele.Amount;

                    //get payement_due data
                    if (ele.PaymentStatus == Enums.PaymentState.PartialyPaid || ele.PaymentStatus == Enums.PaymentState.Pending)
                    {
                        PaymentDue += ele.Amount;
                    }
                });
                //FromDate
                var FromDate = fromDate.Date;
                //memberlevel group by memberlevel and memberstatus
                var memberLevelData = memberAllData.GroupBy(a => new { a.MemberLevel, a.MemberStatus });

                var FilterMemberLevel = memberAllData.Where(a => a.MemberStatus != Enums.MembershipStatus.Pending && a.MemberStatus != Enums.MembershipStatus.Terminated);

                var memberLevelDashBoardData = new MemberLevelGraphBo();
                //remove duplicate memberlevels
                memberLevelDashBoardData.MemberLevels = FilterMemberLevel.Select(a => a.MemberLevel).Distinct().ToList();


                foreach (var item in memberLevelDashBoardData.MemberLevels)
                {
                    var activeMembers = memberAllData.Where(a => a.MemberLevel == item && (int)a.MemberStatus == (int)Enums.MembershipStatus.Active).Count();
                    memberLevelDashBoardData.ActiveMembers.Add(activeMembers);

                    var inActiveMembers = memberAllData.Where(a => a.MemberLevel == item && (int)a.MemberStatus == (int)Enums.MembershipStatus.Inactive).Count();
                    memberLevelDashBoardData.InActiveMembers.Add(inActiveMembers);
                }

                //memberlevel group by memberlevel,memberstatus and createon date
                //in here dates can be duplicated becoz of the status
                var memberActivationData = memberAllData.GroupBy(a => new { a.MemberSubscriptionDate, a.MemberStatus }).ToList();

                var memberActivationDashboardData = new MemberActiveGraphBo();

                //remove duplicate dates
                memberActivationDashboardData.MemberSubscriptionDate = memberActivationData.Select(a => a.Key.MemberSubscriptionDate).Distinct().ToList();

                //get according to the select date active members
                foreach (var date in memberActivationDashboardData.MemberSubscriptionDate)
                {
                    var _activeMembers = memberAllData.Where(a => a.MemberSubscriptionDate == date && (int)a.MemberStatus == (int)Enums.MembershipStatus.Active).Count();
                    memberActivationDashboardData.ActiveMembers.Add(_activeMembers);
                }
                var PaymentDueDashboardData = new DashboardCountsBo();

                PaymentDueDashboardData.PaymentDue.Add(PaymentDue);
                PaymentDueDashboardData.NetRevenue.Add(TotalNetRevenue);
                PaymentDueDashboardData.FromDate = FromDate.Date;

                var dashBoardGraphBo = new DashboardGraphBo
                {
                    MemberLevelGraph = memberLevelDashBoardData,
                    MemberActiveGraph = memberActivationDashboardData,
                    DashboardCounts = PaymentDueDashboardData
                };

                return dashBoardGraphBo;
            }
            catch (Exception e)
            {

                throw e.HandleException();
            }

        }
        #endregion 
    }
}
