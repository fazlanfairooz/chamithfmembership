﻿using Inx.Core.Base;
using Inx.Module.Membership.Core.Bo.Settings.CustomFieldDefinition;
using Inx.Module.Membership.Core.Service.Settings.CustomFieldDefinition.Interface;
using Inx.Module.Membership.Data.DbContext;
using Inx.Module.Membership.Data.Entity.Settings.CustomFieldDefinition;
using Inx.Utility.Extentions;
using Inx.Utility.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inx.Module.Membership.Core.Service.Settings.CustomFieldDefinition
{
    public class MemberFieldDefinitionService : MemberBaseService, IMemberFieldDefinitionService
    {
        private readonly IUnitOfWokMembership uowMembership;
        public MemberFieldDefinitionService(IUnitOfWokMembership _uowMembership, ICoreInjector coreInjector) : base(coreInjector)
        {
            uowMembership = _uowMembership;
        }

        public async Task<int> Create(Request<MemberFieldDefinitionBo> request)
        {
            try
            {
                request.Item.Key = Guid.NewGuid();
                var fields = (await uowMembership.MemberFieldDefinitionRepository.TableAsNoTracking
                                   .Where(t => t.CreatedById == request.UserId)
                                   .ToListAsync())
                                   .OrderByDescending(f => f.Order).First();

                request.Item.Order = fields.Order + 1;

                var field = (await uowMembership.MemberFieldDefinitionRepository.CreateAndSave(request.MapRequestObject<MemberFieldDefinitionBo, MemberFieldDefinition>()))
                    .MapObject<MemberFieldDefinition, MemberFieldDefinitionBo>();
                return field.Id;
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task Delete(Request<int> request)
        {
            using (var transaction = await uowMembership.Context.Database.BeginTransactionAsync())
            {
                try
                {
                    var memberTab = await uowMembership.MemberTabFieldsRepository.Read(d => d.FieldId == request.Item);
                    await uowMembership.MemberTabFieldsRepository.DeletePermanentAndSave(Request(memberTab.Id, request.TenantId, request.UserId));
                    await uowMembership.MemberFieldDefinitionRepository.DeletePermanentAndSave(request);
                    transaction.Commit();
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    throw e.HandleException();
                }
            }
        }

        public async Task<PageList<MemberFieldDefinitionBo>> Read(Search search)
        {
            try
            {
                var result = (await uowMembership.MemberFieldDefinitionRepository.TableAsNoTracking.Where(p =>
                        p.TenantId == search.TenantId).ToListAsync())
                    .MapListObject<MemberFieldDefinition, MemberFieldDefinitionBo>();
                return new PageList<MemberFieldDefinitionBo>(result, 0, 0, result.Count());
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task<MemberFieldDefinitionBo> Read(Request<int> request)
        {
            try
            {
                return (await uowMembership.MemberFieldDefinitionRepository.Read(request))
                  .MapObject<MemberFieldDefinition, MemberFieldDefinitionBo>();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task<List<MemberFieldDefinitionBo>> ReadDefault()
        {
            try
            {
                return (await uowMembership.MemberFieldDefinitionRepository.TableAsNoTracking
                        .Where(p => p.TenantId == 0).ToListAsync())
                    .MapListObject<MemberFieldDefinition, MemberFieldDefinitionBo>().ToList();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task<List<KeyValueListItem<int>>> ReadKeyValue(Search request)
        {
            try
            {
                return await uowMembership.MemberFieldDefinitionRepository.ReadKeyValue<MemberFieldDefinition>(request);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task Update(Request<MemberFieldDefinitionBo> request)
        {
            try
            {
                var field = request.MapRequestObject<MemberFieldDefinitionBo, MemberFieldDefinition>();
                field.Id = request.Item.Id;
                await uowMembership.MemberFieldDefinitionRepository.UpdateAndSave(field);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        Task<MemberFieldDefinitionBo> IBaseService<MemberFieldDefinitionBo>.Create(Request<MemberFieldDefinitionBo> request)
        {
            throw new NotImplementedException();
        }
    }
}
