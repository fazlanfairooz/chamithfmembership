﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Inx.Core.Base;
using Inx.Module.Membership.Core.Bo.MemberGroup;
using Inx.Module.Membership.Core.Service.MemberGroup.Interface;
using Inx.Module.Membership.Data.DbContext;
using Inx.Utility.Extentions;
using Inx.Utility.Models;
using Microsoft.EntityFrameworkCore;
using Enums = Inx.Utility.Utility.Enums;
using mg = Inx.Module.Membership.Data.Entity.MemberGroup;
namespace Inx.Module.Membership.Core.Service.MemberGroup
{
    public class MemberGroupService : MemberBaseService, IMemberGroupService
    {
        private readonly IUnitOfWokMembership uow;
        public MemberGroupService(IUnitOfWokMembership _uow, ICoreInjector inject) : base(inject)
        {
            uow = _uow;
        }

        public async Task<MemberGroupBo> Create(Request<MemberGroupBo> req)
        {
            try
            {
                var memberidList = req.Item.MemberIds;
                foreach (var item in memberidList)
                {
                    req.Item.MapObject<MemberGroupBo, mg.MemberGroup>();
                    await uow.MemberGroupRepository.Create(new Request<mg.MemberGroup>()
                    {
                        Item = new mg.MemberGroup
                        {
                            MemberId = item,
                            GroupId = req.Item.GroupId,
                        },
                        TenantId = req.TenantId,
                        UserId = req.UserId
                    });
                }
                await uow.SaveAsync();
                await RemoveContactCacheAsync(req.TenantId);
                return req.Item;
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task AssignToGroups(Request<MemberToGroupsBo> req)
        {
            try
            {
                var groupIds = req.Item.GroupIds;
                foreach (var item in groupIds)
                {
                    await uow.MemberGroupRepository.Create(new Request<mg.MemberGroup>()
                    {
                        Item = new mg.MemberGroup
                        {
                            MemberId = req.Item.MemberId,
                            GroupId = item,
                        },
                        TenantId = req.TenantId,
                        UserId = req.UserId
                    });
                }
                await uow.SaveAsync();
                await RemoveContactCacheAsync(req.TenantId);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task Delete(Request<int> req)
        {
            try
            {
                await uow.MemberGroupRepository.DeletePermanentAndSave(req);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public async Task<PageList<MemberGroupBo>> Read(Search search)
        {
            try
            {
                return (await uow.MemberGroupRepository.Read<mg.MemberGroup>(search))
                    .MapPageObject<mg.MemberGroup, MemberGroupBo>();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public async Task<List<KeyValueListItem<int>>> ReadKeyValue(Search req)
        {
            try
            {
                return await uow.MemberGroupRepository.ReadKeyValue<mg.MemberGroup>(req);

            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public async Task Update(Request<MemberGroupBo> req)
        {
            try
            {
                await uow.MemberGroupRepository.UpdateAndSave(req.MapRequestObject<MemberGroupBo, mg.MemberGroup>(req.Item.Id));

            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public async Task<List<GroupMembersResult>> GetMemberGroups(Request<int> req)
        {
            try
            {
                var query = from member in uow.MemberRepository.TableAsNoTracking
                            join memberSubscription in uow.MemberSubscriptionRepository.TableAsNoTracking on member.Id equals
                                memberSubscription.MemberId
                            join subscription in uow.SubscriptionRepository.TableAsNoTracking on memberSubscription
                                .SubscriptinId equals subscription.Id
                            join subscriptionType in uow.SubscriptionTypeRepository.TableAsNoTracking on subscription
                                .Id equals subscriptionType.Id
                            join memberGroup in uow.MemberGroupRepository.TableAsNoTracking on member.Id equals memberGroup
                                .MemberId
                            where memberGroup.GroupId == req.Item && member.RecordState == Enums.RecordStatus.Active
                            select new GroupMembersResult
                            {
                                Id = memberGroup.Id,
                                GroupId = memberGroup.GroupId,
                                MemberId = memberGroup.MemberId,
                                Name = member.Name,
                                Level = subscriptionType.Name,
                                Email = member.Email,
                                Phone = member.Phone,

                            };
                var result = await query.ToListAsync();
                return result;
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public Task<MemberGroupBo> Read(Request<int> req)
        {
            throw new NotImplementedException();
        }

    }
}
