﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Inx.Core.Base;
using Inx.Module.Membership.Core.Bo;
using Inx.Module.Membership.Core.Bo.MemberGroup;
using Inx.Module.Membership.Core.Service.MemberGroup.Interface;
using Inx.Module.Membership.Data.DbContext;
using Inx.Module.Membership.Data.Entity.MemberGroup;
using Inx.Utility.Extentions;
using Inx.Utility.Models;
using Microsoft.EntityFrameworkCore;

namespace Inx.Module.Membership.Core.Service.MemberGroup
{
    public class GroupService : MemberBaseService, IGroupService
    {
        private readonly IUnitOfWokMembership uow;
        public GroupService(IUnitOfWokMembership _uow, ICoreInjector inject) : base(inject)
        {
            uow = _uow;
        }

        #region command
        public async Task<GroupBo> Create(Request<GroupBo> req)
        {
            try
            {
                return (await uow.GroupRepository.CreateAndSave(req.MapRequestObject<GroupBo, Group>())).MapObject<Group, GroupBo>();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task Delete(Request<int> req)
        {
            try
            {
                await uow.GroupRepository.DeletePermanentAndSave(req);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task Update(Request<GroupBo> req)
        {
            try
            {
                await uow.GroupRepository.UpdateAndSave(req.MapRequestObject<GroupBo, Group>(req.Item.Id));
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        #endregion

        #region query
        public async Task<PageList<GroupBo>> Read(Search search)
        {
            try
            {
                return (await uow.GroupRepository.Read<Group>(search))
                    .MapPageObject<Group, GroupBo>();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task<GroupBo> Read(Request<int> req)
        {
            try
            {
                return (await uow.GroupRepository.Read(req))
                  .MapObject<Group, GroupBo>();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task<List<KeyValueListItem<int>>> ReadKeyValue(Search req)
        {
            try
            {
                return await uow.GroupRepository.ReadKeyValue<Group>(req);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task<List<GroupBo>> ReadMemberGroupCount(Search search)
        {
            try
            {
                var groupdetails = new List<GroupBo>();
                var MemberIdList = new List<int>();

                var memberGroups = await uow.GroupRepository.TableAsNoTracking.Where(p => p.TenantId == search.TenantId).Select(a => new
                {
                    a.Name,
                    a.Description,
                    a.Id
                }).ToListAsync();

                foreach (var item in memberGroups)
                {
                    var groupmembers = await uow.MemberGroupRepository.TableAsNoTracking.Where(p => p.TenantId == search.TenantId && p.GroupId == item.Id).Select(p => new
                    {
                        p.MemberId,
                        p.RecordState,
                        p.GroupId
                    }).ToListAsync();

                    foreach (var i in groupmembers)
                    {
                        if (i.RecordState == Enums.RecordStatus.Active || i.RecordState == Enums.RecordStatus.Inactive)
                        {
                            var mem = i.MemberId;
                            MemberIdList.Add(mem);
                        }
                        else
                        {
                            MemberIdList.Add(0);
                        }
                    }
                    var membercount = MemberIdList.Count();
                    groupdetails.Add(new GroupBo { Name = item.Name, Description = item.Description, MemberCount = membercount, GroupId = item.Id });
                    MemberIdList.Clear();
                }
                return groupdetails;
            }
            catch (Exception e)
            {

                throw e.HandleException();
            }
        }


        #endregion

    }
}
