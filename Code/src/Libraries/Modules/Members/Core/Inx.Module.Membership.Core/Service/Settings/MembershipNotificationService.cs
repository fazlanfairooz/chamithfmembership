﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Inx.Core.Base;
using Inx.Core.Base.Service;
using Inx.Core.Base.Service.Interface;
using Inx.Data.Base.DbaContext;
using Inx.Module.Membership.Core.Bo.Settings.SearchViewResult;
using Inx.Module.Membership.Core.Service.Settings.Interface;
using Inx.Module.Membership.Data.DbContext;
using Inx.Utility.Exceptions;
using Inx.Utility.Extentions;
using Inx.Utility.Models;
using Inx.Utility.Models.Notification;
using Microsoft.EntityFrameworkCore;
using UnitOfWork = Inx.Data.Base.DbaContext.UnitOfWork;

namespace Inx.Module.Membership.Core.Service.Settings
{
    public class MembershipNotificationService : MemberBaseService, IMembershipNotificationService
    {
        #region placeholders

        private const string names = "#names#";
        private const string ifMoreNamesCondition = "{and #count# others}";
        private const string nameCount = "#count#";

        #endregion
        private INotificationService baseNotificationService;
        private IUnitOfWokMembership uow = null;
        public MembershipNotificationService(IUnitOfWokMembership _uow,ICoreInjector inject):base(inject)
        {
            uow = _uow;
            baseNotificationService = new NotificationService(new UnitOfWork(new ApplicationDbContext(uow.ConnectionString)));
        }
        public async Task MakeAsRead(Request<List<int>> request)
        {
            try
            {
                await baseNotificationService.MakeAsRead(request);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task<NotificationCountInfo> NotificationCount(Search search)
        {
            try
            {
                return await baseNotificationService.NotificationCount(search);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task<PageList<MemberNotificationViewResult>> Read(Search search)
        {
            Func<List<int>, List<KeyValueListItem<int>>, NotificationGenaratehelper> _names = (ids, names) =>
            {
                var namesResult = new NotificationGenaratehelper();
                if (ids.Count==0)
                {
                    throw new RecordNotFoundException();
                }
                if (ids.Count <= 2)
                {
                    namesResult.Names = string.Join(",", names.Where(p => ids.Contains(p.Value)));
                    namesResult.IfMoreNamesCondition = false;
                    namesResult.IfNameCount = false;
                }
                else
                {
                    namesResult.Names = string.Join(",", names.Where(p => ids.Contains(p.Value)).Skip(0).Take(2));
                    namesResult.IfMoreNamesCondition = true;
                    namesResult.IfNameCount = true;
                    namesResult.NameCount = ids.Count - 2;
                }
                return namesResult;
            };
            try
            {
                var result = await baseNotificationService.Read(search);
                var notificationTupples = result.Items;
                // set all members regarding notifications
                var memberids = new List<int>();
                foreach (var item in notificationTupples)
                {
                    memberids.AddRange(item.SenderId);
                }
                memberids = memberids.Distinct().ToList();
                var memberNames = await uow.MemberRepository.TableAsNoTracking
                    .Where(p => memberids.Contains(p.Id)).Select(p => new {name = p.Name,id = p.Id})
                    .ToListAsync();

                var nameList = new List<KeyValueListItem<int>>();
                foreach (var item in memberNames)
                {
                    nameList.Add(new KeyValueListItem<int>
                    {
                        Value = item.id,Text = item.name
                    });
                }
                // genarate notifications
                var notificationsResult = new List<MemberNotificationViewResult>();
                foreach (var item in notificationTupples)
                {
                    var temaplate = item.Template;
                    var resultx = _names(memberids, nameList);
                    temaplate.Replace(names, resultx.Names);
                    if (item.PlaceHolders.Contains(ifMoreNamesCondition)&& resultx.IfMoreNamesCondition && resultx.IfNameCount)
                    {
                        var r = ifMoreNamesCondition.Replace(nameCount, resultx.NameCount.ToString());
                        temaplate.Replace(ifMoreNamesCondition, r);
                    }
                    else
                    {
                        temaplate.Replace(ifMoreNamesCondition, "");
                    }
                    notificationsResult.Add(new MemberNotificationViewResult
                    {
                        Message = temaplate,Read = item.Read, GroupId = item.GroupId,Url = ""
                    });
                }

                return new PageList<MemberNotificationViewResult>
                {
                    Take = search.Take,
                    Skip = search.Skip,
                    Items = notificationsResult
                };
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        private class NotificationGenaratehelper
        {
            public string Names { get; set; }
            public bool IfMoreNamesCondition { get; set; }
            public bool IfNameCount { get; set; }
            public int NameCount { get; set; }
        }
    }
}
