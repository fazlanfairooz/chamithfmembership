﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Inx.Core.Base;
using Inx.Module.Membership.Core.Bo.Settings.CustomFieldDefinition;

namespace Inx.Module.Membership.Core.Service.Settings.CustomFieldDefinition.Interface
{
    public interface IMemberTabFieldService :IBaseService<MemberTabFieldBo>
    {
        Task<List<MemberTabFieldBo>> ReadDefault();
    }
}
