﻿using System.Threading.Tasks;
using Inx.Module.Membership.Core.Bo.Settings.CustomFieldDefinition;
using Inx.Utility.Models;

namespace Inx.Module.Membership.Core.Service.Settings.CustomFieldDefinition.Interface
{
    public interface IMemberTabFieldsSetupService
    {
        Task Seed(Request<MemberTabFieldsSetupBo> request);
    }
}
