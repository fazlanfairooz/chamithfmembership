﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Inx.Module.Membership.Core.Bo.Settings.SearchViewResult;
using Inx.Utility.Models;
using Inx.Utility.Models.Notification;

namespace Inx.Module.Membership.Core.Service.Settings.Interface
{
    public interface IMembershipNotificationService
    {
        Task MakeAsRead(Request<List<int>> request);
        Task<NotificationCountInfo> NotificationCount(Search search);
        Task<PageList<MemberNotificationViewResult>> Read(Search search);
    }
}
