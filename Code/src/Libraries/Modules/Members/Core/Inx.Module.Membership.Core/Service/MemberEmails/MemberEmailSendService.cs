﻿using Inx.Module.Membership.Core.Bo.Member.Master;
using Inx.Module.Membership.Core.Bo.MemberEmail;
using Inx.Module.Membership.Core.Bo.Subscription;
using Inx.Module.Membership.Core.Extensions;
using Inx.Module.Membership.Core.Service.MemberEmails.Interface;
using Inx.Module.Membership.Data.DbContext;
using Inx.Module.Messenger.Core.Service.Interfaces;
using Inx.Module.Messenger.Data.DbContext;
using Inx.Utility.Exceptions;
using Inx.Utility.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;

namespace Inx.Module.Membership.Core.Service.MemberEmails
{
    public class MemberEmailSendService : IMemberEmailSendService
    {
        private readonly IUnitOfWokMembership uow;
        private readonly IUnitOfWorkMassengers uowm;
        private readonly IEmailSendService serviceMail;

        public MemberEmailSendService(IUnitOfWokMembership _uow, IUnitOfWorkMassengers _uowm, IEmailSendService _serviceMail)
        {
            uow = (IUnitOfWokMembership)_uow;
            uowm = _uowm;
            serviceMail = _serviceMail;
        }

        /// <summary>
        /// Send multiple emails base on member level and member status
        /// </summary>
        public async Task SendEmails(Request<EmailSendingInfoModel> emailInfoModel)
        {
            var memeberLevelPropertyNames = new MemberLevelHeaderBo().GetProperties(new List<string> { "MemberLevelId", "MemberStatusId", "Id" }).ToList();
            var propertyNames = new MemberHeaderBo().GetProperties(new List<string> { "Id" }).ToList();

            propertyNames.AddRange(memeberLevelPropertyNames);

            var members = (from member in uow.MemberRepository.TableAsNoTracking
                           join membership in uow.MemberSubscriptionRepository.TableAsNoTracking on member.Id equals membership.MemberId
                           select new MemberHeaderBo
                           {
                               FirstName = member.FirstName,
                               Id = member.Id,
                               Email = member.Email,
                               PhoneNumber = member.Phone,
                               SubscriptionName = membership.Name,
                               TenantId = member.TenantId,
                               MembershipStatus = membership.MembershipStatus,
                               RecordStatus = member.RecordState
                           }).AsQueryable();

            members = members.FilterMembersById(emailInfoModel.Item, emailInfoModel.TenantId);

            var emailTemplate = await uowm.EmailTemplateRepository.TableAsNoTracking.FirstOrDefaultAsync(a => a.Id == emailInfoModel.Item.TemplateId && a.TenantId == emailInfoModel.TenantId);
            var emailBody = emailTemplate != null ? emailTemplate.EmailBody : emailInfoModel.Item.EmailBody;

            //if (emailInfoModel.Item.TemplateId != 0 && emailTemplate.EmailTemplateTypes == EmailTemplateTypes.MemberLevel)
            if (emailInfoModel.Item.TemplateId != 0 && emailInfoModel.Item.TemplateId != null)
            {
                var memberLevels = (from st in uow.SubscriptionTypeRepository.TableAsNoTracking
                                    join s in uow.SubscriptionRepository.TableAsNoTracking on st.Id equals s.Id
                                    join ms in uow.MemberSubscriptionRepository.TableAsNoTracking on s.Id equals ms.SubscriptinId
                                    where st.TenantId == emailInfoModel.TenantId
                                    select new Bo.Subscription.MemberLevelHeaderBo
                                    {
                                        MemberId = ms.MemberId,
                                        LevelName = st.Name,
                                        Capacity = s.Capacity,
                                        Price = s.Price,
                                        JoinDate = ms.JoinDate,
                                        RenewDate = ms.RenewDate,
                                        MemberLevelId = st.Id,
                                        MemberStatusId = ms.MembershipStatus
                                    }).AsQueryable();

                memberLevels = memberLevels.FilterMembersByLevel(emailInfoModel.Item);


                var memberWithLevels = members.Where(item => memberLevels.Any(m => m.MemberId.Equals(item.Id)));

                if (memberWithLevels.Count() == 0) throw new EmailNotSendException();


                var memberInfo = (from m in members.ToList()
                                  join ml in memberLevels.ToList() on m.Id equals ml.MemberId
                                  select new
                                  {
                                      m.FirstName,
                                      m.Email,
                                      m.TenantId,
                                      ml.LevelName,
                                      ml.Price,
                                      ml.JoinDate,
                                      ml.RenewDate,
                                      ml.Capacity,
                                      ml.MemberId,
                                  }).ToList();
                await SetPropertyEmail(memberInfo, propertyNames, emailBody, emailInfoModel.Item.Subject, emailInfoModel.Item.Attachment);
            }
            else
            {
                propertyNames = new MemberHeaderBo().GetProperties(new List<string> { "Id" }).ToList();
                var memberInfo = (from m in members.ToList()
                                  select new
                                  {
                                      m.FirstName,
                                      m.Email,
                                      m.PhoneNumber,
                                      m.TenantId
                                  }).ToList();
                await SetPropertyEmail(memberInfo, propertyNames, emailBody, emailInfoModel.Item.Subject, emailInfoModel.Item.Attachment);
            }
        }

        public async Task SetPropertyEmail(IEnumerable<dynamic> memberInfo, List<string> propertyNames, string body, string subject, List<Attachment> Attachment)
        {
            var emailSendingInfoList = new List<EmailSendingInfoModel>();
            var files = Attachment.Select(a =>
            new Messenger.Core.Bo.Emails.Attachment()
            {
                Extention = a.Extention,
                FileName = a.FileName,
                File = a.File
            }).ToList();
           

            foreach (var item in memberInfo)
            {
                files.ForEach(a => a.File.Seek(0, System.IO.SeekOrigin.Begin));
                var sb = GetFormatedBody(body, item, propertyNames);
                var emailModel = (new Messenger.Core.Bo.Emails.EmailSendingInfoModel
                {
                    ToAddress = item.Email,
                    Subject = subject,
                    TenantId = item.TenantId,
                    Id = 0,
                    Name = "",
                    EmailBody = sb.ToString(),
                    Attachment = files
                });
                await serviceMail.SendEmail(emailModel);
            }
        }

        private object GetFormatedBody(string body, dynamic item, List<string> propertyNames)
        {
            var sb = new StringBuilder(body);
            foreach (var propertyName in propertyNames)
            {

                var property = item.GetType().GetProperty(propertyName);
                if (property == null) continue;

                var value = property.GetValue(item, null);
                var replaceProperty = $"#{propertyName}#";
                if (value != null && (propertyName != "TenantId"))
                {
                    sb.Replace(replaceProperty, value.ToString());
                }
            }
            return sb;
        }
    }
}
