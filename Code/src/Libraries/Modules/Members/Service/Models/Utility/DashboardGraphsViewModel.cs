﻿namespace Inx.Module.Membership.Service.Models.Utility
{
   public  class DashboardGraphsViewModel 
    {
        public MemberActiveGraphViewModel MemberActiveGraphVM { get; set; }
        public MemberLevelGraphViewModel MemberLevelGraphVM { get; set; }
        
    }

    
}
