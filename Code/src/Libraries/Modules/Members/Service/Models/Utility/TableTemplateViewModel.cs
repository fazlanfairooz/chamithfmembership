﻿using System;
using System.Collections.Generic;
using Inx.Service.Base.Models;
using Inx.Module.Membership.Core.Bo;

namespace Inx.Module.Membership.Service.Models.Utility
{
    public class TableTemplateViewModel : BaseViewModel
    {
        public Enums.TableTemplate TemplateType { get; set; }
        public List<string> Keys { get; set; } = new List<string>();
        [Obsolete]
        public new string Name { get; set; }
    }
}
