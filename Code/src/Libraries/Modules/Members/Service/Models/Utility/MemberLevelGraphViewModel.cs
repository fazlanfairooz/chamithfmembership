﻿using System.Collections.Generic;

namespace Inx.Module.Membership.Service.Models.Utility
{
    public class MemberLevelGraphViewModel
    {
        public List<string> MemberLevels { get; set; }
        public List<int> ActiveMembers { get; set; }
        public List<int> InActiveMembers { get; set; }
        public MemberLevelGraphViewModel()
        {
            MemberLevels = new List<string>();
            ActiveMembers = new List<int>();
            InActiveMembers = new List<int>();
        }
    }
}
