﻿using Inx.Service.Base.Models;
namespace Inx.Module.Membership.Service.Models.Subscription
{
    public class SubscriptionTypeViewModel : BaseViewModel
    {
        public string Description { get; set; }
    }
}
