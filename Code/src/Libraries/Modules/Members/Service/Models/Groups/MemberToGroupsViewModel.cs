﻿using Inx.Service.Base.Models;
using System.Collections.Generic;

namespace Inx.Module.Membership.Service.Models.Groups
{
    public class MemberToGroupsViewModel : BaseViewModel
    {
        public int MemberId { get; set; }
        public List<int> GroupIds { get; set; }
    }
}
