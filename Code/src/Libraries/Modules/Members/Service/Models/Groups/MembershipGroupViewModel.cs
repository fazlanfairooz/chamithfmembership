﻿using Inx.Service.Base.Models;
using System.ComponentModel.DataAnnotations;
using Inx.Data.Base.Utility;

namespace Inx.Module.Membership.Service.Models.Groups
{
    public class MembershipGroupViewModel: BaseViewModel
    {
        [StringLength(DbConstraints.NameLength)]
        public new string Name { get; set; }
        [StringLength(500)]
        public string Description { get; set; }
    }
}
