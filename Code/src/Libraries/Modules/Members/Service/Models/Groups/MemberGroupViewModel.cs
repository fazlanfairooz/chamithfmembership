﻿using Inx.Service.Base.Models;
using System.Collections.Generic;

namespace Inx.Module.Membership.Service.Models.Groups
{
   public  class MemberGroupViewModel : BaseViewModel
    {
        public List<int> MemberIds { get; set; }
        public int GroupId { get; set; }
    }
}
