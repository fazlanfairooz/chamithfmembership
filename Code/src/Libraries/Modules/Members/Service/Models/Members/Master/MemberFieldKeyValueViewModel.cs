﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Inx.Module.Membership.Service.Models.Members.Master
{
    public class MemberFieldKeyValueViewModel
    {
        [Required]
        public Guid Key { get; set; }
        public string Value { get; set; }
    }
}
