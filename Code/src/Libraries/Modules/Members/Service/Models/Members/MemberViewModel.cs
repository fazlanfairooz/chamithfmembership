﻿using System.Collections.Generic;
using Inx.Utility.Attributes;
namespace Inx.Module.Membership.Service.Models.Members
{
    public class MemberViewModel
    {
        [ObjectNotNull]
        public MemberInfoViewModel MemberInfo { get; set; }
       
        public int Id { get; set; }
        [NumberNotZero]
        public int Subscription { get; set; }
        public List<int> Groups { get; set; }

        public void SetProfileImage(string profileimage)
        {
            MemberInfo.ProfileImage = profileimage;
        }
        public MemberViewModel()
        {
            MemberInfo = new MemberInfoViewModel();
        }
    }
}
