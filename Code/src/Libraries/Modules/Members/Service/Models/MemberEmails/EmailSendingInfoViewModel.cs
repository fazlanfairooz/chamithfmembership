﻿using Inx.Service.Base.Models;
using System.Collections.Generic;
using System.IO;
using static Inx.Utility.Utility.Enums;

namespace Inx.Module.Membership.Service.Models.MemberEmails
{
    public class EmailSendingInfoViewModel : BaseViewModel
    {

        public string EmailBody { get; set; }
        public string Subject { get; set; }
        public List<Attachment> Attachment { get; set; }
        public EmailSendTypes EmailSendType { get; set; }
        public int? MemberStatusId { get; set; }
        public int? MemberLevelId { get; set; }
        public int? MemberId { get; set; }
        public int? TemplateId { get; set; }
        public string ToAddress { get; set; }
        public List<int> MemberIds { get; set; }
    }

    public class Attachment
    {
        public Stream File { get; set; }
        public string FileName { get; set; }
        public string Extention { get; set; }
    }
}
