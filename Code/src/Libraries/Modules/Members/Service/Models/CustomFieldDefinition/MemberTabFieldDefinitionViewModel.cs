﻿using Inx.Service.Base.Models;
using System.ComponentModel.DataAnnotations;

namespace Inx.Module.Membership.Service.Models.CustomFieldDefinition
{
    public class MemberTabFieldDefinitionViewModel : BaseViewModel
    {
        [Required]
        public int TabId { get; set; }
        [Required]
        public int FieldId { get; set; }
    }
}
