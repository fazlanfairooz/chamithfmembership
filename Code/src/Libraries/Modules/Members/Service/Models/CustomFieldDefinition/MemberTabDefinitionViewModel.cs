﻿using Inx.Service.Base.Models;
using Inx.Utility.Utility;
using System.Collections.Generic;

namespace Inx.Module.Membership.Service.Models.CustomFieldDefinition
{
    public class MemberTabDefinitionViewModel : BaseViewModel
    {
        public string DisplayName { get; set; }
        public bool IsVisible { get; set; }
        public List<Enums.MemberType> MemberType { get; set; } = new List<Enums.MemberType>();
        public string TabDescription { get; set; }
        public bool IsSystemTab { get; set; }
    }
}
