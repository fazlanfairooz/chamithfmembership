﻿using System;

namespace Inx.Module.Membership.Service.Models.CustomFieldDefinition
{
    public class MemberFieldDefinitionViewModel
    {
        public int Id { get; set; }
        public int TabId { get; set; }
        public Guid Key { get; set; }
        public int DataType { get; set; }
        public string DisplayName { get; set; }
        public string Value { get; set; }
        public bool IsRequired { get; set; }
        public int Order { get; set; }
        public string Placeholder { get; set; }
        public string ToolTip { get; set; }
    }
}
