﻿using Abp.AspNetCore;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Inx.Module.Membership.Service.Utility.Config;
using Inx.Module.Messenger.Service;
using Inx.Module.Payment.Service;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace Inx.Module.Membership.Service
{
    [DependsOn(typeof(AbpAspNetCoreModule)),
     DependsOn(typeof(MessengerServiceModule)),
     DependsOn(typeof(PaymentServiceModule))]
    public class MembershipServiceModule : AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public MembershipServiceModule(IHostingEnvironment env)
        {
        }
        public override void PreInitialize()
        {
            IocConfig.Register(IocManager);
        }
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(MembershipServiceModule).GetAssembly());
            Automapper.Register(Configuration);
        }
        public override void PostInitialize()
        {
           BackgroundProcessConfig.Register(IocManager);
        }
    }
}
