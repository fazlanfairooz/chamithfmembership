﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Inx.Module.Membership.Core.Bo.Settings;
using Inx.Module.Membership.Core.Service.Settings.Interface;
using Inx.Module.Membership.Service.Models.Utility;
using Inx.Module.Membership.Service.Utility;
using Inx.Service.Base.Attributes;
using Inx.Service.Base.Controllers;
using Inx.Service.Base.Models;
using Inx.Utility.Utility;
using Microsoft.AspNetCore.Mvc;
using Inx.Module.Membership.Core.Service.Dashboard.Interface;

namespace Inx.Module.Membership.Service.Controllers.Api.Settings
{
    [Route(Constraints.ApiPrefix), AuthorizeRoles(Enums.Roles.SuperAdmin, Enums.Roles.TenantAdmin)]
    public class MembershipUtilityApiController : MembershipBaseApiController
    {
        //private readonly ITableTemplateService service;
        private readonly IDashboardService memberservice;
        public MembershipUtilityApiController(IDashboardService _memberservice, IBaseInjector baseinject) : base(baseinject)
        {
            memberservice = _memberservice;
        }
        //public MembershipUtilityApiController(ITableTemplateService _service, IBaseInjector baseinject) : base(baseinject)
        //{
        //    service = _service; 
        //}
        //[HttpGet, Route("utility/tabletemplate/{type:int}")]
        //public async Task<IActionResult> Read(int type)
        //{
        //    try
        //    {
        //        var stopwatch = Stopwatch.StartNew();
        //        var result = await service.Read(Request(type));
        //        stopwatch.Stop();
        //        return Ok(new Response<List<string>>
        //        {
        //            Item = result,
        //            TimeTaken = stopwatch.ElapsedMilliseconds
        //        });
        //    }
        //    catch (Exception e)
        //    {
        //        return await HandleException(e);
        //    }
        //}

        //[HttpGet, Route("utility/tabletemplate/headers/{type:int}")]
        //public async Task<IActionResult> ReadHeaders(Enums.TableTemplateTypes type)
        //{
        //    try
        //    {
        //        var stopwatch = Stopwatch.StartNew();
        //        var result = service.GetColomHeaders(type);
        //        stopwatch.Stop();
        //        return Ok(new Response<List<string>>
        //        {
        //            Item = result,
        //            TimeTaken = stopwatch.ElapsedMilliseconds
        //        });
        //    }
        //    catch (Exception e)
        //    {
        //        return await HandleException(e);
        //    }
        //}

        //[HttpPut, Route("utility/tabletemplate"), ModelValidation]
        //public async Task<IActionResult> Update([FromBody]  TableTemplateViewModel item)
        //{
        //    return await Update<TableTemplateViewModel, TableTemplateBo, ITableTemplateService>(item, service);
        //}

        [HttpGet, Route("utility/dashboardgraphs/{type:int}")]
        public async Task<IActionResult> DashboardGraphRead(Core.Bo.Enums.DateRangeFilter type)
        {
            try
            {
                var stopwatch = Stopwatch.StartNew();
                return Ok(await memberservice.DashboardGraph(Request(type)));
                stopwatch.Stop();
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }

        }
    }
}
