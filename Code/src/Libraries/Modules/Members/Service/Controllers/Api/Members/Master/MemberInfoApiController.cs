﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Inx.Module.Membership.Core.Bo;
using Inx.Module.Membership.Core.Bo.Member.Master;
using Inx.Module.Membership.Core.Service.Members.interfaces;
using Inx.Module.Membership.Core.Service.Settings.CustomFieldDefinition.Interface;
using Inx.Module.Membership.Service.Models.Members.Master;
using Inx.Module.Membership.Service.Utility;
using Inx.Service.Base.Attributes;
using Inx.Service.Base.Controllers;
using Inx.Utility.Exceptions;
using Inx.Utility.Extentions;
using Inx.Utility.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Enums = Inx.Utility.Utility.Enums;
using Inx.Service.Base.Models;
using System.Collections.Generic;
using System.Diagnostics;

namespace Inx.Module.Membership.Service.Controllers.Api.Members.Master
{
    [Route(Constraints.ApiPrefix)/*, AuthorizeRoles(Enums.Roles.SuperAdmin, Enums.Roles.TenantAdmin)*/]
    public class MemberInfoApiController : MembershipBaseApiController, IBaseApi<MemberInfoViewModel, int>
    {
        private IMemberInfoService memberInfoService;
        private IMemberFieldRequiredService memberFieldRequiredService;
        public MemberInfoApiController(IMemberInfoService _memberInfoService, IMemberFieldRequiredService _memberFieldRequiredService,
            IBaseInjector baseinject) : base(baseinject)
        {
            memberInfoService = _memberInfoService;
            memberFieldRequiredService = _memberFieldRequiredService;
        }

        [HttpPost, Route("members"), ModelValidation]
        public async Task<IActionResult> Create()
        {
            try
            {
                var systemFields = await memberFieldRequiredService.Read();
                var memberid = 0;
                var imagename = string.Empty;

                var httpRequest = HttpContext.Request;
                var memberViewData = httpRequest.Form["memberInfoViewData"];
                var memberInfoViewModel = JsonConvert.DeserializeObject<MemberInfoViewModel>(memberViewData);
                var result = await UploadBlob(Enums.FileType.ProfileImageDefault);
                if (result.FileIsThere)
                {
                    imagename = result.FileNames[0];
                    await UploadBlob(Enums.FileType.ProfileImageThumbnail, imagename);
                }

                if (memberInfoViewModel.MemberType == Enums.MemberType.Person)
                {
                    systemFields = systemFields.Where(p => p.MemberType == Enums.MemberType.Person).ToList();
                    var requestItem = new MemberInfoBo<PersonalInfoBo> { MemberType = Enums.MemberType.Person };
                    requestItem.ProfileImage = imagename;
                    foreach (var field in systemFields)
                    {
                        var obj = memberInfoViewModel.MemberFieldKeyValue.FirstOrDefault(p => p.Key == field.Key);
                        if (obj.IsNull())
                        {
                            throw new DataInconsistencyClientException();
                        }
                        requestItem.GetType().GetProperty(field.Name).SetValue(requestItem, obj.Value);
                        requestItem.MemberFieldKeyValue = memberInfoViewModel.MemberFieldKeyValue
                            .MapListObject<MemberFieldKeyValueViewModel, MemberFieldKeyValueBo>().ToList();
                    }
                    memberid = await memberInfoService.Create(Request(requestItem));
                }
                else
                {
                    systemFields = systemFields.Where(p => p.MemberType == Enums.MemberType.Organization).ToList();
                    var requestItem = new MemberInfoBo<OrganizationInfoBo> { MemberType = Enums.MemberType.Organization };
                    requestItem.ProfileImage = imagename;
                    foreach (var field in systemFields)
                    {
                        var obj = memberInfoViewModel.MemberFieldKeyValue.FirstOrDefault(p => p.Key == field.Key);
                        if (obj.IsNull())
                        {
                            throw new DataInconsistencyClientException();
                        }
                        requestItem.GetType().GetProperty(field.Name).SetValue(requestItem, obj.Value);
                        requestItem.MemberFieldKeyValue = memberInfoViewModel.MemberFieldKeyValue
                            .MapListObject<MemberFieldKeyValueViewModel, MemberFieldKeyValueBo>().ToList();
                    }
                    memberid = await memberInfoService.Create(Request(requestItem));
                }
                return Ok(memberid);
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }


        [HttpDelete, Route("members/{id:int}"), Description("member will be archive")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await memberInfoService.Delete(Request(id));
                return Ok();
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }
        [HttpDelete, Route("members/{id:int}/permanent"), Description("member will be delete")]
        public async Task<IActionResult> DeleteStatus(int id)
        {
            try
            {
                await memberInfoService.DeletePermanent(Request(id));
                return Ok();
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }
        [HttpPut, Route("members/{id:int}/restore"), Description("member will be active")]
        public async Task<IActionResult> Restore(int id)
        {
            try
            {
                await memberInfoService.Restore(Request(id));
                return Ok();
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }
        [HttpGet, Route("members")]
        public async Task<IActionResult> Read(int skip = 0, int take = 0, string search = null, string orderby = null)
        {
            try
            {
                return Ok(await memberInfoService.Search(SearchRequest(skip, take, searchTerm: search,
                    orderByTerm: orderby)));
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }
        [HttpGet, Route("membersArchive")]
        public async Task<IActionResult> ReadArchive(int skip = 0, int take = 0, string search = null, string orderby = null)
        {
            try
            {
                return Ok(await memberInfoService.SearchArchive(SearchRequest(skip, take, searchTerm: search,
                    orderByTerm: orderby)));
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }
        [HttpGet, Route("members/{id:int}")]
        public async Task<IActionResult> Read(int id)
        {
            try
            {
                return Ok(await memberInfoService.Read(Request(id)));
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }
        [HttpGet, Route("members/keyvalue")]
        public async Task<IActionResult> ReadKeyValue()
        {
            try
            {
                return Ok(await memberInfoService.ReadKeyValue(SearchRequest()));
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }
        [HttpGet, Route("members/memberstatus")]
        public async Task<IActionResult> Read()
        {
            try
            {
                var stopwatch = Stopwatch.StartNew();
                var emailTypes = EnumsExtention.GetList<Enums.MembershipStatus>();
                stopwatch.Stop();
                return Ok(new Response<List<EnumMember>>
                {
                    Item = emailTypes,
                    TimeTaken = stopwatch.ElapsedMilliseconds
                });
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }
        [HttpPut, Route("members")]
        public new Task<IActionResult> Update([FromBody] MemberInfoViewModel item)
        {
            throw new NotImplementedException();
        }
        [ApiExplorerSettings(IgnoreApi = true)]
        public new Task<IActionResult> Create(MemberInfoViewModel item)
        {
            throw new NotImplementedException();
        }
    }
}
