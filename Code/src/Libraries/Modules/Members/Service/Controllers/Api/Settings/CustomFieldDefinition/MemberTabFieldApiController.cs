﻿using Inx.Module.Membership.Core.Bo.Settings.CustomFieldDefinition;
using Inx.Module.Membership.Core.Service.Settings.CustomFieldDefinition.Interface;
using Inx.Module.Membership.Service.Models.CustomFieldDefinition;
using Inx.Module.Membership.Service.Utility;
using Inx.Service.Base.Attributes;
using Inx.Service.Base.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Inx.Module.Membership.Service.Controllers.Api.Settings.CustomFieldDefinition
{
    [Route(Constraints.ApiPrefix)]
    public class MemberTabFieldApiController : MembershipBaseApiController, IBaseApi<MemberTabFieldDefinitionViewModel, int>
    {
        private readonly IMemberTabFieldService service;
        public MemberTabFieldApiController(IMemberTabFieldService _service, IBaseInjector baseinject) : base(baseinject)
        {
            service = _service;
        }

        [HttpPost, Route("tabfield"), ModelValidation]
        public async Task<IActionResult> Create([FromBody]MemberTabFieldDefinitionViewModel item)
        {
            return Ok(await Create<MemberTabFieldDefinitionViewModel, MemberTabFieldBo, IMemberTabFieldService>(item, service));
        }

        [HttpPut, Route("tabfield"), ModelValidation]
        public async Task<IActionResult> Update([FromBody]MemberTabFieldDefinitionViewModel item)
        {
            return await Update<MemberTabFieldDefinitionViewModel, MemberTabFieldBo, IMemberTabFieldService>(item, service);
        }

        [HttpDelete, Route("tabfield/{id:int}"), ModelValidation]
        public async Task<IActionResult> Delete(int id)
        {
            return await Delete<MemberTabFieldBo, IMemberTabFieldService>(id, service);
        }
        #region Not Implemented
        [HttpGet, Route("tabfield/{id:int}"), ModelValidation]
        public async Task<IActionResult> Read(int id)
        {
            throw new System.NotImplementedException();
        }
        [HttpGet, Route("tabfield"), ModelValidation]
        public async Task<IActionResult> Read(int skip = 0, int take = 0, string search = null, string orderby = null)
        {
            throw new System.NotImplementedException();
        }
        [HttpGet, Route("tabfield/keyvalue"), ModelValidation]
        public async Task<IActionResult> ReadKeyValue()
        {
            throw new System.NotImplementedException();
        }
        #endregion

    }
}
