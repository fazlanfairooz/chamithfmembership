﻿using System;
using System.Threading.Tasks;
using Inx.Module.Membership.Core.Bo.Member;
using Inx.Module.Membership.Core.Service.Members.interfaces;
using Inx.Module.Membership.Service.Models.Members;
using Inx.Module.Membership.Service.Utility;
using Inx.Service.Base.Attributes;
using Inx.Service.Base.Controllers;
using Inx.Utility.Utility;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Inx.Module.Membership.Service.Controllers.Api.Members
{
    [Route(Constraints.ApiPrefix), AuthorizeRoles(Enums.Roles.SuperAdmin, Enums.Roles.TenantAdmin)]
    public class MemberFileApiController : MembershipBaseApiController, IBaseApi<MemberFileViewModel, int>
    {

        private readonly IMemberFileService service;
        public MemberFileApiController(IMemberFileService _service, IBaseInjector baseinject) : base(baseinject)
        {
            service = _service;
        }
        [HttpPost, Route("memberfiles")]
        public async Task<IActionResult> Create()
        {
            string[] filename = null;
            try
            {
                var httpRequest = HttpContext.Request;
                var memberViewData = httpRequest.Form["memberFileViewData"];
                var item = JsonConvert.DeserializeObject<MemberFileViewModel>(memberViewData);
                var result = await UploadBlobFiles(Enums.FileType.MembersFiles);
                filename = result.FileNamesKeyValue[0];
                item.SetFileInfo(filename[0], filename[1],filename[2]);
                return await Create<MemberFileViewModel, MemberFileBo, IMemberFileService>(item, service);
            }
            catch (Exception e)
            {
                //if any happen delete file if exist.
                if (!string.IsNullOrEmpty(filename?[1]))
                {
                    Hangfire.BackgroundJob.Enqueue(() => DeleteBlob(Enums.FileType.MembersFiles, filename[1]));
                }

                return await HandleException(e);
            }
        }
        [HttpDelete, Route("memberfiles/{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var result = await service.Delete(Request(id));
                Hangfire.BackgroundJob.Enqueue(() => DeleteBlob(Enums.FileType.MembersFiles, result));
                return Ok();
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }
       
        [HttpGet, Route("memberfiles")]
        public async Task<IActionResult> Read(int skip = 0, int take = 0, string search = null, string orderby = null)
        {
            return await Read<MemberFileViewModel, MemberFileBo, IMemberFileService>(SearchRequest(skip,
                take, searchTerm: search, orderByTerm: orderby), service);
        }
        #region ignored

        [HttpPut, Route("memberfiles"), ModelValidation,Microsoft.AspNetCore.Mvc.ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> Update([FromBody]  MemberFileViewModel item)
        {
            return await Update<MemberFileViewModel, MemberFileBo, IMemberFileService>(item, this.service);
        }
        [HttpGet, Route("memberfiles/{id:int}"), Microsoft.AspNetCore.Mvc.ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> Read(int id)
        {
            return await Read<MemberFileViewModel, MemberFileBo, IMemberFileService>(id, this.service);
        }
        [HttpGet, Route("memberfiles/keyvalue"), Microsoft.AspNetCore.Mvc.ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> ReadKeyValue()
        {
            return await ReadKeyValue<MemberFileBo, IMemberFileService>(service);
        }
        [Microsoft.AspNetCore.Mvc.ApiExplorerSettings(IgnoreApi = true)]
        public new Task<IActionResult> Create(MemberFileViewModel item)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
