﻿using System;
using System.Threading.Tasks;
using Inx.Module.Membership.Core.Bo.Settings.CustomFieldDefinition;
using Inx.Module.Membership.Core.Service.Settings.CustomFieldDefinition.Interface;
using Inx.Module.Membership.Service.Utility;
using Inx.Service.Base.Attributes;
using Inx.Service.Base.Controllers;
using Inx.Utility.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Inx.Module.Membership.Service.Controllers.Api
{
    [Route(Constraints.ApiPrefix),/* AuthorizeRoles(Enums.Roles.SuperAdmin, Enums.Roles.TenantAdmin)*/]
    public class MembershipSetupApiController : MembershipBaseApiController
    {
        private readonly IMemberTabFieldsSetupService memberTabFieldsSetupService;
        private readonly IMemberTabDefinitionService memberTabDefinitionService;
        private readonly IMemberFieldDefinitionService memberFieldDefinitionService;
        private readonly IMemberTypeTabService memberTypeTabService;
        private readonly IMemberTabFieldService memberTabFieldService;
        public MembershipSetupApiController(IMemberTabFieldsSetupService _memberTabFieldsSetupService,
            IMemberTabDefinitionService _memberTabDefinitionService,
            IMemberFieldDefinitionService _memberFieldDefinitionService,
            IMemberTypeTabService _memberTypeTabService,
            IBaseInjector baseinject,IMemberTabFieldService _memberTabFieldService) : base(baseinject)
        {
            memberTabFieldsSetupService = _memberTabFieldsSetupService;
            memberTabDefinitionService = _memberTabDefinitionService;
            memberFieldDefinitionService = _memberFieldDefinitionService;
            memberTypeTabService = _memberTypeTabService;
            memberTabFieldService = _memberTabFieldService;
        }
        [Route("intialsetup"),HttpPost]
        public async Task<IActionResult> SeedMemberSetupWizard()
        {
            try
            {
                var tabs = await memberTabDefinitionService.ReadDefault();
                var fields = await memberFieldDefinitionService.ReadDefault();
                var tabfields = await memberTabFieldService.ReadDefault();
                var typetab = await memberTypeTabService.ReadDefault();

                await memberTabFieldsSetupService.Seed(Request(new MemberTabFieldsSetupBo
                {
                    MemberFieldDefinition = fields,
                    MemberTabDefinition = tabs,
                    MemberTabField = tabfields,
                    MemberTypeTab = typetab
                }));
                return Ok();
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }
    }
}
