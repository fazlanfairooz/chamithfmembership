﻿using System.Threading.Tasks;
using Inx.Module.Membership.Core.Bo.Settings.CustomFieldDefinition;
using Inx.Module.Membership.Core.Service.Settings.CustomFieldDefinition.Interface;
using Inx.Module.Membership.Service.Models.CustomFieldDefinition;
using Inx.Module.Membership.Service.Utility;
using Inx.Service.Base.Attributes;
using Inx.Service.Base.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace Inx.Module.Membership.Service.Controllers.Api.Settings.CustomFieldDefinition
{
    [Route(Constraints.ApiPrefix)]
    public class MemberTypeTabApiController : MembershipBaseApiController, IBaseApi<MemberTypeTabViewModel, int>
    {
        private readonly IMemberTypeTabService service;
        public MemberTypeTabApiController(IMemberTypeTabService _service, IBaseInjector baseinject) : base(baseinject)
        {
            service = _service;
        }
        [HttpPost, Route("typetab"), ModelValidation]
        public async Task<IActionResult> Create(MemberTypeTabViewModel item)
        {
            return await Create<MemberTypeTabViewModel, MemberTypeTabBo, IMemberTypeTabService>(item, service);
        }

        [HttpPut, Route("typetab"), ModelValidation]
        public async Task<IActionResult> Update(MemberTypeTabViewModel item)
        {
            return await Update<MemberTypeTabViewModel, MemberTypeTabBo, IMemberTypeTabService>(item, this.service);
        }
        [HttpGet, Route("typetab/{id:int}"), ModelValidation]
        public async Task<IActionResult> Read(int id)
        {
            return await Read<MemberTypeTabViewModel, MemberTypeTabBo, IMemberTypeTabService>(id, service);
        }
        [HttpDelete, Route("typetab/{id:int}"), ModelValidation]
        public async Task<IActionResult> Delete(int id)
        {
            return await Delete<MemberTypeTabBo, IMemberTypeTabService>(id, service);
        }
        [HttpGet, Route("typetab"), ModelValidation]
        public async Task<IActionResult> Read(int skip = 0, int take = 0, string search = null, string orderby = null)
        {
            return await Read<MemberTypeTabViewModel, MemberTypeTabBo, IMemberTypeTabService>(SearchRequest(skip,
               take, searchTerm: search, orderByTerm: orderby), service);
        }
        [HttpGet, Route("typetab/keyvalue"), ModelValidation]
        public async Task<IActionResult> ReadKeyValue()
        {
            return await ReadKeyValue<MemberTypeTabBo, IMemberTypeTabService>(service);
        }
    }
}
