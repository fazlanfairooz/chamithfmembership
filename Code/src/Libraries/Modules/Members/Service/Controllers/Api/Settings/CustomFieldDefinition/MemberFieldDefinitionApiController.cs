﻿using Inx.Module.Membership.Core.Bo.Settings.CustomFieldDefinition;
using Inx.Module.Membership.Core.Service.Settings.CustomFieldDefinition.Interface;
using Inx.Module.Membership.Service.Models.CustomFieldDefinition;
using Inx.Module.Membership.Service.Utility;
using Inx.Service.Base.Attributes;
using Inx.Service.Base.Controllers;
using Inx.Utility.Extentions;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
namespace Inx.Module.Membership.Service.Controllers.Api.Settings.CustomFieldDefinition
{
    [Route(Constraints.ApiPrefix)]
    public class MemberFieldDefinitionApiController : MembershipBaseApiController, IBaseApi<MemberFieldDefinitionViewModel, int>
    {
        private readonly IMemberFieldDefinitionService service;
        private readonly IMemberTabFieldService tabFieldService;
        public MemberFieldDefinitionApiController(IMemberFieldDefinitionService _service, IMemberTabFieldService _tabFieldService, IBaseInjector baseinject) : base(baseinject)
        {
            service = _service;
            tabFieldService = _tabFieldService;
        }
        [HttpPost, Route("fielddefinition"), ModelValidation]
        public async Task<IActionResult> Create([FromBody]MemberFieldDefinitionViewModel item)
        {
            try
            {
                var fieldId = await service.Create(Request(item.MapObject<MemberFieldDefinitionViewModel, MemberFieldDefinitionBo>()));
                var requestItem = new MemberTabFieldBo()
                {
                    FieldId = fieldId,
                    TabId = item.TabId
                };

                await tabFieldService.Create(Request(requestItem));
                return Ok();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        [HttpPut, Route("fielddefinition"), ModelValidation]
        public async Task<IActionResult> Update([FromBody]MemberFieldDefinitionViewModel item)
        {
            return await Update<MemberFieldDefinitionViewModel, MemberFieldDefinitionBo, IMemberFieldDefinitionService>(item, this.service);
        }
        [HttpGet, Route("fielddefinition/{id:int}"), ModelValidation]
        public async Task<IActionResult> Read(int id)
        {
            return await Read<MemberFieldDefinitionViewModel, MemberFieldDefinitionBo, IMemberFieldDefinitionService>(id, service);
        }
        [HttpDelete, Route("fielddefinition/{id:int}"), ModelValidation]
        public async Task<IActionResult> Delete(int id)
        {
            return await Delete<MemberFieldDefinitionBo, IMemberFieldDefinitionService>(id, service);
        }
        [HttpGet, Route("fielddefinition"), ModelValidation]
        public async Task<IActionResult> Read(int skip = 0, int take = 0, string search = null, string orderby = null)
        {
            return await Read<MemberFieldDefinitionViewModel, MemberFieldDefinitionBo, IMemberFieldDefinitionService>(SearchRequest(skip,
               take, searchTerm: search, orderByTerm: orderby), service);
        }
        [HttpGet, Route("fielddefinition/keyvalue"), ModelValidation]
        public async Task<IActionResult> ReadKeyValue()
        {
            return await ReadKeyValue<MemberFieldDefinitionBo, IMemberFieldDefinitionService>(service);
        }

    }
}
