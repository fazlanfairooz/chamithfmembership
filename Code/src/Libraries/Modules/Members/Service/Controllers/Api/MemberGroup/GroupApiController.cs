﻿using System.Threading.Tasks;
using Inx.Module.Membership.Core.Bo.MemberGroup;
using Inx.Module.Membership.Core.Service.MemberGroup.Interface;
using Inx.Module.Membership.Service.Models.Groups;
using Inx.Module.Membership.Service.Utility;
using Inx.Service.Base.Attributes;
using Inx.Service.Base.Controllers;
using Inx.Utility.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Inx.Module.Membership.Service.Controllers.Api.MemberGroup
{
    [Route(Constraints.ApiPrefix), AuthorizeRoles(Enums.Roles.SuperAdmin, Enums.Roles.TenantAdmin)]
    public class GroupApiController : MembershipBaseApiController, IBaseApi<MembershipGroupViewModel, int>
    {
        private readonly IGroupService service;
        public GroupApiController(IGroupService _service, IBaseInjector baseinject) : base(baseinject)
        {
            service = _service;
        }
        [HttpPost, Route("groups"), ModelValidation]
        public async Task<IActionResult> Create([FromBody] MembershipGroupViewModel item)
        {
            return await Create<MembershipGroupViewModel, GroupBo, IGroupService>(item, service);
        }
        [HttpDelete, Route("groups/{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            return await Delete<GroupBo, IGroupService>(id, service);
        }
        [HttpGet, Route("groups/readWithMemberCount")]
        public async Task<IActionResult> ReadWithMemberCount(int skip = 0, int take = 0, string search = null, string orderby = null)
        {
           
          return Ok(await service.ReadMemberGroupCount(SearchRequest(skip,take, searchTerm: search, orderByTerm: orderby)));
        } 
        [HttpGet, Route("groups")]
        public async Task<IActionResult> Read(int skip = 0, int take = 0, string search = null, string orderby = null)
        {
            return await Read<MembershipGroupViewModel, GroupBo, IGroupService>(SearchRequest(skip,
                take, searchTerm: search, orderByTerm: orderby), service);
        }
        [HttpGet, Route("groups/keyvalue")]
        public async Task<IActionResult> ReadKeyValue()
        {
            return await ReadKeyValue<GroupBo, IGroupService>(service);
        }
        [HttpPut, Route("groups"), ModelValidation]
        public async Task<IActionResult> Update([FromBody] MembershipGroupViewModel item)
        {
            return await Update<MembershipGroupViewModel, GroupBo, IGroupService>(item, service);
        }

        [HttpGet, Route("groups/{id:int}")]
        public async Task<IActionResult> Read(int id)
        {
            return await Read<MembershipGroupViewModel, GroupBo, IGroupService>(id, service);
        }
    }
}
