﻿using Inx.Module.Membership.Core.Bo.MemberEmail;
using Inx.Module.Membership.Core.Service.MemberEmails.Interface;
using Inx.Module.Membership.Core.Service.Members.interfaces;
using Inx.Module.Membership.Core.Service.Settings.CustomFieldDefinition.Interface;
using Inx.Module.Membership.Service.Models.MemberEmails;
using Inx.Module.Membership.Service.Utility;
using Inx.Service.Base.Controllers;
using Inx.Service.Base.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace Inx.Module.Membership.Service.Controllers.Api.MemberEmails
{
    [Route(Constraints.ApiPrefix)/*, AuthorizeRoles(Enums.Roles.SuperAdmin, Enums.Roles.TenantAdmin)*/]
    public class MemberEmailApiController : MembershipBaseApiController
    {
        private IMemberEmailSendService service;
        public MemberEmailApiController(IMemberEmailSendService _service, IBaseInjector baseinject) : base(baseinject)
        {
            service = _service;
        }

        [HttpPost, Route("memberemail/send")]
        public async Task<IActionResult> Send()
        {
            try
            {
                var httpRequest = HttpContext.Request;
                var emailFileViewData = httpRequest.Form["EmailFileViewData"];
                var item = JsonConvert.DeserializeObject<EmailSendingInfoViewModel>(emailFileViewData);
                var stopwatch = Stopwatch.StartNew();
                var fresilt = new FileSaveResultFiles();
                if (HttpContext.Request.Form.Files.Count != 0)
                {
                    var files = HttpContext.Request.Form.Files;
                    Stream streamFiles;
                    item.Attachment = new List<Models.MemberEmails.Attachment>();
                    foreach (var file in files)
                    {
                        if (file.Length <= 0) continue;
                        var extention = Path.GetExtension(file.FileName).Substring(1);
                        streamFiles = file.OpenReadStream();
                        fresilt.FileIsThere = true;
                        item.Attachment.Add(new Models.MemberEmails.Attachment
                        {
                            File = streamFiles,
                            Extention = extention,
                            FileName = file.FileName
                        });
                    }
                }
                await service.SendEmails(Request<EmailSendingInfoViewModel, EmailSendingInfoModel>(item));
                stopwatch.Stop();

                return Ok();
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }
    }
}
