﻿using System.Threading.Tasks;
using Inx.Module.Membership.Core.Bo.Subscription;
using Inx.Module.Membership.Core.Service.Subscription.Interfaces;
using Inx.Module.Membership.Service.Models.Subscription;
using Inx.Module.Membership.Service.Utility;
using Inx.Service.Base.Attributes;
using Inx.Service.Base.Controllers;
using Microsoft.AspNetCore.Mvc;
using Enums = Inx.Utility.Utility.Enums;

namespace Inx.Module.Membership.Service.Controllers.Api.Subscription
{
    [Route(Constraints.ApiPrefix), ApiExplorerSettings(IgnoreApi = true),
     AuthorizeRoles(Inx.Utility.Utility.Enums.Roles.SuperAdmin, Enums.Roles.TenantAdmin)]
    public class SubscriptionTypeApiController : MembershipBaseApiController, IBaseApi<SubscriptionTypeViewModel, int>
    {

        private readonly ISubscriptionTypeService service;

        public SubscriptionTypeApiController(ISubscriptionTypeService _service, IBaseInjector baseinject) : base(
            baseinject)
        {
            service = _service;
        }

        [HttpPost, Route("subscriptiontype"), ModelValidation]
        public async Task<IActionResult> Create([FromBody] SubscriptionTypeViewModel item)
        {
            return await Create<SubscriptionTypeViewModel, SubscriptionTypeBo, ISubscriptionTypeService>(item, service);
        }

        [HttpDelete, Route("subscriptiontype/{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            return await Delete<SubscriptionTypeBo, ISubscriptionTypeService>(id, service);
        }

        [HttpGet, Route("subscriptiontype/{id:int}")]
        public async Task<IActionResult> Read(int id)
        {
            return await Read<SubscriptionTypeViewModel, SubscriptionTypeBo, ISubscriptionTypeService>(id,
                service);
        }

        [HttpGet, Route("subscriptiontype")]
        public async Task<IActionResult> Read(int skip = 0, int take = 0, string search = null, string orderby = null)
        {
            return await Read<SubscriptionTypeViewModel, SubscriptionTypeBo, ISubscriptionTypeService>(SearchRequest(
                skip,
                take, searchTerm: search, orderByTerm: orderby), service);
        }

        [HttpGet, Route("subscriptiontype/keyvalue")]
        public async Task<IActionResult> ReadKeyValue()
        {
            return await ReadKeyValue<SubscriptionTypeBo, ISubscriptionTypeService>(service);
        }

        [HttpPut, Route("subscriptiontype"), ModelValidation]
        public async Task<IActionResult> Update([FromBody] SubscriptionTypeViewModel item)
        {
            return await Update<SubscriptionTypeViewModel, SubscriptionTypeBo, ISubscriptionTypeService>(item,
                service);
        }
    }
}