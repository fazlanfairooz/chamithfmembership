﻿using Inx.Service.Base.Controllers;

namespace Inx.Module.Membership.Service.Controllers
{
    public class MembershipBaseApiController: BaseApiController
    {
        public MembershipBaseApiController(IBaseInjector baseinject) : base(baseinject)
        {
        }

    }
}
