﻿using Abp.Dependency;
using Hangfire;
using Hangfire.SqlServer;
using Inx.Module.Membership.Core.Service.Settings;
using Inx.Module.Membership.Core.Service.Subscription;
using Inx.Utility;

namespace Inx.Module.Membership.Service.Utility.Config
{
    public class BackgroundProcessConfig
    {
        public static void Register(IIocManager IocManager)
        {
            JobStorage.Current = new SqlServerStorage(GlobleConfig.ConnectionString);
            var backgroundProcess = IocManager.Resolve<BackgroundProcess>();
            var backgroundProcessNotification = IocManager.Resolve<NotificationBackgroundProcess>();

            RecurringJob.AddOrUpdate("ChangeSubscriptionStatusJob", () => backgroundProcess.ChangeSubscriptionStatus(), Cron.Daily);
            RecurringJob.AddOrUpdate("SendRenewalEmailJob", () => backgroundProcess.SendRenewalEmail(), Cron.Daily);

            RecurringJob.AddOrUpdate("AboveToExpire", () => backgroundProcessNotification.AboveToExpire(), Cron.Minutely);
            RecurringJob.AddOrUpdate("HasExpired", () => backgroundProcessNotification.HasExpired(), Cron.Minutely);
            RecurringJob.AddOrUpdate("IsPending", () => backgroundProcessNotification.IsPending(), Cron.Minutely);
        }
    }
}
