﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Inx.Module.Identity.Data.Entity.Application;

namespace Inx.Module.Membership.Data.Entity.MemberGroup
{
    [Table("Group", Schema = Constraints.Schema)]
    public class Group: AuditableEntity
    {
        [StringLength(500)]
        public string Description { get; set; }
    }
}
