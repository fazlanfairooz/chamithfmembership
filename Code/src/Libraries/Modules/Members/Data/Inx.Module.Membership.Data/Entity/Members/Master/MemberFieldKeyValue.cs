﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Inx.Data.Base.Utility;
using Inx.Module.Identity.Data.Entity.Application;

namespace Inx.Module.Membership.Data.Entity.Members.Master
{
    [Table("MemberFieldKeyValue", Schema = Constraints.Schema)]
    public class MemberFieldKeyValue : AuditableEntity
    {
        public int MemberId { get; set; }
        public Guid Key { get; set; }
        [StringLength(DbConstraints.MaxLength)]
        public string Value { get; set; }

        #region not mapped
        [Obsolete,NotMapped]
        public new string Name { get; set; }
        #endregion

        #region relations
        [ForeignKey("MemberId")]
        public Member Member { get; set; }

        #endregion

    }
}
