﻿using Inx.Module.Identity.Data.Entity.Application;
using Inx.Module.Membership.Data.Entity.Members.Master;
using Inx.Utility.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Inx.Module.Membership.Data.Entity.Members
{
    [Table("MemberTimeLines", Schema = Constraints.Schema)]
    public class MemberTimeLine : AuditableEntity
    {
        [NumberNotZero]
        public int MemberId { get; set; }
        public int Year { get; set; }
        public int TimelineTemplateType { get; set; }
        public string Content { get; set; }
        [Obsolete, NotMapped]
        public new string Name { get; set; }
        public DateTime Date { get; set; }
        #region relations
        [ForeignKey("MemberId")]
        public Member Member { get; set; }
        #endregion
    }
}
