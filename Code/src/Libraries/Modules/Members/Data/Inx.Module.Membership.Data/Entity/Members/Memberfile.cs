﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Inx.Module.Identity.Data.Entity.Application;
using Inx.Module.Membership.Data.Entity.Members.Master;
using Inx.Utility.Attributes;

namespace Inx.Module.Membership.Data.Entity.Members
{
    [Table("MemberFiles", Schema = Constraints.Schema)]
    public class MemberFile : AuditableEntity
    {
        [NumberNotZero]
        public int MemberId { get; set; }
        [StringLength(100)]
        public string FileName { get; set; }
        [StringLength(100)]
        public string FileSaveName { get; set; }
        public decimal Size { get; set; }
        [Obsolete,NotMapped]
        public new string Name { get; set; }
        #region relations
        [ForeignKey("MemberId")]
        public Member Member { get; set; }
        #endregion
    }
}
