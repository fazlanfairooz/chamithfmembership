﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Inx.Module.Identity.Data.Entity.Application;
using Inx.Utility.Utility;

namespace Inx.Module.Membership.Data.Entity.Settings.CustomFieldDefinition
{
    [Table("MemberTypeTab", Schema = Constraints.Schema)]
    public class MemberTypeTab : AuditableEntity
    {
        public int TabId { get; set; }
        public Enums.MemberType MemberType { get; set; }

        #region Relations
        [ForeignKey("TabId")]
        public MemberTabDefinition MemberTabDefinition { get; set; }
        #endregion

        [NotMapped, Obsolete]
        public new string Name { get; set; }

    }
}
