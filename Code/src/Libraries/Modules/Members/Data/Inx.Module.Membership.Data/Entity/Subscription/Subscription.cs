﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Inx.Module.Identity.Data.Entity.Application;
using Inx.Utility.Attributes;
using Inx.Utility.Utility;
using System.ComponentModel.DataAnnotations;

namespace Inx.Module.Membership.Data.Entity.Subscription
{
    [Table("Subscription", Schema = Constraints.Schema)]
    public class Subscription : AuditableEntity
    {
        [Key]
        public new int Id { get; set; }
        [NumberNotZero]
        public decimal Price { get; set; }
        public bool IsLimited { get; set; }
        public int Capacity { get; set; }
        public DateTime SubscriptionFrom { get; set; } = DateTime.UtcNow;
        public DateTime SubscriptionTo { get; set; }
        public Enums.MembershipExpired MembershipExpired { get; set; } = Enums.MembershipExpired.None;
        public bool IsRenewable { get; set; } = true;
        public bool IsSendEmail { get; set; } 
        public int RenewalDays { get; set; }
        public int EmaillTemplateId { get; set; }
        public bool HasApproval { get; set; } = false;
        #region relations
        [ForeignKey("Id")]
        public SubscriptionType SubscriptionType { get; set; }
        #endregion

        [Obsolete,NotMapped]
        public new string Name { get; set; }
    }
}
