﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Inx.Module.Identity.Data.Entity.Application;
using Inx.Module.Membership.Data.Entity.Members;
using Inx.Module.Membership.Data.Entity.Members.Master;
using Inx.Utility.Utility;
namespace Inx.Module.Membership.Data.Entity.Subscription
{
    [Table("MemberSubscription", Schema = Constraints.Schema)]
    public class MemberSubscription : AuditableEntity
    {
        public int MemberId { get; set; }
        public int SubscriptinId { get; set; }
        public DateTime RenewDate { get; set; } = DateTime.UtcNow;
        public Enums.MembershipStatus MembershipStatus { get; set; } = Enums.MembershipStatus.Pending;
        public DateTime JoinDate { get; set; } = DateTime.UtcNow;
        public bool IsRenewed { get; set; } = false;
        [ForeignKey("SubscriptinId")]
        public Subscription Subscription { get; set; }
        [ForeignKey("MemberId")]
        public Member MemberInfo { get; set; }
        [Obsolete,NotMapped]
        public new string Name { get; set; }
    }
}
