﻿using System.ComponentModel.DataAnnotations.Schema;
using Inx.Module.Identity.Data.Entity.Application;

namespace Inx.Module.Membership.Data.Entity.Subscription
{
    [Table("TerminationNote", Schema = Constraints.Schema)]
    public class TerminationNote : AuditableEntity
    {
        public string Notes { get; set; }
        public int MemberSubscriptionId { get; set; }
        [ForeignKey("MemberSubscriptionId")]
        public MemberSubscription Subscription { get; set; }
        [NotMapped]
        public new string Name { get; set; }
    }
}
