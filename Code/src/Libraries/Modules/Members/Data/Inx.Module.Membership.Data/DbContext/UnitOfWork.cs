﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Inx.Data.Base.Repository;
using Inx.Module.Identity.Data.Entity.Application;
using Inx.Module.Membership.Data.Entity.MemberGroup;
using Inx.Module.Membership.Data.Entity.Members;
using Inx.Module.Membership.Data.Entity.Members.Master;
using Inx.Module.Membership.Data.Entity.Settings;
using Inx.Module.Membership.Data.Entity.Settings.CustomFieldDefinition;
using Inx.Module.Membership.Data.Entity.Subscription;
using Microsoft.EntityFrameworkCore;

namespace Inx.Module.Membership.Data.DbContext
{
    public partial class UnitOfWork : IUnitOfWokMembership
    {
        #region fields

        private bool disposed;
        public Microsoft.EntityFrameworkCore.DbContext Context { get; }

        #region contact

        private GenericRepository<Member> memberRepository;
        private GenericRepository<OrganizationInfo> organizationInfoRepository;
        private GenericRepository<PersonalInfo> personalInfoRepository;
        private GenericRepository<MemberFieldKeyValue> memberFieldKeyValueRepository;

        private GenericRepository<Group> membershipGroupRepository;
        private GenericRepository<SubscriptionType> subscriptionTypeRepository;
        private GenericRepository<Subscription> subscriptionRepository;
        private GenericRepository<MemberFile> memberFileRepository;
        private GenericRepository<MemberSubscription> memberSubscriptionRepository; 
        private GenericRepository<MemberGroup> memberGroupRepository;
        private GenericRepository<TerminationNote> terminationNoteRepository;
        private GenericRepository<MembershipTimeline> memberTimelineRepository;

        //definitions
        private GenericRepository<MemberFieldDefinition> memberTabFieldDefinitionRepository;
        private GenericRepository<MemberTabDefinition> memberTabDefinitionRepository;
        private GenericRepository<MemberTypeTab> memberTypeTabRepository;
        private GenericRepository<MemberTabField> memberTabFieldsRepository;
        private GenericRepository<MemberFieldRequired> memberFieldRequiredRepository;
        #endregion

        public string ConnectionString => Context.Database.GetDbConnection().ConnectionString;

        #endregion

        #region ctor

        public UnitOfWork(IMembershipDbSet dbcontext)
        {
            Context = (MembershipDbContext) dbcontext;
        }

        #endregion

        #region repositories

        public GenericRepository<Group> GroupRepository =>
            membershipGroupRepository ?? (membershipGroupRepository = new GenericRepository<Group>(Context));

        public GenericRepository<SubscriptionType> SubscriptionTypeRepository =>
            subscriptionTypeRepository ??
            (subscriptionTypeRepository = new GenericRepository<SubscriptionType>(Context));

        public GenericRepository<Subscription> SubscriptionRepository =>
            subscriptionRepository ?? (subscriptionRepository = new GenericRepository<Subscription>(Context));

        public GenericRepository<MemberFile> MemberFileRepository =>
            memberFileRepository ?? (memberFileRepository = new GenericRepository<MemberFile>(Context));

        public GenericRepository<MemberSubscription> MemberSubscriptionRepository =>
            memberSubscriptionRepository ??
            (memberSubscriptionRepository = new GenericRepository<MemberSubscription>(Context));
 
        public GenericRepository<MemberGroup> MemberGroupRepository =>
            memberGroupRepository ?? (memberGroupRepository = new GenericRepository<MemberGroup>(Context));

        public GenericRepository<TerminationNote> TerminationNoteRepository =>
            terminationNoteRepository ?? (terminationNoteRepository = new GenericRepository<TerminationNote>(Context));

        public GenericRepository<MembershipTimeline> MembershipTimeRepository =>
            memberTimelineRepository ?? (memberTimelineRepository = new GenericRepository<MembershipTimeline>(Context));

        public GenericRepository<OrganizationInfo> OrganizationInfoRepository =>
            organizationInfoRepository ??
            (organizationInfoRepository = new GenericRepository<OrganizationInfo>(Context));

        public GenericRepository<PersonalInfo> PersonalInfoRepository =>
            personalInfoRepository ?? (personalInfoRepository = new GenericRepository<PersonalInfo>(Context));

        public GenericRepository<MemberTabDefinition> MemberTabDefinitionRepository =>
            memberTabDefinitionRepository ?? (memberTabDefinitionRepository = new GenericRepository<MemberTabDefinition>(Context));

        public GenericRepository<MemberFieldDefinition> MemberFieldDefinitionRepository =>
            memberTabFieldDefinitionRepository ?? (memberTabFieldDefinitionRepository = new GenericRepository<MemberFieldDefinition>(Context));

        public GenericRepository<MemberTypeTab> MemberTypeTabRepository =>
          memberTypeTabRepository ?? (memberTypeTabRepository = new GenericRepository<MemberTypeTab>(Context));

        public GenericRepository<Member> MemberRepository =>
            memberRepository ?? (memberRepository = new GenericRepository<Member>(Context));

        public GenericRepository<MemberTabField> MemberTabFieldsRepository =>
            memberTabFieldsRepository ?? (memberTabFieldsRepository = new GenericRepository<MemberTabField>(Context));

        public GenericRepository<MemberFieldKeyValue> MemberFieldKeyValueRepository =>
            memberFieldKeyValueRepository ?? (memberFieldKeyValueRepository = new GenericRepository<MemberFieldKeyValue>(Context));

        public GenericRepository<MemberFieldRequired> MemberFieldRequiredRepository =>
            memberFieldRequiredRepository ?? (memberFieldRequiredRepository = new GenericRepository<MemberFieldRequired>(Context));


        #endregion

        #region methods

        public void Save()
        {
            Context.SaveChanges();
        }

        public async Task SaveAsync()
        {
            var auditableEntires = Context.ChangeTracker.Entries<AuditableEntity>()
                .Where(p => p.State == EntityState.Modified)
                .Select(p => p.Entity);

            await Context.SaveChangesAsync();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    Context.Dispose();
                }
            }

            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
