﻿using Inx.Data.Base.Repository;
using Inx.Module.Membership.Data.Entity.MemberGroup;
using Inx.Module.Membership.Data.Entity.Members;
using Inx.Module.Membership.Data.Entity.Members.Master;
using Inx.Module.Membership.Data.Entity.Settings;
using Inx.Module.Membership.Data.Entity.Settings.CustomFieldDefinition;
using Inx.Module.Membership.Data.Entity.Subscription;

namespace Inx.Module.Membership.Data.DbContext
{
    public interface IUnitOfWokMembership : IUnitOfWork
    {
        GenericRepository<Member> MemberRepository { get; }
        GenericRepository<OrganizationInfo> OrganizationInfoRepository { get; }
        GenericRepository<PersonalInfo> PersonalInfoRepository { get; }
        GenericRepository<MemberFieldKeyValue> MemberFieldKeyValueRepository { get; }

        GenericRepository<Group> GroupRepository { get; }
        GenericRepository<SubscriptionType> SubscriptionTypeRepository { get; }
        GenericRepository<Subscription> SubscriptionRepository { get; }
        GenericRepository<MemberFile> MemberFileRepository { get; }
        GenericRepository<MemberSubscription> MemberSubscriptionRepository { get; }
        GenericRepository<MemberGroup> MemberGroupRepository { get; } 
        GenericRepository<TerminationNote> TerminationNoteRepository { get; }
        GenericRepository<MembershipTimeline> MembershipTimeRepository { get; }

        //definitions
        GenericRepository<MemberTabDefinition> MemberTabDefinitionRepository { get; }
        GenericRepository<MemberFieldDefinition> MemberFieldDefinitionRepository { get; }
        GenericRepository<MemberTypeTab> MemberTypeTabRepository { get; }
        GenericRepository<MemberTabField> MemberTabFieldsRepository { get; }
        GenericRepository<MemberFieldRequired> MemberFieldRequiredRepository { get; }
    }
}
