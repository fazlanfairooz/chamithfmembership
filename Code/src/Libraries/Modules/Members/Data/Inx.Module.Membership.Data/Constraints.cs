﻿using Inx.Data.Base.Utility;
namespace Inx.Module.Membership.Data
{
    public class Constraints : DbConstraints
    {
        public new const string Schema = "Inexis.Membership";
        public const int IdentityLength = 50;
    }
}
