﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Inx.Module.Membership.Data.Migrations
{
    public partial class adding_isSytemTab_tabDefinition : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsSystemTab",
                schema: "Inexis.Membership",
                table: "MemberTabDefinition",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsSystemTab",
                schema: "Inexis.Membership",
                table: "MemberTabDefinition");
        }
    }
}
