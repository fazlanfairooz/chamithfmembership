﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Inx.Module.Membership.Data.Migrations
{
    public partial class Adding_field_tooltip : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Placeholder",
                schema: "Inexis.Membership",
                table: "MemberFieldDefinition",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ToolTip",
                schema: "Inexis.Membership",
                table: "MemberFieldDefinition",
                maxLength: 200,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Placeholder",
                schema: "Inexis.Membership",
                table: "MemberFieldDefinition");

            migrationBuilder.DropColumn(
                name: "ToolTip",
                schema: "Inexis.Membership",
                table: "MemberFieldDefinition");
        }
    }
}
