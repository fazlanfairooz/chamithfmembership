﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Inx.Module.Membership.Data.Migrations
{
    public partial class removing_tabheader : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TabHeader",
                schema: "Inexis.Membership",
                table: "MemberTabDefinition");

            migrationBuilder.RenameColumn(
                name: "TabSubHeader",
                schema: "Inexis.Membership",
                table: "MemberTabDefinition",
                newName: "TabDescription");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TabDescription",
                schema: "Inexis.Membership",
                table: "MemberTabDefinition",
                newName: "TabSubHeader");

            migrationBuilder.AddColumn<string>(
                name: "TabHeader",
                schema: "Inexis.Membership",
                table: "MemberTabDefinition",
                maxLength: 50,
                nullable: false,
                defaultValue: "");
        }
    }
}
