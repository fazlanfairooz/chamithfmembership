﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Inx.Module.Membership.Data.Migrations
{
    public partial class Adding_HeadersToTab : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "TabHeader",
                schema: "Inexis.Membership",
                table: "MemberTabDefinition",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "TabSubHeader",
                schema: "Inexis.Membership",
                table: "MemberTabDefinition",
                maxLength: 200,
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TabHeader",
                schema: "Inexis.Membership",
                table: "MemberTabDefinition");

            migrationBuilder.DropColumn(
                name: "TabSubHeader",
                schema: "Inexis.Membership",
                table: "MemberTabDefinition");
        }
    }
}
