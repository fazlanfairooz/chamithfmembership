﻿using Inx.Module.Membership.Core.Bo.Subscription;
using Inx.Module.Membership.Core.Service.Subscription;
using Inx.Module.Membership.Core.Service.Subscription.Interfaces;
using Inx.Module.Membership.Test.Utility;
using Inx.Test.Base.Core;
using Inx.Utility.Cache.Runtime;
using Inx.Utility.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace Inx.Module.Membership.Test.IntegrationTesting
{
    public class SubscriptionTypeTest : CoreTestBase, ICoreTestBase
    {
        private ISubscriptionTypeService service;
        //[TestInitialize]
        public void Initialize()
        {
            base.MockCache<PageList<SubscriptionTypeBo>>();
            service = new SubscriptionTypeService(new Comman().IUnitOfWokMembership, null);
        }

        [DataTestMethod]
        [DataRow("test Gold", "testdescription")] //Pass
        [DataRow("test Silver", "test description")] //Pass
        [DataRow("test Gold", "test description")] //Dupicate Fail
        [DataRow("", "test description")] //Name Null Fail
        [DataRow("testtesttesttesttesttesttesttesttesttesttesttesttesttesttest", "test description")] //Name Lengh Fail
        [ExpectedException(typeof(NullReferenceException))]
        public async Task Create(string name, string description)
        {
            try
            {
                var mock = new Mock<IMemoryCacheManager>();
                //mock.Setup(foo => foo.Remove(It.IsAny<string>())).Returns(Task.FromResult(true));
                var bo = new SubscriptionTypeBo
                {
                    Name = name,
                    Description = description
                };
                await base.Create(bo, service);
                True("MemebrLevelTest=>Create");
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        [DataTestMethod]
        [DataRow(1)] //Pass
        [DataRow(1545)] //Record Not Found Fail
        [ExpectedException(typeof(NullReferenceException))]
        public async Task Delete(int id)
        {
            try
            {
                await base.Delete<SubscriptionTypeBo, ISubscriptionTypeService>(Convert.ToInt32(id), service);
                True("MemebrLevelTest=>Delete");
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        [DataTestMethod]
        [DataRow(0, 25, "searchTerm")] //SearchTeam 
        [ExpectedException(typeof(NullReferenceException))]
        public async Task Read(int skip, int take, string search)
        {
            try
            {
                await base.Read<SubscriptionTypeBo, ISubscriptionTypeService>(
                    base.TestSearchRequest(skip: skip, take: take
                        , searchTerm: search, fk: 0, orderByTerm: string.Empty), service);
                True("MemebrLevelTest=>Read");
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [DataTestMethod]
        [DataRow(1)] //Pass
        [DataRow(1000)] //Record Not Found Fail
        public async Task ReadById(int id)
        {
            try
            {
                await service.Read(TestRequest(id));
                True("MemebrLevelTest=>ReadById");
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        [DataTestMethod]
        [DataRow(1)]
        public async Task ReadKeyValue()
        {
            try
            {
                await base.ReadKeyValue<SubscriptionTypeBo, ISubscriptionTypeService>(service);
                True("MemebrLevelTest=>ReadKeyValue");
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        [DataTestMethod]
        [DataRow(1)]
        [ExpectedException(typeof(NullReferenceException))]
        public async Task Update()
        {
            try
            {
                await base.Update(new SubscriptionTypeBo()
                {
                }, service);
                True("MemebrLevelTest=>Update");
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        public Task Search(int skip, int take, string search)
        {
            throw new NotImplementedException();
        }

        public new Task Create()
        {
            throw new NotImplementedException();
        }
    }
}
