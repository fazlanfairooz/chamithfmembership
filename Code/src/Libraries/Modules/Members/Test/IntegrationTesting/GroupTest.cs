﻿using Inx.Module.Contacts.Core.Bo.Contacts;
using Inx.Module.Contacts.Core.Service.Contacts.interfaces;
using Inx.Module.Membership.Core.Bo.Groups;
using Inx.Module.Membership.Core.Service.Members;
using Inx.Module.Membership.Core.Service.Members.interfaces;
using Inx.Module.Membership.Test.Utility;
using Inx.Test.Base.Core;
using Inx.Utility.Cache.Runtime;
using Inx.Utility.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace Inx.Module.Membership.Test.IntegrationTesting
{
    public class GroupTest : CoreTestBase, ICoreTestBase
    {
        private IMembershipGroupService service;
        //[TestInitialize]
        public void Initialize()
        {
            base.MockCache<PageList<MembershipGroupBo>>();
            service = new MembershipGroupService(new Comman().IUnitOfWokMembership, null);
        }

        [DataTestMethod]
        [DataRow("test Group", "testdescription")] //Pass
        [DataRow("test 18+", "test description")] //Pass
        [DataRow("test Group", "test description")] //Dupicate Fail
        [DataRow("", "test description")] //Name Null Fail
        [DataRow("testtesttesttesttesttesttesttesttesttesttesttesttesttesttest", "test description")] //Name Lengh Fail
        [ExpectedException(typeof(NullReferenceException))]
        public async Task Create(string name, string description)
        {
            try
            {
                var mock = new Mock<IMemoryCacheManager>();
                mock.Setup(foo => foo.Remove(It.IsAny<string>())).Returns(Task.FromResult(true));
                var bo = new MembershipGroupBo
                {
                    Name = name
                };
                await base.Create(bo, service);
                True("MemebrGroupTest=>Create");
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        [DataTestMethod]
        [DataRow(1)] //Pass
        [DataRow(1000)] //Record Not Found Fail
        [ExpectedException(typeof(NullReferenceException))]
        public async Task Delete(int id)
        {
            try
            {
                await base.Delete<MembershipGroupBo, IMembershipGroupService>(Convert.ToInt32(id), service);
                True("MemebrGroupTest=>Delete");
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        [DataTestMethod]
        [DataRow(0, 25, "searchTerm")] //SearchTeam 
        [ExpectedException(typeof(NullReferenceException))]
        public async Task Read(int skip, int take, string search)
        {
            try
            {
                await base.Read<MembershipGroupBo, IMembershipGroupService>(
                    base.TestSearchRequest(skip: skip, take: take
                        , searchTerm: search, fk: 0, orderByTerm: string.Empty), service);
                True("MemebrGroupTest=>Read");
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [DataTestMethod]
        [DataRow(1)] //Pass
        [DataRow(1000)] //Record Not Found Fail
        public async Task ReadById(int id)
        {
            try
            {
                await service.Read(TestRequest(id));
                True("MemebrGroupTest=>ReadById");
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        [DataTestMethod]
        [DataRow(1)]
        public async Task ReadKeyValue()
        {
            try
            {
                await base.ReadKeyValue<MembershipGroupBo, IMembershipGroupService>(service);
                True("MemebrGroupTest=>ReadKeyValue");
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        [DataTestMethod]
        [DataRow(1)]
        [ExpectedException(typeof(NullReferenceException))]
        public async Task Update()
        {
            try
            {
                await base.Update(new MembershipGroupBo()
                {
                }, service);
                True("MemebrGroupTest=>Update");
            }
            catch (Exception e)
            {
                throw e;
            }
        }
       
        public new Task Create()
        {
            throw new NotImplementedException();
        }

        public Task Search(int skip, int take, string search)
        {
            throw new NotImplementedException();
        }
    }
}
