using Abp.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc;
namespace Inx.Membership.Web.Host.Controllers
{
    public class HomeController : AbpController
    {
        [HttpGet]
        public IActionResult Api()
        {
            return Redirect("/swagger");
        }
    }
}
