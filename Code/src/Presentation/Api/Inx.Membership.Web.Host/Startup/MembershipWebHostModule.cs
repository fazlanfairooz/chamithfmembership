﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Inx.Membership.Web.Host.Configuration;
using Inx.Module.Contacts.Service;
using Inx.Module.Identity.Service;
using Inx.Module.Membership.Service;
using Inx.Module.Messenger.Service;
using Inx.Module.Payment.Service;

namespace Inx.Membership.Web.Host.Startup
{
    [DependsOn(
        typeof(IdentityApplicationModule),
        typeof(MembershipServiceModule),
        typeof(MessengerServiceModule),
        typeof(ContactsServiceModule),
        typeof(PaymentServiceModule)

        )]
    public class MembershipWebHostModule : AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public MembershipWebHostModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(MembershipWebHostModule).GetAssembly());
        }
    }
}
