import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemberCardViewComponent } from './member-card-view.component';

describe('MemberCardViewComponent', () => {
  let component: MemberCardViewComponent;
  let fixture: ComponentFixture<MemberCardViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemberCardViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberCardViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
