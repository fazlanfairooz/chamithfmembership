import { Injectable } from '@angular/core';

@Injectable()
export class SortBaseService {

  constructor() { }

  changeSorting(columnName, paginationModel): void {

    if (paginationModel.sortColumnName == columnName) {
      paginationModel.isDescending = !paginationModel.isDescending;
    } else {
      paginationModel.sortColumnName = columnName;
      paginationModel.isDescending = false;
    }

    paginationModel.orderType = paginationModel.isDescending ? "desc" : "asc";
    paginationModel.orderByTerm = `${paginationModel.sortColumnName}:${paginationModel.orderType}`

  }

  selectedClass(columnName, paginationModel): string {
    return columnName == paginationModel.sortColumnName ? 'sort-' + paginationModel.isDescending : "false";

  }
}
