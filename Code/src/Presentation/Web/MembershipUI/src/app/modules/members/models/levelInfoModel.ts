import { MembershipLevelStates } from "../../core/enums/membershipLevelState";

export class LevelInfoModel {
    capacity: number;
    createdOn: Date;
    description: string;
    editedOn: Date;
    emaillTemplateId: number;
    id: number;
    isLimited: boolean;
    isRenewable: boolean;
    isSendEmail: boolean;
    membershipExpired: MembershipLevelStates;
    name: string;
    price: number;
    renewalDays: number;
    subscriptionFrom: Date;
    subscriptionTo: Date;
    subscriptionTypeId: number;
    tenantId: number;
    remainingCount: number;
    utilizeCount: number;
}