import { Injectable } from '@angular/core';
import { BaseService } from '../../core/services/base.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CustomFieldService extends BaseService {

  private renderViewEndPoint = `${this.baseApiEndPoint}membership/v1/settings/`;
  private tabDefinitionEndPoint = `${this.baseApiEndPoint}membership/v1/tabdefinition`;
  private fieldDefinitionEndPoint = `${this.baseApiEndPoint}membership/v1/fielddefinition`;

  constructor(private http: HttpClient) {
    super();
  }

  getRenderFieldData(memberType) {
    return this.http.get(`${this.renderViewEndPoint}membertype/${memberType}/wizardrenderinfo`)
      .map((response) => response)
      .catch(this.errorHandler)
  }

  createTab(tab) {
    return this.http.post(`${this.tabDefinitionEndPoint}`, tab)
      .map((response) => response)
      .catch(this.errorHandler)
  }

  updateTab(tab) {
    return this.http.put(`${this.tabDefinitionEndPoint}`, tab)
      .map((response) => response)
      .catch(this.errorHandler)
  }
  
  getTabById(id){
    return this.http.get(`${this.tabDefinitionEndPoint}/${id}`)
    .map((response) => response)
    .catch(this.errorHandler)
  }

  createField(field) {
    return this.http.post(`${this.fieldDefinitionEndPoint}`, field)
      .map((response) => response)
      .catch(this.errorHandler)
  }

  getFieldById(id){
    return this.http.get(`${this.fieldDefinitionEndPoint}/${id}`)
    .map((response) => response)
    .catch(this.errorHandler)
  }
  
  updateField(field) {
    return this.http.put(`${this.fieldDefinitionEndPoint}`, field)
      .map((response) => response)
      .catch(this.errorHandler)
  }

  deleteField(id) {
    return this.http.delete(`${this.fieldDefinitionEndPoint}/${id}`, id)
      .map((response) => response)
      .catch(this.errorHandler)
  }
}
