import { ModalDirective } from 'ngx-bootstrap/modal';
import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { OnChanges } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'membership-email-preview',
  templateUrl: './email-preview.component.html',
  styleUrls: ['./email-preview.component.scss']
})
export class EmailPreviewComponent implements OnInit,OnChanges {

  @ViewChild('emailPreviewTemplateModal') templateModal: ModalDirective;

  @Input() templateHtml: string;
  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(){
    console.log(this.templateHtml)
  }

  public showTemplateModal(): void {
    this.templateModal.show();

  }
  selectTemplate(){
    
  }
  hideTemplateModal(){
    this.templateModal.hide();
  }

}
