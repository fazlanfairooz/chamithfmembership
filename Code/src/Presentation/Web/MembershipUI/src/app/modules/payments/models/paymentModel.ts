export class PaymentModel {
    id: number;
    memberSubscriptionId: number;
    paymentMasterId: number;
    memberId: number;
    amount: number;
    transActionType: number;
    paymentType: number;
    paymentState: number;
    description: string;
    name: string;
}