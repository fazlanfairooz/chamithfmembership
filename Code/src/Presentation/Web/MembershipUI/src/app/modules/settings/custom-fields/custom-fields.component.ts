import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { DragulaService } from 'ng2-dragula';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { HTMLElements } from '../../core/enums/htmlElement';
import { CustomFieldService } from '../services/custom-field.service';
import { MemberType } from '../../core/enums/memberType';
import { Field } from '../models/field';
import { Tab } from '../models/tab';
import { ToasterMessages } from '../../core/utility/toasterMessages';
import { toasterHeadings } from '../../core/utility/toasterHeadings';
import { ToastsManager } from '../../../../../node_modules/ng2-toastr';

@Component({
  selector: 'membership-custom-fields',
  templateUrl: './custom-fields.component.html',
  styleUrls: ['./custom-fields.component.scss']
})
export class CustomFieldsComponent implements OnInit {

  @ViewChild('addNewFieldModal') public addNewFieldModal: ModalDirective;
  @ViewChild('addNewTabModal') public addNewTabModal: ModalDirective;

  fieldForm: FormGroup;
  tabForm: FormGroup;
  memberType = MemberType;

  field = new Field();
  tab = new Tab();

  isMultipleElement = false;
  isEditField = false;
  isEditTab = false;
  isBlocked = false;
  isTabFormSubmitted = false;
  isFieldFormSubmitted = false;

  ddValues = [];
  customTabData = [];
  ddValue: string = '';
  selectedType: number;
  selectedTab: number;
  type: number;
  fieldHeader = 'Add New Field';
  tabHeader = 'Add New Tab';

  types = [
    { 'value': 1, text: 'Text' },
    { 'value': 2, text: 'TextArea' },
    { 'value': 3, text: 'CheckBox' },
    { 'value': 4, text: 'CheckboxGroup' },
    { 'value': 6, text: 'RadioButtonGroup' },
    { 'value': 7, text: 'Dropdown' },
    { 'value': 8, text: 'Calendar' },
    { 'value': 9, text: 'File' },
    { 'value': 10, text: 'Email' }
  ];

  constructor(private fb: FormBuilder, private dragulaService: DragulaService, private customFieldService: CustomFieldService,
    private toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);

    const bagTab: any = this.dragulaService.find('bag-tab-data');
    if (bagTab !== undefined) this.dragulaService.destroy('bag-tab-data');

    const bagField: any = this.dragulaService.find('bag-tabfield-data');
    if (bagField !== undefined) this.dragulaService.destroy('bag-tabfield-data');

    dragulaService.createGroup("bag-tab-data", {
      moves: (el, container, handle) => {
        return handle.className === 'bagtabdata';
      },
    });

    dragulaService.createGroup("bag-tabfield-data", {
      revertOnSpill: true
    });
  }

  ngOnInit() {
    this.getFieldForm();
    this.getTabForm();
    this.selectedType = this.memberType.member;
    this.getTabFields(this.selectedType);
  }

  getFieldForm() {
    this.fieldForm = this.fb.group({
      displayName: ["", [Validators.required]],
      dataType: [null, [Validators.required]],
      value: [""],
      isRequired: [false],
      placeholder: [""],
      toolTip: [""]
    });
  }

  getTabForm() {
    this.tabForm = this.fb.group({
      displayName: ["", [Validators.required]],
      isVisible: [true],
      tabDescription: [""],
    });
  }

  showFieldModal(tabId) {
    this.selectedTab = tabId;
    this.fieldForm.reset();
    this.isMultipleElement = false;
    this.addNewFieldModal.show();
  }

  getTabFields(memberType) {
    this.customTabData = [];
    this.selectedType = memberType;
    this.customFieldService.getRenderFieldData(memberType)
      .subscribe(tabFields => {
        this.customTabData = tabFields.result.memberTypeTabs;
      }, error => {
        this.toastr.error("Error in loading tab fields", toasterHeadings.Error);
      });
  }

  hideAddFieldModal(): void {
    this.isEditField = false;
    this.fieldForm.reset();
    this.fieldHeader = 'Add New Field';
    this.addNewFieldModal.hide();
  }

  hideAddTabModal(): void {
    this.isEditTab = false;
    this.tabForm.reset();
    this.tabHeader = 'Add New Tab'
    this.addNewTabModal.hide();
  }

  deleteField(id) {
    this.customFieldService.deleteField(id)
      .subscribe(res => {
        this.getTabFields(this.selectedType);
        this.toastr.success("Field has been successfully deleted", toasterHeadings.Success);
      }, error => {
        this.toastr.error("Error in deleting field", toasterHeadings.Error);
      });
  }

  editField(id) {
    this.isMultipleElement = false;
    this.ddValues = [];
    this.fieldHeader = 'Update Field';
    this.getFieldById(id);
    this.isEditField = true;
    this.addNewFieldModal.show();
  }

  editTab(id) {
    this.getTabById(id);
    this.isEditTab = true;
    this.tabHeader = 'Update Tab';
    this.addNewTabModal.show();
  }

  getFieldById(id) {
    this.customFieldService.getFieldById(id)
      .subscribe(res => {
        this.patchFieldForm(res.result.item);
      }, error => {
        this.toastr.error("Error in loading field data", toasterHeadings.Error);
      });
  }

  getTabById(id) {
    this.customFieldService.getTabById(id)
      .subscribe(res => {
        this.patchTabForm(res.result.item);
      }, error => {
        this.toastr.error("Error in loading field data", toasterHeadings.Error);
      });
  }

  private patchTabForm(tab: Tab) {
    this.tab = tab;
    this.tabForm.patchValue({
      displayName: this.tab.displayName,
      isVisible: this.tab.isVisible,
      tabDescription: this.tab.tabDescription,
    });
  }

  private patchFieldForm(field: Field) {
    this.field = field;
    this.fieldForm.patchValue({
      displayName: this.field.displayName,
      dataType: this.field.dataType,
      value: this.field.value,
      isRequired: this.field.isRequired,
      placeholder: this.field.placeholder,
      toolTip: this.field.toolTip,
    });

    if (this.field.dataType == HTMLElements.dropdown || this.field.dataType == HTMLElements.checkboxGroup || this.field.dataType == HTMLElements.radioButtonGroup) {
      this.isMultipleElement = true;
      this.ddValues = JSON.parse(this.field.value);
    }
  }

  multipleSelector(event) {
    this.type = event.currentTarget.value;
    if (this.type == HTMLElements.dropdown || this.type == HTMLElements.checkboxGroup || this.type == HTMLElements.radioButtonGroup) {
      this.isMultipleElement = true;
    } else {
      this.isMultipleElement = false;
    }
  }

  addMultiChoiceValues() {
    if (this.ddValue != '') {
      this.ddValues.push({ 'value': this.ddValues.length + 1, 'text': this.ddValue });
      this.ddValue = '';
    }
  }

  deleteChoice(id) {
    const index = this.ddValues.findIndex(v => v.value == id);
    this.ddValues.splice(index, 1);
  }

  addNewTab() {
    this.isTabFormSubmitted = true;
    if (this.tabForm.invalid) return;

    this.tab = Object.assign({}, this.tab, this.tabForm.value);
    this.tab.isVisible = true;
    this.tab.memberType.push(this.selectedType);
    this.customFieldService.createTab(this.tab)
      .subscribe(res => {
        this.getTabFields(this.selectedType);
        this.tabHeader = 'Add New Tab'
        this.hideAddTabModal();
        this.isTabFormSubmitted = false;
        this.tab = new Tab();
        this.toastr.success("Tab has been successfully created", toasterHeadings.Success);
      }, error => {
        this.toastr.error("Error in creating tab", toasterHeadings.Error);
      });
  }

  updateTab() {
    this.isTabFormSubmitted = true;
    if (this.tabForm.invalid) return;

    this.tab = Object.assign({}, this.tab, this.tabForm.value);
    this.tab.memberType.push(this.selectedType);

    this.customFieldService.updateTab(this.tab)
      .subscribe(res => {
        this.getTabFields(this.selectedType);
        this.tabHeader = 'Add New Tab'
        this.hideAddTabModal();
        this.tab = new Tab();
        this.isTabFormSubmitted = false;
        this.toastr.success("Tab has been successfully updated", toasterHeadings.Success);
      }, error => {
        this.toastr.error("Error in updating tab", toasterHeadings.Error);
      });
  }

  addNewField() {
    this.isFieldFormSubmitted = true;

    if (this.fieldForm.value.isRequired == null) {
      this.fieldForm.value.isRequired = false;
    }

    if (this.fieldForm.invalid) return;
    this.field = Object.assign({}, this.field, this.fieldForm.value);
    this.field.tabId = this.selectedTab;
    this.field.value = '';

    if (this.type == HTMLElements.dropdown || this.type == HTMLElements.checkboxGroup || this.type == HTMLElements.radioButtonGroup) {
      this.field.value = JSON.stringify(this.ddValues);
    }

    this.customFieldService.createField(this.field)
      .subscribe(res => {
        this.getTabFields(this.selectedType);
        this.fieldHeader = 'Add New Field';
        this.hideAddFieldModal();
        this.isFieldFormSubmitted = false;
        this.toastr.success("Field has been successfully created", toasterHeadings.Success);
      }, error => {
        this.toastr.error("Error in creating field", toasterHeadings.Error);
      });
  }

  updateField() {
    this.isFieldFormSubmitted = true;
    if (this.fieldForm.invalid) return;

    this.field = Object.assign({}, this.field, this.fieldForm.value);
    this.field.value = '';

    if (this.type == HTMLElements.dropdown || this.type == HTMLElements.checkboxGroup || this.type == HTMLElements.radioButtonGroup) {
      this.field.value = JSON.stringify(this.ddValues);
    }

    this.customFieldService.updateField(this.field)
      .subscribe(res => {
        this.getTabFields(this.selectedType);
        this.hideAddFieldModal();
        this.fieldHeader = 'Add New Field';
        this.isFieldFormSubmitted = false;
        this.toastr.success("Field has been successfully updated", toasterHeadings.Success);
      }, error => {
        this.toastr.error("Error in updating field", toasterHeadings.Error);
      });
  }

  saveChanges() {

  }
}
