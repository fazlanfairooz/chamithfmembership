import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PatternModel } from '../../shared/patternvalidator/pattern';
import { Router } from '@angular/router';
import { RegisteruserService } from '../service/registeruser.service';
import { ToastsManager } from 'ng2-toastr';
import { patternValidator } from '../../shared/patternvalidator/patternvalidator';
import { ToasterMessages } from '../../core/utility/toasterMessages';
import { toasterHeadings } from '../../core/utility/toasterHeadings';

@Component({
  selector: 'membership-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  isRequested = false;
  email: string;
  isMailSent = false;
  forgotpasswordForm: FormGroup;
  patternModel = new PatternModel();
  constructor(private fb: FormBuilder, private router: Router, private registerUser: RegisteruserService, public toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
  }
  createform() {
    this.forgotpasswordForm = this.fb.group({
      resetEmail: ['', [Validators.required, patternValidator(this.patternModel.emailRegex)]],
    });
  }
  sendResetEmail() {
    if (!this.forgotpasswordForm.valid) return;
    this.isRequested = true;
    var email = this.forgotpasswordForm.value.resetEmail;
    var emailObj = { value: email }
    this.registerUser.sendForgotpasswordMail(emailObj)
      .subscribe(res => {
        this.isMailSent = true;
        this.isRequested = false;
      }, error => {
        this.isMailSent = false;
        this.isRequested = false;
        this.toastr.error('User doesn\'t exist with the provided mail', 'Oops');
      })
  }
  goBack() {
    this.isMailSent = false;
  }
  ngOnInit() {
    this.createform();
  }

}
