import { ProfileImageElement } from './elements/profile-image-element';
import { Injectable } from '@angular/core';
import { CheckboxElement } from './elements/checkbox-element';
import { TextboxElement } from './elements/textbox-element';
import { DropdownElement } from './elements/dropdown-element';
import { TextAreaElement } from './elements/textarea-element';
import { CalendarElement } from './elements/calendar-element';
import { RadioElement } from './elements/radio-element';
import { FileElement } from './elements/file-element';
import { CheckboxGroupElement } from './elements/checkboxgroup-element';
import { RadioGroupElement } from './elements/radiogroup-element';
import { EmailElement } from './elements/email-element';

@Injectable()
export class ElementInitialisationService {

  constructor() { }

  textElement(fields) {
    return new TextboxElement({
      key: fields.key,
      label: fields.displayName,
      required: fields.isRequired,
      order: fields.order,
      placeholder: fields.placeholder,
      value: fields.value
    });
  }

  emailElement(fields) {
    return new EmailElement({
      key: fields.key,
      label: fields.displayName,
      required: fields.isRequired,
      order: fields.order,
      placeholder: fields.placeholder,
      value: fields.value
    });
  }

  checkboxElement(fields) {
    if (fields.value == 'true')
      fields.value = true;
    else if (fields.value == 'false')
      fields.value = false;
    else if (fields.value == '')
      fields.value = false;

    return new CheckboxElement({
      key: fields.key,
      label: fields.displayName,
      required: fields.isRequired,
      order: fields.order,
      type: 'checkbox',
      value: fields.value
    });
  }

  checkboxgroupElement(fields) {
    var options = [];
    var selectedValues = [];
    
    if (fields.value[fields.value.length - 1].text == 'selected' && fields.value[fields.value.length - 1].value != "") {
      selectedValues = JSON.parse(fields.value[fields.value.length - 1].value);
      options = fields.value.splice(-1, 1);
      fields.value = selectedValues;
    } else {
      fields.value = JSON.parse(fields.value);
      fields.value.forEach(element => {
        element.status = false;
      });
    }

    return new CheckboxGroupElement({
      key: fields.key,
      label: fields.displayName,
      required: fields.isRequired,
      order: fields.order,
      type: 'checkbox',
      multipleValues: fields.value,
      value: selectedValues.length == 0 ? JSON.stringify(fields.value) : selectedValues
    });
  }

  dropdownElement(fields) {
    var options = [];
    var selectedValue: number = 0;
    if (fields.value[fields.value.length - 1].text == 'selected') {
      selectedValue = Number(fields.value[fields.value.length - 1].value);
      options = fields.value.splice(-1, 1);
    } else {
      fields.value = JSON.parse(fields.value);
    }
    return new DropdownElement({
      key: fields.key,
      label: fields.displayName,
      required: fields.isRequired,
      order: fields.order,
      options: fields.value,
      value: selectedValue
    });
  }

  textareaElement(fields) {
    return new TextAreaElement({
      key: fields.key,
      label: fields.displayName,
      required: fields.isRequired,
      order: fields.order,
      placeholder: fields.placeholder,
      value: fields.value
    });
  }

  calendarElement(fields) {
    return new CalendarElement({
      key: fields.key,
      label: fields.displayName,
      required: fields.isRequired,
      order: fields.order,
      value: fields.value == '' ? null : new Date(fields.value),
      type: 'Date'
    });
  }

  radioGroupElement(fields) {
    var options = [];
    var selectedValue: number;
    if (fields.value[fields.value.length - 1].text == 'selected') {
      selectedValue = fields.value[fields.value.length - 1].value;
      options = fields.value.splice(-1, 1);
    } else {
      fields.value = JSON.parse(fields.value);
    }
    return new RadioGroupElement({
      key: fields.key,
      label: fields.displayName,
      required: fields.isRequired,
      order: fields.order,
      type: 'radio',
      multipleValues: fields.value,
      value: selectedValue
    });
  }

  fileElement(fields): any {
    return new FileElement({
      key: fields.key,
      label: fields.displayName,
      required: fields.isRequired,
      order: fields.order,
      value: fields.value,
      type: 'File'
    });
  }

  profileElement(fields): any {
    return new ProfileImageElement({
      key: fields.key,
      label: fields.displayName,
      required: fields.isRequired,
      order: fields.order,
      value: fields.value,
      type: 'File'
    });
  }

}
