import { Injectable } from '@angular/core';
import { BaseService } from '../../core/services/base.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class MemberSubscriptionService extends BaseService {

  private apiEndPointMemberSubscription = `${this.baseApiEndPoint}membership/v1`;
  private apiEndPointPayment = `${this.baseApiEndPoint}payments/v1/payments`;
  
  constructor(private http: HttpClient) {
    super();
  }

  createSubscription(memberSubscription) {
    return this.http.post(`${this.apiEndPointMemberSubscription}/members/subscriptions`, memberSubscription, this.httpOptions)
      .map(res => res)
      .catch(this.errorHandler)
  }

  upadteSubscription(memberId,memberSubscriptionId) {
    return this.http.put(`${this.apiEndPointMemberSubscription}/members/${memberId}/subscriptions/${memberSubscriptionId}`, this.httpOptions)
      .map(res => res)
      .catch(this.errorHandler)
  }

  getAllMemberSubscription(subscriptionTypeId) {
    return this.http.get(`${this.apiEndPointMemberSubscription}/subscriptions/${subscriptionTypeId}/members`)
      .map((response) => response)
      .catch(this.errorHandler)
  }

  updateMemberSubscription(memberSubscription) {
    return this.http.put(`${this.apiEndPointMemberSubscription}/members/subscriptions`,
      (memberSubscription), this.httpOptions)
      .map(res => res).catch(this.errorHandler)
  }

  terminateSubscription(terminationModel) {
    return this.http.post(`${this.apiEndPointMemberSubscription}/members/subscriptions/terminate`,
      (terminationModel), this.httpOptions)
      .map(res => res).catch(this.errorHandler)
  }

  getSubscribedMembershipData(memberId) {
    return this.http.get(`${this.apiEndPointMemberSubscription}/members/${memberId}/subscription`)
      .map((response) => response)
      .catch(this.errorHandler)
  }

  activateMembership(memberId) {
    return this.http.put(`${this.apiEndPointMemberSubscription}/members/${memberId}/subscriptions/activate`,
      (memberId), this.httpOptions)
      .map(res => res).catch(this.errorHandler)
  }

  renewMembership(memberSubscription) {
    return this.http.post(`${this.apiEndPointMemberSubscription}/members/${memberSubscription.MemberId}/subscriptions/renew`,
      (memberSubscription), this.httpOptions)
      .map(res => res).catch(this.errorHandler)
  }

  sendRenewalMail(memberId) {
    return this.http.post(`${this.apiEndPointMemberSubscription}/members/${memberId}/subscriptions/sendrenewalemail`, this.httpOptions)
      .map(res => res)
      .catch(this.errorHandler)
  }

  updatePayment(updatePayment) {
    return this.http.put(`${this.apiEndPointPayment}`,
      updatePayment, this.httpOptions)
      .map(res => res)
      .catch(this.errorHandler)
  }

  getSubscriptionPayment(memberSubscriptionId){
    return this.http.get(`${this.apiEndPointPayment}/subscription/${memberSubscriptionId}`)
      .map((response) => response)
      .catch(this.errorHandler)
  }

}
