import { Component, OnInit, ViewContainerRef, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '../../../../../node_modules/@angular/forms';
import { PaymentModel } from '../../payments/models/paymentModel';
import { MembersService } from '../../core/services/members.service';
import { MemberSubscriptionService } from '../services/member-subscription.service';
import { PaymentService } from '../../core/services/payment.service';
import { BlockUiService } from '../../core/services/block-ui.service';
import { ActivatedRoute, Router, Params } from '../../../../../node_modules/@angular/router';
import { LevelService } from '../../core/services/level.service';
import { ToastsManager } from '../../../../../node_modules/ng2-toastr';
import { MemberType } from '../../core/enums/memberType';
import { PaymentStates } from '../../core/enums/paymentState';
import { ToasterMessages } from '../../core/utility/toasterMessages';
import { toasterHeadings } from '../../core/utility/toasterHeadings';
import { TimeLine } from '../models/memberTimelineModel';
import { LevelInfoModel } from '../models/levelInfoModel';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/mergeMap';
import { MemberSubscriptionModel } from '../models/memberSubscriptionModel';


@Component({
  selector: 'membership-member-level-payment',
  templateUrl: './member-level-payment.component.html',
  styleUrls: ['./member-level-payment.component.scss']
})
export class MemberLevelPaymentComponent implements OnInit {

  paymentForm: FormGroup;
  payment = new PaymentModel();
  timeLine = new TimeLine();
  levelInfo = new LevelInfoModel();
  memberSubscription = new MemberSubscriptionModel();
  paymentState = PaymentStates;

  transActionTypes = [];
  paymentTypes = [];

  memberType: MemberType;
  subscriptionChanged: string = null;
  currency = "Rs";

  isRequested: boolean = false;
  isPackageUtilized: boolean = false;
  isBlocked: boolean = false;
  isPayLater: boolean = false;
  isEdit: boolean = false;

  @Input() memberId: number = 0;
  @Input() levelPrice: number = 0;
  @Input() memberSubscriptionId: number = 0;
  @Input() subscriptionId: number = 0;

  constructor(private fb: FormBuilder, private memberService: MembersService, private memberSubscriptionService: MemberSubscriptionService,
    private router: Router, private activatedRoute: ActivatedRoute, private blockUiService: BlockUiService,
    private paymentService: PaymentService, private levelService: LevelService, private toastr: ToastsManager, vcr: ViewContainerRef) {
  }

  ngOnInit() {
    this.paymentForm = this.getPaymentForm();
    this.getMemberLookupData();
    this.activatedRoute.params.subscribe((params: Params) => {
      this.memberType = params['type'];

      if (params['id'] != 0 && params['id'] != undefined) {
        this.memberId = params['id'];
        this.subscriptionChanged = params['state'];
        this.isEdit = true;
      }
    });
  }

  private getPaymentForm() {
    return this.fb.group({
      transActionType: [1, [Validators.required]],
      paymentType: [1, [Validators.required]],
      amount: [{ value: "Rs." + this.levelPrice, disabled: true }, [Validators.required]],
      description: [""],
    })
  }

  private getMemberLookupData() {
    this.isBlocked = true;
    Observable.forkJoin(
      this.paymentService.getTransActionTypes(),
      this.paymentService.getPaymentTypes(),
    ).subscribe(data => {
      this.transActionTypes = data[0].result.item;
      this.paymentTypes = data[1].result.item;
      this.isBlocked = false;
    }, error => {
      this.isBlocked = false;
    });
  }

  save() {
    this.isRequested = true;
    this.payment.paymentState = this.isPayLater ? PaymentStates.Pending : PaymentStates.Paid;
    this.payment.amount = this.levelPrice;
    this.payment.memberId = this.memberId;
    this.payment.memberSubscriptionId = this.memberSubscriptionId;
    this.isBlocked = true;
    var payment = Object.assign({}, this.payment, this.paymentForm.value)
    this.paymentService.savePaymnets(payment)
      .mergeMap(payment => {
        return this.levelService.changeMemberStatus(payment.result);
      }).
      subscribe(response => {
        this.toastr.success(ToasterMessages.save('Member'), toasterHeadings.Success);
        this.isBlocked = false;
        setTimeout(() => {
          this.router.navigate(["/members"])
        }, 1000);

      }, error => {
        this.isBlocked = false;
        this.toastr.error(ToasterMessages.saveError('Member'), toasterHeadings.Error);
      });
  }

  payLater() {
    this.isPayLater = true;
    this.payment.paymentState = PaymentStates.Pending;
    this.save();
  }

  renewPayLater() {
    this.isPayLater = true;
    this.payment.paymentState = PaymentStates.Pending;
    this.renewSave();
  }

  renewSave() {
    this.payment.paymentState = this.isPayLater ? PaymentStates.Pending : PaymentStates.Paid;
    this.payment.amount = this.levelPrice;
    this.payment.memberId = this.memberId;
    this.payment.memberSubscriptionId = this.memberSubscriptionId;
    // payment.name = this.subscribedLevelInfo.name;
    var payment = Object.assign({}, this.payment, this.paymentForm.value);
    this.isBlocked = true;

    this.paymentService.savePaymnets(payment)
      .mergeMap(payment => {
        return this.levelService.changeMemberStatus(payment.result);
      })
      .subscribe(response => {
        this.toastr.success(ToasterMessages.save('Member'), toasterHeadings.Success);
        this.isBlocked = false;
        this.router.navigate(["/members"])
      }, error => {
        this.isBlocked = false;
        this.toastr.error(ToasterMessages.saveError('Member'), toasterHeadings.Error);
      });
  }

  changePayLater() {
    this.isPayLater = true;
    this.payment.paymentState = PaymentStates.Pending;
    this.changeSave();
  }

  changeSave() {
    this.isBlocked = true;
    this.payment.paymentState = this.isPayLater ? PaymentStates.Pending : PaymentStates.Paid;
    this.payment.amount = this.levelPrice;
    this.payment.memberId = this.memberId;
    this.payment.memberSubscriptionId = this.memberSubscriptionId;
    this.memberSubscriptionService.getSubscriptionPayment(this.memberSubscriptionId)
      .mergeMap(res => {
        this.payment.id = res.result.id;
        var payment = Object.assign({}, this.payment, this.paymentForm.value);
        return this.memberSubscriptionService.updatePayment(payment)
      }).mergeMap(xx => {
        return this.levelService.changeMemberStatus(this.payment);
      })
      .subscribe(res => {
        this.toastr.success(ToasterMessages.update('Membership package'), toasterHeadings.Success);
        this.isBlocked = false;
        this.router.navigate(['/members']);
      }, error => {
        this.isBlocked = false;
        this.toastr.error(ToasterMessages.updateError('Membership package'), toasterHeadings.Error)
      });
  }
}
