import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemberLevelsComponent } from './member-levels.component';

describe('MemberLevelsComponent', () => {
  let component: MemberLevelsComponent;
  let fixture: ComponentFixture<MemberLevelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemberLevelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberLevelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
