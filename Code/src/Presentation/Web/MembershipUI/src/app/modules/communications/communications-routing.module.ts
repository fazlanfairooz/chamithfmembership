import { EmailInboxComponent } from './email-inbox/email-inbox.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmailTemplateCreateComponent } from './email-template-create/email-template-create.component';
import { EmailDetailsComponent } from './email-details/email-details.component';

const routes: Routes = [
  { path: '', component: EmailDetailsComponent },
  { path: 'createTemplate/:type', component: EmailTemplateCreateComponent },
  { path: 'updateTemplate/:id/:type', component: EmailTemplateCreateComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommunicationsRoutingModule { }
