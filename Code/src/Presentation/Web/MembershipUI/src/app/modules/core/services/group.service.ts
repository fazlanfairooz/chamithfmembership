import { RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../core/services/base.service';
import { HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class GroupService extends BaseService {

  private apiEndPointMembershipGroup = `${this.baseApiEndPoint}membership/v1/groups/readWithMemberCount`;
  private apiEndPointMemberGroup = `${this.baseApiEndPoint}membership/v1/membergroups`;

  constructor(private http: HttpClient) {
    super();
  }

  getAllMemberGroup(skip, take, orderby) {
    return this.http.get(`${this.apiEndPointMemberGroup}?skip=${skip}&take=${take}&orderby=${orderby}`)
      .map((response) => response)
      .catch(this.errorHandler)
  }

  getAllGroupMemberCount(skip, take, search, orderby) {
    return this.http.get(`${this.apiEndPointMembershipGroup}/?skip=${skip}&take=${take}&search=${search}&orderby=${orderby}`)
      .map((response) => response)
      .catch(this.errorHandler)
  }

  getMemberDetails(membergroupId) {

    return this.http.get(`${this.apiEndPointMemberGroup}/${membergroupId}`,
      this.httpOptions)
      .map((response) => response)
      .catch(this.errorHandler)
  }

  AddMembers(membergroup) {
    return this.http.post(`${this.apiEndPointMemberGroup}`
      , membergroup, this.httpOptions)
      .map(res => res)
      .catch(this.errorHandler)
  }
  archiveGroup(groupId) {
    return this.http.delete(`${this.baseApiEndPoint}membership/v1/groups/${groupId}`, this.httpOptions).map((response) => response).catch(this.errorHandler)
  }
  addGroup(group) {
    return this.http.post(`${this.baseApiEndPoint}membership/v1/groups`, (group), this.httpOptions).map(res => res).catch(this.registerErrorHandler)
  }
  updateGroup(group) {
    return this.http.put(`${this.baseApiEndPoint}membership/v1/groups`, (group), this.httpOptions).map(res => res).catch(this.errorHandler)
  }
  getAllGroup(skip, take) {
    return this.http.get(`${this.baseApiEndPoint}membership/v1/groups?skip=` + skip + '&take=' + take, this.httpOptions).map((response) => response).catch(this.errorHandler)
  }
}
