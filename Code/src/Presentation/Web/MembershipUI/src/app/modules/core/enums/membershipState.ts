export enum MembershipState {
    Active = 1,
    Inactive = 2,
    Pending = 3,
    Terminated = 4,
    All = 5,
    unTerminated =6,
    Approval = 7,
    Rejected = 8
}