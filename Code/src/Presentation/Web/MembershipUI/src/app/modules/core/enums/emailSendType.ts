export enum EmailSendType {
    single = 1,
    individuals = 2,
    multiple = 3,
    all = 4
}