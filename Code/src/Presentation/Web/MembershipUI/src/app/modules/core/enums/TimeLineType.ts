export enum TimeLineType {
    MembershipRenewalEmailReminder = 1,
    MembershipExpire = 2,
    MembershipPayment = 3,
    ChangeMembership = 4,
    MembershipCreated = 5,
    MembershipRenew = 6,

}