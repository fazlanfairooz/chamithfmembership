import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';

import { ContactsRoutingModule } from './contacts-routing.module';
import { ContactDetailsComponent } from './contact-details/contact-details.component';
import { ContactCreateComponent } from './contact-create/contact-create.component';
import { SharedModule } from '../shared/shared.module';
import { ContactDocumentComponent } from './contact-document/contact-document.component';
import { ContactRestoreComponent } from './contact-restore/contact-restore.component';
import { TranslateModule } from '@ngx-translate/core';
import { ContactCardViewComponent } from './contact-card-view/contact-card-view.component';


@NgModule({
  imports: [
    ContactsRoutingModule,
    SharedModule,
    TranslateModule,
    BsDatepickerModule.forRoot(),
    
  ],
  declarations: [ContactDetailsComponent, ContactCreateComponent, ContactDocumentComponent, ContactRestoreComponent, ContactCardViewComponent],
  providers: [DatePipe]
})
export class ContactsModule { }
