export class PersonalModel {
    id: number;
    firstName: string;
    lastName: string;
    middleName: string;
    dob: Date;
    martialStatus: string;
    identity: string;
    contactCategoryId: string;
    profileImage: string;
}