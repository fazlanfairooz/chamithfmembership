export class PatternModel {
    emailRegex: RegExp = /^[_a-zA-Z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$/;
    passwordRegex: RegExp = /(?=^.{9,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;
    hostNameRegex: RegExp = /^[a-zA-Z][a-zA-Z\\s]+$/;
    phoneregx: RegExp = /^\d{9}$/;
    whiteSpace: RegExp = /^\S/;
    url: RegExp = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;
}
