import { BlockUiService } from './../../core/services/block-ui.service';
import { Component, OnInit, ViewContainerRef, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '../../../../../node_modules/@angular/forms';
import { LevelInfoModel } from '../models/levelInfoModel';
import { SubscribedLevelInfo } from '../models/subscribedLevelInfo';
import { MembershipState } from '../../core/enums/membershipState';
import { LookupService } from '../../core/services/lookup.service';
import { MemberSubscriptionService } from '../services/member-subscription.service';
import { ToastsManager } from '../../../../../node_modules/ng2-toastr';
import { ActivatedRoute, Params, Router } from '../../../../../node_modules/@angular/router';
import { PaymentService } from '../../core/services/payment.service';
import { LevelService } from '../../core/services/level.service';
import { MemberType } from '../../core/enums/memberType';
import { MembershipLevelStates } from '../../core/enums/membershipLevelState';
import { AssignGroups } from '../models/assignGroups';
import { CutomMemberService } from '../services/member.service';
import { Observable } from '../../../../../node_modules/rxjs/Observable';
import { ToasterMessages } from '../../core/utility/toasterMessages';
import { toasterHeadings } from '../../core/utility/toasterHeadings';
import { MemberSubscriptionModel } from '../models/memberSubscriptionModel';

@Component({
  selector: 'membership-member-level-subscribe',
  templateUrl: './member-level-subscribe.component.html',
  styleUrls: ['./member-level-subscribe.component.scss']
})
export class MemberLevelSubscribeComponent implements OnInit {

  membershipForm: FormGroup;

  levelInfo = new LevelInfoModel();
  subscribedLevelInfo = new SubscribedLevelInfo();
  assignGroups = new AssignGroups();
  memberSubscription = new MemberSubscriptionModel();

  memberLevels = [];
  transActionTypes = [];
  paymentTypes = [];
  memberGroups = [];
  dropdownSettings = {};
  memberGroupDropdown = [];
  selectedItems = [];

  membershipState = MembershipState;
  memberType: MemberType;
  selectedSubscription: number = 0;
  subscription: number = 0;
  currency = "";
  levelPrice: number = 0;
  subscriptionChanged: string = null;
  memberSubscriptionId: number = 0;
  subscriptionId: number = 0;

  isMembershipFormSubmitted = false;
  isLevelDropDownClicked = false;
  isPackageUtilized = false;
  isRequested = false;
  isDisplayLevels = false;
  isEdit = false;
  isBlocked = false;

  @Input() memberId: number = 0;
  @Output() onMemberSubscripitionSave: EventEmitter<any> = new EventEmitter<any>();

  constructor(private fb: FormBuilder, private toastr: ToastsManager, private paymentService: PaymentService, private router: Router,
    private memberSubscriptionService: MemberSubscriptionService, private memberService: CutomMemberService, private activatedRoute: ActivatedRoute,
    private levelService: LevelService, private lookUpService: LookupService, private blockUiService: BlockUiService) {
  }

  ngOnInit() {
    this.membershipForm = this.getMembershipForm();
    this.getmemberLevelData();
    this.getMemberGroups();
    this.getDropDownSettings();

    this.activatedRoute.params.subscribe((params: Params) => {
      this.memberType = params['type'];
      if (params['id'] != 0 && params['id'] != undefined) {
        this.memberId = params['id'];
        this.subscriptionChanged = params['state'];
        this.isEdit = true;
        this.getSubscribedMembershipData(this.memberId);
      }
    });
  }

  getMembershipForm() {
    return this.fb.group({
      membershipLevel: [null, [Validators.required]],
      memberGroup: []
    })
  }

  getSubscribedMembershipData(memberId) {
    this.isBlocked = true;
    this.memberSubscriptionService.getSubscribedMembershipData(memberId)
      .subscribe(res => {
        this.subscribedLevelInfo = (res.result);
        this.isBlocked = false;
      },
        error => {
          this.isBlocked = false;
          this.toastr.error(ToasterMessages.loadError('Subscription'), toasterHeadings.Error);
        });
  }

  getDropDownSettings() {
    this.dropdownSettings = {
      singleSelection: false,
      placeholder: "Search a Group to Add...",
      text: "Select Group",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
    };
  }

  getmemberLevelData() {
    this.isBlocked = true;
    this.levelService.getAllMemberLevel(0, 0)
      .subscribe(memberLevels => {
        this.isBlocked = false;
        this.memberLevels = memberLevels.result.items;
        this.currency = localStorage.getItem('Currency');
      }, error => {
        this.isBlocked = false;
      });
  }

  getMemberGroups() {
    this.lookUpService.getMembershipGroups()
      .subscribe(groups => {
        this.memberGroups = groups.result.item;
        this.memberGroups.forEach(group => {
          this.memberGroupDropdown.push({ "id": group.value, "itemName": group.text });
        });
      }, error => {
        this.toastr.warning("Error in loading member groups", "Error");
      })
  }

  displayLevelInfo() {
    var today = new Date();
    var year = today.getFullYear();
    var month = today.getMonth();
    var day = today.getDate();

    this.isPackageUtilized = false;
    this.isLevelDropDownClicked = true;
    this.isRequested = true;
    this.selectedSubscription = this.membershipForm.value.membershipLevel;
    this.levelInfo = this.memberLevels.find(t => t.id == this.selectedSubscription);

    this.levelPrice = this.levelInfo.price;
    this.levelInfo.subscriptionFrom = new Date();

    if (this.levelInfo.membershipExpired == MembershipLevelStates.Annualy) {
      this.levelInfo.subscriptionTo = new Date(year + 1, month, day)
    } else if (this.levelInfo.membershipExpired == MembershipLevelStates.Monthly) {
      this.levelInfo.subscriptionTo = new Date(year, month + 1, day)
    } else if (this.levelInfo.membershipExpired == MembershipLevelStates.Weekly) {
      this.levelInfo.subscriptionTo = new Date(year, month, day + 7)
    } else if (this.levelInfo.membershipExpired == MembershipLevelStates.Daily) {
      this.levelInfo.subscriptionTo = new Date(year, month, day + 1)
    }

    if (this.levelInfo.capacity != 0) {
      this.memberSubscriptionService.getAllMemberSubscription(this.selectedSubscription)
        .subscribe(membersubscription => {
          let utilizeCount = membersubscription.result.totalRecordCount;
          let remainingCount = this.levelInfo.capacity - utilizeCount;
          if (remainingCount < 0 || remainingCount == 0) {
            this.isPackageUtilized = true;
          }
          this.levelInfo.remainingCount = remainingCount;
          this.levelInfo.utilizeCount = utilizeCount;
        });
    }
  }

  updateMembersubscription(memberId, memberSubscriptionId) {
    this.memberSubscriptionService.upadteSubscription(memberId, memberSubscriptionId).subscribe();
  }

  saveMemberLevel() {
    this.isMembershipFormSubmitted = true;
    if (this.membershipForm.invalid) return;
    this.isBlocked = true;

    this.assignGroups.memberId = this.memberId;
    if (this.membershipForm.value.memberGroup != null) {
      var groupIds = this.membershipForm.value.memberGroup.map(function (item) { return item.id; });
      this.assignGroups.groupIds = groupIds;
      this.memberService.assignMemberToGroups(this.assignGroups).subscribe();
    }

    this.subscribedLevelInfo.memberId = this.memberId;
    this.subscribedLevelInfo.subscriptionTypeId = this.membershipForm.value.membershipLevel;
    this.memberSubscriptionService.createSubscription(this.subscribedLevelInfo)
      .subscribe(res => {
        this.memberSubscriptionId = res.result.id;
        this.subscriptionId = res.result.subscriptionId;
        this.updateMembersubscription(this.memberId, this.memberSubscriptionId);
        this.onMemberSubscripitionSave.emit({ 'levelCreated': true, 'levelPrice': this.levelPrice, 'memberSubscriptionId': this.memberSubscriptionId });
        this.isBlocked = false;
      },
        error => {
          this.isBlocked = false;
          this.toastr.error("Error in saving member level", "Error");
        });
  }

  changeMemberSubscription() {
    if (this.selectedSubscription == 0) {
      this.isMembershipFormSubmitted = true;
      if (this.membershipForm.invalid) return;
    }
    this.isBlocked = true;
    this.memberSubscription.MemberId = this.memberId;
    this.memberSubscription.Id = this.subscribedLevelInfo.id;
    this.memberSubscription.SubscriptionId = this.membershipForm.value.membershipLevel;
    this.memberSubscriptionId = this.subscribedLevelInfo.id;
    this.memberSubscriptionService.updateMemberSubscription(this.memberSubscription)
      .subscribe(res => {
        this.onMemberSubscripitionSave.emit({ 'levelCreated': true, 'levelPrice': this.levelPrice, 'memberSubscriptionId': this.memberSubscriptionId });
        this.isBlocked = false;
      }, error => {
        this.isBlocked = false;
        this.toastr.error("Error in saving member level", "Error");
      });
  }

  renewMemberSubscription() {
    this.isBlocked = true;
    this.memberSubscription.MemberId = this.memberId;
    this.memberSubscription.SubscriptionId = this.selectedSubscription == 0 ? this.subscribedLevelInfo.subscriptionId : this.selectedSubscription;
    this.levelPrice = this.selectedSubscription == 0 ? this.subscribedLevelInfo.price : this.levelInfo.price;

    this.memberSubscriptionService.renewMembership(this.memberSubscription)
      .subscribe(res => {
        this.isBlocked = false;
        this.memberSubscriptionId = res.result.id;
        this.subscriptionId = res.result.subscriptionId;
        this.memberId = res.result.memberId;
        this.updateMembersubscription(this.memberId, this.memberSubscriptionId);
        this.onMemberSubscripitionSave.emit({ 'levelCreated': true, 'levelPrice': this.levelPrice, 'memberSubscriptionId': this.memberSubscriptionId });
      }, error => {
        this.isBlocked = false;
        this.toastr.error(ToasterMessages.saveError('Member'), toasterHeadings.Error);
      })
  }
}
