import { SettingService } from '../settings/services/setting.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PaymentsRoutingModule } from './payments-routing.module';
import { PaymentCreateComponent } from './payment-create/payment-create.component';
import { PaymentDetailsComponent } from './payment-details/payment-details.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { SharedModule } from '../shared/shared.module';
import { InvoiceCreateComponent } from './invoice-create/invoice-create.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    PaymentsRoutingModule,
    SharedModule,
    TranslateModule,
    BsDatepickerModule.forRoot()
  ],
  declarations: [PaymentCreateComponent, PaymentDetailsComponent, InvoiceCreateComponent]
})
export class PaymentsModule { }
