export class ContactModel {
    id: number;
    tenantId: number;
    country: string;
    zip: string;
    city: string;
    address: string;
    phone: string;
    telephone: string;
    email: string;

    constructor() {
        this.id = 0;
        this.tenantId = 0;
    }
}