export class Field {
    id: number;
    tabId: number;
    key: string;
    dataType: number;
    displayName: string;
    value: string;
    isRequired: boolean;
    placeholder: string;
    toolTip: string;
}