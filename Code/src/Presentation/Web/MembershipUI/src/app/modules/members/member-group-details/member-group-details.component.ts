import { GroupService } from './../../core/services/group.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Group } from '../models/memberGroup';
import { LookupService } from '../../core/services/lookup.service';
import { ToastsManager } from 'ng2-toastr';
import { SortBaseService } from '../../core/services/sort-base.service';
import { MembersService } from '../../core/services/members.service';
import { Router } from '@angular/router';
import { Component, OnInit, ViewContainerRef, ViewChild, Output, EventEmitter } from '@angular/core';
import { BlockUiService } from '../../core/services/block-ui.service';
import { PaginationModel } from '../../core/models/paginationModel';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs/Subject';
import { MemberSearchModel } from '../models/memberSearchModel';

@Component({
  selector: 'membership-member-group-details',
  templateUrl: './member-group-details.component.html',
  styleUrls: ['./member-group-details.component.scss']
})
export class MemberGroupDetailsComponent implements OnInit {

  
  @ViewChild('groupModal') public groupModal: ModalDirective;
  @Output() group: EventEmitter<any> = new EventEmitter();

  groupList = [];

  searchTerm$ = new Subject<string>();
  memberSearch = new MemberSearchModel();
  orderby = "";
  isBlocked = false;
  paginationModel = new PaginationModel(0, '', false);
  constructor(private router: Router, private groupService: GroupService,
    private sortBaseService: SortBaseService,
    private blockUiService: BlockUiService,
    translate: TranslateService,
    public toastr: ToastsManager, vcr: ViewContainerRef, private lookUpService: LookupService) {
    this.toastr.setRootViewContainerRef(vcr);

    this.searchTerm$.debounceTime(300)
    .distinctUntilChanged().subscribe(data => {
      if (data != "" && data) {
        this.memberSearch.search = data;
      }
      else {
        this.memberSearch.search = '';
      }
      this.getmemberGroupDeatials();
    })
  }

  ngOnInit() {

    this.getmemberGroupDeatials()
  
  }

  editGroup(id) {
    this.router.navigate(["/members/createGroup", id])
  }

getmemberGroupDeatials() {
    this.isBlocked = true;
    this.groupService.getAllGroupMemberCount(this.paginationModel.skip, this.paginationModel.take,JSON.stringify(this.memberSearch),this.orderby)
      .subscribe(allmemberGroups => {
        this.isBlocked = false;
        this.groupList = allmemberGroups.result; 
        if (allmemberGroups.result.totalRecodeCount == 0) {
        }
      },
      error => {
        this.isBlocked = false;
        this.toastr.error('Error in Loading Group', 'Oops');
      });
  }


  gotonewGroup() {
    this.router.navigate(["/members/createGroup"])
  }
}
