import { AccountSettingsComponent } from './account-settings/account-settings.component';
import { UserSettingsComponent } from './user-settings/user-settings.component';
import { CommonModule } from '@angular/common';
import { SharedModule } from './../shared/shared.module';
import { CreateCategoriesComponent } from './create-categories/create-categories.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PermissionsComponent } from './permissions/permissions.component';
import { PermissionPasswordComponent } from './permission-password/permission-password.component';
import { CustomFieldsComponent } from './custom-fields/custom-fields.component';

const routes: Routes = [
  { path: '', component: CreateCategoriesComponent },
  { path: 'organaizationsettings', component: AccountSettingsComponent },
  { path: 'usersettings', component: UserSettingsComponent },
  { path: 'permissions', component: PermissionsComponent },
  { path: 'customfields', component: CustomFieldsComponent }

];
@NgModule({
  imports: [RouterModule.forChild(routes), SharedModule, CommonModule],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
