import { MemberDocumentModel } from '../models/memberDocumentModel';
import { toasterHeadings } from '../../core/utility/toasterHeadings';
import { MembersService } from '../../core/services/members.service';
import { ToastsManager } from 'ng2-toastr';
import { ActivatedRoute, Params } from '@angular/router';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { ToasterMessages } from '../../core/utility/toasterMessages';

@Component({
  selector: 'membership-member-document',
  templateUrl: './member-document.component.html',
  styleUrls: ['./member-document.component.scss']
})
export class MemberDocumentComponent implements OnInit {

  skip=0
  take=0
  url: any;
  documentId=0;
  DocumentList= [];
  DocumentCount=0;
  documentForm: FormGroup;
  isDocumentFormSubmitted = false;
  fileList: FileList;
  memberId = 0;
  document = new MemberDocumentModel();

  @ViewChild('staticTabs') staticTabs: TabsetComponent;

  constructor(private fb: FormBuilder, private memberService: MembersService,
    private activatedRoute: ActivatedRoute,
    private toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.documentForm = this.getDocumentForm();
    this.activatedRoute.params.subscribe((params: Params) => {
      this.memberId = params['id'];
    });

    this.getDocuments();
  }

  archiveDocument(){
    this.memberService.archiveDocument(this.documentId).subscribe(res => {
      this.toastr.success(ToasterMessages.delete('Document'),toasterHeadings.Success);      
    },
      error => { this.toastr.success(ToasterMessages.delete('Document'),toasterHeadings.Success) 
      this.getDocuments();
    })
  }

  deleteId(id){
    this.documentId = id
  }
  
  getDocumentForm(){
   return this.fb.group({
    file: ["", [Validators.required]],
    })
  }
  
  getDocuments(){
    var docObj = {MemberId: this.memberId }
    this.memberService.getDocumentsById(this.skip, this.take,docObj)
      .subscribe(allmembers => {
        this.DocumentList = allmembers.result.item.items;
        this.DocumentCount = allmembers.result.item.totalRecodeCount;
      },
      error => {
        this.toastr.error('Error in Loading Documents', 'Oops');
      }, () => {
      });
  }
  
  save() {
    this.isDocumentFormSubmitted = true;
    var docObj = {MemberId: this.memberId }
    this.memberService.saveDocument(this.fileList, docObj ).subscribe(res => {
      this.toastr.success(ToasterMessages.save('Document'),toasterHeadings.Success);
      this.getDocuments();
    }, error => {
      this.toastr.error(ToasterMessages.saveError('Document'),toasterHeadings.Error);
    }, () => {

    })

  }

  fileUpload(event) {
    this.fileList = event.target.files;
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event:any) => {
        this.url = event.target.result;
      }
      reader.readAsDataURL(event.target.files[0]);
    }
  }
}