import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PatternModel } from '../../shared/patternvalidator/pattern';
import { EmailToken } from '../../login/model/emailToken';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';
import { patternValidator } from '../../shared/patternvalidator/patternvalidator';
import { PermissionService } from '../services/permission.service';
import { ChangePassword } from '../models/passwordModel';
import { SettingService } from '../services/setting.service';
import { PermissionUserLoginModel } from '../models/permissionuserlogin';
import { RegisteruserService } from '../../login/service/registeruser.service';
import { TenantInfoService } from '../../core/services/tenant-info.service';
import { PermissionModel } from '../models/permission';

@Component({
  selector: 'membership-permission-password',
  templateUrl: './permission-password.component.html',
  styleUrls: ['./permission-password.component.scss']
})
export class PermissionPasswordComponent implements OnInit {

  isTokenValid = true;
  isPasswordMatch = false;
  permissionPasswordForm: FormGroup;
  patternModel = new PatternModel();
  emailToken = new EmailToken();
  isBlocked = false;
  password = new ChangePassword();
  userLogin = new PermissionUserLoginModel();
  isRequested = false;
  newpassworad :any;


  constructor(private fb: FormBuilder,private tenantInfoService:TenantInfoService, private registerUser: RegisteruserService, private route: ActivatedRoute,private permissionService: PermissionService,private router: Router,public toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
   }

  ngOnInit() {
    this.route
    .queryParams
    .subscribe(params => {
      this.emailToken.email = params['email'];
      this.emailToken.token = params['token'];
    });
    if (this.emailToken.token != "") {
      this.validatenewEmail(this.emailToken);
    } 
  this.createForm();
  }
 
  createForm() {
    this.permissionPasswordForm = this.fb.group({
      newPassword: ['',[Validators.required, patternValidator(this.patternModel.passwordRegex)]],
      confirmPassword: ['', [Validators.required]]
    }, { validator: this.comparePasswords });
  }
  comparePasswords(group: FormGroup) {
    let password = group.controls.newPassword.value;
    let confirmPassword = group.controls.confirmPassword.value;
    return (password === confirmPassword) ? null : { isPasswordNotSame: true }
  }
  

  permissionpassword() {
    if (!this.permissionPasswordForm.valid) return;
    this.isBlocked = true;
    this.password = Object.assign({}, this.password, this.permissionPasswordForm.value)
    this.newpassworad = this.password.newPassword;
    this.userLogin.email = this.emailToken.email 
    this.userLogin.password= this.newpassworad
    this.userLogin.token =this.emailToken.token 
    this.permissionService.changepermissionsPassword(this.userLogin)
      .subscribe(res => {
        this.isBlocked = false;
        this.toastr.success('Password has been successfully', 'Success');
        this.permissionPasswordForm.reset();
        this.router.navigate(['/dashboard']);
      },
        error => {
          this.isBlocked = false;
          this.toastr.error(' Password is Invalid', 'Oops');
        });   
  }
  
    validatenewEmail(emailToken) {
      if (emailToken.email === "") return;
      this.permissionService.newuservalidate(emailToken)
        .subscribe(res => {
          localStorage.setItem('Token_id', res.result.item.token);
          this.isTokenValid = true;
        }, error => {
          this.isTokenValid = false;
        })
    }
  
 
  



}
