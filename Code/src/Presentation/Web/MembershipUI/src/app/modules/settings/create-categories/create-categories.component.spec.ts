import { SortBaseService } from '../../core/services/sort-base.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ContactCategoryService } from '../services/ContactCategory.service';
import { RouterTestingModule } from '@angular/router/testing';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { LookupService } from '../../core/services/lookup.service';
import { HttpClientModule } from '@angular/common/http';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { SharedModule } from '../../shared/shared.module';
import { SettingService } from '../services/setting.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';
import { By } from '@angular/platform-browser';
import { CreateCategoriesComponent } from './create-categories.component';

describe('CreateCategoriesComponent', () => {
  let component: CreateCategoriesComponent;
  let fixture: ComponentFixture<CreateCategoriesComponent>;
  let de: DebugElement;
  let el: HTMLElement;

  let lookupService: LookupService;
  let categoryService: ContactCategoryService;
  let sortBaseService: SortBaseService;

  let getCategorySpy: jasmine.Spy

  let mockcatogaryData = {
    result: {
      item:
        {
          catogaryInfo: { Name: "Staff" },
        }
    }
  }

  let mockcatogareis = {
    result: {
      items: [
        { name: 'Staff' },
        { name: 'Member' },
        { name: 'Workers' },
      ]
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreateCategoriesComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [SharedModule, HttpClientModule,RouterTestingModule],
      providers: [SortBaseService, ContactCategoryService ,Ng4LoadingSpinnerService, LookupService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateCategoriesComponent);
    component = fixture.componentInstance;

    jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;

    categoryService = fixture.debugElement.injector.get(categoryService);
    lookupService = fixture.debugElement.injector.get(LookupService);

    spyOn(categoryService, "getAllCategories").and.returnValue(Observable.of(mockcatogareis))
    spyOn(categoryService, "addCategory").and.returnValue(Observable.of(mockcatogaryData)) 

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call update Category method  when click update button', () => {
        component.categorie.Id  = 10;
        fixture.detectChanges();
       de = fixture.debugElement.query(By.css("#updateCategory"));
        de.triggerEventHandler("click", null);
    
        fixture.whenStable().then(() => {
          expect(component.updateCategory()).toHaveBeenCalled();
        })
    });

  it('should call add Category method when click add Category button', () => {
    component.categorie.Id = 0;
    fixture.detectChanges();
    de = fixture.debugElement.query(By.css("#addcategory"));
    de.triggerEventHandler("click", null);

    fixture.whenStable().then(() => {
      expect(component.addcategory()).toHaveBeenCalled();
    })
  });

  it('should call update Category method  when click update button', () => {
    component.categorie.Id  = 10;
    fixture.detectChanges();
    de = fixture.debugElement.query(By.css("#updateCategory"));
    de.triggerEventHandler("click", null);

    fixture.whenStable().then(() => {
      expect(component.updateCategory()).toHaveBeenCalled();
    })
  });

});
