export class EmailSendModel {
    emailSendType: number;
    subject: string;
    templateId: number;
    emailBody: string;
    memberId: number;
    memberStatusId: number;
    toAddress: string;
    memberLevelId: string;
    memberIds: number[];

    constructor() {
        this.memberIds = [];

    }

}
