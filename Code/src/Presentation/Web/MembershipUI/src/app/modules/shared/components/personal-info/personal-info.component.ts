import { Router, ActivatedRoute } from '@angular/router';
import { MemberAllDataModel } from './../../../members/models/memberAllDataModel';
import { OnChanges } from '@angular/core/src/metadata/lifecycle_hooks';
import { Component, OnInit, Input } from '@angular/core';
import { MembersService } from '../../../core/services/members.service';
import { Output } from '@angular/core/src/metadata/directives';

@Component({
  selector: 'membership-personal-info',
  templateUrl: './personal-info.component.html',
  styleUrls: ['./personal-info.component.scss']
})
export class PersonalInfoComponent implements OnInit, OnChanges {

  isBlocked = false;
  @Input() memberId: string;
  member= new MemberAllDataModel();
  membershipID :string;
  constructor(private memberService: MembersService,
    private router: Router,
    private activatedRoute: ActivatedRoute,) { }

  ngOnInit() {
    this.getMemberInfo();
  }

  ngOnChanges() {
    console.log(this.memberId)
  }
  getMemberInfo() {
    this.isBlocked = true
    this.memberService.getMemberById(this.memberId).subscribe(res => {
      this.member = res.result.item;
      var prefix  = localStorage.getItem('Prefix');
      var ID = this.memberId;
      this.membershipID = prefix+"-" + ID;
    this.isBlocked = false;    
    }, error => {

    })
  }
}
