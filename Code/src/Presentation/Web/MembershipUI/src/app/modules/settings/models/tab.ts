import { MemberType } from "../../core/enums/memberType";

export class Tab {
    id: number;
    displayName: string;
    isVisible: boolean;
    tabDescription: string;
    memberType = Array<MemberType>();
    order: number;
    isSystemTab: boolean;
}