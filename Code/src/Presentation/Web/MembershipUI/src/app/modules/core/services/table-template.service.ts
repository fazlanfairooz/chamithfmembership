import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { TableTemplate } from '../enums/tableTemplate';

@Injectable()
export class TableTemplateService extends BaseService {

  private apiEndPoint = this.baseApiEndPoint;

  constructor(private http: HttpClient) {
    super();
  }

  init(templateId) {
    this.apiEndPoint = this.baseApiEndPoint;
    switch (templateId) {
      case TableTemplate.ContactTableTemplate: {
        this.apiEndPoint = `${this.apiEndPoint}contacts/v1/utility/tabletemplate/`
        break;
      }
      case TableTemplate.MemberTableTemplate: {
        this.apiEndPoint = `${this.apiEndPoint}membership/v1/utility/tabletemplate/`
        break;
      }
    }
  }

  getTemplateColumns(type) {
    return this.http.get(`${this.apiEndPoint}headers/${type}`).map(res => res).catch(this.errorHandler)
  }

  getSavedTemplateColumns(type) {
    return this.http.get(`${this.apiEndPoint}${type}`).map(res => res).catch(this.errorHandler)
  }
  saveTemplateColumns(columns) {
    return this.http.put(`${this.apiEndPoint}`, columns).map(res => res).catch(this.errorHandler)
  }

}
