import { Injectable } from "@angular/core";
import { environment } from "../../../../environments/environment";

import { Http } from "@angular/http";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { BaseService } from "../../core/services/base.service";

@Injectable()
export class DashboardService extends BaseService{

    private apiEndPoint = `${this.baseApiEndPoint}membership/v1/utility/dashboardgraphs`;

    constructor(private http: HttpClient) {
        super();
    }

    getAllDashBoardDetails(type)
    {
       return this.http.get(`${this.apiEndPoint}/${type}`,
       this.httpOptions)
       .map(res => res)
       .catch(this.errorHandler)
    }

} 