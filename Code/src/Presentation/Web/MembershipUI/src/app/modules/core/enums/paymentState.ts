export enum PaymentStates {
    None = 0,
    Pending = 1,
    PartialyPaid=2,
    Paid = 3
}