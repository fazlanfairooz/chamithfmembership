import { ContactSearchModel } from '../models/contactSearchModel';
import { ToastsManager } from 'ng2-toastr';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Router, ActivatedRoute } from '@angular/router';
import { ContactsService } from '../../core/services/contacts.service';
import { FormBuilder } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import { Component, OnInit, ViewContainerRef } from '@angular/core';

@Component({
  selector: 'membership-contact-restore',
  templateUrl: './contact-restore.component.html',
  styleUrls: ['./contact-restore.component.scss']
})
export class ContactRestoreComponent implements OnInit {
  archivedList = [];
  noRecords: boolean;
  searchTerm$ = new Subject<string>();
  contactSearch = new ContactSearchModel();
  skip = 0;
  take = 0;
  contactId = 0;
  ContactCount = 0;
  orderby = "";

  constructor(private fb: FormBuilder, private contactService: ContactsService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private spinnerService: Ng4LoadingSpinnerService,
    private toastr: ToastsManager, vcr: ViewContainerRef) {
      this.toastr.setRootViewContainerRef(vcr);
      
      this.searchTerm$.debounceTime(300)
      .distinctUntilChanged().subscribe(data => {
        if (data != "" && data) {
          this.contactSearch.search = data;
        }
        else {
          this.contactSearch.search = '';
        }
              this.getArchiveContacts();
            })
        }

  ngOnInit() {
    this.noRecords = false;
    this.getArchiveContacts();
  }

  getArchiveContacts(){
    this.contactService.getArchiveContacts(this.skip, this.take,JSON.stringify(this.contactSearch),this.orderby)
      .subscribe(allArchivemember => {
        this.archivedList = allArchivemember.result.items
        this.ContactCount = allArchivemember.result.totalRecodeCount
        if (allArchivemember.result.totalRecodeCount == 0) {
          this.noRecords = true;
        }
      },
      error => {
        this.toastr.error('Error in Loading Member Level', 'Oops');
      });
  }

  restoreContactId(Id){
    this.contactId = Id;
  }

  restoreContact(){
    
    this.contactService.restoreContact(this.contactId)
    .subscribe(allCategories => {
      this.toastr.success('Restored Successfully!', 'Success');
      this.getArchiveContacts();
    },
      error => {
        this.toastr.error('Error in Restoring Member', 'Oops');
      });
  }

}
