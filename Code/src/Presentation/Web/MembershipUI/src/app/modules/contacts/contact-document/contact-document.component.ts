import { Params, ActivatedRoute } from '@angular/router';
import { toasterHeadings } from '../../core/utility/toasterHeadings';
import { ToastsManager } from 'ng2-toastr';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ContactsService } from '../../core/services/contacts.service';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { ContactDocumentModel } from '../models/contactDocument';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { ToasterMessages } from '../../core/utility/toasterMessages';

@Component({
  selector: 'membership-contact-document',
  templateUrl: './contact-document.component.html',
  styleUrls: ['./contact-document.component.scss']
})
export class ContactDocumentComponent implements OnInit {

  skip=0
  take=0
  url: any;
  documentForm: FormGroup;
  isDocumentFormSubmitted = false;
  fileList: FileList;
  contactId = 0;
  documentId=0;
  DocumentList= [];
  DocumentCount=0;
  document = new ContactDocumentModel();

  @ViewChild('staticTabs') staticTabs: TabsetComponent;
  constructor(private fb: FormBuilder, private contactService: ContactsService,
    private spinnerService: Ng4LoadingSpinnerService,
    private activatedRoute: ActivatedRoute,
    private toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.documentForm = this.getDocumentForm();
    this.activatedRoute.params.subscribe((params: Params) => {
      this.contactId = params['id'];
    });
    this.getDocuments();
  }

  archiveDocument(){
    this.contactService.archiveDocument(this.documentId).subscribe(res => {
      this.toastr.success(ToasterMessages.delete('Document'),toasterHeadings.Success);      
    },
      error => { this.toastr.error(ToasterMessages.deleteError('Document'), toasterHeadings.Error) })
  }

  deleteId(id){
    this.documentId = id
  }
  getDocuments(){
    var docObj = {ContactId: this.contactId }
    this.contactService.getDocumentsById(this.skip, this.take,docObj)
      .subscribe(allmembers => {
        this.DocumentList = allmembers.result.item.items;
        this.DocumentCount = allmembers.result.item.totalRecodeCount;
      },
      error => {
        this.toastr.error('Error in Loading Documents', 'Oops');
      }, () => {
      });
  }
  getDocumentForm(){
   return this.fb.group({
    file: ["", [Validators.required]],
    })
  }
  
  save() {
    this.isDocumentFormSubmitted = true;
    this.spinnerService.show(); 
    var docObj = {ContactId: this.contactId }
    this.contactService.saveDocument(this.fileList, docObj).subscribe(res => {
      this.toastr.success(ToasterMessages.save('Document'),toasterHeadings.Success);
      this.spinnerService.hide();
      this.getDocuments()
    }, error => {
      this.spinnerService.hide();
      this.toastr.error(ToasterMessages.saveError('Document'),toasterHeadings.Error);
    }, () => {

    })

  }

  fileUpload(event) {
    this.fileList = event.target.files;
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event:any) => {
        this.url = event.target.result;
      }
      reader.readAsDataURL(event.target.files[0]);
    }
  }
}
