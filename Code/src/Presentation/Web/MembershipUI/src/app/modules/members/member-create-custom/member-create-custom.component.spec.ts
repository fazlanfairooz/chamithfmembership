import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemberCreateCustomComponent } from './member-create-custom.component';

describe('MemberCreateCustomComponent', () => {
  let component: MemberCreateCustomComponent;
  let fixture: ComponentFixture<MemberCreateCustomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemberCreateCustomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberCreateCustomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
