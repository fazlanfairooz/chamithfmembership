import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemberGroupMemberComponent } from './member-group-member.component';

describe('MemberGroupMemberComponent', () => {
  let component: MemberGroupMemberComponent;
  let fixture: ComponentFixture<MemberGroupMemberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemberGroupMemberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberGroupMemberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
