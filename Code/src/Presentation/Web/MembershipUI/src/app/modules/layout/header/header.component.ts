import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'membership-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private router:Router) { }

  logoutUser(){
    localStorage.removeItem('Token_id');
    localStorage.removeItem('tenantObj');
    localStorage.removeItem('userObj');
    localStorage.removeItem('Image_Data');
    this.router.navigate(['/login'])
  }
  ngOnInit() {
  }

}
