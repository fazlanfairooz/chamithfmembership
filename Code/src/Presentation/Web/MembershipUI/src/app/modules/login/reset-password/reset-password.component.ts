import { EmailToken } from '../model/emailToken';
import { ToastsManager } from 'ng2-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RegisteruserService } from '../service/registeruser.service';
import { patternValidator } from '../../shared/patternvalidator/patternvalidator'
import { PatternModel } from '../../shared/patternvalidator/pattern';

@Component({
  selector: 'membership-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  isTokenValid = true;
  isPasswordMatch = false;
  resetPasswordForm: FormGroup;
  patternModel = new PatternModel();
  emailToken = new EmailToken();
  constructor(private fb: FormBuilder, private router: Router, private route: ActivatedRoute, private registerUser: RegisteruserService, public toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
  }
  createForm() {
    this.resetPasswordForm = this.fb.group({
      newPassword: ['',[Validators.required, patternValidator(this.patternModel.passwordRegex)]],
      confirmPassword: ['', [Validators.required]]
    }, { validator: this.comparePasswords });
  }
  comparePasswords(group: FormGroup) {
    let password = group.controls.newPassword.value;
    let confirmPassword = group.controls.confirmPassword.value;
    return (password === confirmPassword) ? null : { isPasswordNotSame: true }
  }
  validateResetEmail(emailToken) {
    if (emailToken.email === "") return;
    this.registerUser.validateResetEmail(emailToken)
      .subscribe(res => {
        localStorage.setItem('Token_id', res.result.item.token);
        this.isTokenValid = true;
      }, error => {
        this.isTokenValid = false;
      })
  }
  resetpassword() {
    if (!this.resetPasswordForm.valid) return;
    let password = this.resetPasswordForm.value.newPassword;
    let pwdObj = { value: password }
    this.registerUser.resetforgetpassword(pwdObj)
      .subscribe(res => {
        this.toastr.success('Password has been successfully Updated', 'Success');
        this.router.navigate(['/login']);
      }, error => {
        this.toastr.error('Password not created', 'Oops');
      })
  }
  goBack() {
    this.router.navigate(['/forgotpassword'])
  }
  ngOnInit() {
    this.route
      .queryParams
      .subscribe(params => {
        this.emailToken.email = params['email'];
        this.emailToken.token = params['token'];
      });
    if (this.emailToken.token != "") {
      this.validateResetEmail(this.emailToken);
    }
    this.createForm();
  }
}
