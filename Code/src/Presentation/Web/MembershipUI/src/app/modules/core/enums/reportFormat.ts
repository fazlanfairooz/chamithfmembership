export enum ReportFormats {
    PDF = "PDF",
    CSV = "CSV"
}