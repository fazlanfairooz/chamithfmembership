import { BaseService } from "../../core/services/base.service";
import { Injectable } from "@angular/core";
import { PermissionModel } from "../models/permission";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { HttpHeaders } from '@angular/common/http';

@Injectable()
export class PermissionService extends BaseService {

    inviterEmail = new PermissionModel();
   
    constructor(private http: HttpClient) {
        super();
      }

   sendInvite(inviterEmail){
    return this.http.post(`${this.baseApiEndPoint}identity/v1/security/permission`,
    (inviterEmail), this.httpOptions)
    .map(res => res).catch(this.errorHandler)
   }
   changepermissionsPassword(userLogin) {
    return this.http.put(`${this.baseApiEndPoint}identity/v1/security/changepermissionspassword`,
    userLogin, this.httpOptions)
      .map((response) => response)
      .catch(this.errorHandler)
  }

   getAllInviters()
   {
     return this.http.get(`${this.baseApiEndPoint}identity/v1/security/getpermissionusers`,
     this.httpOptions)
     .map((response) => response)
     .catch(this.errorHandler)
   }
  

  newuservalidate(emailToken) {
    return this.http.put(`${this.baseApiEndPoint}identity/v1/security/register/validaterequest?email=` + emailToken.email + `&token=` + emailToken.token,
    emailToken,this.httpOptions)
      .map((response) => response)
      .catch(this.errorHandler)
  }
 
  
}