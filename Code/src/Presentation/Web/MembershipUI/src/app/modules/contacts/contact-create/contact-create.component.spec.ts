import { ContactsService } from '../../core/services/contacts.service';
/*unit testing official tutorial:https://angular.io/guide/testing*/

import { Observable } from 'rxjs/Observable';
import { RouterTestingModule } from '@angular/router/testing';
import { LookupService } from '../../core/services/lookup.service';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Router, ActivatedRoute } from '@angular/router';

import { ContactCreateComponent } from './contact-create.component';
import { SharedModule } from '../../shared/shared.module';
import { DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';
import { By } from '@angular/platform-browser';


describe('ContactCreateComponent', () => {
  let component: ContactCreateComponent;
  let fixture: ComponentFixture<ContactCreateComponent>;
  let de: DebugElement;
  let el: HTMLElement;

  let lookupService: LookupService;
  let contactService: ContactsService;

  let mockRouter = {
    navigate: jasmine.createSpy('navigate')
  }

  let mockCountryData = {
    result: [
      { value: 0, text: "SL" },
      { value: 1, text: "AUS" }]
  }

  let mockMartialData = {
    result: [
      { value: 0, text: "Single" },
      { value: 1, text: "Married" }]
  }

  let mockCategoryData = {
    result: {
      item: [
        { value: 0, text: "Staff" },
        { value: 1, text: "Member" }
      ]
    }
  }



  let mockContactData = {
    result: {
      item:
        {
          personInfo: { firstName: "user" },
          contactInfo: {},
          workingInfo: {}
        }
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ContactCreateComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [SharedModule, BsDatepickerModule.forRoot(), HttpClientModule,
        RouterTestingModule],
      providers: [ContactsService, LookupService, { provide: ActivatedRoute, useValue: { 'params': Observable.of({ 'id': 100 }) } },
        { provide: Router, useValue: mockRouter }]
    })
      .compileComponents();//The TestBed.compileComponents method asynchronously compiles all the components configured in the testing module.
  }));

  beforeEach(() => {
    //The createComponent method returns a ComponentFixture, a handle on the test environment surrounding the created component.
    // The fixture provides access to the component instance itself and to the DebugElement, which is a handle on the component's DOM element.

    fixture = TestBed.createComponent(ContactCreateComponent);
    component = fixture.componentInstance;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;

    lookupService = fixture.debugElement.injector.get(LookupService);
    contactService = fixture.debugElement.injector.get(ContactsService);

    spyOn(contactService, "getContactById").and.returnValue(Observable.of(mockContactData))
    spyOn(lookupService, "getCountries").and.returnValue(Observable.of(mockCountryData))
    spyOn(lookupService, "getMartialStatus").and.returnValue(Observable.of(mockMartialData))
    spyOn(lookupService, "getContactTypes").and.returnValue(Observable.of(mockCategoryData))

    fixture.detectChanges();//update the view with values in components
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should call save method when click save button', () => {
    component.contactId = 0;
    fixture.detectChanges();
    de = fixture.debugElement.query(By.css("#btnSaveContact"));
    de.triggerEventHandler("click", null);

    fixture.whenStable().then(() => {
      expect(component.save()).toHaveBeenCalled();
    })
  });

  it('should call update method in person when click update button', () => {
    component.contactId = 10;
    fixture.detectChanges();
    de = fixture.debugElement.query(By.css("#btnPersonWork"));
    de.triggerEventHandler("click", null);

    fixture.whenStable().then(() => {
      expect(component.personUpdate()).toHaveBeenCalled();
    })
  });

  it('should call update method in contact when click update button', () => {
    component.contactId = 10;
    fixture.detectChanges();
    de = fixture.debugElement.query(By.css("#btnContactWork"));
    de.triggerEventHandler("click", null);

    fixture.whenStable().then(() => {
      expect(component.updateContact()).toHaveBeenCalled();
    })
  });

  it('should not display update button in person when contactId equals 0', () => {
    component.contactId = 0;
    fixture.detectChanges();
    de = fixture.debugElement.query(By.css("#btnContactWork"));
    expect(de).toBeNull();
  });

  it('should display update button in person when contactId greater than 0', () => {
    component.contactId = 10;
    fixture.detectChanges();
    de = fixture.debugElement.query(By.css("#btnContactWork"));
    expect(de).toBeTruthy();
  });


  // it('personal form invalid when empty', () => {
  //   component.personalForm.reset();
  //   expect(component.personalForm.valid).toBeFalsy();
  // });

  // it('contact form invalid when empty', () => {
  //   component.contactForm.reset();
  //   expect(component.contactForm.valid).toBeFalsy();
  // });

  // it('work form invalid when empty', () => {
  //   component.workForm.reset();
  //   expect(component.workForm.valid).toBeFalsy();
  // });

  // it('first name field validity', () => {
  //   component.personalForm.reset();
  //   let errors = {};
  //   let firstName = component.personalForm.controls['firstName'];
  //   expect(firstName.valid).toBeFalsy();

  //   // first name field is required
  //   errors = firstName.errors || {};
  //   expect(errors['required']).toBeTruthy();

  //   // Set first name to something
  //   firstName.setValue("firstName");
  //   errors = firstName.errors || {};
  //   expect(errors['required']).toBeFalsy();

  // });

  // it('last name field validity', () => {
  //   let errors = {};
  //   let lastName = component.personalForm.controls['lastName'];
  //   expect(lastName.valid).toBeFalsy();

  //   // last name field is required
  //   errors = lastName.errors || {};
  //   expect(errors['required']).toBeTruthy();

  //   // Set last name to something
  //   lastName.setValue("lastName");
  //   errors = lastName.errors || {};
  //   expect(errors['required']).toBeFalsy();

  // });


  // it('should country drop down list has first value as SL', () => {
  //   el = fixture.debugElement.query(By.css("#country")).queryAll(By.css("option"))[0].nativeElement;

  //   expect(el.textContent).toEqual("SL")

  // });

  // it('should category drop down list has first value as Staff', () => {
  //   el = fixture.debugElement.query(By.css("#contactType")).queryAll(By.css("option"))[0].nativeElement;

  //   expect(el.textContent).toEqual("Staff")

  // });

  // it('should martial status drop down list has first value as Single', () => {
  //   el = fixture.debugElement.query(By.css("#martialStatus")).queryAll(By.css("option"))[0].nativeElement;

  //   expect(el.textContent).toEqual("Single")

  // });

  // it('should display first name as user when cotntact id is greater than 0', () => {

  //   const input = fixture.debugElement.query(By.css('#fname')).nativeElement;
  //   expect(input.value).toEqual("user")

  // });

});
