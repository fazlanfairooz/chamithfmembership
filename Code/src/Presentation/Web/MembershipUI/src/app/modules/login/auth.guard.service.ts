import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()

export class AuthGuardService implements CanActivate 
{
  constructor(private router: Router){  
  }
  canActivate(route: ActivatedRouteSnapshot)
  {
    if ((localStorage.getItem('Token_id') === null || localStorage.getItem('Token_id') === undefined)) 
    {
      this.router.navigate(['/login']);
    }
    else
      return true;

  }
}

