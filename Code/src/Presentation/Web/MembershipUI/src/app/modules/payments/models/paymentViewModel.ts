export class PaymentViewModel {

    id:number;
    amount: number;
    remaining: number;
    transActionType: string;
    paymentType: string;
    paymentState: number;
    paymentDate:Date;
    description: string;
    memberName: string;
    name:string;
    createdDate:any;
    createdOn:any;
   editedOn :Date;
}