import { Pipe, PipeTransform } from '@angular/core';
import { HTMLElements } from '../../core/enums/htmlElement';

@Pipe({
  name: 'dataType'
})
export class DataTypePipe implements PipeTransform {

  dataTypeText: string;
  transform(value: any): any {
   
    switch (value) {
      case HTMLElements.text: {
        return this.dataTypeText = "Text"
      }
      case HTMLElements.textarea: {
        return this.dataTypeText = "Textarea"
      }
      case HTMLElements.checkbox: {
        return this.dataTypeText = "Checkbox"
      }
      case HTMLElements.checkboxGroup: {
        return this.dataTypeText = "Checkbox Group"
      }
      case HTMLElements.radio: {
        return this.dataTypeText = "Radio"
      }
      case HTMLElements.radioButtonGroup: {
        return this.dataTypeText = "Radio Button Group"
      }
      case HTMLElements.dropdown: {
        return this.dataTypeText = "Dropdown"
      }
      case HTMLElements.calendar: {
        return this.dataTypeText = "Calendar"
      }
      case HTMLElements.file: {
        return this.dataTypeText = "File"
      }
      case HTMLElements.email: {
        return this.dataTypeText = "Email"
      }
      case HTMLElements.profileImage: {
        return this.dataTypeText = "Profile Image"
      }
    }
  }
}

