import { DeleteModalComponent } from './../../shared/delete-modal/delete-modal.component';
import { ModalDirective, ModalModule } from 'ngx-bootstrap/modal';
import { Component, OnInit, Input, ViewContainerRef, ViewChild, Output } from '@angular/core';
import { ActivatedRoute, Params, Router } from '../../../../../node_modules/@angular/router';
import { ToastsManager } from 'ng2-toastr';

import { ElementControlService } from '../../core/custom-field-base/element-control.service';
import { ElementBase } from '../../core/custom-field-base/element-base';
import { TabModel } from '../models/tabModel';
import { ElementInitialisationService } from '../../core/custom-field-base/element-initialisation.service';
import { HTMLElements } from '../../core/enums/htmlElement';
import { RenderViewService } from '../services/render-view.service';
import { CustomMemberModel } from '../models/customMemberModel';
import { toasterHeadings } from '../../core/utility/toasterHeadings';
import { ToasterMessages } from '../../core/utility/toasterMessages';
import { CutomMemberService } from '../services/member.service';
import { MemberType } from '../../core/enums/memberType';
import { FormBuilder } from '../../../../../node_modules/@angular/forms';
import { LevelService } from '../../core/services/level.service';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { LevelInfoModel } from '../models/levelInfoModel';
import { BlockUiService } from '../../core/services/block-ui.service';

@Component({
  selector: 'membership-member-create-custom',
  templateUrl: './member-create-custom.component.html',
  styleUrls: ['./member-create-custom.component.scss']
})
export class MemberCreateCustomComponent implements OnInit {

  tab = new TabModel();
  tabs = new Array<TabModel>();
  customMemberModel = new CustomMemberModel();
  fields: ElementBase<any>[] = new Array();
  levelInfo = new LevelInfoModel();

  renderData = [];
  memberFieldKeyValue = [];

  memberType: MemberType;
  header: string;
  subHeader: string;
  fileList = FileList;
  documentFileList = FileList;
  documentName: string;
  files = Array<FileList>();
  memberId: number = 0;
  tabCount: number = 0;
  subscriptionChanged: string = null;
  imageLogoSource = 'assets/img/user.svg';
  levelPrice: number = 0;
  memberSubscriptionId: number = 0;
  levelCount: number;
  paymentCount: number;
  selectedTab: any = [];
  firstName: string = "Full Name";
  lastName: string;
  email: string= "Email Address";
  phone: string= "Phone Number";

  formUsed: boolean = false;
  isBlocked: boolean = false;
  isLevelEdit: boolean = false;
  isActive: boolean = true;
  isDisabled: boolean = false;
  isDynamicTabLoaded: boolean = false;
  isEdit: boolean = false;
  isMemberLevelActive: boolean = false;
  isMemberLevelDisabled: boolean = true;
  isMemberPaymentActive: boolean = false;
  isMemberPaymentDisabled: boolean = true;
  isMemberLevelSelected: boolean = false;
  isMemberPaymentSelected: boolean = false;

  @ViewChild('staticTabs') staticTabs: TabsetComponent;
  @Input() elements: ElementBase<any>[] = [];


  constructor(private fb: FormBuilder, private elementControlService: ElementControlService, private initalizeElement: ElementInitialisationService,
    private renderViewService: RenderViewService, private memberService: CutomMemberService, private toastr: ToastsManager, private blockUiService: BlockUiService,
    vcr: ViewContainerRef, private activatedRoute: ActivatedRoute, private levelService: LevelService, private router: Router) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.memberType = params['type'];
      this.memberId = params['id'];
      this.subscriptionChanged = params['state'];

      if (this.memberId != 0 && this.memberId != undefined && this.subscriptionChanged == null && this.subscriptionChanged == undefined) {
        this.header = "Update Member";
        this.subHeader = "Use this wizard to update member";
        this.isEdit = true;
        this.renderUpdateView();
      } else if (this.memberId != 0 && this.memberId != undefined && this.subscriptionChanged != null && this.subscriptionChanged != undefined) {
        this.memberLevelUpdate();
      }
      else {
        this.renderView();
        this.header = "Create New Member";
        this.subHeader = "Use this wizard to Create New Member";
      }
    });
  }


  renderView() {
    this.isBlocked = true;
    this.renderViewService.getRenderFieldData(this.memberType)
      .subscribe(renderElements => {
        this.renderData = renderElements.result.memberTypeTabs;

        if (this.isEdit) {
          for (let memberField of this.memberFieldKeyValue) {
            for (let fields of this.renderData) {
              for (let element of fields.tabsFields) {
                if (element.key == memberField.key) {
                  if (element.displayName == 'First Name') {
                    this.firstName = memberField.value;
                  } else if (element.displayName == 'Last Name') {
                    this.lastName = memberField.value;
                  } else if (element.displayName == 'Email') {
                    this.email = memberField.value;
                  } else if (element.displayName == 'Phone') {
                    this.phone = memberField.value;
                  }
                  if (element.value == '') {
                    element.value = memberField.value;
                    break;
                  } else {
                    var multipleValues = JSON.parse(element.value);
                    var hh = JSON.stringify(multipleValues.push({ 'value': memberField.value, 'text': 'selected' }));
                    element.value = multipleValues;
                    break;
                  }
                }
              }
            }
          }
        }

        for (let t of this.renderData) {
          if (t.isVisible) {
            this.tab.displayName = t.displayName;
            this.tab.tabDescription = t.tabDescription;

            for (let fields of t.tabsFields) {
              this.addFieldsToTabs(fields);
            }

            this.tab.elements = this.fields.sort((a, b) => a.order - b.order);
            this.tab.form = this.elementControlService.toFormGroup(this.tab.elements);

            this.tab.active = this.isActive;
            this.tab.disabled = this.isDisabled;
            this.tab.isFormSubmitted = false;

            this.tabs.push(this.tab);
            this.tab.tabCount = this.tabs.length;

            if (this.tab.tabCount == 1) {
              this.selectedTab[this.tab.tabCount] = true;
            } else {
              this.selectedTab[this.tab.tabCount] = false;
            }

            this.isActive = false;
            this.isDisabled = this.isEdit == true ? false : true;
            this.fields = [];
            this.tab = new TabModel();
          }
        }

        this.tabCount = this.tabs.length;
        this.levelCount = this.tabCount + 1;
        this.paymentCount = this.levelCount + 1;
        this.isDynamicTabLoaded = true;
        this.isBlocked = false;
      }, error => {
        this.isBlocked = false;
        this.toastr.error('Error in rendering the view', 'Oops');
      });
  }

  renderUpdateView() {
    this.memberService.getById(this.memberId)
      .subscribe(res => {
        if (this.getImageName(res.result.profileImage) != "noprofile.png") {
          this.imageLogoSource = res.result.profileImage;
        }
        this.memberFieldKeyValue = res.result.memberFieldKeyValue;
        this.renderView();
      }, error => {
        this.toastr.error('Error in rendering the view', 'Oops');
      });
  }

  memberLevelUpdate() {
    this.isLevelEdit = true;
    this.isDynamicTabLoaded = true;
    this.isMemberLevelDisabled = false;
    this.isMemberLevelActive = true;
    this.levelCount = 1;
    this.paymentCount = 2;
    this.isMemberLevelSelected = true;
    this.header = "Update Subscription Package";
    this.subHeader = "Use this wizard to update subscription";
  }

  addFieldsToTabs(fields) {
    switch (fields.dataType) {
      case HTMLElements.text:
        this.fields.push(this.initalizeElement.textElement(fields));
        break;
      case HTMLElements.checkbox:
        this.fields.push(this.initalizeElement.checkboxElement(fields));
        break;
      case HTMLElements.checkboxGroup:
        this.fields.push(this.initalizeElement.checkboxgroupElement(fields));
        break;
      case HTMLElements.radioButtonGroup:
        this.fields.push(this.initalizeElement.radioGroupElement(fields));
        break;
      case HTMLElements.dropdown:
        this.fields.push(this.initalizeElement.dropdownElement(fields));
        break;
      case HTMLElements.textarea:
        this.fields.push(this.initalizeElement.textareaElement(fields));
        break;
      case HTMLElements.calendar:
        this.fields.push(this.initalizeElement.calendarElement(fields));
        break;
      case HTMLElements.file:
        this.fields.push(this.initalizeElement.fileElement(fields));
        break;
      case HTMLElements.profileImage:
        this.fields.push(this.initalizeElement.profileElement(fields));
        break;
      case HTMLElements.email:
        this.fields.push(this.initalizeElement.emailElement(fields));
        break;
    }
  }

  goToNextTab(nextTab) {
    this.formUsed = true;
    this.tabs[nextTab - 1].isFormSubmitted = true;
    if (this.tabs[nextTab - 1].form.valid == false) return;
    this.isBlocked = true;
    this.selectedTab[nextTab + 1] = true;
    this.tabs[nextTab].disabled = false;
    this.tabs[nextTab].active = true;
    this.isBlocked = false;
  }

  saveMember(tab) {
    this.tabs[tab - 1].isFormSubmitted = true;
    if (this.tabs[tab - 1].form.valid == false) return;

    this.isBlocked = true;

    if (this.memberId != 0 && this.memberId != undefined) {
      this.tabs[tab - 1].active = false;
      this.isMemberLevelDisabled = false;
      this.isMemberLevelActive = true;
      this.isBlocked = false;
      return;
    }
    this.customMemberModel.memberType = this.memberType;

    for (let tabs of this.tabs) {
      let formValues = tabs.form.value;
      for (let formField of tabs.elements) {
        this.customMemberModel.memberFieldKeyValue.push({ 'key': formField.key, 'value': formValues[formField.key] });
      }
    }
    this.memberService.createMember(this.fileList, this.customMemberModel)
      .subscribe(res => {
        this.isMemberLevelSelected = true;
        this.tabs[tab - 1].active = false;
        this.memberId = res.result;
        this.isMemberLevelDisabled = false;
        this.isMemberLevelActive = true;
        this.isBlocked = false;

        if (this.documentFileList.length != 0) {
          var docObj = { MemberId: this.memberId }
          this.memberService.saveDocument(this.documentFileList, docObj).subscribe();
        }
      }, error => {
        this.isBlocked = false;
        this.toastr.error(ToasterMessages.saveError('Member'), toasterHeadings.Error);
      });
  }

  updateMember(tab) {
    this.tabs[tab - 1].isFormSubmitted = true;
    if (this.tabs[tab - 1].form.valid == false) return;

    this.customMemberModel.memberId = this.memberId;
    this.customMemberModel.memberType = this.memberType;

    for (let tabs of this.tabs) {
      let formValues = tabs.form.value;
      for (let formField of tabs.elements) {
        this.customMemberModel.memberFieldKeyValue.push({ 'key': formField.key, 'value': formValues[formField.key] });
      }
    }

    this.memberService.updateMember(this.customMemberModel)
      .subscribe(res => {
        this.toastr.success(ToasterMessages.update('Member'), toasterHeadings.Success);
      }, error => {
        this.toastr.error(ToasterMessages.updateError('Member'), toasterHeadings.Error);
      });
  }

  goToPaymentTab($event) {
    this.levelPrice = $event.levelPrice;
    this.memberSubscriptionId = $event.memberSubscriptionId;

    if ($event.levelCreated == true) {
      this.isMemberLevelActive = false;
      this.isMemberPaymentDisabled = false;
      this.isMemberPaymentActive = true;
      this.isMemberPaymentSelected = true;
    }
  }

  fileUpload(event) {
    this.documentFileList = event.target.files;
  }

  imageUpload(event) {
    this.fileList = event.target.files;
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageLogoSource = event.target.result;
    }
    reader.readAsDataURL(event.target.files[0]);
  }

  private getImageName(profileImage) {
    let splitFilePath = profileImage.split("/");
    return splitFilePath[splitFilePath.length - 1]
  }

  archiveMember() {
    this.isBlocked = true;
    this.memberService.archiveMember(this.memberId).subscribe(res => {
      this.toastr.success(ToasterMessages.delete('Member'), toasterHeadings.Success);
      this.isBlocked = false;
      setTimeout(() => {
        this.router.navigate(['/members']);
      }, 1000);
    },
      error => {
        this.isBlocked = false;
        this.toastr.error(ToasterMessages.deleteError('Member'), toasterHeadings.Error)
      })
  }

  deleteMember() {
    this.isBlocked = true;
    this.memberService.permanentDeleteMember(this.memberId).subscribe(res => {
      this.toastr.success(ToasterMessages.cancel('Member'), toasterHeadings.Success);
      this.isBlocked = false;
      setTimeout(() => {
        this.router.navigate(['/members']);
      }, 1000);
    },
      error => {
        this.isBlocked = false;
        this.toastr.error(ToasterMessages.deleteError('Member'), toasterHeadings.Error)
      })
  }
  ngOnDestroy(){
    if(this.formUsed){
//add the modal     
    }
  }
}
