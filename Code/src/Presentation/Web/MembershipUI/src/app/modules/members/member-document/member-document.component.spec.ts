import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemberDocumentComponent } from './member-document.component';

describe('MemberDocumentComponent', () => {
  let component: MemberDocumentComponent;
  let fixture: ComponentFixture<MemberDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemberDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
