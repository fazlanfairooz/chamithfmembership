import { FormControl } from '@angular/forms';
import { LookupService } from '../../core/services/lookup.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { SharedModule } from '../../shared/shared.module';
import { BlockUiService } from '../../core/services/block-ui.service';
import { MemberSubscriptionService } from '../services/member-subscription.service';
import { MembersService } from '../../core/services/members.service';
import { By } from '@angular/platform-browser';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';
import { MemberLevelCreateComponent } from './member-level-create.component';

fdescribe('MemberLevelCreateComponent', () => {
  let component: MemberLevelCreateComponent;
  let fixture: ComponentFixture<MemberLevelCreateComponent>;
  let de: DebugElement;
  let el: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, BsDatepickerModule.forRoot(), HttpClientModule,
        RouterTestingModule],
      providers: [MembersService, MemberSubscriptionService,LookupService, BlockUiService],
      declarations: [ MemberLevelCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberLevelCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call save method when click save button', () => {
    fixture.detectChanges();
    de = fixture.debugElement.query(By.css("#btnSave"));
    de.triggerEventHandler("click", null);

    fixture.whenStable().then(() => {
      expect(component.save()).toHaveBeenCalled();
    })
  });

  it('Member Level name field validity', () => {
    component.memberLevelForm.reset();
    let errors = {};
    let Name = component.memberLevelForm.controls['name'];
    expect(Name.valid).toBeFalsy();

    // Level name field is required
    errors = Name.errors || {};
    expect(errors['required']).toBeTruthy();

    // Set Level name to something
    Name.setValue("name");
    errors = Name.errors || {};
    expect(errors['required']).toBeFalsy();

  });

  it('Member Level Capacity field disable check', () => {
    component.memberLevelForm.reset();
    let errors = {};
    let Radio = component.memberLevelForm.controls['subscriptionTypeId'];
    Radio.setValue('1');
    de = fixture.debugElement.query(By.css("#divCapacity"));
    el = fixture.debugElement.query(By.css("#divCapacity")).nativeElement;
    expect(de.properties.hidden).toBeFalsy();
  });

  it('Member Level Capacity field enable check', () => {
    component.memberLevelForm.reset();
    let errors = {};
    let Radio = component.memberLevelForm.controls['subscriptionTypeId'];
    Radio = new FormControl('2');
    de = fixture.debugElement.query(By.css("#divCapacity"));
    el = fixture.debugElement.query(By.css("#divCapacity")).nativeElement;
    console.log(el)
    expect(de.properties.hidden).toBeTruthy();
  });


  // it('Calculate Date or Specific date check', () => {
  //   component.memberLevelForm.reset();
  //   var event = {
  //     defaultValue : 1,
  // };
  //   let errors = {};
  //   let radioButton = component.memberLevelForm.controls['Limited'];
  //   radioButton.setValue("2");
  //   de.triggerEventHandler("change", null);
  //   fixture.whenStable().then(() => {
  //     expect(component.dateRange(event)).toHaveBeenCalled();
  //   })
  // });

  // it('On Specific date click Date Range Visible', () => {
  //   component.memberLevelForm.reset();
  //   let errors = {};
  //   let radioButton = component.memberLevelForm.controls['Limited'];
  //   radioButton.setValue("2");
  //   el = fixture.debugElement.query(By.css("#divDateDisable")).nativeElement;
  //   expect(el.style.visibility).toBe('true');
  // });

});
