export enum MembershipLevelStates {
    Annualy = "1",
    Monthly = "2",
    Weekly = "3",
    Daily = "4",
    LifeTime = "5"
}