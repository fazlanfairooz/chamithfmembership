import { LevelService } from './../../core/services/level.service';
import { Observable } from 'rxjs/Observable';
import { toasterHeadings } from '../../core/utility/toasterHeadings';
import { MembershipLevelStates } from '../../core/enums/membershipLevelState';
import { ToastsManager } from 'ng2-toastr';
import 'rxjs/add/operator/mergeMap';
import { PaymentService } from '../../core/services/payment.service';
import { BlockUiService } from '../../core/services/block-ui.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MemberSubscriptionService } from '../../members/services/member-subscription.service';
import { LookupService } from '../../core/services/lookup.service';
import { MembersService } from '../../core/services/members.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LevelInfoModel } from '../../members/models/levelInfoModel';
import { SubscribedLevelInfo } from '../../members/models/subscribedLevelInfo';
import { MemberAllDataModel } from '../../members/models/memberAllDataModel';
import { MembershipState } from '../../core/enums/membershipState';
import { TimeLineType } from '../../core/enums/TimeLineType';
import { PaymentStates } from '../../core/enums/paymentState';
import { TerminationModel } from '../../members/models/terminationModel';
import { MemberSubscriptionModel } from '../../members/models/memberSubscriptionModel';
import { MemberDocumentModel } from '../../members/models/memberDocumentModel';
import { PatternModel } from '../../shared/patternvalidator/pattern';
import { WorkModel } from '../../contacts/models/workModel';
import { MemberModel } from '../../members/models/memberModel';
import { ContactModel } from '../../contacts/models/contactModel';
import { TimeLine } from '../../members/models/memberTimelineModel';
import { PaymentModel } from '../../payments/models/paymentModel';
import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { ToasterMessages } from '../../core/utility/toasterMessages';
import { patternValidator } from '../../shared/patternvalidator/patternvalidator';

@Component({
  selector: 'membership-organization-create',
  templateUrl: './organization-create.component.html',
  styleUrls: ['./organization-create.component.scss']
})
export class OrganizationCreateComponent implements OnInit {

  url: any;
  memberForm: FormGroup;
  contactForm: FormGroup;
  workForm: FormGroup;
  membershipForm: FormGroup;
  paymentForm: FormGroup;

  IsRequred = true;
  isMemberFormSubmitted = false;
  isContactFormSubmitted = false;
  isWorkFormSubmitted = false;
  isMembershipFormSubmitted = false;
  showimage = false;
  isLevelDropDownClicked = false;
  isPackageUtilized = false;
  isDisplayLevels = false;
  isBlocked = false;
  isPayLater = false;
  isRequested = false;
  isMobileVerified = false;
  isEmailVerified = false;
  showMobile = false;
  showEmail = false;

  memberId = 0;
  phone: string;
  email: string;
  countryCode = "";
  workCountryCode = "";
  currency = "";
  profileImageName = "";
  countries = [];
  workcountries = [];
  maritialStatus = [];
  contactTypes = [];
  memberLevels = [];
  transActionTypes = [];
  paymentTypes = [];

  contact = new ContactModel();
  member = new MemberModel();
  work = new WorkModel();
  payment = new PaymentModel();
  timeLine = new TimeLine();
  subscription: number = 0;
  patternModel = new PatternModel();
  document = new MemberDocumentModel();
  memberSubscription = new MemberSubscriptionModel();
  terminationModel = new TerminationModel();
  paymentState = PaymentStates;
  timeLineType = TimeLineType;
  membershipState = MembershipState;
  selectedSubscription: number = 0;
  memberAllData = new MemberAllDataModel();
  subscribedLevelInfo = new SubscribedLevelInfo();
  levelInfo = new LevelInfoModel();
  fileList: FileList;
  fileDocList: FileList;
  maxDate = new Date();
  memberHeading ='New Organization';
  subheading ="Please fill in the following information to create a new Organization";
  terminateNote: any;
  todaydate: number;
  subscriptionChanged: string = null;
 

  //Multiselect Elements
  dropdownSettings = {};
  memberGroupDropdown = [];
  memberGroups = [];
  selectedItems = [];


  language="";
  createword :any;
  editword:any;
  _subheading:any;
  edithubheading :any;
  profileheading:any;

  @ViewChild('terminateNoteModal') public terminateNoteModal: ModalDirective;
  @ViewChild('staticTabs') staticTabs: TabsetComponent;

  constructor(private fb: FormBuilder, private memberService: MembersService,
    private memberSubscriptionService: MemberSubscriptionService,
    private lookUpService: LookupService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private blockUiService: BlockUiService,
    private paymentService: PaymentService,
    private levelService: LevelService,
    private toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.currency  = localStorage.getItem('Currency');
    this.getTranslator();
    this.profileImageName = this.profileheading;
    this.memberForm = this.getMemberForm();
    this.contactForm = this.getContactForm();
    this.workForm = this.getWorkForm();
    this.paymentForm = this.getPaymentForm();
    this.membershipForm = this.getMembershipForm();
    this.getmemberLevelData();
    this.getMemberLookupData();
    this.getMemberGroups();
      
    let enteredPhone = this.contactForm.get('phone');
    enteredPhone.valueChanges
      .debounceTime(1000)
      .distinctUntilChanged().subscribe(phone => {
        this.phone = phone;
        this.phoneVerified();
      })

      let enteredEmail = this.contactForm.get('email');
      enteredEmail.valueChanges
        .debounceTime(1000)
        .distinctUntilChanged().subscribe(email => {
          this.email = email;
          this.emailVerified();
        })

    this.activatedRoute.params.subscribe((params: Params) => {
      this.memberId = params['id'];
      this.subscriptionChanged = params['state'];

      if (this.memberId && this.memberId != 0) {
        this.memberHeading = this.editword;//"Edit Member"
        this.subheading =  this.edithubheading;
        this.getMember();
      }
      else {
        this.memberId = 0;
        this.memberHeading = this.createword;
        this.subheading = this._subheading;
        this.url = 'assets/img/skyline.svg';
      }
    });
   
    this.dropdownSettings = {
      singleSelection: false,
      placeholder: "Search a Group to Add...",
      text:"Select Group",
      selectAllText:'Select All',
      unSelectAllText:'UnSelect All',
      enableSearchFilter: true,
    };
  }

  getTranslator(){
    this.language = localStorage.getItem('Language');
    if (this.language == "en") {
      this.createword = 'New Organization';
      this.editword ="Edit Organization";
      this.profileheading ='Choose your Organization Logo';
      this._subheading ="Please fill in the following information to create a new Organization";
      this.edithubheading ="Please fill in the following information to edit a Organization";

    }
    if (this.language == "sh") {
      this.createword = 'නව ආයතනය';
      this.editword = "ආයතනය සංස්කරණය කරන්න";
      this.profileheading = 'ඔබගේ පැතිකඩ පින්තූරය තෝරන්න';
      this._subheading = "නව ආයතනය සෑදීමට පහත සඳහන් තොරතුරු පුරවන්න";
      this.edithubheading = "ආයතනය සංස්කරණය කිරීම සඳහා පහත සඳහන් තොරතුරු පුරවන්න";
    }
    if (this.language == "tm") {
      this.createword = 'புதிய அமைப்பு';
      this.editword = "அமைப்பு திருத்து";
      this.profileheading = 'உங்கள் சுயவிவர படத்தை தேர்வு செய்க';
      this._subheading = "ஒரு புதிய அமைப்பு உருவாக்க பின்வரும் தகவலை பூர்த்தி செய்க";
      this.edithubheading = "புதிய அமைப்பு திருத்த பின்வரும் தகவலை பூர்த்தி செய்யவும்";
    }
  }

  getmemberLevelData() {
    this.levelService.getAllMemberLevel(0, 0)
      .subscribe(memberLevels => {
        this.memberLevels = memberLevels.result.items;
        this.currency  = localStorage.getItem('Currency');        
      });
  }

  phoneVerified() {
    let contactObj = { phone: this.phone }
    this.memberService.verifyPhone(contactObj)
      .subscribe(res => {
        if (res.result.item === false) {
          this.showMobile = false;                    
          return this.isMobileVerified = true;
        }
        else if (res.result.item === true) {
          this.showMobile = true;          
          return this.isMobileVerified = false;
        }
      }, error => {
        this.toastr.error('Host name is not validated', 'Oops');
      });
  }

  emailVerified() {
    let contactObj = { email: this.email }
    this.memberService.verifyemail(contactObj)
      .subscribe(res => {
        if (res.result.item === false) {
          this.showEmail = false;                    
          return this.isEmailVerified = true;
        }
        else if (res.result.item === true) {
          this.showEmail = true;          
          return this.isEmailVerified = false;
        }
      }, error => {
        this.toastr.error('Host name is not validated', 'Oops');
      });
  }

  displayLevelInfo() {
    var today = new Date();
    var year = today.getFullYear();
    var month = today.getMonth();
    var day = today.getDate();

    this.isPackageUtilized = false;
    this.isLevelDropDownClicked = true;
    this.isRequested = true;
    this.selectedSubscription = this.membershipForm.value.membershipLevel;
    this.levelInfo = this.memberLevels.find(t => t.id == this.selectedSubscription);

    this.paymentForm.patchValue({
      amount: this.currency +" "+ this.levelInfo.price
    })

    this.payment.name = this.levelInfo.name;
    this.levelInfo.subscriptionFrom = new Date();

    if (this.levelInfo.membershipExpired == MembershipLevelStates.Annualy) {
      this.levelInfo.subscriptionTo = new Date(year + 1, month, day)
    } else if (this.levelInfo.membershipExpired == MembershipLevelStates.Monthly) {
      this.levelInfo.subscriptionTo = new Date(year, month + 1, day)
    } else if (this.levelInfo.membershipExpired == MembershipLevelStates.Weekly) {
      this.levelInfo.subscriptionTo = new Date(year, month, day + 7)
    } else if (this.levelInfo.membershipExpired == MembershipLevelStates.Daily) {
      this.levelInfo.subscriptionTo = new Date(year, month, day + 1)
    }

    if (this.levelInfo.capacity != 0) {
      this.memberSubscriptionService.getAllMemberSubscription(this.selectedSubscription)
        .subscribe(membersubscription => {
          let utilizeCount = membersubscription.result.totalRecordCount;
          let remainingCount = this.levelInfo.capacity - utilizeCount;

          if (remainingCount == 0) {
            this.isPackageUtilized = true;
          }
          this.levelInfo.remainingCount = remainingCount;
          this.levelInfo.utilizeCount = utilizeCount;
        });
    }
  }


  save() {
    this.isMemberFormSubmitted = true;
    this.isContactFormSubmitted = true;
    this.isWorkFormSubmitted = true;
    this.isMembershipFormSubmitted = true;

    if (!this.membershipForm.valid || !this.memberForm.valid || !this.contactForm.valid) return;

    this.isBlocked = true;

    this.payment.paymentState = this.isPayLater ? PaymentStates.Pending : PaymentStates.Paid;
    this.payment.amount = this.levelInfo.price

    var member = Object.assign({}, this.member, this.memberForm.value)
    var contact = Object.assign({}, this.contact, this.contactForm.value)
    var work = Object.assign({}, this.work, this.workForm.value)
    var payment = Object.assign({}, this.payment, this.paymentForm.value)

    this.memberAllData.memberInfo = member;
    this.memberAllData.contactInfo = contact;
    this.memberAllData.workingInfo = work;
    this.memberAllData.subscription = this.membershipForm.value.membershipLevel;

    if (this.membershipForm.value.memberGroup != null) {
      var groupIds = this.membershipForm.value.memberGroup.map(function (item) { return item.id; });
      this.memberAllData.groups = groupIds;
    }
    this.memberAllData.id = this.memberId;
    this.memberService.saveMember(this.fileList, this.memberAllData).
      mergeMap(member => {
        payment.memberSubscriptionId = member.result.subscription;
        payment.memberId = member.result.memberInfo.id;
        this.timeLine.subscriptionName = this.levelInfo.name;
        this.timeLine.subscriptionId = this.membershipForm.value.membershipLevel;
        this.timeLine.memberId = payment.memberId;
        this.timeLine.amount = this.levelInfo.price
        this.timeLine.templateType = this.timeLineType.MembershipCreated;
        if(payment.paymentState == 3){
          this.timeLine.name = "Paid";
        }
        else{
          this.timeLine.name = "UnPaid";
        }
        
        return this.paymentService.savePaymnets(payment)
      })
      .mergeMap(payment => {
        this.timeLine.paymentId = payment.result.paymentMasterId;
        return this.levelService.changeMemberStatus(payment.result);
      })
      .mergeMap(payments => {
        return this.memberService.saveTimeLine(this.timeLine)
      }).
      subscribe(response => {
        this.toastr.success(ToasterMessages.save('Member'), toasterHeadings.Success);
        this.isBlocked = false;
        setTimeout(() => {
          this.router.navigate(["/members"])
        },1000);
        
      }, error => {
        this.isBlocked = false;
        this.toastr.error(ToasterMessages.saveError('Member'), toasterHeadings.Error);
      }, () => {

      })
  }
  
  getMember() {
    this.isBlocked = true;
    this.memberService.getMemberById(this.memberId).subscribe(res => {
      this.patchMemberForm(res.result.item.memberInfo);
      this.patchContactForm(res.result.item.contactInfo);
      this.patchWorkForm(res.result.item.workingInfo);
      this.subscription = res.result.item.subscription;
      if (this.subscription != 0) {
        this.getSubscribedMembershipData(this.memberId);
      }
      this.isBlocked = false;
      this.isBlocked = false;
    },
      error => {
        this.isBlocked = false;
        this.toastr.error(ToasterMessages.loadError('Member'), toasterHeadings.Error);
      })
  }

  payLater() {
    this.isPayLater = true;
    this.payment.paymentState = PaymentStates.Pending;
    this.save();
  }

  private patchSubscriptionForm(subscription) {
    this.membershipForm.patchValue({
      membershipLevel: subscription
    })
  }

  private patchMemberForm(member: MemberModel) {
    this.member = member;
    this.memberForm.patchValue({
      firstName: member.firstName,
      contactCategoryId: member.contactCategoryId,
    })
  }

  private patchContactForm(contact: ContactModel) {
    this.contact = contact;
    this.countries.forEach(element => {
      if (element.value == contact.country) {
        this.countryCode = element.phoneCode
      }
    });
    this.contactForm.patchValue({
      country: contact.country,
      city: contact.city,
      zip: contact.zip,
      address: contact.address,
      phone: contact.phone,
      telephone: contact.telephone,
      email: contact.email
    })
  }

  countryCodechange(event) {
    this.countries.forEach(element => {
      if (element.text.trim() == event.label) {
        this.countryCode = element.phoneCode
      }
    });
  }

  workCountryCodechange(event) {
    this.workcountries.forEach(element => {
      if (element.text.trim() == event.label) {
        this.workCountryCode = element.phoneCode
      }
    });
  }

  private patchWorkForm(work: WorkModel) {
    this.work = work;
    this.workcountries.forEach(element => {
      if (element.value == work.country) {
        this.workCountryCode = element.phoneCode
      }
    });
    this.workForm.patchValue({
      designation: work.designation,
      country: work.country,
      company: work.company,
      address: work.address,
      telephone: work.telephone,
      phone: work.phone,
      workcontactname: work.workContactName
    })

  }

  setTabActive(tab_id: number) {
    switch (tab_id) {
      case 1: {
        this.isMemberFormSubmitted = true;
        if (!this.memberForm.valid) return;
        this.staticTabs.tabs[tab_id].active = true;
        break;
      }
      case 2: {
        this.isContactFormSubmitted = true;
        if (!this.contactForm.valid || !this.isMobileVerified || !this.isEmailVerified) return;        
        this.staticTabs.tabs[tab_id].active = true;
        break;
      }
      case 3: {
        this.isWorkFormSubmitted = true;
        if (!this.workForm.valid) return;
        this.staticTabs.tabs[tab_id].active = true;
        break;
      }
      case 4: {
        this.isMembershipFormSubmitted = true;
        if (!this.membershipForm.valid) return;
        this.staticTabs.tabs[tab_id].active = true;
        break;
      }
    }
  }

  updateWork() {
    if (!this.contactForm.valid) return;
    this.isBlocked = true;
    var work = Object.assign({}, this.work, this.workForm.value)
    this.memberService.updateWork(work).subscribe(res => {
      this.toastr.success(ToasterMessages.update('Work info'), toasterHeadings.Success);
      this.isBlocked = false;
      setTimeout(() => {
        this.router.navigate(['/members/memberprofile/',this.memberId]);
      },1000);   
    },
      error => {
        this.isBlocked = false;
        this.toastr.error(ToasterMessages.updateError('Work info'), toasterHeadings.Error)
      })
  }

  updateContact() {
    if (!this.contactForm.valid) return;
    this.isBlocked = true;
    var contact = Object.assign({}, this.contact, this.contactForm.value)
    this.memberService.updateContact(contact).subscribe(res => {
      this.toastr.success(ToasterMessages.update('Contact info'), toasterHeadings.Success);
      this.isBlocked = false;
      setTimeout(() => {
        this.router.navigate(['/members/memberprofile/',this.memberId]);
      },1000);          
    },
      error => {
        this.isBlocked = false;
        this.toastr.error(ToasterMessages.updateError('Contact info'), toasterHeadings.Error)
      })
  }

  updateMember() {
    if (!this.memberForm.valid) return;
    this.isBlocked = true;
    if (this.member.profileImage)
      this.member.profileImage = this.getImageName(this.member.profileImage);

    var member = Object.assign({}, this.member, this.memberForm.value)
    this.memberService.updateMember(this.fileList, member).subscribe(res => {
      this.toastr.success(ToasterMessages.update('Member info'), toasterHeadings.Success);
      this.isBlocked = false;
      setTimeout(() => {
        this.router.navigate(['/members/memberprofile/',this.memberId]);
      },1000);           
    },
      error => {
        this.isBlocked = false;
        this.toastr.error(ToasterMessages.updateError('Member info'), toasterHeadings.Error)
      })
  }

  archiveMember() {
    this.isBlocked = true;
    this.memberService.archiveMember(this.memberId).subscribe(res => {
      this.toastr.success(ToasterMessages.delete('Member'), toasterHeadings.Success);
      this.isBlocked = false;
      setTimeout(() => {
        this.router.navigate(['/members']);
      },1000);  
    },
      error => {
        this.isBlocked = false;
        this.toastr.error(ToasterMessages.deleteError('Member'), toasterHeadings.Error)
      })
  }

  private getPaymentForm() {
    return this.fb.group({
      transActionType: [1, [Validators.required]],
      paymentType: [1, [Validators.required]],
      amount: [{ value: "", disabled: true }, [Validators.required]],
      description: [""],

    })
  }

  private getMemberForm() {
    return this.fb.group({
      firstName: ["", [Validators.required, Validators.maxLength(50), patternValidator(this.patternModel.whiteSpace)]],
      website: ["", [patternValidator(this.patternModel.url)]],
      dob: [new Date(), ""],
      martialStatus: [1],
      identity: [""],
    })
  }

  private getContactForm() {
    return this.fb.group({
      country: [null, [Validators.required]],
      city: ["", [Validators.required, patternValidator(this.patternModel.whiteSpace)]],
      zip: [""],
      address: ["", [Validators.required, patternValidator(this.patternModel.whiteSpace)]],
      phone: ["", [Validators.required, patternValidator(this.patternModel.phoneregx)]],
      telephone: ["", [patternValidator(this.patternModel.phoneregx)]],
      email: ["", [Validators.required, patternValidator(this.patternModel.emailRegex)]]
    })
  }

  private getWorkForm() {
    return this.fb.group({
      country: [null, [Validators.required]],
      designation: ["", [Validators.required, patternValidator(this.patternModel.whiteSpace)]],
      company: [""],
      address: [""],
      workcontactname: ["", [Validators.required, Validators.maxLength(50), patternValidator(this.patternModel.whiteSpace)]],      
      phone: ["", [Validators.required, patternValidator(this.patternModel.phoneregx)]],
      telephone: ["", [patternValidator(this.patternModel.phoneregx)]],
    })
  }

  private getMembershipForm() {
    return this.fb.group({
      membershipLevel: [null, [Validators.required]],
      memberGroup: []
    })
  }

  fileUpload(event) {
    this.fileList = event.target.files;
    this.profileImageName = this.fileList[0].name
    if (event.target.files && event.target.files[0]) {
      this.showimage = true;
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.url = event.target.result;
      }
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  private getMemberLookupData() {
    Observable.forkJoin(
      this.lookUpService.getCountries(),
      this.lookUpService.getMartialStatus(),
      this.paymentService.getTransActionTypes(),
      this.paymentService.getPaymentTypes(),
    ).subscribe(data => {
      this.countryCode = "";
      this.countries = data[0].result.item;
      this.workcountries = data[0].result.item;
      this.maritialStatus = data[1].result;
      this.transActionTypes = data[2].result.item;
      this.paymentTypes = data[3].result.item;
    },
      error => {
      },
      () => {
      })
  }

  private getImageName(profileImage) {
    let splitFilePath = profileImage.split("/");
    return splitFilePath[splitFilePath.length - 1]
  }

  getMemberGroups() {
    this.lookUpService.getMembershipGroups()
      .subscribe(groups => {
        this.memberGroups = groups.result.item;
        this.memberGroups.forEach(group => {
          this.memberGroupDropdown.push({ "id": group.value, "itemName": group.text });
        });
      }, error => {
      })
  }
  getSubscribedMembershipData(memberId) {
    this.isBlocked = true;
    this.memberSubscriptionService.getSubscribedMembershipData(memberId)
      .subscribe(res => {
        this.subscribedLevelInfo = (res.result);

        this.paymentForm.patchValue({
          amount: this.subscribedLevelInfo.price
        })
    this.isBlocked = false;    
      },
        error => {
          this.toastr.error(ToasterMessages.loadError('Subscription'), toasterHeadings.Error);
        })
    this.isBlocked = false;    
    
  }

  isShowTerminate() {
    if (this.memberId &&
      this.memberId != 0 &&
      this.subscription != 0 &&
      this.subscribedLevelInfo.membershipStatus == this.membershipState.Pending ||
      this.subscribedLevelInfo.membershipStatus == this.membershipState.Active) {
      return true;
    }
    return false;
  }

  isShowActivateMembership() {
    if (this.memberId &&
      this.memberId != 0 &&
      this.subscription != 0 &&
      this.subscribedLevelInfo.membershipStatus == this.membershipState.Terminated) {
      return true;
    }
    else false;
  }

  isShowUpdateMembership() {
    if (this.memberId &&
      this.memberId != 0 &&
      this.subscription != 0 &&
      this.subscribedLevelInfo.membershipStatus == this.membershipState.Pending) {
      return true;
    }
    else false;
  }

  isShowRenewMembership() {
    if (this.memberId &&
      this.memberId != 0 &&
      this.subscription != 0 &&
      this.subscribedLevelInfo.membershipStatus == this.membershipState.Inactive) {
      return true;
    }
    else false;
  }

  isDisplayMemberLevel() {
    if (this.memberId == 0 ||
      this.subscribedLevelInfo.membershipStatus == this.membershipState.Pending ||
      this.isDisplayLevels == true) {
      return true;
    }
    else false;
  }

  isDisplayMemberGroup() {
    if (this.memberId == 0) { return true }
    else { return false }
  }

  showLevels() {
    this.isDisplayLevels = true;
  }

  activateMembership() {
    this.memberSubscriptionService.activateMembership(this.memberId)
      .subscribe(res => {
        this.subscribedLevelInfo = (res.result);
        this.toastr.success(ToasterMessages.update('Subscription'), toasterHeadings.Success);
      },
        error => {
          this.toastr.error(ToasterMessages.updateError('Subscription'), toasterHeadings.Error);
        })
  }

  renewSave() {
    this.isBlocked = true;
    if (this.subscribedLevelInfo.membershipStatus != MembershipState.Inactive) {
      this.isBlocked = false;
      this.toastr.error(ToasterMessages.updateError('Membership'), toasterHeadings.Error);
      return;
    } else {
      this.isBlocked = true;

      this.memberSubscription.MemberId = this.memberId;
      this.memberSubscription.PaymentState = this.paymentState.Pending;
      var payment = Object.assign({}, this.payment, this.paymentForm.value);

      payment.paymentState = this.isPayLater ? PaymentStates.Pending : PaymentStates.Paid;
      payment.amount = this.levelInfo.price

      if (this.selectedSubscription == 0) {
        this.memberSubscription.SubscriptionId = this.subscribedLevelInfo.subscriptionId;
        payment.name = this.subscribedLevelInfo.name;
        this.timeLine.templateType = this.timeLineType.MembershipRenew;        
        this.timeLine.subscriptionName = this.subscribedLevelInfo.name; 
        this.timeLine.amount = this.subscribedLevelInfo.price;    
      }
      else {
        this.memberSubscription.SubscriptionId = this.selectedSubscription;
        payment.name = this.levelInfo.name;
        this.timeLine.subscriptionName = this.levelInfo.name + " To " + this.subscribedLevelInfo.name; 
        this.timeLine.templateType = this.timeLineType.ChangeMembership;  
        this.timeLine.amount = this.levelInfo.price 
      }

      this.memberSubscriptionService.renewMembership(this.memberSubscription)
        .mergeMap(res => {
          this.isDisplayLevels = false;
          this.subscribedLevelInfo = (res.result);
          payment.memberSubscriptionId = res.result.id;
          payment.memberId = res.result.memberId;
          if(payment.paymentState == 3){
            this.timeLine.name = "Paid";
          }
          else{
            this.timeLine.name = "UnPaid";
          }
          return this.paymentService.savePaymnets(payment)
        })
        .mergeMap(payment => {
          this.timeLine.paymentId = payment.result.paymentMasterId;
          return this.levelService.changeMemberStatus(payment.result);
        })
        .mergeMap(payment => {
          this.timeLine.subscriptionId = this.memberSubscription.SubscriptionId;
          this.timeLine.memberId = this.memberId;
          return this.memberService.saveTimeLine(this.timeLine)
        }).
        subscribe(response => {
          this.toastr.success(ToasterMessages.save('Member'), toasterHeadings.Success);
          this.isBlocked = false;
          this.router.navigate(["/members"])
        }, error => {
          this.isBlocked = false;
          this.toastr.error(ToasterMessages.saveError('Member'), toasterHeadings.Error);
        }, () => {

        });
    }
  }

  renewPayLater() {
    this.isPayLater = true;
    this.payment.paymentState = PaymentStates.Pending;
    this.renewSave();
  }

  sendRenewalMail() {
    this.memberSubscriptionService.sendRenewalMail(this.memberId)
      .subscribe(res => {
        this.toastr.success(ToasterMessages.mailSent('Renewal'), toasterHeadings.Success);
      }, error => {
        this.toastr.error(ToasterMessages.mailSendError('Renewal'), toasterHeadings.Error);
      });
  }

  changeSave() {
    this.isBlocked = true;
    this.memberSubscription.MemberId = this.memberId;
    this.memberSubscription.PaymentState = this.paymentState.Pending;
    this.memberSubscription.SubscriptionId = this.selectedSubscription;
    if(this.memberSubscription.SubscriptionId == 0){
      this.toastr.error("Please select a Membership Level/Type ");
    this.isBlocked = false;    
    }
    else{
    var payment = Object.assign({}, this.payment, this.paymentForm.value);
    payment.paymentState = this.isPayLater ? PaymentStates.Pending : PaymentStates.Paid;
    if(payment.paymentState == 3){
      this.timeLine.name = "Paid";
    }
    else{
      this.timeLine.name = "UnPaid";
    }
    payment.amount = this.levelInfo.price;
    //this.timeLine.paymentId = payment.name;

    this.memberSubscriptionService.updateMemberSubscription(this.memberSubscription).
      mergeMap(membersubscription => {
        payment.memberSubscriptionId = membersubscription.result.id;
        return this.memberSubscriptionService.updatePayment(payment);
      })
      .mergeMap(payment => {
        this.timeLine.amount = this.levelInfo.price;
        this.timeLine.paymentId = payment.result.paymentMasterId
        this.timeLine.subscriptionId = this.memberSubscription.SubscriptionId;
        this.timeLine.subscriptionName = this.levelInfo.name; 
        this.timeLine.templateType = this.timeLineType.ChangeMembership;
        this.timeLine.memberId = this.memberId;
        return this.memberService.saveTimeLine(this.timeLine)
      })
      .subscribe(res => {
        this.toastr.success(ToasterMessages.update('Membership package'), toasterHeadings.Success);
        this.isBlocked = false;
        this.router.navigate(['/members']);
      }, error => {
        this.isBlocked = false;
        this.toastr.error(ToasterMessages.updateError('Membership package'), toasterHeadings.Error)
      })
            
    }
  }

  changePayLater() {
    this.isPayLater = true;
    this.payment.paymentState = PaymentStates.Pending;
    this.changeSave();
  }

  updateMembership() {
    this.isBlocked = true;
    this.memberSubscription.MemberId = this.memberId;
    this.memberSubscription.PaymentState = this.paymentState.Pending;
    this.memberSubscription.SubscriptionId = this.selectedSubscription;

    this.memberSubscriptionService.updateMemberSubscription(this.memberSubscription)
      .subscribe(res => {
        this.toastr.success(ToasterMessages.update('Membership package'), toasterHeadings.Success);
        this.isBlocked = false;
        this.router.navigate(['/members']);
      }, error => {
        this.isBlocked = false;
        this.toastr.error(ToasterMessages.updateError('Membership package'), toasterHeadings.Error)
      })
  }
  goToProfile(){
    this.router.navigate(['/members/memberprofile',this.memberId]);
  }

  
}

