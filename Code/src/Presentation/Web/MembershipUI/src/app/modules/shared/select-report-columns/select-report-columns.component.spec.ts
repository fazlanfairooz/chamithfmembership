import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectReportColumnsComponent } from './select-report-columns.component';

describe('SelectReportColumnsComponent', () => {
  let component: SelectReportColumnsComponent;
  let fixture: ComponentFixture<SelectReportColumnsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectReportColumnsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectReportColumnsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
