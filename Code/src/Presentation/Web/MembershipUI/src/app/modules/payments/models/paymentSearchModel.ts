export class PaymentSearch {
    search: string;
    toDate: Date;
    fromDate: Date;
    /**
     *
     */
    constructor() {
        this.search = "";
        this.toDate = new Date();
        this.fromDate = new Date(new Date().setDate(new Date().getDate()-7));

    }
}