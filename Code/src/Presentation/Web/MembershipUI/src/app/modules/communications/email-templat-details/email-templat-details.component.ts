import { BlockUiService } from '../../core/services/block-ui.service';
import { EmailTemplateService } from '../../core/services/email-template.service';

import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'membership-email-templat-details',
  templateUrl: './email-templat-details.component.html',
  styleUrls: ['./email-templat-details.component.scss']
})
export class EmailTemplatDetailsComponent implements OnInit {

  take = 100;
  skip = 0;
  emailTemplates = [];
  isBlocked = false;

  constructor(private emailTemplateService: EmailTemplateService, private router: Router,
    blockUiService: BlockUiService) { }

  ngOnInit() {
    this.getEmailTemplates();
  }

  getEmailTemplates() {
    this.isBlocked = true;
    this.emailTemplateService.getTemplates(this.skip, this.take, "", "").subscribe(data => {
      this.emailTemplates = data.result.item.items;
      this.isBlocked = false;
    }, error => { }, () => {

    })
  }

  goToCreateTemplate(id,type) {
    this.router.navigate(["/communication/updateTemplate", id,type])
  }

}
