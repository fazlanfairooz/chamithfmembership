import { InvoiceCreateComponent } from './invoice-create/invoice-create.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PaymentCreateComponent } from './payment-create/payment-create.component';
import { PaymentDetailsComponent } from './payment-details/payment-details.component';


const routes: Routes = [
  { path: '', component: PaymentDetailsComponent },
  { path: 'invoice/:paymentId/:memberId', component: InvoiceCreateComponent },
  { path: 'createPayment', component: PaymentCreateComponent }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentsRoutingModule { }
