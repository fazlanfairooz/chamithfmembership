import { Component, OnInit, Input } from '@angular/core';
import { OnChanges } from '@angular/core/src/metadata/lifecycle_hooks';
import { BlockUiService } from '../../core/services/block-ui.service';

@Component({
  selector: 'membership-block-ui',
  templateUrl: './block-ui.component.html',
  styleUrls: ['./block-ui.component.scss']
})
export class BlockUiComponent implements OnInit, OnChanges {

  @Input() isBlocked = false;
  constructor(blockUIService: BlockUiService) {

    blockUIService.start$.subscribe(message => { this.isBlocked = message; });
  }

  ngOnInit() {
   
  }

  ngOnChanges() {
   
  }
}
