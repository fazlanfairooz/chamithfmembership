import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../core/services/base.service';

@Injectable()
export class CutomMemberService extends BaseService {

  private memberEndPoint = `${this.baseApiEndPoint}membership/v1/`;
  private memberGroupsEndPoint = `${this.baseApiEndPoint}membership/v1/membergroups/`;
  private memberFileEndPoint = `${this.baseApiEndPoint}membership/v1/memberfiles`;

  constructor(private http: HttpClient) {
    super();
  }

  createMember(files, memberData) {
    let formData: FormData = new FormData();

    if (files != null && files != undefined && files.length > 0) {
      for (let i = 0; i < files.length; i++) {
        formData.append('uploadFile' + i, files[i]);
      }
    }
    formData.append('memberInfoViewData', JSON.stringify(memberData));

    return this.http.post(`${this.memberEndPoint}members`, formData, this.httpOptions)
      .map((response) => response)
      .catch(this.errorHandler)
  }

  getById(memberId) {
    return this.http.get(`${this.memberEndPoint}members/${memberId}`)
      .map((response) => response)
      .catch(this.errorHandler)
  }

  updateMember(memberData) {
    return this.http.put(`${this.memberEndPoint}members`, memberData, this.httpOptions)
      .map((response) => response)
      .catch(this.errorHandler)
  }

  assignMemberToGroups(assignedGroups) {
    return this.http.post(`${this.memberGroupsEndPoint}assigntogroups`, assignedGroups, this.httpOptions)
      .map((response) => response)
      .catch(this.errorHandler)
  }

  archiveMember(memberId) {
    return this.http.delete(`${this.memberEndPoint}/members/${memberId}`,
      this.httpOptions)
      .map((response) => response)
      .catch(this.errorHandler)
  }

  permanentDeleteMember(memberId) {
    return this.http.delete(`${this.memberEndPoint}/members/${memberId}/permanent`,
      this.httpOptions)
      .map((response) => response)
      .catch(this.errorHandler)
  }

  saveDocument(files, docObj) {
    let formData: FormData = new FormData();
    if (files != null && files != undefined && files.length > 0) {
      for (let i = 0; i < files.length; i++) {
        formData.append('uploadFile' + i, files[i]);
      }
    }
    formData.append('memberFileViewData', JSON.stringify(docObj));
    return this.http.post(`${this.memberFileEndPoint}`, formData, this.httpOptions)
      .map(res => res)
      .catch(this.errorHandler)
  }

}
