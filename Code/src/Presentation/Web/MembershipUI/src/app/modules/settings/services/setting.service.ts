import { error } from 'protractor';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../core/services/base.service';
import { HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class SettingService extends BaseService {

  constructor(private http: HttpClient) {
    super();
  }

  changePassword(passwordmodel) {
    return this.http.put(`${this.baseApiEndPoint}identity/v1/security/changepassword`,
      passwordmodel, this.httpOptions)
      .map((response) => response)
      .catch(this.errorHandler)
  }

  saveTenantInfo(tenantInfo) {
    return this.http.post(`${this.baseApiEndPoint}identity/v1/tenantinfo`,
      tenantInfo, this.httpOptions)
      .map((response) => response)
      .catch(this.errorHandler)
  }

  getTenantInfo(){
    return this.http.get(`${this.baseApiEndPoint}identity/v1/tenantinfo`,this.httpOptions)
    .map((response) => response)
    .catch(this.errorHandler)
  }

  updateTenantInfo(tenantInfo) {
    return this.http.put(`${this.baseApiEndPoint}identity/v1/tenantinfo`,
      tenantInfo, this.httpOptions)
      .map((response) => response)
      .catch(this.errorHandler)
  }
}
