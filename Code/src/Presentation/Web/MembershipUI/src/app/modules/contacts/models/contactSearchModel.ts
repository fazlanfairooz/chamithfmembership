export class ContactSearchModel {
    contactCategoryTypes: number[];
    search: string;
    status: string;

    constructor() {
        this.search = '';
    }
}

