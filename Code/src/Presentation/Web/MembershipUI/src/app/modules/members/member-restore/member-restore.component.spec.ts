import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemberRestoreComponent } from './member-restore.component';

describe('MemberRestoreComponent', () => {
  let component: MemberRestoreComponent;
  let fixture: ComponentFixture<MemberRestoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemberRestoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberRestoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
