export class TenantInfoModel {
    language: any;
    currency: any;
    email: string;
    address: string;
    city: string;
    state: string;
    zip: string;
    country: number;
    phone: string;
    fax: string;
    prefix:string;

}