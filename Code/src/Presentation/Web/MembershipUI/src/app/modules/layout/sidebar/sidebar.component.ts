import { TranslateService } from '@ngx-translate/core';
import { LookupService } from '../../core/services/lookup.service';
import { Router } from '@angular/router';
import { TenantInfoService } from '../../core/services/tenant-info.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'membership-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  email: string;
  orgName: string;
  imageSource: string;
  language = "";
  prefix = "";
  currency = "";
  @Output() toggled = new EventEmitter<boolean>();
  isToggle = false;

  constructor(private tenantInfoService: TenantInfoService,
    private lookupService: LookupService,
    private translateService: TranslateService,
    private router:Router) { }

  loadUserData() {
    let tenantInfo = this.tenantInfoService.loadTenantData();
    this.orgName = tenantInfo.name;
    this.imageSource = tenantInfo.organizationLogo;
    let userData = this.tenantInfoService.loadUserData();
    this.email = userData.email;
  }
  ngOnInit() {
    this.language  = localStorage.getItem('Language');
    this.prefix  = localStorage.getItem('Prefix');

    if(this.language == null){
      this.translateService.use("en");
      this.translateService.setDefaultLang("en");
    }
    else if(this.language != null){
    this.translateService.use(this.language);    
    }

    if(this.prefix == null){
      localStorage.setItem('Prefix', " ");
    }
    if(this.currency == ""){
      localStorage.setItem('Currency', "Rs"); 
    }

    this.loadUserData();
    this.getTenantInfo();
  }
  logoutUser(){
    localStorage.removeItem('Token_id');
    localStorage.removeItem('tenantObj');
    localStorage.removeItem('Prefix');
    localStorage.removeItem('userObj');
    localStorage.removeItem('Image_Data');
    this.router.navigate(['/login'])
  }
  getTenantInfo() {
    this.lookupService.getTenantInfo().subscribe(res => {
    localStorage.setItem('Prefix', res.result.prefix)
      if(res.result.language == 2)
      {
        this.language = 'sh';
      }
     else if(res.result.language == 3)
     {
      this.language = 'tm';
     }
     else 
     {
      this.language = 'en';
     }
    this.translateService.use(this.language);
    this.translateService.setDefaultLang(this.language);
    localStorage.setItem('Language', this.language)
    },
      error => {
      }, () => {
      })
  }
  btnMenuToggle() {
    console.log("clicks")
    this.isToggle =! this.isToggle;
    this.toggled.emit(this.isToggle)
  }
}


