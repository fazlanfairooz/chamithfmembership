import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'membership-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  isToggled = false;
  constructor() {


  }

  ngOnInit() {
  }

  switchMenu(event) {
   this.isToggled =! this.isToggled;
    //this.isToggled=true;
  }
}
