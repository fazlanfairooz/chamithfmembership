import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ElementBase } from '../../core/custom-field-base/element-base';
import { FormGroup } from '@angular/forms';


@Component({
  selector: 'membership-element',
  templateUrl: './dynamic-form-element.component.html',
  styleUrls: ['./dynamic-form-element.component.scss']
})
export class DynamicFormElementComponent implements OnInit {

  values = [];
  isEmailValid: boolean = false;
  imageName: string = 'Click here to upload profile image';
  documentName: string = 'Click here to Upload file';

  @Input() element: ElementBase<any>;
  @Input() form: FormGroup;
  @Input() isFormSubmitted: boolean;
  @Output() onFileUploads: EventEmitter<any> = new EventEmitter<any>();
  @Output() onImageUploads: EventEmitter<any> = new EventEmitter<any>();

  ngOnInit() {
    if (this.element.controlType == 'checkboxgroup') {
      this.values = this.element.multipleValues;
    }
  }

  get isValid() {
    // if (this.element.controlType == 'email') {
    //   // let emailRegex: RegExp = /^[_a-zA-Z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$/;
    //   if (this.isValid)
    //     this.isEmailValid = true;
    //   else this.isEmailValid = false;
    // }
    return (this.form.controls[this.element.key].valid)
  }


  fileUpload($event) {
    this.onFileUploads.emit($event);
    this.documentName = $event.target.files[0].name;
  }

  imageUpload($event) {
    this.onImageUploads.emit($event);
    this.imageName = $event.target.files[0].name;
  }

  checkboxValues($event, id) {
    let result = this.values.find(r => r.value == id);
    if (result == undefined) {
      this.values.push({ 'value': id, 'status': $event.target.checked });
    } else {
      result.status = $event.target.checked;
    }
    return JSON.stringify(this.values);
  }
}
