import { ContactRestoreComponent } from './contact-restore/contact-restore.component';
import { ContactDetailsComponent } from './contact-details/contact-details.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactCreateComponent } from './contact-create/contact-create.component';

const routes: Routes = [
  { path: '', component: ContactDetailsComponent },
  { path: 'createContact', component: ContactCreateComponent },
  { path: 'createContact/:id', component: ContactCreateComponent },
  { path: 'contactRestore', component: ContactRestoreComponent }  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContactsRoutingModule { }
