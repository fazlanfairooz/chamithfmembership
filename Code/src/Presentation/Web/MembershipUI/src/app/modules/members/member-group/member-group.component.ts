import { GroupService } from './../../core/services/group.service';
import { MembersService } from '../../core/services/members.service';
import { Group } from '../models/memberGroup';
import { ToastsManager } from 'ng2-toastr';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { LookupService } from '../../core/services/lookup.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ToasterMessages } from '../../core/utility/toasterMessages';
import { toasterHeadings } from '../../core/utility/toasterHeadings';
import { BlockUiService } from '../../core/services/block-ui.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'membership-member-group',
  templateUrl: './member-group.component.html',
  styleUrls: ['./member-group.component.scss']
})
export class MemberGroupComponent implements OnInit {

  groupForm: FormGroup;
  group = new Group();
  isGroupFormSubmitted = false;
  groupId = 0;
  groupHeading = 'Create Group'
  groupsubHeading = 'Use the wizard to create a new group'
  groupeditsubHeading ='';
  isBlocked = false;
  grpname :any;

  language="";
  createword :any;
  editword:any;
  subheading:any;
  edithubheading :any;

  constructor(private fb: FormBuilder, 
    private lookUpService: LookupService, 
    private membergroupService: MembersService,
    private groupService: GroupService,    
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private blockUiService: BlockUiService,
    translate: TranslateService,
    private toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.getTranslator();
    this.groupForm = this.getgroupForm();
    this.activatedRoute.params.subscribe((params: Params) => {
      this.groupId = params['id'];
      if (this.groupId && this.groupId != 0) {
        this.groupHeading =this.editword; //"Edit Group"
        this.groupsubHeading = this.edithubheading;
        this.getGroup(this.groupId);
      }
      else{
        this.groupId = 0;
        this.groupHeading = this.createword;
        this.groupsubHeading = this.subheading;
      }
    });
  }

  getTranslator(){
    this.language = localStorage.getItem('Language');
    if (this.language == "en") {
      this.createword ='Create Group';
      this.editword = "Edit Group";
      this.subheading ='Use the wizard to create a new group';
      this.edithubheading ='';
    }
    if (this.language == "sh") {
       this.createword = 'කණ්ඩායම සාදන්න';
       this.editword = "සංස්කරණය කරන්න";
       this.subheading = 'නව කණ්ඩායමක් සෑදීමට මෙම විශාරද භාවිතා කරන්න';    
       this.edithubheading = 'සංස්කරණ සමූහයක් සෑදීම සඳහා විශාරද භාවිතා කරන්න';
     
    }
    if (this.language == "tm") {
      this.createword = 'குழுவை உருவாக்கு';
      this.editword = "தொகு குழு";
      this.subheading = 'புதிய குழுவை உருவாக்க வழிகாட்டி பயன்படுத்தவும்'; 
      this.edithubheading = 'திருத்து குழுவை உருவாக்க வழிகாட்டி பயன்படுத்தவும்';
    }
  }

  private getgroupForm(){
    return this.fb.group({
      name: ["", [Validators.required, Validators.maxLength(50)]],
      description: ["", [Validators.maxLength(500)]]
    })
  }

  getGroup(groupId) {
    this.isBlocked = true;
    this.membergroupService.getGroupById(this.groupId).subscribe(res => {
      
      this.patchGroupForm(res.result.item);
      this.isBlocked = false;
    },
      error => {
        this.toastr.error(ToasterMessages.loadError('Group'),toasterHeadings.Error);
        this.isBlocked = false;
      })
  }

  private patchGroupForm(group: Group){
    this.group = group;
    this.grpname = this.group.name;
    this.groupForm.patchValue({
      name: group.name,
      description: group.description,
    })
  }

  addGroup(){
    this.isGroupFormSubmitted = false;
   if (!this.groupForm.valid)
       {
         this.isBlocked = true;
         var name = this.groupForm.value.name.trim();
         var description = this.groupForm.value.description.trim();
         if(name == ""){
           this.toastr.error('Group Name Cannot be Empty', 'Oops');
           this.isBlocked = false;
         }
        return;
       } 
       else
        {
          this.isBlocked = true;
          var name = this.groupForm.value.name.trim();
          var description = this.groupForm.value.description.trim();
          if(name == ""){
            this.toastr.error('Group Name Cannot be Empty', 'Oops');
            this.isBlocked = false;
          }
        else{
        var nameObj = { name: name, description: description }
        var categorie = Object.assign({}, this.group, this.groupForm.value)
        this.groupService.addGroup(nameObj) 
        .subscribe(
          group => {
            this.groupId = group.result.id
            this.router.navigate(['/members/createGroup/',this.groupId]);
            this.toastr.success(ToasterMessages.save('Group'),toasterHeadings.Success);
            this.groupForm = this.getgroupForm();
            this.isBlocked = false;
          }, error => {
            this.toastr.error(ToasterMessages.alreadyExistError('Group'),toasterHeadings.Error);
            this.isBlocked = false;
          });

          }
    }
  }

  updateGroup(){
    this.isGroupFormSubmitted = false;
    if (!this.groupForm.valid)
    {
      this.isBlocked = true;
      var name = this.groupForm.value.name.trim();
      var description = this.groupForm.value.description.trim();
      if(name == ""){
        this.toastr.error('Group Name Cannot be Empty', 'Oops');
        this.isBlocked = false;
      }
      return;
    } 
    else
    {
      this.isBlocked = true;
      var name = this.groupForm.value.name.trim();
      var description = this.groupForm.value.description.trim();
      if(name == ""){
        this.toastr.error('Group Name Cannot be Empty', 'Oops');
        this.isBlocked = false;
      }
      else{
      var nameObj = { name: name, description: description, Id:this.groupId }
      var categorie = Object.assign({}, this.group, this.groupForm.value)
      this.groupService.updateGroup(nameObj) 
      .subscribe(
        group => {
          this.toastr.success(ToasterMessages.update('Group'),toasterHeadings.Success);
          this.isBlocked = false;
        }, error => {
          this.toastr.error(ToasterMessages.alreadyExistError('Group'),toasterHeadings.Error);
          this.isBlocked = false;
        });
      }
    }
  }

  archiveGroup() {
    this.toastr.success(ToasterMessages.delete('Group'),toasterHeadings.Success);
    this.groupService.archiveGroup(this.groupId).subscribe(res => {
      this.router.navigate(["/members/memberGroup"])
    },
      error => {this.toastr.error(ToasterMessages.deleteError('Group'),toasterHeadings.Error); })
  }
}
