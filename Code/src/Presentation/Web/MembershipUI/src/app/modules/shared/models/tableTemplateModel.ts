export class TableTemplateModel {

    // "templateType": 0,
    // "keys": [
    //   "string"
    // ],
    // "name": "string",
    // "id": 0

    templateType: number;
    keys: string[];
    name: string;
    id: number;

}