import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PermissionPasswordComponent } from './permission-password.component';

describe('PermissionPasswordComponent', () => {
  let component: PermissionPasswordComponent;
  let fixture: ComponentFixture<PermissionPasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PermissionPasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PermissionPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
