import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { SettingService } from './modules/settings/services/setting.service';

@Component({
  selector: 'membership-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  languages =[];
  language :string;
  constructor(private translate: TranslateService) {
    translate.setDefaultLang('en');
  }

  title = 'membership';
  switchLanguage(language: string) {
    this.translate.use(language);
  }

  
}
