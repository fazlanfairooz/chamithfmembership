import { SettingsModule } from './modules/settings/settings.module';
import { SignUpComponent } from './modules/login/sign-up/sign-up.component';
import { LayoutComponent } from './modules/layout/layout/layout.component';
import { DashboardComponent } from './modules/dashboard/dashboard/dashboard.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './modules/login/login/login.component';
import { RegisterComponent } from './modules/login/register/register.component';
import { ForgotPasswordComponent } from './modules/login/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './modules/login/reset-password/reset-password.component';
import { AuthGuardService } from './modules/login/auth.guard.service';
import { PermissionPasswordComponent } from './modules/settings/permission-password/permission-password.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: "full",
        canActivate: [AuthGuardService]
      },
      {
        path: 'dashboard',
        loadChildren: './modules/dashboard/dashboard.module#DashboardModule',
        canActivate: [AuthGuardService]
      },
      {
        path: 'dashboard/:bool',
        loadChildren: './modules/dashboard/dashboard.module#DashboardModule',
        canActivate: [AuthGuardService]
      },
      {
        path: 'contacts',
        loadChildren: './modules/contacts/contacts.module#ContactsModule',
        canActivate: [AuthGuardService]
      },
      {
        path: 'settings',
        loadChildren: './modules/settings/settings.module#SettingsModule',
        canActivate: [AuthGuardService]
      },
      {
        path: 'members',
        loadChildren: './modules/members/members.module#MembersModule',
        canActivate: [AuthGuardService]
      },
      {
        path: 'organization',
        loadChildren: './modules/organization/organization.module#OrganizationModule',
        canActivate: [AuthGuardService]
      },
      {
        path: 'communication',
        loadChildren: './modules/communications/communications.module#CommunicationsModule',
        canActivate: [AuthGuardService]
      },
      {
        path: 'payments',
        loadChildren: './modules/payments/payments.module#PaymentsModule',
        canActivate: [AuthGuardService]
      }
    ]
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'register',
    component: RegisterComponent,
  },
  {
    path: 'signup',
    component: SignUpComponent,
  },
  {
    path: 'forgotpassword',
    component: ForgotPasswordComponent,
  },
  {
    path: 'resetpassword',
    component: ResetPasswordComponent,
  },
  {
    path: 'permissionpassword',
   
    component:  PermissionPasswordComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
