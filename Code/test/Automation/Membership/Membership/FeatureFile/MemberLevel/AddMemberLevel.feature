﻿Feature: As an Admin, I should be able to add a new member level


@mytag
Scenario Outline: As an Admin, I should be able to add a new member level with Limited & Annual Members
Given The Application login page
Then User entered valid username and password "membership.inexis@gmail.com" and "Admin@123"
And I Click on Submit btn
And I navigated to Home Page
And I Click on Member btn
And I click on levels
And I click on New Level
And I entered level name <LevelName>
And I entered Fee <Fee>
And I selects Limited member capacity
And I entered member <Capacity>
And I select membershsip renewal type as Annual
And I entered Description <Description>
And I click on Add Button
And I click on Close button
And Click on Signout

Examples: 
| LevelName | Fee  |Capacity | Description |
| Onyx      | 2500 |  50      |$10 per month with no commitment..$19.99 for a Black Card membership, which gives you  to any location and the option to bring a friend for free.. |

Scenario Outline: As an Admin, I should be able to add a new member level with Limited & Monthly Members
Given The Application login page
Then User entered valid username and password "membership.inexis@gmail.com" and "Admin@123"
And I Click on Submit btn
And I navigated to Home Page
And I Click on Member btn
And I click on levels
And I click on New Level
And I entered level name <LevelName>
And I entered Fee <Fee>
And I selects Limited member capacity
And I entered member <Capacity>
And I select membershsip renewal type as monthly
And I entered Description <Description>
And I click on Add Button
And I click on Close button
And Click on Signout

Examples: 
| LevelName | Fee | Capacity | Description                                 |
| Onyx      | 870 | 20       | This Member Level is limited for 20 Members |

Scenario Outline: As an Admin, I should be able to add a new member level with Limited & Weekly Members
Given The Application login page
Then User entered valid username and password "membership.inexis@gmail.com" and "Admin@123"
And I Click on Submit btn
And I navigated to Home Page
And I Click on Member btn
And I click on levels
And I click on New Level
And I entered level name <LevelName>
And I entered Fee <Fee>
And I selects Limited member capacity
And I entered member <Capacity>
And I select membershsip renewal type as weekly
And I entered Description <Description>
And I click on Add Button
And I click on Close button
And Click on Signout

Examples: 
| LevelName | Fee | Capacity | Description                                 |
|Solutions Members     | 980 | 30       | This Member Level is limited for 30 Members |

Scenario Outline: As an Admin, I should be able to add a new member level with Limited & Daily Members
Given The Application login page
Then User entered valid username and password "membership.inexis@gmail.com" and "Admin@123"
And I Click on Submit btn
And I navigated to Home Page
And I Click on Member btn
And I click on levels
And I click on New Level
And I entered level name <LevelName>
And I entered Fee <Fee>
And I selects Limited member capacity
And I entered member <Capacity>
And I select membershsip renewal type as daily
And I entered Description <Description>
And I click on Add Button
And I click on Close button
And Click on Signout

Examples: 
| LevelName         | Fee | Capacity | Description                                 |
| Associate Members | 950 | 40       | This Member Level is limited for 40 Members |

Scenario Outline: 2 As an Admin, I should be able to add a new member level with Unlimited Members & Annual
Given The Application login page
Then User entered valid username and password "membership.inexis@gmail.com" and "Admin@123"
And I Click on Submit btn
And I navigated to Home Page
And I Click on Member btn
And I click on levels
And I click on New Level
And I entered level name <LevelName>
And I entered Fee <Fee>
And I selects Unlimited member capacity
And I select membershsip renewal type as Annual
And I entered Description <Description>
And I click on Add Button
And I click on Close button
And Click on Signout

Examples: 
| LevelName | Fee  | Description |
| Strategic    | 3000  |$10 per month with no commitment|


Scenario Outline: As an Admin, I should be able to add a new member level with Unlimited Members & Monthly
Given The Application login page
Then User entered valid username and password "membership.inexis@gmail.com" and "Admin@123"
And I Click on Submit btn
And I navigated to Home Page
And I Click on Member btn
And I click on levels
And I click on New Level
And I entered level name <LevelName>
And I entered Fee <Fee>
And I selects Unlimited member capacity
And I select membershsip renewal type as monthly
And I entered Description <Description>
And I click on Add Button
And I click on Close button
And Click on Signout

Examples: 
| LevelName | Fee  | Description |
| Strat   | 5000  |$10 per month with no commitment|


Scenario Outline: As an Admin, I should be able to add a new member level with Unlimited Members & Weekly
Given The Application login page
Then User entered valid username and password "membership.inexis@gmail.com" and "Admin@123"
And I Click on Submit btn
And I navigated to Home Page
And I Click on Member btn
And I click on levels
And I click on New Level
And I entered level name <LevelName>
And I entered Fee <Fee>
And I selects Unlimited member capacity
And I select membershsip renewal type as weekly
And I entered Description <Description>
And I click on Add Button
And I click on Close button
And Click on Signout

Examples: 
| LevelName | Fee  | Description |
| Strangers   | 800  |$30 per month with no commitment|


Scenario Outline:1 As an Admin, I should be able to add a new member level with Unlimited Members & Daily
Given The Application login page
Then User entered valid username and password "membership.inexis@gmail.com" and "Admin@123"
And I Click on Submit btn
And I navigated to Home Page
And I Click on Member btn
And I click on levels
And I click on New Level
And I entered level name <LevelName>
And I entered Fee <Fee>
And I selects Unlimited member capacity
And I select membershsip renewal type as daily
And I entered Description <Description>
And I click on Add Button
And I click on Close button
And Click on Signout

Examples: 
| LevelName | Fee  | Description |
| DreamTeam   | 500  |$20 per month with no commitment|
#Negative

Scenario Outline: 4 As an Admin, I should not be able to add a new member level with Limited & Annual Members with invalid data
Given The Application login page
Then User entered valid username and password "membership.inexis@gmail.com" and "Admin@123"
And I Click on Submit btn
And I navigated to Home Page
And I Click on Member btn
And I click on levels
And I click on New Level
And I entered level name <LevelName>
And I entered Fee <Fee>
And I selects Limited member capacity
And I entered member <Capacity>
And I select membershsip renewal type as Annual
And I entered Description <Description>
And I click on Add Button
And I click on Close button
And Click on Signout

Examples: 
| LevelName | Fee  |Capacity | Description |
|      | 4600 |  60      |$19.99 for a Black Card membership, which gives you  to any location and the option to bring a friend for free.. |
|     Orio | |  60      |$19.99 for a Black Card membership, which gives you  to any location and the option to bring a friend for free.. |
|     Oppo|80 |       |$19.99 for a Black Card membership, which gives you  to any location and the option to bring a friend for free.. |

Scenario Outline: As an Admin, I should not be able to add a new member level with Limited & Monthly Members with invalid data
Given The Application login page
Then User entered valid username and password "membership.inexis@gmail.com" and "Admin@123"
And I Click on Submit btn
And I navigated to Home Page
And I Click on Member btn
And I click on levels
And I click on New Level
And I entered level name <LevelName>
And I entered Fee <Fee>
And I selects Limited member capacity
And I entered member <Capacity>
And I select membershsip renewal type as monthly
And I entered Description <Description>
And I click on Add Button
And I click on Close button
And Click on Signout

Examples: 
| LevelName | Fee | Capacity | Description                                 |
|   | 870 | 20       | This Member Level is limited for 20 Members |
| Orex    | | 20       | This Member Level is limited for 20 Members |
| Orex    | 870 |       | This Member Level is limited for 20 Members |

Scenario Outline: As an Admin, I should not be able to add a new member level with Limited & Weekly Members with invalid data
Given The Application login page
Then User entered valid username and password "membership.inexis@gmail.com" and "Admin@123"
And I Click on Submit btn
And I navigated to Home Page
And I Click on Member btn
And I click on levels
And I click on New Level
And I entered level name <LevelName>
And I entered Fee <Fee>
And I selects Limited member capacity
And I entered member <Capacity>
And I select membershsip renewal type as weekly
And I entered Description <Description>
And I click on Add Button
And I click on Close button
And Click on Signout

Examples: 
| LevelName | Fee | Capacity | Description                                 |
|  | 980 | 30       | This Member Level is limited for 30 Members |
|Silver Members   |  | 30       | This Member Level is limited for 30 Members |
|Silver Members   | 980 |      | This Member Level is limited for 30 Members |

Scenario Outline: As an Admin, I should not be able to add a new member level with Limited & Daily Members with invalid data
Given The Application login page
Then User entered valid username and password "membership.inexis@gmail.com" and "Admin@123"
And I Click on Submit btn
And I navigated to Home Page
And I Click on Member btn
And I click on levels
And I click on New Level
And I entered level name <LevelName>
And I entered Fee <Fee>
And I selects Limited member capacity
And I entered member <Capacity>
And I select membershsip renewal type as daily
And I entered Description <Description>
And I click on Add Button
And I click on Close button
And Click on Signout

Examples: 
| LevelName         | Fee | Capacity | Description                                 |
|                   | 950 | 40       | This Member Level is limited for 40 Members |
| Associate Members |     | 40       | This Member Level is limited for 40 Members |
| Associate Members | 950 |          | This Member Level is limited for 40 Members |

Scenario Outline: As an Admin, I should not be able to add a new member level with Unlimited Members & Annual with invalid data
Given The Application login page
Then User entered valid username and password "membership.inexis@gmail.com" and "Admin@123"
And I Click on Submit btn
And I navigated to Home Page
And I Click on Member btn
And I click on levels
And I click on New Level
And I entered level name <LevelName>
And I entered Fee <Fee>
And I selects Unlimited member capacity
And I select membershsip renewal type as Annual
And I entered Description <Description>
And I click on Add Button
And I click on Close button
And Click on Signout

Examples: 
| LevelName | Fee  | Description                      |
|           | 3000 | $10 per month with no commitment |
| Strategic |      | $10 per month with no commitment |

Scenario Outline: As an Admin, I should not be able to add a new member level with Unlimited Members & Monthly with invalid data
Given The Application login page
Then User entered valid username and password "membership.inexis@gmail.com" and "Admin@123"
And I Click on Submit btn
And I navigated to Home Page
And I Click on Member btn
And I click on levels
And I click on New Level
And I entered level name <LevelName>
And I entered Fee <Fee>
And I selects Unlimited member capacity
And I select membershsip renewal type as monthly
And I entered Description <Description>
And I click on Add Button
And I click on Close button
And Click on Signout

Examples: 
| LevelName | Fee  | Description |
|   | 5000  |$10 per month with no commitment|
| Strat   |   |$10 per month with no commitment|

Scenario Outline:  3 As an Admin, I should not be able to add a new member level with Unlimited Members & Weekly with invalid data
Given The Application login page
Then User entered valid username and password "membership.inexis@gmail.com" and "Admin@123"
And I Click on Submit btn
And I navigated to Home Page
And I Click on Member btn
And I click on levels
And I click on New Level
And I entered level name <LevelName>
And I entered Fee <Fee>
And I selects Unlimited member capacity
And I select membershsip renewal type as weekly
And I entered Description <Description>
And I click on Add Button
And I click on Close button
And Click on Signout

Examples: 
| LevelName | Fee  | Description |
|    | 800  |$30 per month with no commitment|
|  Worriors  |  |$30 per month with no commitment|


Scenario Outline: aaaaaaAs an Admin, I should not be able to add a new member level with Unlimited Members & Daily with invalid data
Given The Application login page
Then User entered valid username and password "membership.inexis@gmail.com" and "Admin@123"
And I Click on Submit btn
And I navigated to Home Page
And I Click on Member btn
And I click on levels
And I click on New Level
And I entered level name <LevelName>
And I entered Fee <Fee>
And I selects Unlimited member capacity
And I select membershsip renewal type as daily
And I entered Description <Description>
And I click on Add Button
And I click on Close button
And Click on Signout

Examples: 
| LevelName | Fee  | Description |
|   | 500  |$20 per month with no commitment|
|  Gold Member | s  |$20 per month with no commitment|