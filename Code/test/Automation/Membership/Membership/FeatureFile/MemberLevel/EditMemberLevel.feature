﻿Feature: As an Admin, I should be able to Edit member level


@mytag
Scenario Outline: As an Admin, I should be able to Edit Member level with Limited & Annual Members
Given The Application login pg
Then User entered valid uname and pword "membership.inexis@gmail.com" and "Admin@123"
And I Clicked on Submit
And I navigated to home pg
And I Click on Add New Member btn
And I click on levels btn
And I navigated to Member level page and i select a membership package
And I click on basic settings Edit button
And I have entered level name <Level Name>
And I have entered Fee <Description>
And I clicked on update
And I clicked on renewal Settings Edit button
