﻿Feature: As an Admin, I should be able to filter dashboard by Today, Week, Month & Year

@mytag
Scenario: As an Admin, I should be able to filter dashboard by 7 days
Given Load membership login page
And Entered username & password "membership.inexis@gmail.com" and "Admin@123" 
And Clicked on Signin button 
Then Navigated to Dashboard pg
And I click on Last Seven days
And I Click on Logout button

Scenario: As an Admin, I should be able to filter dashboard by 3 months
Given Load membership login page
And Entered username & password "membership.inexis@gmail.com" and "Admin@123" 
And Clicked on Signin button 
Then Navigated to Dashboard pg
And I clicked on Three months
And I Click on Logout button

Scenario: As an Admin, I should be able to filter dashboard by 6 months
Given Load membership login page
And Entered username & password "membership.inexis@gmail.com" and "Admin@123" 
And Clicked on Signin button 
Then Navigated to Dashboard pg
And I clicked on six months
And I Click on Logout button


Scenario: As an Admin, I should be able to filter dashboard by 1 year
Given Load membership login page
And Entered username & password "membership.inexis@gmail.com" and "Admin@123" 
And Clicked on Signin button 
Then Navigated to Dashboard pg
And I clicked on One year
And I Click on Logout button