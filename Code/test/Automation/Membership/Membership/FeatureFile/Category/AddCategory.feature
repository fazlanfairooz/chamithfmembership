﻿Feature: As an Admin, I must be able to add categories

@mytag
Scenario Outline:As an Admin, I should be able to add Categories 
Given Load Membership Login page
And Enter username & password "membership.inexis@gmail.com" and "Admin@123" 
And Click on Sign in button 
Then Navigated to Dashboard
And Click on Settings
And i have entered <Category> as Category
And Click on Add button
And Click on Signout button
 
Examples: 
| Category        |
| Java Consultant |
| IT Manager      |
| QA Consultant   |
#
#Scenario Outline:As an Admin, I should not be able to add Categories 
#Given Load Membership
#And I Enter username & password "membership.inexis@gmail.com" and "Admin@123" 
#And Clicked on Sign in button 
#Then Navigat to Dashboard
#And Clicked on Settings
#And I Enter <Category> as Category
#And Clicked on Add button
#And Clicked on Signout button
# 
#Examples: 
#| Category        |
#|         |
#
#Scenario Outline:As an Admin, I should be able to Cancel the Category field
#Given Load Membership
#And I Enter username & password "membership.inexis@gmail.com" and "Admin@123" 
#And Clicked on Sign in button 
#Then Navigat to Dashboard
#And Clicked on Settings
#And I Enter <Category> as Category
#And Clicked on Cancel button
#And Clicked on Signout button
# 
#Examples: 
#| Category        |
#|   HR Admin      |