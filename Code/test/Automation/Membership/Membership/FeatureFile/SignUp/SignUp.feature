﻿Feature: As a User, I should be able to register the business profile information.

@mytag
Scenario Outline: As a User, I should be able to register the business profile information
Given Navigate to Membership Signup page
When I enter Email as <Email>
And I Entered Password as <Password>
And I entered confirm password as <ConfirmPwd>
And I Click on Register button

Examples: 
| Email                       | Password  | ConfirmPwd |
| membership.inexis@gmail.com | Admin@123 | Admin@123  |


Scenario Outline: As a User, I should not be able to register the business profile information when the email is already registered
Given Navigate to Membership Signup
When Enter Email as <Email>
And Entered Password as <Password>
And Entered confirm password as <ConfirmPwd>
And Click on Register button
And Click on go to login button
And Enter username & password "membership.inexis@gmail.com" and "Admin@123" 
And Click on Sign in button 
And Navigate to dashboard
And Click on logout button
Examples: 
| Email                       | Password  | ConfirmPwd |
| membership.inexis@gmail.com | Admin@123 | Admin@123  |

Scenario Outline: As a User, I should not be able to register the business profile information with invalid data
Given Navigated to Membership Signup
When Entered Email as <Email>
And I entered Password as <Password>
And Enter confirm password as <ConfirmPwd>
And Clicked on Register button

Examples: 
| Email                       | Password  | ConfirmPwd |
| membership.inexis@gmail.com | Admin@123 | ADmin@123  |
|                             | Admin@123 | ADmin@123  |
| membership.inexis@gmail.com |           | Admin@123  |
| membership.inexis@gmail.com | Admin@123 |            |
| membership.inexis@gmail.com | admin@123 | admin@123  |