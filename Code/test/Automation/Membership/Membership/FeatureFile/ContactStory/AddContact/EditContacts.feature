﻿Feature: As an Admin, I should be able to edit existing contact information
	
@mytag
Scenario Outline: As an Admin, I should be able to edit existing contact information
	Given The memnership Application Page
	Then  I entered un & pw "membership.inexis@gmail.com" and "Admin@123"
	And Click on Submit button
	And I navigated to dashboard
	And I click on Contact button
	And I select a contact and click on edit button
	And Enters First name as <_FN>
	And Enters Last name as <_LN>
	And I select profile Image
	And Enter DOB as <DOB>
	And I select martial Status
	And Entered NIC as <_NIC>
	And I Select Category 
	And I Click on the NEXT button to navigate Contact info section
	And I select a country
	And Enters City <_City>
	And I Enters Postal as <_Postal>
	And Enter Address as <_Address>
	And Enter Mobile as <_Mob>
	And Enter Telephone as <_Tele>
	And Enter Email as <_Email>
	And I Clicks on NEXT button to navigate work info section
	And Enter Designation as <_desig>
	And Enter Company as <_Cmpy>
	And I enters Company Address as <_CmpAdd>
	And I enters Office Telephone as <_TeleAdd>
	And I enters Office Mobile as <_TeleMob>
	And I Clicked on Save
	And I clicked on Log Out 
Examples: 
| _FN       | _LN   | _DOB      | _NIC      |  _City      | _Postal | _Address        | _Mob			| _Tele     | _Email            | _desig         | _Cmpy				| _CmpAdd                      | _TeleAdd  | _TeleMob   |
| Oshani | Shenaya | 1990-2-4 | 966713229V  |  Amsterdaam | 2020    | 22nd Olsen Lane | 266441344	| 266442344 | perri@hotmail.com | Finance Intern | High Octane Fitness | 68th Postman Suite, New York | 0827738120 | 0266738290	|

Scenario Outline: As an Admin, I should not be able to edit existing contact information with invalid data (NIC)
	Given The memnership Application Pg
	Then I entered un & pw "membership.inexis@gmail.com" and "Admin@123"
	And Click on Submit
	And I navigated to the dashboard
	And I click on Contact
	And I select a contact and click on edit
	And Entered Firstname as <_FN>
	And Entered Lastname as <_LN>
	And I browse profile Image
	And I entered DOB as <DOB>
	And I select the martial Status
	And I entered NIC as <_NIC>
	And I Select the Category 
	And I Click on the next button to navigate Contact info section
	And I clicked on Log Out button
Examples: 
| _FN    | _LN     | _DOB     | _NIC |
| Rehani | Shenaya | 1990-2-4 |      |