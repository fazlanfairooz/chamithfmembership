﻿Feature: As an Admin, I should be able to archive existing contact information

@mytag
Scenario: As an Admin, I should be able to archive existing contact information
	Given Load the membership Application
	Then User enter "membership.inexis@gmail.com" and "Admin@123"
	And I Clicks on Submit btn
	And direct to Dashboard 
	And I clicked on Contacts pg
	And I select a contact and clicked on Edit btn
	And I clicked on Archive btn
	And I confirm
	And Click on Logout btn

	Scenario: As an Admin, I should not be able to archive existing contact information
	Given Load the membership Application
	Then User enter "membership.inexis@gmail.com" and "Admin@123"
	And I Clicks on Submit btn
	And direct to Dashboard 
	And I clicked on Contacts pg
	And I select a contact and clicked on Edit btn
	And I clicked on Archive btn
	And I click no Button
	And Click on Logout btn