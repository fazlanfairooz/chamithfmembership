﻿Feature: As an Admin, i should be able to restore deleted contacts

@mytag
Scenario: As an Admin, i should be able to restore deleted contacts
	Given Load the membership login page
	Then I "membership.inexis@gmail.com" and "Admin@123"
	And I Clicks on Submit button
	And I direct to Dashboard 
	And Clicked on Contacts pg
	And I clicked on Action button
	And Clicked on Restore Archive Contacts
	And I select a contact and clicked on Button Restore
	And I confirm the restore contact
	And I Click on Logout btn


	Scenario: As an Admin, i should not be able to restore deleted contacts
	Given Load the membership login page
	Then I "membership.inexis@gmail.com" and "Admin@123"
	And I Clicks on Submit button
	And I direct to Dashboard 
	And Clicked on Contacts pg
	And I clicked on Action button
	And Clicked on Restore Archive Contacts
	And I select a contact and clicked on Button Restore
	And I did not confirm the restore contact
	And I Click on Logout btn