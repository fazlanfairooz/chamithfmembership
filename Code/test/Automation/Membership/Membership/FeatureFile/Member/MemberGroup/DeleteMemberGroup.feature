﻿Feature: As an Admin, I should be able to Delete a new member group

@mytag
Scenario: As an Admin, I should be able to Delete a new member group
	Given The application Page opens
	Then  Users enters the "membership.inexis@gmail.com" and "Admin@123"
	And  User click on SignIn 
	And  User Navigated to Homepage 
	And  User Directed to Members Section
	And User Navigate To Groups
	And Select a Member Group 
	And I click on the Archive button 
	And Confirm the Archive
	And a Sucess message appears
	And LOG Out

#Scenario: As an Admin, I should NOT be able to Delete a new member group
#	Given The Application Page opens
#	Then  Users enters "membership.inexis@gmail.com" and "Admin@123"
#	And  User Click on SignIn 
#	And  User navigated to Homepage 
#	And  User directed to Members Section
#	And User Navigate to Groups
#	And Select a Member Group 
#	And I click on the Archive button
#	Then Not able to delete since Members are assigned  
#	And Log Out