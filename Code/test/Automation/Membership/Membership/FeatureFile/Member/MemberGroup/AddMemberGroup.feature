﻿Feature: As an Admin, I should be able to add a new member group

@mytag
Scenario Outline: As an Admin, I should be able to add a new member group
	Given The Application Page opens
	Then  Users enters "membership.inexis@gmail.com" and "Admin@123"
	And  User Click on SignIn 
	And  User navigated to Homepage 
	And  User directed to Members Section
	And User Navigate to Groups
	And User Click on New group
	And Enter <_GroupName> 
	And Enters <_Description>
	And User Click on Save Group 
	Then a Succes message is displayed
	And User Click on members 
	And User Click on Drop Down 
	And User Select a Member 
	And UserClick to Close Drop Down List
	And User Click on Add Member To Group
	Then a Sucess message appears 
	And The added Members are shown 
#	And User Click on back 
	And User logout
Examples: 
| _GroupName     | _Description            |
| Testing Indigo | Hello, How you doin ??? | 

	 
Scenario Outline: As an Admin, I should NOT be able to add a new member group
	Given The Application Page opens
	Then  Users enters "membership.inexis@gmail.com" and "Admin@123"
	And User Click on SignIn 
	And  User navigated to Homepage 
	And  User directed to Members Section
	And User Navigate to Groups
	And User Click on New group
	And does not Enter for "      "
	And Enter <_Description>
	And User Click on Save Group 
	Then User Error  message is displayed
	And User logout
Examples: 
| _Description			|
|Testing Error messag	|  
