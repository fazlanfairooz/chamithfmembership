﻿Feature: As an Admin, I should be able to search a member by the following criteria's: Fist Name, Last Name, Mobile, Email, Organization, Member Status (Active/Lapsed), Membership Type and Member ID

@mytag

Scenario: I should be able to search a member by the following criteria's: First Name
	Given The Application Page Load
	Then User entered "membership.inexis@gmail.com" and "Admin@123"
	And Clicks on Submit button 
	And Navigated to Dashboard page
	And I navigates to Contacts Section
	And I search firstname as "Iruni"
	And Display the results
	And click on logout button

	Scenario: I should be able to search a contact by the following criteria's: Last Name
	Given The Application Page
	Then I entered "membership.inexis@gmail.com" and "Admin@123"
	And Clicked on Submit button 
	And I Navigated to Dashboard page
	And I navigated to Contacts Section
	And I searched lastname as "Heshani"
	And Display the related results
	And click on logout bttn

	Scenario: I should be able to search a contact by the following criteria's: Mobile
	Given The membership login page
	Then Entered "membership.inexis@gmail.com" and "Admin@123"
	And I Clicked on Submit button 
	And I Navigat to Dashboard page
	And I navigat to Contacts Section
	And I searched Mobile as "0770713880"
	And Display the related result
	And click on logout

	Scenario: I should be able to search a contact by the following criteria's: Email
	Given The membership page
	Then User enter "membership.inexis@gmail.com" and "Admin@123"
	And User Clicked on Submit button 
	And User Navigat to Dashboard page
	And User navigat to Contacts Section
	And User searched Email as "heshani@gmail.com"
	And Displays related result
	And User click on logout button

	Scenario: I should be able to search a contact by the following criteria's:  Organization
	Given The membership home page
	Then I enters "membership.inexis@gmail.com" and "Admin@123"
	And I click on Submit button 
	And I navigate to Dashboard page
	And I navigate to Contacts Section
	And I search company as "Inexis"
	And Displays related results
	And User click on logout btn

	Scenario Outline: I should not be able to search a contact by the following criteria's: NIC, Martial status, DOB, Postal, Designation, Company address
	Given Membership home page
	Then Enter "membership.inexis@gmail.com" and "Admin@123"
	And Click Submit button 
	And Navigated Dashboard
	And I navigated to Contact Section
	And I searched company <Criteria>
	And Display related results
	And User clicked on logout btn

	Examples: 
	| Criteria      |
	| 983399908V    |
	| Single        |
	| 2018-03-19    |
	| 10230         |
	| Manager       |
	| Dutugemunu Rd |