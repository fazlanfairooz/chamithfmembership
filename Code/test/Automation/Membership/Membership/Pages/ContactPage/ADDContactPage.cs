﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using Protractor;
using System.Threading;
using System;

namespace Membership.Pages.ContactPage
{
    public class ADDContactPage
    {
        private NgWebDriver ngDriver;

        public ADDContactPage(NgWebDriver ngDriver)
        {
            this.ngDriver = ngDriver;
        }

        public void Load_Contact()
        {
            Assert.AreEqual("http://membershipinexis.azurewebsites.net/#/contacts", "http://membershipinexis.azurewebsites.net/#/contacts");
        }

        public void Load_AddContact()
        {
            Assert.AreEqual("http://membershipinexis.azurewebsites.net/#/contacts/createContact", "http://membershipinexis.azurewebsites.net/#/contacts/createContact");
        }
      

        /*internal void Load_Contact(NgWebDriver ngDriver)
        {
            throw new NotImplementedException();
        }
        */
    }
}
