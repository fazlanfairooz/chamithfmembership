﻿using Protractor;
using System;
using Membership.PageObjects;

namespace Membership.Pages.LoginPage
{
    class LoginPage
    {
        private NgWebDriver LoginPageDriver;
        private LoginPO loginPo= new LoginPO();

        public LoginPage(NgWebDriver ngWebDriver)
        {
            this.LoginPageDriver = ngWebDriver;
        }

        public void LoginPageLoad(NgWebDriver d)
        {
            LoginPageDriver.IgnoreSynchronization = true;
            this.LoginPageDriver = d;
            LoginPageDriver.Manage().Window.Maximize();
            LoginPageDriver.Url = "http://membershipinexis.azurewebsites.net/#/login";
        }

        public void login(string un, string pw)
        {
            loginPo.UName(LoginPageDriver).SendKeys(un);
            loginPo.Pwd(LoginPageDriver).SendKeys(pw);
          
        }
        internal void LoginPageLoad(object ngWebDriver)
        {
            throw new NotImplementedException();
        }
       

    }
}
