﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using Protractor;

namespace Membership.PageObjects.SignUpPO
{
    class SignUpPO
    {
        public NgWebElement Email(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtUserMail"));
        }
        public NgWebElement Pwd(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtNewPassword"));
        }
        public NgWebElement ConfirmPwd(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtConfirmPassword"));
        }
        private NgWebElement Btn_SignUp(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnSignUp"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_SignUp_Click(NgWebDriver d)
        {
            this.Btn_SignUp(d).Click();
                    }
        private NgWebElement Btn_GoToLogin(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnLogin1"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_GoToLogin_Click(NgWebDriver d)
        {
            this.Btn_GoToLogin(d).Click();
        }
        private void ScrollToElem(NgWebElement elem, NgWebDriver d)
        {
            Actions actions = new Actions(d);
            actions.MoveToElement(elem);
            actions.Perform();
        }
    }
}
