﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using Protractor;
using System;
using System.Threading;

namespace Membership.PageObjects.MemberLevelPO
{
    class MemberLevelPO
    {
        public void Btn_MemberLevel_click(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnAddnewMember"));
            this.ScrollToElem(elem, d);
            elem.Click();
            Thread.Sleep(2000);
        }

        public NgWebElement LevelName(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtName"));
        }
        public NgWebElement Fee(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtPrice"));
        }
        public void Btn_Limited_click(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("rdLimited"));
            this.ScrollToElem(elem, d);
            elem.Click();
            Thread.Sleep(2000);
        }
        public NgWebElement Capacity(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtCapacity"));
        }
        public void Btn_UnLimited_click(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("rdUnlimited"));
            this.ScrollToElem(elem, d);
            elem.Click();
            Thread.Sleep(2000);
        }
        public void Btn_Annual_click(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.CssSelector("#divDateDisable > div > label:nth-child(1) > span"));
            this.ScrollToElem(elem, d);
            elem.Click();
            Thread.Sleep(2000);
        }
        public void Btn_Monthly_click(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.CssSelector("#divDateDisable > div > label:nth-child(2) > span"));
            this.ScrollToElem(elem, d);
            elem.Click();
            Thread.Sleep(2000);
        }
        public void Btn_Weekly_click(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.CssSelector("#divDateDisable > div > label:nth-child(3) > span"));
            this.ScrollToElem(elem, d);
            elem.Click();
            Thread.Sleep(2000);
        }
        public void Btn_Daily_click(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.CssSelector("#divDateDisable > div > label:nth-child(4) > span"));
            this.ScrollToElem(elem, d);
            elem.Click();
            Thread.Sleep(2000);
        }
        public NgWebElement Description(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtDescription"));
        }
        public void Btn_Add_click(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnSave"));
            this.ScrollToElem(elem, d);
            elem.Click();
            Thread.Sleep(2000);
        }
        public void Btn_Close_click(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnHide"));
            this.ScrollToElem(elem, d);
            elem.Click();
            Thread.Sleep(2000);
        }
        private void ScrollToElem(NgWebElement elem, NgWebDriver d)
        {
            Actions actions = new Actions(d);
            actions.MoveToElement(elem);
            actions.Perform();
        }
    }
}
