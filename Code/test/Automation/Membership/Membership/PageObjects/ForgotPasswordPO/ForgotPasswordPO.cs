﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using Protractor;
using System;
using System.Threading;

namespace Membership.PageObjects.ForgotPasswordPO
{
    class ForgotPasswordPO
    {
        private NgWebElement Btn_ResetHere(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.CssSelector("body > membership-root > membership-login > section > div.form.col-md-5 > div > div:nth-child(1) > div > div.login-form > form > div.forget.m-t-15 > p > a"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_ResetHere_Click(NgWebDriver d)
        {
            this.Btn_ResetHere(d).Click();
        }
        public NgWebElement Email(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtResetEmail"));
        }
        private NgWebElement Btn_ResetPassword(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnReset"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_ResetPassword_Click(NgWebDriver d)
        {
            this.Btn_ResetPassword(d).Click();
        }

        private NgWebElement Btn_BackToLogin(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.XPath("/html/body/membership-root/membership-forgot-password/section/div[2]/div/div[1]/div[2]/div/div/button[1]"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_BackToLogin_Click(NgWebDriver d)
        {
            this.Btn_BackToLogin(d).Click();
        }

        private NgWebElement Btn_Back(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnBack"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_Back_Click(NgWebDriver d)
        {
            this.Btn_Back(d).Click();
        }
        private void ScrollToElem(NgWebElement elem, NgWebDriver d)
        {
            Actions actions = new Actions(d);
            actions.MoveToElement(elem);
            actions.Perform();
        }
    }
}
