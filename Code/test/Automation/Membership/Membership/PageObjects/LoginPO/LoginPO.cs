﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using Protractor;

namespace Membership.PageObjects
{
    class LoginPO
    {
        public NgWebElement UName(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtEmail"));
        }
        public NgWebElement Pwd(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtPassword"));
        }
        private NgWebElement Btn_SignIn(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnLogin1"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_SignIn_Click(NgWebDriver d)
        {
            this.Btn_SignIn(d).Click();
        }
        private NgWebElement Btn_KeepMeLogin(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.XPath("/html/body/membership-root/membership-login/div/div/div/div/div/div/div/div/div[2]/form/div[3]/div/label/input"));
            ScrollToElem(elem, d);
            return elem;
         }
        public void Btn_KeepMeLogin_Click(NgWebDriver d)
        {
            this.Btn_KeepMeLogin(d).Click();
        }
        private NgWebElement Btn_ResetHere(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.CssSelector("body > membership-root > membership-login > div > div > div > div > div > div > div > div > div.login-form > form > div.forget.m-t-15 > p > a"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_ResetHere_Click(NgWebDriver d)
        {
            this.Btn_ResetHere(d).Click();
        }
        //conflicts because Reset Here
        private NgWebElement Btn_Profile_Logout(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnLogoutt"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_Profile_Logout_Click(NgWebDriver d)
        {
            this.Btn_Profile_Logout(d).Click();
        }


        private NgWebElement Btn_Logout(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnLogoutt"));
           ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_Logout_Click(NgWebDriver d)
        {
            this.Btn_Logout(d).Click();
        }
        
        public NgWebElement Txt_ErrMessaage(NgWebDriver d)
        {
            return d.FindElement(By.CssSelector("#login>div.login-panel>div:nth-child(3)>form>div.dan-validation"));
        }
        private void ScrollToElem(NgWebElement elem, NgWebDriver d)
        {
            Actions actions = new Actions(d);
            actions.MoveToElement(elem);
            actions.Perform();
        }
    }
}
