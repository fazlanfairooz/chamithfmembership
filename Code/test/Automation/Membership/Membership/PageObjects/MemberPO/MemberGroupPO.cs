﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using Protractor;
using System;
using System.Threading;

namespace Membership.PageObjects.MemberPO
{
    class MemberGroupPO
    {
        private NgWebElement Btn_Group(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnMemberGroupTab"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_Group_Click(NgWebDriver d)
        {
            this.Btn_Group(d).Click();
        }

        private NgWebElement Btn_NewGroup(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnNewgroup"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_NewGroup_Click(NgWebDriver d)
        {
            this.Btn_NewGroup(d).Click();
        }

        public NgWebElement GrpName(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtName"));
        }

        public NgWebElement GrpDescription(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtDescription"));
        }

        private NgWebElement Btn_GrpSave(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnSaveGroup"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_GrpSave_Click(NgWebDriver d)
        {
            this.Btn_GrpSave(d).Click();
        }

        private NgWebElement Btn_MemberToGroup(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.CssSelector("body > membership-root > membership-layout > div > div > div > membership-member-group > div > div > div > section > div > div > div > div > ul > li.ng-star-inserted > a"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_GrpMember_Click(NgWebDriver d)
        {
            this.Btn_MemberToGroup(d).Click();
        }

        private NgWebElement Btn_MemberDD(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.CssSelector("#btnMessages > membership-member-group-member > div > div > div:nth-child(1) > div > div > angular2-multiselect > div > div.selected-list > div"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_MemberDD_Click(NgWebDriver d)
        {
            this.Btn_MemberDD(d).Click();
        }


        private NgWebElement Btn_SelectMember(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.CssSelector("#btnMessages > membership-member-group-member > div > div > div:nth-child(1) > div > div > angular2-multiselect > div > div.dropdown-list > div.list-area > ul > span > li:nth-child(2)"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_SelectMember_Click(NgWebDriver d)
        {
            this.Btn_SelectMember(d).Click();
        }

        private NgWebElement Btn_SaveMemToGrp(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnSendmembers"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_SaveMemToGrp_Click(NgWebDriver d)
        {
            this.Btn_SaveMemToGrp(d).Click();
        }

        //=========

        public NgWebElement Btn_viewMembGrp(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnEditGroup"));
            ScrollToElem(elem, d);
            return elem;
        }
        public NgWebElement Btn_Archive_MemGrp(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.XPath("/html/body/membership-root/membership-layout/div/div/div/membership-member-group/div/div/div/div/div/div[2]/form/button[1]"));
            ScrollToElem(elem, d);
            return elem;
        }

        public NgWebElement Btn_ArchYes_MemGrp(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnYes"));
            ScrollToElem(elem, d);
            return elem;
        }

        //=========
        public NgWebElement Btn_update_MemGrp(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnUpdateGroup"));
            ScrollToElem(elem, d);
            return elem;
        }

        private void ScrollToElem(NgWebElement elem, NgWebDriver d)
        {
            Actions actions = new Actions(d);
            actions.MoveToElement(elem);
            actions.Perform();
        }
    }
}
