﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using Protractor;
using System;
using System.Threading;

namespace Membership.PageObjects.PaymentPO
{
    class paymentPO
    {


        private NgWebElement Btn_paymentSection(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnPayment"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_PaySec_Click(NgWebDriver d)
        {
            this.Btn_paymentSection(d).Click();
        }

        public NgWebElement searchField(NgWebDriver d)
        {
            return d.FindElement(By.Id("btnSearch"));
        }

        private NgWebElement Btn_payment(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnPayment"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_Pay_Click(NgWebDriver d)
        {
            this.Btn_payment(d).Click();
        }

        private NgWebElement Btn_ViewPayInvoice(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnViwePayment"));  
            return elem;
        }
        public void Btn_payview_click(NgWebDriver d)
        {
            this.Btn_ViewPayInvoice(d).Click();
        }

        private NgWebElement Btn_ViewInvoice(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnInvoice"));  
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_view_click(NgWebDriver d)
        {
            this.Btn_ViewInvoice(d).Click();
        }


        private NgWebElement Btn_Settle(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnSettle"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_Settle_Click(NgWebDriver d)
        {
            this.Btn_Settle(d).Click();
        }

        private NgWebElement Btn_PayNow(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("send"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_PayNow_Click(NgWebDriver d)
        {
            this.Btn_PayNow(d).Click();
        }

        private NgWebElement Btn_Back(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnBack"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_Back_Click(NgWebDriver d)
        {
            this.Btn_Back(d).Click();
        }
        public NgWebElement Btn_payType(NgWebDriver d)
        {
            return d.FindElement(By.Id("ddPaymentType")).FindElement(By.CssSelector("#ddPaymentType > option:nth-child(3)"));
        }

        public void Btn_payType_Click(NgWebDriver d)
        {
            this.Btn_payType(d).Click();
        }

        public NgWebElement Btn_filter(NgWebDriver d)
        {
            return d.FindElement(By.Id("btnFilter"));
        }

        public void Btn_filter_Click(NgWebDriver d)
        {
            this.Btn_filter(d).Click();
        }

        public NgWebElement Btn_filterPend(NgWebDriver d)
        {
            return d.FindElement(By.CssSelector("#btnDefault_1 > div.panel.panel-default.no-shadow.bottom-round > div > div > div > div.pull-right > form > div > div.btn-group.open > ul > li:nth-child(4) > a"));
        }

        public void Btn_filterPend_Click(NgWebDriver d)
        {
            this.Btn_filterPend(d).Click();
        }

        private NgWebElement Btn_memPay(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnPayment-link"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_memPay_Click(NgWebDriver d)
        {
            this.Btn_memPay(d).Click();
        }

        private void ScrollToElem(NgWebElement elem, NgWebDriver d)
        {
            Actions actions = new Actions(d);
            actions.MoveToElement(elem);
            actions.Perform();
        }

    }
}
