﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using Protractor;
using System;
using System.Threading;

namespace Membership.PageObjects.ContactPO
{
    class FilterContactPO
    {
        public NgWebElement FilterbyCategory(NgWebDriver d)
        {
            return d.FindElement(By.Id("btnFilter")).FindElement(By.XPath("/html/body/membership-root/membership-layout/div/div/div/membership-contact-details/div/div/div/div/section/div[2]/div/div/div[2]/div/div[1]/ul/li[6]/a"));
        }
        
        public NgWebElement Click_FilterCategory(NgWebDriver d)
        {
            return d.FindElement(By.Id("btnFilter"));
        }
        public NgWebElement ShowAll(NgWebDriver d)
        {
            return d.FindElement(By.Id("btnFilter")).FindElement(By.XPath("//*[@id='btnResetFilter']"));
        }
        public NgWebElement HeadofOperation(NgWebDriver d)
        {
            return d.FindElement(By.Id("btnFilter")).FindElement(By.XPath("/html/body/membership-root/membership-layout/div/div/div/membership-contact-details/div/div/div/div/section/div[2]/div/div/div[2]/div/div[1]/ul/li[5]/a"));
        }

    }
}
