﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using Protractor;
using System;
using System.Threading;


namespace Membership.PageObjects.ContactPO
{
    class SearchContactPO
    {
        public NgWebElement Search(NgWebDriver d)
        {
            return d.FindElement(By.Id("btnSearch"));
        }
        private void ScrollToElem(NgWebElement elem, NgWebDriver d)
        {
            Actions actions = new Actions(d);
            actions.MoveToElement(elem);
            actions.Perform();
        }
    }
}
