﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using Protractor;
using System;
using System.Threading;

namespace Membership.PageObjects.ContactPO
{
    class EditContactPO
    {
        private NgWebElement Btn_Edit(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnEditContact"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_Edit_Click(NgWebDriver d)
        {
            this.Btn_Edit(d).Click();
        }

        public NgWebElement Firstname(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtFname"));
        }
        public NgWebElement Lastname(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtLastName"));
        }
        private NgWebElement Btn_ProfileImage(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.CssSelector("#btnPersonal > div > div > div > form > fieldset > div:nth-child(1) > div:nth-child(3) > label.fileContainer"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_ProfileImage_Click(NgWebDriver d)
        {
            this.Btn_ProfileImage(d).Click();
        }
        public NgWebElement DOB(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtDob"));
        }
        public NgWebElement MartialStatus(NgWebDriver d)
        {

            return d.FindElement(By.Id("ddMartialStatus")).FindElement(By.CssSelector("#ddMartialStatus > option:nth-child(3)"));
        }
        public NgWebElement NIC(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtIdentity"));
        }
        public NgWebElement Category(NgWebDriver d)
        {
            return d.FindElement(By.Id("ddContactType")).FindElement(By.CssSelector("#ddContactType > option:nth-child(6)"));
        }
        private NgWebElement Btn_Update(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnUpdateWork"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_Update_Click(NgWebDriver d)
        {
            this.Btn_Update(d).Click();
        }
        private NgWebElement Btn_ProfileNextButton(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnNextContact"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_ProfileNextButton_Click(NgWebDriver d)
        {
            this.Btn_ProfileNextButton(d).Click();
        }
        public NgWebElement Country(NgWebDriver d)
        {
            return d.FindElement(By.Id("ddCountry")).FindElement(By.CssSelector("#ddCountry > option:nth-child(14)"));
        }
        public NgWebElement City(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtCity"));
        }
        public NgWebElement Postal(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtZip"));
        }
        public NgWebElement Address(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtAddress"));
        }
        public NgWebElement Mobile(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtPhone"));
        }
        public NgWebElement Telephone(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtTelephone"));
        }
        public NgWebElement Email(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtEmail"));
        }
        private NgWebElement Btn_UpdateContact(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("txtContactWork"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_UpdateContact_Click(NgWebDriver d)
        {
            this.Btn_UpdateContact(d).Click();
        }
        private NgWebElement Btn_ContactNextButton(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnNextWork"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_ContactNextButton_Click(NgWebDriver d)
        {
            this.Btn_ContactNextButton(d).Click();
        }
        public NgWebElement Designation(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtDesignation"));
        }
        public NgWebElement Company(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtCompany"));
        }
        public NgWebElement CompanyAddress(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtComAddress"));
        }
        public NgWebElement TeleWork(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtComtelephone"));
        }
        public NgWebElement MobileWork(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtComphone"));
        }
        private NgWebElement Btn_UpdateWork(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnUpdateWork"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_UpdateWork_Click(NgWebDriver d)
        {
            this.Btn_UpdateWork(d).Click();
        }
        private void ScrollToElem(NgWebElement elem, NgWebDriver d)
        {
            Actions actions = new Actions(d);
            actions.MoveToElement(elem);
            actions.Perform();
        }

        public NgWebElement Click_Status(NgWebDriver d)
        {
            return d.FindElement(By.Id("ddMartialStatus"));
        }

        public NgWebElement Click_Cat(NgWebDriver d)
        {
            return d.FindElement(By.Id("ddContactType"));
        }
       
    }
}
