﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using Protractor;
using System;
using System.Threading;

namespace Membership.PageObjects.ContactPO
{
    class ARCHIVEcontactPO
    {
        public void actionsBtn(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnEditContact"));
            ScrollToElem(elem, d);
            elem.Click();
            Thread.Sleep(2000);
        }
        public void archiveBtn(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnArchive"));
            ScrollToElem(elem, d);
            elem.Click();
            Thread.Sleep(2000);
        }
        public void confirmArchive(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnYes"));
            ScrollToElem(elem, d);
            elem.Click();
            Thread.Sleep(2000);
        }
        public NgWebElement searchField(NgWebDriver d)
        {
            return d.FindElement(By.XPath("/html/body/membership-root/membership-layout/div/div/div/membership-contact-details/div/div/div/div/section/div[1]/div/div/div[1]/div[1]/input"));
        }

        public void ConfirmArchive_no(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnNo"));
            ScrollToElem(elem, d);
            elem.Click();
            Thread.Sleep(2000);
        }

        public void contact_back(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Name("btnBack"));
            ScrollToElem(elem, d);
            elem.Click();
            Thread.Sleep(2000);
        }


        private void ScrollToElem(NgWebElement elem, NgWebDriver d)
        {
            Actions actions = new Actions(d);
            actions.MoveToElement(elem);
            actions.Perform();
        }
    }
}

