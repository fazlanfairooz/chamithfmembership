﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using Protractor;
using System;
using System.Threading;

namespace Membership.PageObjects.ContactPO
{
    class RestoreContactPO
    {
        public void BtnActionRestoreContact(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnAction"));
            ScrollToElem(elem, d);
            elem.Click();
            Thread.Sleep(2000);
        }

        public NgWebElement RestoreArchiveContact(NgWebDriver d)
        {

            return d.FindElement(By.Id("btnRestore"));
        }

        public void BtnActionRestore(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnRestoreMember"));
            ScrollToElem(elem, d);
            elem.Click();
            Thread.Sleep(2000);
        }

        public void ConfirmRestore(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnYes"));
            ScrollToElem(elem, d);
            elem.Click();
            Thread.Sleep(2000);
        }

        public void CancelRestore(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnNo"));
            ScrollToElem(elem, d);
            elem.Click();
            Thread.Sleep(2000);
        }
        private void ScrollToElem(NgWebElement elem, NgWebDriver d)
        {
            Actions actions = new Actions(d);
            actions.MoveToElement(elem);
            actions.Perform();
        }
    }
}
