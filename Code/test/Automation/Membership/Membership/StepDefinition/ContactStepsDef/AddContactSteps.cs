﻿using Membership.PageObjects;
using Membership.PageObjects.CategoryPO;
using Membership.PageObjects.NavPage;
using Membership.Pages.LoginPage;
using OpenQA.Selenium.Chrome;
using Protractor;
using System.Threading;
using TechTalk.SpecFlow;
using Membership.PageObjects.ContactPO;
using OpenQA.Selenium;
using System.Windows.Forms;

namespace Membership.StepDefinition.ContactStepsDef
{
    [Binding]
    public sealed class AddContactSteps
    {

        private static NgWebDriver ngDriver = new NgWebDriver(new ChromeDriver());
        private Pages.LoginPage.LoginPage loginPG = new Pages.LoginPage.LoginPage(ngDriver);
        private PageObjects.LoginPO loginPO = new PageObjects.LoginPO();
       private PageObjects.ContactPO.ADDContactPO addCPO = new PageObjects.ContactPO.ADDContactPO();
        private PageObjects.NavPage.NavPage navPG = new PageObjects.NavPage.NavPage();
       // private Pages.ContactPage.ADDContactPage addCPG= new Pages.ContactPage.ADDContactPage(ngDriver);


        [Given(@"The Application Page Loads")]
        public void GivenTheApplicationPageLoads()
        {
            loginPG.LoginPageLoad(ngDriver);
        }

        [Then(@"User enters ""(.*)"" and ""(.*)""")]
        public void ThenUserEnters(string p0,string p1)
        {
            loginPG.login(p0,p1);
            Thread.Sleep(3000);
        }

        [Then(@"Clicks on Submit")]
        public void ThenClicksOnSubmit()
        {
            loginPO.Btn_SignIn_Click(ngDriver);
            Thread.Sleep(3000);
        }

        [Then(@"is navigated to Homepage")]
        public void ThenIsNavigatedToHomepage()
        {
            navPG.Load_Dashboard();
            Thread.Sleep(8000);
        }

        [Then(@"User navigates to Contacts Section")]
        public void ThenUserNavigatesToContactsSection()
        {
            navPG.Btn_Contact_click(ngDriver);
            Thread.Sleep(5000);
                //addCPG.Load_Contact();
                //Thread.Sleep(2000);
        }

        [Then(@"Clicks on Add New Contact")]
        public void ThenClicksOnAddNewContact()
        {
            addCPO.Btn_AddContact_Click(ngDriver);
            Thread.Sleep(5000);
        }

        //[Then(@"Personal Info wizard appears")]
        //public void ThenPersonalInfoWizardAppears()
        //{
        //    addCPG.Load_AddContact();
        //    Thread.Sleep(1000);
        //}
    
        [Then(@"user enters First name as (.*)")]
        public void ThenUserEntersFirstNameAs(string p0)
        {
            addCPO.fName(ngDriver).SendKeys(p0);
        }

        [Then(@"user enters Middle name as (.*)")]
        public void ThenUserEntersMiddleNameAs()
        {
           //IGNORE------
        }

        [Then(@"user enters Last name as (.*)")]
        public void ThenUserEntersLastNameAs(string p0)
        {
            addCPO.lName(ngDriver).SendKeys(p0);
        }

        [Then(@"user selects profile Image")]
        public void ThenUserSelectsProfileImage()
        {
            addCPO.Btn_ProfileImage_Click(ngDriver);
            ngDriver.CurrentWindowHandle.GetType();
            Thread.Sleep(2000);
            SendKeys.SendWait(@"E:\New folder\G.jpg");
            Thread.Sleep(3000);
            SendKeys.SendWait(@"{Enter}");
            Thread.Sleep(3000);
        }

        [Then(@"user enters DOB  as (.*)")]
        public void ThenUserEntersDOBAs(string p0)
        {
          addCPO.dob(ngDriver).Clear();
           addCPO.dob(ngDriver).SendKeys(p0);
        }

        [Then(@"user selects martial Status")]
        public void ThenUserSelectsMartialStatus()
        {
            addCPO.maritalStat(ngDriver);
                addCPO.mStatType(ngDriver).Click();
        }

        [Then(@"user enters NIC as (.*)")]
        public void ThenUserEntersNICAsV(string p0)
        {
            addCPO.nic(ngDriver).SendKeys(p0);
        }

        [Then(@"Selects Category")]
        public void ThenSelectsCategory()
        {
            addCPO.contactType(ngDriver);
            Thread.Sleep(1000);
                addCPO.categoryTyp(ngDriver).Click();
            Thread.Sleep(3000);
        }

        [Then(@"Clicks on the NEXT button")]
        public void ThenClicksOnTheNEXTButton()
        {
            addCPO.nextBtn_P(ngDriver);
            Thread.Sleep(2000);
        }


        [Then(@"Clicks on NEXT button")]
        public void ThenClicksOnNEXTButton()
        {
            addCPO.nextBtn_C(ngDriver); //From contact to work 
            Thread.Sleep(1000);
        }

        [Then(@"Clicks on Contact Information")]
        public void ThenClicksOnContactInformation()
        {
            Thread.Sleep(1000);
        }

        [Then(@"user enters Country as (.*)")]
        public void ThenUserEntersCountryAsUSA(string p0)
        {
            addCPO.countryBtn(ngDriver);
            Thread.Sleep(2000);
                addCPO.countryTyp(ngDriver).Click();
        }

        [Then(@"user enters City as (.*)")]
        public void ThenUserEntersCityAsAmsterdaam(string p0)
        {
            addCPO.city(ngDriver).SendKeys(p0);
        }

        [Then(@"user enters Postal as (.*)")]
        public void ThenUserEntersPostalAs(string p0)
        {
            addCPO.postalCode(ngDriver).SendKeys(p0);
        }
    
        [Then(@"user enters Address as (.*)")]
        public void ThenUserEntersAddressAsNdOlsenLane(string p0)
        {
            addCPO.address(ngDriver).SendKeys(p0);
        }

        [Then(@"user enters Mobile as (.*)")]
        public void ThenUserEntersMobileAs(string p0)
        {
            addCPO.phone(ngDriver).SendKeys(p0);
        }

        [Then(@"user enters Telephone as (.*)")]
        public void ThenUserEntersTelephoneAs(string p0)
        {
            addCPO.telephone(ngDriver).SendKeys(p0);
        }

        [Then(@"user enters Email as (.*)")]
        public void ThenUserEntersEmailAs(string p0)
        {
            addCPO.email(ngDriver).SendKeys(p0);
        }

        [Then(@"user enters Designation as (.*)")]
        public void ThenUserEntersDesignationAs(string p0)
        {
            addCPO.designation(ngDriver).SendKeys(p0);
        }

        [Then(@"user enters Company as (.*)")]
        public void ThenUserEntersCompanyAs(string p0)
        {
            addCPO.company(ngDriver).SendKeys(p0);
            Thread.Sleep(1000);
        }

        [Then(@"user enters Company Address as (.*)")]
        public void ThenUserEntersCompanyAddressAs(string p0)
        {
            addCPO.companyAdd(ngDriver).SendKeys(p0);
            Thread.Sleep(3000);
        }
        [Then(@"user select Company country")]
        public void ThenUserSelectCompanyCountry()
        {
            addCPO.CompanycountryBtn(ngDriver);
            Thread.Sleep(2000);
            addCPO.CompanycountryTyp(ngDriver).Click();
        }

        [Then(@"user enters Office Telephone as (.*)")]
        public void ThenUserEntersOfficeTelephoneAs(string p0)
        {
            addCPO.workTele(ngDriver).SendKeys(p0);
            Thread.Sleep(1000);
        }

        [Then(@"user enters Office Mobile as (.*)")]
        public void ThenUserEntersOfficeMobileAs(string p0)
        {
            addCPO.workMob(ngDriver).SendKeys(p0);
            Thread.Sleep(1000);
        }

        [Then(@"Click on Save")]
        public void ThenClickOnSave()
        {
            addCPO.saveContact(ngDriver);
            Thread.Sleep(1000);
        }

        [Then(@"user selects their martial Status")]
        public void ThenUserSelectsTheirMartialStatus()
        {

            addCPO.maritalStat(ngDriver);
            addCPO.mStatType(ngDriver).Click();
        }

        [Then(@"Selects a Category")]
        public void ThenSelectsACategory()
        {
            addCPO.contactType(ngDriver);
            Thread.Sleep(1000);
            addCPO.categoryTyp(ngDriver).Click();
            Thread.Sleep(3000);
        }


        [Then(@"Error messages are visible on relavant fields")]
        public void ThenErrorMessagesAreVisibleOnRelavantFields()
        {
            Thread.Sleep(5000);
        }

        [Then(@"Log Out")]
        public void ThenLogOut()
        {
            loginPO.Btn_Logout_Click(ngDriver);   
        }

    }
}
