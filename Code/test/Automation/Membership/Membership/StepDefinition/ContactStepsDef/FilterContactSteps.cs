﻿using Membership.PageObjects;
using Membership.PageObjects.CategoryPO;
using Membership.PageObjects.NavPage;
using Membership.Pages.LoginPage;
using OpenQA.Selenium.Chrome;
using Protractor;
using System.Threading;
using TechTalk.SpecFlow;
using Membership.PageObjects.ContactPO;
using OpenQA.Selenium;
using System.Windows.Forms;

namespace Membership.StepDefinition.ContactStepsDef
{
    [Binding]
    public sealed class FilterContactSteps
    {
        private static NgWebDriver ngWebDriver = new NgWebDriver(new ChromeDriver());
        private LoginPage loginPage = new LoginPage(ngWebDriver);
        private LoginPO loginPO = new LoginPO();
        private NavPage navPage = new NavPage();
        private FilterContactPO filterContactPO = new FilterContactPO();

        [Given(@"Load membership Application")]
        public void GivenLoadMembershipApplication()
        {
            loginPage.LoginPageLoad(ngWebDriver);
          
        }

        [Then(@"enter ""(.*)"" and ""(.*)""")]
        public void ThenEnterAnd(string p0, string p1)
        {
            loginPage.login(p0, p1);
            Thread.Sleep(3000);
        }
        [Then(@"I Clicked on Submit btn")]
        public void ThenIClickedOnSubmitBtn()
        {
            loginPO.Btn_SignIn_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        

        [Then(@"Directed to Dashboard")]
        public void ThenDirectedToDashboard()
        {
            navPage.Load_Dashboard();
            Thread.Sleep(4000);
        }

        [Then(@"User click on cotact page")]
        public void ThenUserClickOnCotactPage()
        {
            navPage.Btn_Contact_click(ngWebDriver);
            Thread.Sleep(5000);
        }
        [Then(@"I click on filter by category and clicked on Project Manager")]
        public void ThenIClickOnFilterByCategoryAndClickedOnProjectManager()
        {
            filterContactPO.Click_FilterCategory(ngWebDriver).Click();
            Thread.Sleep(4000);
            filterContactPO.FilterbyCategory(ngWebDriver).Click();
            Thread.Sleep(3000);
        }
        [Then(@"I click on filter by category and clicked on Head of Operation")]
        public void ThenIClickOnFilterByCategoryAndClickedOnHeadOfOperation()
        {
            filterContactPO.Click_FilterCategory(ngWebDriver).Click();
            Thread.Sleep(4000);
            filterContactPO.HeadofOperation(ngWebDriver).Click();
            Thread.Sleep(3000);
        }
        [Then(@"I click on filter by category and clicked show all")]
        public void ThenIClickOnFilterByCategoryAndClickedShowAll()
        {
            filterContactPO.Click_FilterCategory(ngWebDriver).Click();
            Thread.Sleep(5000);
            filterContactPO.ShowAll(ngWebDriver).Click();
            Thread.Sleep(3000);
        }

       
           

       


        [Then(@"I click logout button")]
        public void ThenIClickLogoutButton()
        {
            loginPO.Btn_Logout_Click(ngWebDriver);
        }

    }
}
