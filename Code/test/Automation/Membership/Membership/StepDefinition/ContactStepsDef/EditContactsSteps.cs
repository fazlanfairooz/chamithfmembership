﻿using Membership.PageObjects;
using Membership.PageObjects.CategoryPO;
using Membership.PageObjects.NavPage;
using Membership.Pages.LoginPage;
using OpenQA.Selenium.Chrome;
using Protractor;
using System.Threading;
using TechTalk.SpecFlow;
using Membership.PageObjects.ContactPO;
using OpenQA.Selenium;
using System.Windows.Forms;

namespace Membership.StepDefinition.ContactStepsDef
{
    [Binding]
    public sealed class EditContactsSteps
    {
        private static NgWebDriver ngWebDriver = new NgWebDriver(new ChromeDriver());
        private LoginPage loginPage = new LoginPage(ngWebDriver);
        private LoginPO loginPO = new LoginPO();
        private NavPage navPage = new NavPage();
        private CategoryPO categoryPO = new CategoryPO();
        private EditContactPO editContactsPO = new EditContactPO();

        [Given(@"The memnership Application Page")]
        public void GivenTheMemnershipApplicationPage()
        {
            loginPage.LoginPageLoad(ngWebDriver);
        }

        [Then(@"I enter ""(.*)"" and ""(.*)""")]
        public void ThenIEnterAnd(string p0, string p1)
        {
            loginPage.login(p0, p1);
            Thread.Sleep(3000);
        }

        [Then(@"Click on Submit button")]
        public void ThenClickOnSubmitButton()
        {
            loginPO.Btn_SignIn_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"I navigated to dashboard")]
        public void ThenINavigatedToDashboard()
        {
            navPage.Load_Dashboard();
            Thread.Sleep(4000);
        }

        [Then(@"I click on Contact button")]
        public void ThenIClickOnContactButton()
        {
            navPage.Btn_Contact_click(ngWebDriver);
            Thread.Sleep(5000);
        }

        [Then(@"I select a contact and click on edit button")]
        public void ThenISelectAContactAndClickOnEditButton()
        {
            editContactsPO.Btn_Edit_Click(ngWebDriver);
            Thread.Sleep(2000);
        }
        [Then(@"Enters First name as (.*)")]
        public void ThenEntersFirstNameAsOshani(string p0)
        {
            editContactsPO.Firstname(ngWebDriver).Clear();
            editContactsPO.Firstname(ngWebDriver).SendKeys(p0);
        }

        [Then(@"Enters Last name as (.*)")]
        public void ThenEntersLastNameAsShenaya(string p0)
        {
            editContactsPO.Lastname(ngWebDriver).Clear();
            editContactsPO.Lastname(ngWebDriver).SendKeys(p0);
            Thread.Sleep(3000);
        }

       
        [Then(@"I select profile Image")]
        public void ThenISelectProfileImage()
        {
            editContactsPO.Btn_ProfileImage_Click(ngWebDriver);
            ngWebDriver.CurrentWindowHandle.GetType();
            Thread.Sleep(2000);
            SendKeys.SendWait(@"E:\New folder\G.jpg");
            Thread.Sleep(3000);
            SendKeys.SendWait(@"{Enter}");
            Thread.Sleep(3000);
        }
        [Then(@"Enter DOB as (.*)")]
        public void ThenEnterDOBAs(string p0)
        {
            editContactsPO.DOB(ngWebDriver).SendKeys(p0);
        }
           

        [Then(@"I select martial Status")]
        public void ThenISelectMartialStatus()
        {
            editContactsPO.Click_Status(ngWebDriver).Click();
            Thread.Sleep(2000);
            editContactsPO.MartialStatus(ngWebDriver).Click();
            Thread.Sleep(3000);
        }
        [Then(@"Entered NIC as (.*)")]
        public void ThenEnteredNICAsV(string p0)
        {
            editContactsPO.NIC(ngWebDriver).Clear();
            editContactsPO.NIC(ngWebDriver).SendKeys(p0);
        }

        [Then(@"I Select Category")]
        public void ThenISelectCategory()
        {
            editContactsPO.Category(ngWebDriver).Click();
            Thread.Sleep(3000);
        }

        [Then(@"I Click on the NEXT button to navigate Contact info section")]
        public void ThenIClickOnTheNEXTButtonToNavigateContactInfoSection()
        {
            editContactsPO.Btn_ProfileNextButton_Click(ngWebDriver);
        }

        [Then(@"I select a country")]
        public void ThenISelectACountry()
        {
            editContactsPO.Country(ngWebDriver).Click();
        }
        [Then(@"Enters City (.*)")]
        public void ThenEntersCityAmsterdaam(string p0)
        {
            editContactsPO.City(ngWebDriver).Clear();
            editContactsPO.City(ngWebDriver).SendKeys(p0);
        }

        [Then(@"I Enters Postal as (.*)")]
        public void ThenIEntersPostalAs(string p0)
        {
            editContactsPO.Postal(ngWebDriver).Clear();
            editContactsPO.Postal(ngWebDriver).SendKeys(p0);
        }


        [Then(@"Enter Address as (.*)")]
        public void ThenEnterAddressAsNdOlsenLane(string p0)
        {
            editContactsPO.Address(ngWebDriver).Clear();
            editContactsPO.Address(ngWebDriver).SendKeys(p0);
        }

        [Then(@"Enter Mobile as (.*)")]
        public void ThenEnterMobileAs(string p0)
        {
            editContactsPO.Mobile(ngWebDriver).Clear();
            editContactsPO.Mobile(ngWebDriver).SendKeys(p0);
        }

        [Then(@"Enter Telephone as (.*)")]
        public void ThenEnterTelephoneAs(string p0)
        {
            editContactsPO.Telephone(ngWebDriver).Clear();
            editContactsPO.Telephone(ngWebDriver).SendKeys(p0);
        }

        [Then(@"Enter Email as (.*)")]
        public void ThenEnterEmailAsPerriHotmail_Com(string p0)
        {
            editContactsPO.Email(ngWebDriver).Clear();
            editContactsPO.Email(ngWebDriver).SendKeys(p0);
        }

     
        [Then(@"I Clicks on NEXT button to navigate work info section")]
        public void ThenIClicksOnNEXTButtonToNavigateWorkInfoSection()
        {
            editContactsPO.Btn_ContactNextButton_Click(ngWebDriver);
        }
        [Then(@"Enter Designation as (.*)")]
        public void ThenEnterDesignationAsFinanceIntern(string p0)
        {
            editContactsPO.Designation(ngWebDriver).Clear();
            editContactsPO.Designation(ngWebDriver).SendKeys(p0);
        }

        [Then(@"Enter Company as (.*)")]
        public void ThenEnterCompanyAsHighOctaneFitness(string p0)
        {
            editContactsPO.Company(ngWebDriver).Clear();
            editContactsPO.Company(ngWebDriver).SendKeys(p0);
        }

        [Then(@"I enters Company Address as (.*)")]
        public void ThenIEntersCompanyAddressAsThPostmanSuiteNewYork(string p0)
        {

            editContactsPO.CompanyAddress(ngWebDriver).Clear();
            editContactsPO.CompanyAddress(ngWebDriver).SendKeys(p0);
        }

        [Then(@"I enters Office Telephone as (.*)")]
        public void ThenIEntersOfficeTelephoneAs(string p0)
        {
            editContactsPO.TeleWork(ngWebDriver).Clear();
            editContactsPO.TeleWork(ngWebDriver).SendKeys(p0);
        }

        [Then(@"I enters Office Mobile as (.*)")]
        public void ThenIEntersOfficeMobileAs(string p0)
        {
            editContactsPO.MobileWork(ngWebDriver).Clear();
            editContactsPO.MobileWork(ngWebDriver).SendKeys(p0);
        }

        [Then(@"I Clicked on Save")]
        public void ThenIClickedOnSave()
        {
            editContactsPO.Btn_Update_Click(ngWebDriver);
            Thread.Sleep(3000);
        }

        [Then(@"I clicked on Log Out")]
        public void ThenIClickedOnLogOut()
        {
            loginPO.Btn_Logout_Click(ngWebDriver);
        }

        // Negative

        [Given(@"The memnership Application Pg")]
        public void GivenTheMemnershipApplicationPg()
        {
            loginPage.LoginPageLoad(ngWebDriver);
        }
        [Then(@"I entered un & pw ""(.*)"" and ""(.*)""")]
        public void ThenIEnteredUnPwAnd(string p0, string p1)
        {
          loginPage.login(p0, p1);
            Thread.Sleep(3000);
        }

        [Then(@"Click on Submit")]
        public void ThenClickOnSubmit()
        {
            loginPO.Btn_SignIn_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"I navigated to the dashboard")]
        public void ThenINavigatedToTheDashboard()
        {
            navPage.Load_Dashboard();
            Thread.Sleep(4000);
        }

        [Then(@"I click on Contact")]
        public void ThenIClickOnContact()
        {
            navPage.Btn_Contact_click(ngWebDriver);
            Thread.Sleep(5000);
        }

        [Then(@"I select a contact and click on edit")]
        public void ThenISelectAContactAndClickOnEdit()
        {
            editContactsPO.Btn_Edit_Click(ngWebDriver);
            Thread.Sleep(2000);
        }
        [Then(@"Entered Firstname as (.*)")]
        public void ThenEnteredFirstnameAsRehani(string p0)
        {
            editContactsPO.Firstname(ngWebDriver).Clear();
            editContactsPO.Firstname(ngWebDriver).SendKeys(p0);
        }

        [Then(@"Entered Lastname as (.*)")]
        public void ThenEnteredLastnameAsShenaya(string p0)
        {
            editContactsPO.Lastname(ngWebDriver).Clear();
            editContactsPO.Lastname(ngWebDriver).SendKeys(p0);
        }

              

        [Then(@"I browse profile Image")]
        public void ThenIBrowseProfileImage()
        {
         //RND
        }

        [Then(@"I entered DOB as (.*)")]
        public void ThenIEnteredDOBAs(string p0)
        {
            editContactsPO.DOB(ngWebDriver).SendKeys(p0);
        }

        [Then(@"I select the martial Status")]
        public void ThenISelectTheMartialStatus()
        {
            editContactsPO.Click_Status(ngWebDriver).Click();
            Thread.Sleep(3000);
            editContactsPO.MartialStatus(ngWebDriver).Click();
        }

        [Then(@"I entered NIC as (.*)")]
        public void ThenIEnteredNICAsV(string p0)
        {
            editContactsPO.NIC(ngWebDriver).Clear();
            editContactsPO.NIC(ngWebDriver).SendKeys(p0);
        }

        [Then(@"I Select the Category")]
        public void ThenISelectTheCategory()
        {
            editContactsPO.Click_Cat(ngWebDriver).Click();
            Thread.Sleep(2000);
            editContactsPO.Category(ngWebDriver).Click();
            Thread.Sleep(5000);
        }

        [Then(@"I Click on the next button to navigate Contact info section")]
        public void ThenIClickOnTheNextButtonToNavigateContactInfoSection()
        {
            editContactsPO.Btn_ProfileNextButton_Click(ngWebDriver);
            Thread.Sleep(3000);
        }

        
        [Then(@"I clicked on Log Out button")]
        public void ThenIClickedOnLogOutButton()
        {
            loginPO.Btn_Logout_Click(ngWebDriver);
        }

    }
}
