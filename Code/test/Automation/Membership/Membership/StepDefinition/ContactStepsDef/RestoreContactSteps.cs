﻿using Membership.PageObjects;
using Membership.PageObjects.CategoryPO;
using Membership.PageObjects.NavPage;
using Membership.Pages.LoginPage;
using OpenQA.Selenium.Chrome;
using Protractor;
using System.Threading;
using TechTalk.SpecFlow;
using Membership.PageObjects.ContactPO;
using OpenQA.Selenium;

namespace Membership.StepDefinition.ContactStepsDef
{
    [Binding]
    public sealed class RestoreContactSteps
    {
        private static NgWebDriver ngWebDriver = new NgWebDriver(new ChromeDriver());
        private LoginPage loginPage = new LoginPage(ngWebDriver);
        private LoginPO loginPO = new LoginPO();
        private NavPage navPage = new NavPage();
        private CategoryPO categoryPO = new CategoryPO();
        private EditContactPO editContactsPO = new EditContactPO();
        private RestoreContactPO restoreContactPO = new RestoreContactPO();


        [Given(@"Load the membership login page")]
        public void GivenLoadTheMembershipLoginPage()
        {
            loginPage.LoginPageLoad(ngWebDriver);
        }

        [Then(@"I ""(.*)"" and ""(.*)""")]
        public void ThenIAnd(string p0, string p1)
        {
            loginPage.login(p0, p1);
            Thread.Sleep(3000);
        }


        [Then(@"I Clicks on Submit button")]
        public void ThenIClicksOnSubmitButton()
        {
            loginPO.Btn_SignIn_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"I direct to Dashboard")]
        public void ThenIDirectToDashboard()
        {
            navPage.Load_Dashboard();
            Thread.Sleep(4000);
        }

        [Then(@"Clicked on Contacts pg")]
        public void ThenClickedOnContactsPg()
        {
            navPage.Btn_Contact_click(ngWebDriver);
            Thread.Sleep(5000);
        }

        [Then(@"I clicked on Action button")]
        public void ThenIClickedOnActionButton()
        {
            restoreContactPO.BtnActionRestoreContact(ngWebDriver);
        }

        [Then(@"Clicked on Restore Archive Contacts")]
        public void ThenClickedOnRestoreArchiveContacts()
        {
            restoreContactPO.RestoreArchiveContact(ngWebDriver).Click();
            Thread.Sleep(3000);
        }

        [Then(@"I select a contact and clicked on Button Restore")]
        public void ThenISelectAContactAndClickedOnButtonRestore()
        {
            restoreContactPO.BtnActionRestore(ngWebDriver);
        }

        [Then(@"I confirm the restore contact")]
        public void ThenIConfirmTheRestoreContact()
        {
            restoreContactPO.ConfirmRestore(ngWebDriver);
            Thread.Sleep(3000);
        }
        [Then(@"I did not confirm the restore contact")]
        public void ThenIDidNotConfirmTheRestoreContact()
        {
            restoreContactPO.CancelRestore(ngWebDriver);
            Thread.Sleep(3000);
        }

        [Then(@"I Click on Logout btn")]
        public void ThenIClickOnLogoutBtn()
        {
            loginPO.Btn_Logout_Click(ngWebDriver);
        }

    }
}
