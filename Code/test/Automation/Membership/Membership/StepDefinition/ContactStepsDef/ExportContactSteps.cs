﻿using Membership.PageObjects;
using Membership.PageObjects.CategoryPO;
using Membership.PageObjects.NavPage;
using Membership.Pages.LoginPage;
using OpenQA.Selenium.Chrome;
using Protractor;
using System.Threading;
using TechTalk.SpecFlow;
using Membership.PageObjects.ContactPO;
using OpenQA.Selenium;
using System.Windows.Forms;

namespace Membership.StepDefinition.ContactStepsDef
{

    [Binding]
    public sealed class ExportContactSteps
    {

        private static NgWebDriver ngWebDriver = new NgWebDriver(new ChromeDriver());
        private LoginPage loginPage = new LoginPage(ngWebDriver);
        private LoginPO loginPO = new LoginPO();
        private NavPage navPage = new NavPage();
        private ExportContactPO exportContactPO = new ExportContactPO();


        [Given(@"Load membership Home Page")]
        public void GivenLoadMembershipHomePage()
        {
            loginPage.LoginPageLoad(ngWebDriver);
        }
        [Then(@"I have entered ""(.*)"" and ""(.*)""")]
        public void ThenIHaveEnteredAnd(string p0, string p1)
        {
            loginPage.login(p0, p1);
            Thread.Sleep(3000);
        }


        [Then(@"I Clicked on Submit")]
        public void ThenIClickedOnSubmit()
        {
            loginPO.Btn_SignIn_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"I Directed to Dashboard")]
        public void ThenIDirectedToDashboard()
        {
            navPage.Load_Dashboard();
            Thread.Sleep(4000);
        }

        [Then(@"I click on cotact page")]
        public void ThenIClickOnCotactPage()
        {
            navPage.Btn_Contact_click(ngWebDriver);
            Thread.Sleep(5000);
        }

        [Then(@"I click on export button")]
        public void ThenIClickOnExportButton()
        {
            exportContactPO.ContactExport(ngWebDriver);
            Thread.Sleep(3000);
        }
        [Then(@"I click on Pdf")]
        public void ThenIClickOnPdf()
        {
            exportContactPO.ExportPdf(ngWebDriver).Click();
            Thread.Sleep(2000);
        }
        [Then(@"I click on Csv")]
        public void ThenIClickOnCsv()
        {

            exportContactPO.ExportCsv(ngWebDriver).Click();
            Thread.Sleep(2000);
        }

        [Then(@"I unselect Profile Image")]
        public void ThenIUnselectProfileImage()
        {
            exportContactPO.CheckToggleStatusProfileImage(ngWebDriver);
            Thread.Sleep(1000);
        }

        [Then(@"I unselect Created On")]
        public void ThenIUnselectCreatedOn()
        {
            exportContactPO.CheckToggleStatusCreatedOn(ngWebDriver);
            Thread.Sleep(1000);
        }

        [Then(@"I unselect Identity")]
        public void ThenIUnselectIdentity()
        {
            exportContactPO.CheckToggleStatusIdentity(ngWebDriver);
            Thread.Sleep(1000);
        }

        [Then(@"I unselect DOB")]
        public void ThenIUnselectDOB()
        {
            exportContactPO.CheckToggleStatusDob(ngWebDriver);
            Thread.Sleep(1000);
        }

        [Then(@"I unselect Address")]
        public void ThenIUnselectAddress()
        {
            exportContactPO.CheckToggleStatusAddress(ngWebDriver);
            Thread.Sleep(1000);
        }

        [Then(@"I unselect City")]
        public void ThenIUnselectCity()
        {
            exportContactPO.CheckToggleStatusCity(ngWebDriver);
            Thread.Sleep(1000);
        }

        [Then(@"I unselect Company")]
        public void ThenIUnselectCompany()
        {
            exportContactPO.CheckToggleStatusCompany(ngWebDriver);
            Thread.Sleep(1000);
        }

        [Then(@"I unselect Designation")]
        public void ThenIUnselectDesignation()
        {
            exportContactPO.CheckToggleStatusDesignation(ngWebDriver);
            Thread.Sleep(1000);
        }


        [Then(@"I click Generate Report")]
        public void ThenIClickGenerateReport()
        {
            exportContactPO.GenerateReport(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"I clicked on logout")]
        public void ThenIClickedOnLogout()
        {
            loginPO.Btn_Logout_Click(ngWebDriver);
        }

    }
}
