﻿using Membership.PageObjects;
using Membership.PageObjects.CategoryPO;
using Membership.PageObjects.NavPage;
using Membership.PageObjects.OrganizationSettings;
using Membership.Pages.LoginPage;
using OpenQA.Selenium.Chrome;
using Protractor;
using System.Threading;
using System.Windows.Forms;
using TechTalk.SpecFlow;

namespace Membership.StepDefinition.OrganizationSettings
{
    [Binding]
    public sealed class OrganizationSettingsSteps
    {
        private static NgWebDriver ngWebDriver = new NgWebDriver(new ChromeDriver());
        private LoginPage loginPage = new LoginPage(ngWebDriver);
        private LoginPO loginPO = new LoginPO();
        private NavPage navPage = new NavPage();
        private OrganizationSettingsPO organizationSettingsPO = new OrganizationSettingsPO();

        [Given(@"Load membership login")]
        public void GivenLoadMembershipLogin()
        {
            loginPage.LoginPageLoad(ngWebDriver);
        }

        [Given(@"I have entered username & password ""(.*)"" and ""(.*)""")]
        public void GivenIHaveEnteredUsernamePasswordAnd(string p0, string p1)
        {
            loginPage.login(p0, p1);
        }

        [Given(@"I Click on Sign in button")]
        public void GivenIClickOnSignInButton()
        {
            loginPO.Btn_SignIn_Click(ngWebDriver);
            Thread.Sleep(5000);
        }

        [Then(@"I have Navigated to Dashboard")]
        public void ThenIHaveNavigatedToDashboard()
        {
            navPage.Load_Dashboard();
            Thread.Sleep(4000);
        }

        [Then(@"Click on Settings btn")]
        public void ThenClickOnSettingsBtn()
        {
            navPage.Btn_Settings_Click(ngWebDriver);
            Thread.Sleep(5000);
        }

        [Then(@"I click on Organization Settings")]
        public void ThenIClickOnOrganizationSettings()
        {
            navPage.Btn_OrganizationSettings_Click(ngWebDriver);
            Thread.Sleep(5000);
        }

        [Then(@"I click on image browser button")]
        public void ThenIClickOnImageBrowserButton()
        {
            organizationSettingsPO.Btn_ImageBrowser_Click(ngWebDriver);
            ngWebDriver.CurrentWindowHandle.GetType();
            Thread.Sleep(2000);
            SendKeys.SendWait(@"E:\New folder\G.jpg");
            Thread.Sleep(3000);
            SendKeys.SendWait(@"{Enter}");
            Thread.Sleep(3000);
        }

        [Then(@"I click on image Upload button")]
        public void ThenIClickOnImageUploadButton()
        {
            organizationSettingsPO.Btn_ImageUpload_Click(ngWebDriver);
            Thread.Sleep(10000);
        }

        [Then(@"I have entered email address as (.*)")]
        public void ThenIHaveEnteredEmailAddressAsMembinGmail_Com(string p0)
        {
            organizationSettingsPO.Email(ngWebDriver).Clear();
            organizationSettingsPO.Email(ngWebDriver).SendKeys(p0);
            Thread.Sleep(2000);
        }

        [Then(@"I have entered phone no as (.*)")]
        public void ThenIHaveEnteredPhoneNoAs(string p0)
        {
            organizationSettingsPO.Phone(ngWebDriver).Clear();
            organizationSettingsPO.Phone(ngWebDriver).SendKeys(p0);
            Thread.Sleep(2000);
        }


        [Then(@"I select currency")]
        public void ThenISelectCurrency()
        {
            Thread.Sleep(3000);
            organizationSettingsPO.Click_Currency(ngWebDriver);
        }

        [Then(@"I select country")]
        public void ThenISelectCountry()
        {
            Thread.Sleep(3000);
            organizationSettingsPO.Click_Country(ngWebDriver);
        }

        [Then(@"I have entered fax no as (.*)")]
        public void ThenIHaveEnteredFaxNoAs(string p0)
        {
            organizationSettingsPO.Fax(ngWebDriver).Clear();
            organizationSettingsPO.Fax(ngWebDriver).SendKeys(p0);
        }


        [Then(@"I have entered prefix as (.*)")]
        public void ThenIHaveEnteredPrefixAsMemIn(string p0)
        {
            organizationSettingsPO.Prefix(ngWebDriver).Clear();
            organizationSettingsPO.Prefix(ngWebDriver).SendKeys(p0);
        }

        [Then(@"I have entered address as (.*)")]
        public void ThenIHaveEnteredAddressAsNoNugegoda(string p0)
        {
            organizationSettingsPO.Address(ngWebDriver).Clear();
            organizationSettingsPO.Address(ngWebDriver).SendKeys(p0);
        }

        [Then(@"I have entered state as (.*)")]
        public void ThenIHaveEnteredStateAsWestern(string p0)
        {
            organizationSettingsPO.State(ngWebDriver).Clear();
            organizationSettingsPO.State(ngWebDriver).SendKeys(p0);
        }


        [Then(@"I select language")]
        public void ThenISelectLanguage()
        {
            organizationSettingsPO.Click_Language(ngWebDriver);
        }

        [Then(@"I have entered city as (.*)")]
        public void ThenIHaveEnteredCityAsNugegoda(string p0)
        {
            organizationSettingsPO.City(ngWebDriver).Clear();
            organizationSettingsPO.City(ngWebDriver).SendKeys(p0);
        }

        [Then(@"I have entered zip as (.*)")]
        public void ThenIHaveEnteredZipAs(string p0)
        {
            organizationSettingsPO.Zip(ngWebDriver).Clear();
            organizationSettingsPO.Zip(ngWebDriver).SendKeys(p0);
            Thread.Sleep(3000);
        }
        [Then(@"I click on Update Btn")]
        public void ThenIClickOnUpdateBtn()
        {
            organizationSettingsPO.Btn_Update_Click(ngWebDriver);
        }

        [Then(@"I click on signout")]
        public void ThenIClickOnSignout()
        {
            Thread.Sleep(7000);
            loginPO.Btn_Logout_Click(ngWebDriver);
        }

    }
}

