﻿using Membership.PageObjects;
using Membership.PageObjects.NavPage;
using Membership.PageObjects.ResetPasswordPO;
using Membership.Pages.LoginPage;
using OpenQA.Selenium.Chrome;
using Protractor;
using System.Threading;
using TechTalk.SpecFlow;


namespace Membership.StepDefinition.ResetPassword
{
    [Binding]
    public sealed class ResetPasswordSteps
    {
        private static NgWebDriver ngWebDriver = new NgWebDriver(new ChromeDriver());
        private LoginPage loginPage = new LoginPage(ngWebDriver);
        private LoginPO loginPO = new LoginPO();
        private NavPage navPage = new NavPage();
        private ResetPasswordPO resetPasswordPO = new ResetPasswordPO();


        [Given(@"Load Membership Login pg")]
        public void GivenLoadMembershipLoginPg()
        {
            loginPage.LoginPageLoad(ngWebDriver);
        }

        [Given(@"Enter credentials ""(.*)"" and ""(.*)""")]
        public void GivenEnterCredentialsAnd(string p0, string p1)
        {
            loginPage.login(p0, p1);
        }

        [Given(@"Click on Sign in")]
        public void GivenClickOnSignIn()
        {
            loginPO.Btn_SignIn_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"I Navigated to Dashboard")]
        public void ThenINavigatedToDashboard()
        {
            navPage.Load_Dashboard();
            Thread.Sleep(8000);
        }

        [Then(@"Click on Settings button")]
        public void ThenClickOnSettingsButton()
        {
            navPage.Btn_Settings_Click(ngWebDriver);
            Thread.Sleep(4000);
        }

        [Then(@"Click on Account Settings")]
        public void ThenClickOnAccountSettings()
        {
            navPage.Btn_UserSettings_Click(ngWebDriver);
          //  Assert.AreEqual("http://membershipinexis.azurewebsites.net/#/settings", ngWebDriver.Url);
            Thread.Sleep(3000);
        }
        ///////////////
        [Then(@"I have entered (.*) as Current Password")]
        public void ThenIHaveEnteredADminAsCurrentPassword(string p0)
        {
            resetPasswordPO.CurrentPassword(ngWebDriver).SendKeys(p0);
        }

        [Then(@"I have entered (.*) as new password")]
        public void ThenIHaveEnteredAdminAsNewPassword(string p0)
        {
            resetPasswordPO.NewPassword(ngWebDriver).SendKeys(p0);
        }

        [Then(@"I have entered (.*) as Confirm password")]
        public void ThenIHaveEnteredAdminAsConfirmPassword(string p0)
        {
            resetPasswordPO.ConfirmPassword(ngWebDriver).SendKeys(p0);
        }


               
        [Then(@"I click on Reset Password button")]
        public void ThenIClickOnResetPasswordButton()
        {
            resetPasswordPO.Btn_ResetPassword_Click(ngWebDriver);
           
        }
        [Then(@"I click on Logout")]
        public void ThenIClickOnLogout()
        {
            Thread.Sleep(7000);
            loginPO.Btn_Logout_Click(ngWebDriver);
        }

        //Negative
        [Given(@"Load Membership Login")]
        public void GivenLoadMembershipLogin()
        {
            loginPage.LoginPageLoad(ngWebDriver);
        }

        [Given(@"Enter valid credentials ""(.*)"" and ""(.*)""")]
        public void GivenEnterValidCredentialsAnd(string p0, string p1)
        {
            loginPage.login(p0, p1);
        }

        [Given(@"Click on Signin")]
        public void GivenClickOnSignin()
        {
            loginPO.Btn_SignIn_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"I Navigated to the Dashboard")]
        public void ThenINavigatedToTheDashboard()
        {
            navPage.Load_Dashboard();
            Thread.Sleep(8000);
        }

        [Then(@"Click on the Settings button")]
        public void ThenClickOnTheSettingsButton()
        {
            navPage.Btn_Settings_Click(ngWebDriver);
            Thread.Sleep(4000);
        }

        [Then(@"Click on the Account Settings")]
        public void ThenClickOnTheAccountSettings()
        {
            navPage.Btn_UserSettings_Click(ngWebDriver);
          //  Assert.AreEqual("http://membershipinexis.azurewebsites.net/#/settings", ngWebDriver.Url);
            Thread.Sleep(2000);
        }

        [Then(@"I entered (.*) as Current Password")]
        public void ThenIEnteredADminAsCurrentPassword(string p0)
        {
            resetPasswordPO.CurrentPassword(ngWebDriver).SendKeys(p0);
        }

        [Then(@"I entered (.*) as new password")]
        public void ThenIEnteredADminAsNewPassword(string p0)
        {
            resetPasswordPO.NewPassword(ngWebDriver).SendKeys(p0);
        }

        [Then(@"I entered (.*) as Confirm password")]
        public void ThenIEnteredADminAsConfirmPassword(string p0)
        {
            resetPasswordPO.ConfirmPassword(ngWebDriver).SendKeys(p0);
        }

        [Then(@"I click on Reset Password")]
        public void ThenIClickOnResetPassword()
        {
            resetPasswordPO.Btn_ResetPassword_Click(ngWebDriver);
        }

        [Then(@"I click on Logout button")]
        public void ThenIClickOnLogoutButton()
        {
            Thread.Sleep(7000);
            loginPO.Btn_Logout_Click(ngWebDriver);
        }


    }
}
