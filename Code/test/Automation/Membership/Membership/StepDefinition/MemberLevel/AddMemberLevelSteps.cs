﻿using Membership.PageObjects;
using Membership.PageObjects.CategoryPO;
using Membership.PageObjects.MemberLevelPO;
using Membership.PageObjects.NavPage;
using Membership.PageObjects.OrganizationSettings;
using Membership.Pages.LoginPage;
using OpenQA.Selenium.Chrome;
using Protractor;
using System.Threading;
using System.Windows.Forms;
using TechTalk.SpecFlow;
using OpenQA.Selenium;

namespace Membership.StepDefinition.MemberLevel
{

    [Binding]
    public sealed class AddMemberLevelSteps
    {
        private static NgWebDriver ngWebDriver = new NgWebDriver(new ChromeDriver());
        private LoginPage loginPage = new LoginPage(ngWebDriver);
        private LoginPO loginPO = new LoginPO();
        private NavPage navPage = new NavPage();
        private MemberLevelPO memberLevelPO = new MemberLevelPO();

        [Given(@"The Application login page")]
        public void GivenTheApplicationLoginPage()
        {
            loginPage.LoginPageLoad(ngWebDriver);
        }

        [Then(@"User entered valid username and password ""(.*)"" and ""(.*)""")]
        public void ThenUserEnteredValidUsernameAndPasswordAnd(string p0, string p1)
        {
            loginPage.login(p0, p1);
        }

        [Then(@"I Click on Submit btn")]
        public void ThenIClickOnSubmitBtn()
        {
            loginPO.Btn_SignIn_Click(ngWebDriver);
            Thread.Sleep(5000);
        }

        [Then(@"I navigated to Home Page")]
        public void ThenINavigatedToHomePage()
        {
            navPage.Load_Dashboard();
            Thread.Sleep(4000);
        }
        [Then(@"I Click on Member btn")]
        public void ThenIClickOnMemberBtn()
        {
            navPage.Btn_Member_click(ngWebDriver);
            Thread.Sleep(3000);
        }

     
        [Then(@"I click on levels")]
        public void ThenIClickOnLevels()
        {
            navPage.Btn_MemberLevel_click(ngWebDriver);
        }

        [Then(@"I click on New Level")]
        public void ThenIClickOnNewLevel()
        {
            memberLevelPO.Btn_MemberLevel_click(ngWebDriver);
            Thread.Sleep(3000);
        }

        [Then(@"I entered level name (.*)")]
        public void ThenIEnteredLevelNameOnyx(string p0)
        {
            memberLevelPO.LevelName(ngWebDriver).SendKeys(p0);
        }

        [Then(@"I entered Fee (.*)")]
        public void ThenIEnteredFee(string p0)
        {
            for (int i = 0; i < 15; i++)
            {
                memberLevelPO.Fee(ngWebDriver).SendKeys(OpenQA.Selenium.Keys.Backspace);
            }
            memberLevelPO.Fee(ngWebDriver).SendKeys(p0);
          
        }

        [Then(@"I selects Limited member capacity")]
        public void ThenISelectsLimitedMemberCapacity()
        {
            memberLevelPO.Btn_Limited_click(ngWebDriver);
        }
        [Then(@"I selects Unlimited member capacity")]
        public void ThenISelectsUnlimitedMemberCapacity()
        {
            memberLevelPO.Btn_UnLimited_click(ngWebDriver);
        }

        [Then(@"I entered member (.*)")]
        public void ThenIEnteredMember(string p0)
        {
            memberLevelPO.Capacity(ngWebDriver).Clear();
            memberLevelPO.Capacity(ngWebDriver).SendKeys(p0);
            Thread.Sleep(3000);
        }

        [Then(@"I select membershsip renewal type as weekly")]
        public void ThenISelectMembershsipRenewalTypeAsWeekly()
        {
            memberLevelPO.Btn_Weekly_click(ngWebDriver);
            Thread.Sleep(3000);
        }

        [Then(@"I select membershsip renewal type as daily")]
        public void ThenISelectMembershsipRenewalTypeAsDaily()
        {
            memberLevelPO.Btn_Daily_click(ngWebDriver);
            Thread.Sleep(3000);
        }
        [Then(@"I select membershsip renewal type as Annual")]
        public void ThenISelectMembershsipRenewalTypeAsAnnual()
        {
            memberLevelPO.Btn_Annual_click(ngWebDriver);
            Thread.Sleep(3000);
        }

       
        [Then(@"I select membershsip renewal type as monthly")]
        public void ThenISelectMembershsipRenewalTypeAsMonthly()
        {
            memberLevelPO.Btn_Monthly_click(ngWebDriver);
            Thread.Sleep(3000);
        }

        [Then(@"I entered Description (.*)")]
        public void ThenIEnteredDescriptionPerMonthWithNoCommitment_ForABlackCardMembershipWhichGivesYouAccessToAnyLocationAndTheOptionToBringAFriendForFree_(string p0)
        {
            memberLevelPO.Description(ngWebDriver).SendKeys(p0);
            Thread.Sleep(2000);
        }

        [Then(@"I click on Add Button")]
        public void ThenIClickOnAddButton()
        {
            memberLevelPO.Btn_Add_click(ngWebDriver);
            Thread.Sleep(3000);
        }
        [Then(@"I click on Close button")]
        public void ThenIClickOnCloseButton()
        {
            memberLevelPO.Btn_Close_click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"Click on Signout")]
        public void ThenClickOnSignout()
        {
            Thread.Sleep(2000);
            loginPO.Btn_Logout_Click(ngWebDriver);
        }


    }
}
