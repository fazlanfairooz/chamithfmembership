﻿using Membership.PageObjects;
using Membership.PageObjects.DashboardPO;
using Membership.PageObjects.NavPage;
using Membership.Pages.LoginPage;
using OpenQA.Selenium.Chrome;
using Protractor;
using System.Threading;
using System.Windows.Forms;
using TechTalk.SpecFlow;


namespace Membership.StepDefinition.Dashboard
{
    [Binding]
    public sealed class DashboardSteps
    {
        private static NgWebDriver ngWebDriver = new NgWebDriver(new ChromeDriver());
        private LoginPage loginPage = new LoginPage(ngWebDriver);
        private LoginPO loginPO = new LoginPO();
        private NavPage navPage = new NavPage();
        private DashboardPO dashboardPO = new DashboardPO();
      

        [Given(@"Load membership login page")]
        public void GivenLoadMembershipLoginPage()
        {
            loginPage.LoginPageLoad(ngWebDriver);
        }

        [Given(@"Entered username & password ""(.*)"" and ""(.*)""")]
        public void GivenEnteredUsernamePasswordAnd(string p0, string p1)
        {
            loginPage.login(p0, p1);
        }

        [Given(@"Clicked on Signin button")]
        public void GivenClickedOnSigninButton()
        {
            loginPO.Btn_SignIn_Click(ngWebDriver);
            Thread.Sleep(6000);
        }
       

        [Then(@"Navigated to Dashboard pg")]
        public void ThenNavigatedToDashboardPg()
        {
            navPage.Load_Dashboard();
            Thread.Sleep(6000);
        }

        [Then(@"I click on Last Seven days")]
        public void ThenIClickOnLastSevenDays()
        {
            dashboardPO.Btn_SevenDays_Click(ngWebDriver);
            Thread.Sleep(4000);
        }
        [Then(@"I clicked on Three months")]
        public void ThenIClickedOnThreeMonths()
        {
            dashboardPO.Btn_ThreeMonths_Click(ngWebDriver);
            Thread.Sleep(4000);
        }

        [Then(@"I clicked on six months")]
        public void ThenIClickedOnSixMonths()
        {
            dashboardPO.Btn_SixMonths_Click(ngWebDriver);
            Thread.Sleep(4000);
        }

        [Then(@"I clicked on One year")]
        public void ThenIClickedOnOneYear()
        {
            dashboardPO.Btn_OneYear_Click(ngWebDriver);
            Thread.Sleep(4000);
        }


        [Then(@"I Click on Logout button")]
        public void ThenIClickOnLogoutButton()
        {
            Thread.Sleep(7000);
            loginPO.Btn_Logout_Click(ngWebDriver);
        }

    }
}
