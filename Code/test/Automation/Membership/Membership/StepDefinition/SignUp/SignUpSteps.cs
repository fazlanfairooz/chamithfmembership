﻿using Membership.PageObjects;
using Membership.PageObjects.NavPage;
using Membership.PageObjects.SignUpPO;
using Membership.Pages.LoginPage;
using Membership.Pages.SignUpPage;
using OpenQA.Selenium.Chrome;
using Protractor;
using System.Threading;
using TechTalk.SpecFlow;

namespace Membership.StepDefinition.SignUp
{

    [Binding]
    public sealed class SignUpSteps
    {
        private static NgWebDriver ngWebDriver = new NgWebDriver(new ChromeDriver());
        private SignUpPage signupPage = new SignUpPage(ngWebDriver);
        private LoginPO loginPO = new LoginPO();
        private NavPage navPage = new NavPage();
        private SignUpPO SignUpPO = new SignUpPO();
        private LoginPage loginPage = new LoginPage(ngWebDriver);

        [Given(@"Navigate to Membership Signup page")]
        public void GivenNavigateToMembershipSignupPage()
        {
           signupPage.SignUpPageLoad(ngWebDriver);
        }

        [When(@"I enter Email as (.*)")]
        public void WhenIEnterEmailAsMembership_InexisGmail_Com(string p0)
        {
            SignUpPO.Email(ngWebDriver).SendKeys(p0);
         
        }

        [When(@"I Entered Password as (.*)")]
        public void WhenIEnteredPasswordAsAdmin(string p0)
        {
            SignUpPO.Pwd(ngWebDriver).SendKeys(p0);
        
        }
        [When(@"I entered confirm password as (.*)")]
        public void WhenIEnteredConfirmPasswordAsAdmin(string p0)
        {
            SignUpPO.ConfirmPwd(ngWebDriver).SendKeys(p0);
        }


        [When(@"I Click on Register button")]
        public void WhenIClickOnRegisterButton()
        {
            SignUpPO.Btn_SignUp_Click(ngWebDriver);
        }


        //already registered email
        [Given(@"Navigate to Membership Signup")]
        public void GivenNavigateToMembershipSignup()
        {
            signupPage.SignUpPageLoad(ngWebDriver);
        }

        [When(@"Enter Email as (.*)")]
        public void WhenEnterEmailAsMembership_InexisGmail_Com(string p0)
        {
            SignUpPO.Email(ngWebDriver).SendKeys(p0);
        }

        [When(@"Entered Password as (.*)")]
        public void WhenEnteredPasswordAsAdmin(string p0)
        {
            SignUpPO.Pwd(ngWebDriver).SendKeys(p0);
        }

        [When(@"Entered confirm password as (.*)")]
        public void WhenEnteredConfirmPasswordAsAdmin(string p0)
        {
            SignUpPO.ConfirmPwd(ngWebDriver).SendKeys(p0);
        }

        [When(@"Click on Register button")]
        public void WhenClickOnRegisterButton()
        {
            SignUpPO.Btn_SignUp_Click(ngWebDriver);
            Thread.Sleep(4000);
        }
        [When(@"Click on go to login button")]
        public void WhenClickOnGoToLoginButton()
        {
            SignUpPO.Btn_GoToLogin_Click(ngWebDriver);
            Thread.Sleep(2000);
        }
        //[When(@"Load the membership login page")]
        //public void WhenLoadTheMembershipLoginPage()
        //{
        //    loginPage.LoginPageLoad(ngWebDriver);
        //}

        [When(@"Enter username & password ""(.*)"" and ""(.*)""")]
        public void WhenEnterUsernamePasswordAnd(string p0, string p1)
        {
            loginPage.login(p0, p1);
        }

        [When(@"Click on Sign in button")]
        public void WhenClickOnSignInButton()
        {
            loginPO.Btn_SignIn_Click(ngWebDriver);
            Thread.Sleep(2000);
        }
        [When(@"Navigate to dashboard")]
        public void WhenNavigateToDashboard()
        {
            navPage.Load_Dashboard();
            Thread.Sleep(4000);
        }

        [When(@"Click on logout button")]
        public void WhenClickOnLogoutButton()
        {
            loginPO.Btn_Logout_Click(ngWebDriver);
        }

        //Negative 
        [Given(@"Navigated to Membership Signup")]
        public void GivenNavigatedToMembershipSignup()
        {
            signupPage.SignUpPageLoad(ngWebDriver);
        }

        [When(@"Entered Email as (.*)")]
        public void WhenEnteredEmailAsMembership_InexisGmail_Com(string p0)
        {
            SignUpPO.Email(ngWebDriver).SendKeys(p0);
        }

        [When(@"I entered Password as (.*)")]
        public void WhenIenteredPasswordAsAdmin(string p0)
        {
            SignUpPO.Pwd(ngWebDriver).SendKeys(p0);
        }

        [When(@"Enter confirm password as (.*)")]
        public void WhenEnterConfirmPasswordAsADmin(string p0)
        {
            SignUpPO.ConfirmPwd(ngWebDriver).SendKeys(p0);
        }

        [When(@"Clicked on Register button")]
        public void WhenClickedOnRegisterButton()
        {
            SignUpPO.Btn_SignUp_Click(ngWebDriver);
            Thread.Sleep(3000);
        }

    }
}
