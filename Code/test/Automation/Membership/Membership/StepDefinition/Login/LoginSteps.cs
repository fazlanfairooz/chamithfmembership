﻿using Membership.PageObjects;
using Membership.PageObjects.NavPage;
using Membership.Pages.LoginPage;
using OpenQA.Selenium.Chrome;
using Protractor;
using System.Threading;
using TechTalk.SpecFlow;

namespace Membership.StepDefinition.Login
{
    [Binding]
    class LoginSteps
    {
        private static NgWebDriver ngWebDriver = new NgWebDriver(new ChromeDriver());
        private LoginPage loginPage = new LoginPage(ngWebDriver);
        private LoginPO loginPO = new LoginPO();
        private NavPage navPage = new NavPage();
    
        [Given(@"Navigate to Membership Login Page")]
        public void GivenNavigateToMembershipLoginPage()
        {
            loginPage.LoginPageLoad(ngWebDriver);
        }

        [When(@"I enter Username as (.*)")]
        public void WhenIEnterUsernameAs(string p0)
        {
            loginPO.UName(ngWebDriver).SendKeys(p0);
        }

        [When(@"I Enter Password as (.*)")]
        public void WhenIEnterPasswordAs(string p0)
        {
            loginPO.Pwd(ngWebDriver).SendKeys(p0);
        }


        [When(@"I Click on Sign In")]
        public void WhenIClickOnSignIn()
        {
            loginPO.Btn_SignIn_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"I should be directed to Dashboard")]
        public void ThenIShouldBeDirectedToDashboard()
        {
            navPage.Load_Dashboard();
            Thread.Sleep(4000);

        }

        [Then(@"I Click on Logout Button")]
        public void ThenIClickOnLogoutButton()
        {
            Thread.Sleep(4000);
            loginPO.Btn_Logout_Click(ngWebDriver);
        }

        [Then(@"navigated to login page")]
        public void ThenNavigatedToLoginPage()
        {
            loginPage.LoginPageLoad(ngWebDriver);
        }
        //With Invalid Credintials
        [Given(@"Navigate to Membership Login")]
        public void GivenNavigateToMembershipLogin()
        {
            loginPage.LoginPageLoad(ngWebDriver);
        }

        [When(@"Enter Username as (.*)")]
        public void WhenEnterUsernameAsJayathriGmail_Com(string p0)
        {
            loginPO.UName(ngWebDriver).SendKeys(p0);
        }

        [When(@"Enter Password as (.*)")]
        public void WhenEnterPasswordAsAdmin(string p0)
        {
            loginPO.Pwd(ngWebDriver).SendKeys(p0);
        }

      [When(@"Click on Sign In & user shown an error message")]
        public void WhenClickOnSignInUserShownAnErrorMessage()
        {
            loginPO.Btn_SignIn_Click(ngWebDriver);
            Thread.Sleep(3000);
        }


    }
}
