﻿using Membership.PageObjects;
using Membership.PageObjects.CategoryPO;
using Membership.PageObjects.NavPage;
using Membership.Pages.LoginPage;
using OpenQA.Selenium.Chrome;
using Protractor;
using System.Threading;
using TechTalk.SpecFlow;
using Membership.PageObjects.MemberPO;
using System.Windows.Forms;
using System;
using System.Diagnostics;

namespace Membership.StepDefinition.Member
{
    [Binding]
    public sealed class AddMemberSteps
    {
        private static NgWebDriver ngWebDriver = new NgWebDriver(new ChromeDriver());
        private LoginPage loginPage = new LoginPage(ngWebDriver);
        private LoginPO loginPO = new LoginPO();
        private NavPage navPage = new NavPage();
        private CategoryPO categoryPO = new CategoryPO();
        private MemberPO memberPO = new MemberPO();

        [Given(@"The Application Pg")]
        public void GivenTheApplicationPg()
        {
            loginPage.LoginPageLoad(ngWebDriver);
        }

        [Then(@"I User enters ""(.*)"" and ""(.*)""")]
        public void ThenIUserEntersAnd(string p0, string p1)
        {
            loginPage.login(p0, p1);
        }

        [Then(@"I Click on Submit")]
        public void ThenIClickOnSubmit()
        {
            loginPO.Btn_SignIn_Click(ngWebDriver);
            Thread.Sleep(2000);
            
        }

        [Then(@"I navigated to Homepage")]
        public void ThenINavigatedToHomepage()
        {
            navPage.Load_Dashboard();
            Thread.Sleep(5000);
        }

        [Then(@"I directed to Members Section")]
        public void ThenIDirectedToMembersSection()
        {
            navPage.Btn_Member_click(ngWebDriver);
            Thread.Sleep(5000);
        }

        [Then(@"I Clicks on Add New Member")]
        public void ThenIClicksOnAddNewMember()
        {
            memberPO.Btn_NewMember_Click(ngWebDriver);
            Thread.Sleep(2000);
        }
           

        [Then(@"I enters First name as (.*)")]
        public void ThenIEntersFirstNameAsKrishni(string p0)
        {
            memberPO.Firstname(ngWebDriver).SendKeys(p0);
        }

        [Then(@"I enters Last name as (.*)")]
        public void ThenIEntersLastNameAsAmaya(string p0)
        {
            memberPO.Lastname(ngWebDriver).SendKeys(p0);
        }

        [Then(@"I selects profile Image")]
        public void ThenISelectsProfileImage()
        {
            memberPO.Btn_ProfileImage_Click(ngWebDriver);
            ngWebDriver.CurrentWindowHandle.GetType();
            Thread.Sleep(2000);
            SendKeys.SendWait(@"E:\New folder\G.jpg");
            Thread.Sleep(3000);
            SendKeys.SendWait(@"{Enter}");
            Thread.Sleep(3000);
        }

        [Then(@"I enters DOB  as (.*)")]
        public void ThenIEntersDOBAs(string p0)
        {
            memberPO.DOB(ngWebDriver).Clear();
            memberPO.DOB(ngWebDriver).SendKeys(p0);
        }

        [Then(@"I selects martial Status")]
        public void ThenISelectsMartialStatus()
        {
            memberPO.Click_Status(ngWebDriver).Click();
            Thread.Sleep(3000);
           memberPO.MartialStatus(ngWebDriver).Click();
        }

        [Then(@"I enters NIC as (.*)")]
        public void ThenIEntersNICAsV(string p0)
        {
            memberPO.NIC(ngWebDriver).SendKeys(p0);
            Thread.Sleep(2000);
        }

        [Then(@"I Clicks on the NEXT button")]
        public void ThenIClicksOnTheNEXTButton()
        {
            memberPO.Btn_NextPersonal_Click(ngWebDriver);
            Thread.Sleep(5000);
        }
        [Then(@"I select a Country")]
        public void ThenISelectACountry()
        {
            memberPO.Country(ngWebDriver).Click();
            Thread.Sleep(2000);
            //memberPO.Click_Country(ngWebDriver).Click();
            //Thread.Sleep(3000);
           
        }
        
        [Then(@"I enters City as (.*)")]
        public void ThenIEntersCityAsAuckland(string p0)
        {

            memberPO.City(ngWebDriver).SendKeys(p0);
            Thread.Sleep(2000);
        }

        [Then(@"I enters Postal as (.*)")]
        public void ThenIEntersPostalAs(string p0)
        {
            memberPO.Postal(ngWebDriver).SendKeys(p0);
        }

        [Then(@"I enters Address as (.*)")]
        public void ThenIEntersAddressAsAucklandNewZealand(string p0)
        {
          memberPO.Address(ngWebDriver).SendKeys(p0);
        }

        [Then(@"I entered Mob as (.*)")]
        public void ThenIEnteredMobAs(string p0)
        {
            memberPO.Mobile(ngWebDriver).SendKeys(p0);
        }

        [Then(@"I enters Telephone as (.*)")]
        public void ThenIEntersTelephoneAs(string p0)
        {
            memberPO.Telephone(ngWebDriver).SendKeys(p0);
        }

        [Then(@"I enters Email as (.*)")]
        public void ThenIEntersEmailAsKrishniHotmail_Com(string p0)
        {
            memberPO.Email(ngWebDriver).SendKeys(p0);
        }

        [Then(@"I Clicks on NEXT button")]
        public void ThenIClicksOnNEXTButton()
        {
            memberPO.Btn_ContactNextButton_Click(ngWebDriver);
        }

        [Then(@"I enters Designation as (.*)")]
        public void ThenIEntersDesignationAsHRIntern(string p0)
        {
            memberPO.Designation(ngWebDriver).SendKeys(p0);
        }

        [Then(@"I enters Company as (.*)")]
        public void ThenIEntersCompanyAsMoreOctaneFitness(string p0)
        {
            memberPO.Company(ngWebDriver).SendKeys(p0);
        }
        [Then(@"I entered Company address as (.*)")]
        public void ThenIEnteredCompanyAddressAsPostmanSuiteNewYork(string p0)
        {
            memberPO.CompanyAddress(ngWebDriver).SendKeys(p0);
        }

        [Then(@"I select my work country")]
        public void ThenISelectMyWorkCountry()
        {
            memberPO.Country(ngWebDriver).Click();
            Thread.Sleep(2000);
        }

        [Then(@"I entered Ofc Telephone as (.*)")]
        public void ThenIEnteredOfcTelephoneAs(string p0)
        {
            memberPO.TeleWork(ngWebDriver).SendKeys(p0);
        }

        [Then(@"user enters Ofc Mobile as (.*)")]
        public void ThenUserEntersOfcMobileAs(string p0)
        {
            memberPO.MobileWork(ngWebDriver).SendKeys(p0);
        }

        [Then(@"Click one Next Button")]
        public void ThenClickOneNextButton()
        {
            memberPO.Btn_WorkNextButton_Click(ngWebDriver);
        }

        [Then(@"Select a Membership Level")]
        public void ThenSelectAMembershipLevel()
        {
            memberPO.MemberLevel(ngWebDriver);
        }
        [Then(@"Click on Next button")]
        public void ThenClickOnNextButton()
        {
            memberPO.Btn_MemberLevelNext_Click(ngWebDriver);
        }

        [Then(@"I navigated to Payments")]
        public void ThenINavigatedToPayments()
        {
            Thread.Sleep(3000);
        }

        [Then(@"I select Transaction type")]
        public void ThenISelectTransactionType()
        {
            memberPO.TransactionType(ngWebDriver);
        }

        [Then(@"I select Payment Type")]
        public void ThenISelectPaymentType()
        {
            memberPO.PaymentType(ngWebDriver);
        }
        [Then(@"I Enter Description as (.*)")]
        public void ThenIEnterDescriptionAsMembershipPayment(string p0)
        {
            memberPO.Descript(ngWebDriver).SendKeys(p0);
        }


        [Then(@"Click on Paynow Button")]
        public void ThenClickOnPaynowButton()
        {
            memberPO.Btn_PayNow_Click(ngWebDriver);
        }


        [Then(@"I click on logout")]
        public void ThenIClickOnLogout()
        {
            loginPO.Btn_Logout_Click(ngWebDriver);
        }
        //negative
        [Given(@"The membership portal")]
        public void GivenTheMembershipPortal()
        {
            loginPage.LoginPageLoad(ngWebDriver);
        }
        [Then(@"Entered the ""(.*)"" and ""(.*)""")]
        public void ThenEnteredTheAnd(string p0, string p1)
        {
            loginPage.login(p0, p1);
        }


        [Then(@"I Click on Submit button")]
        public void ThenIClickOnSubmitButton()
        {
            loginPO.Btn_SignIn_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"I navigated to Homepage page")]
        public void ThenINavigatedToHomepagePage()
        {
            navPage.Load_Dashboard();
            Thread.Sleep(5000);
        }

        [Then(@"I directed to Member Section")]
        public void ThenIDirectedToMemberSection()
        {
            navPage.Btn_Member_click(ngWebDriver);
            Thread.Sleep(5000);
        }

        [Then(@"I Click on Add New Member")]
        public void ThenIClickOnAddNewMember()
        {
            memberPO.Btn_NewMember_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"I entered First name as (.*)")]
        public void ThenIEnteredFirstNameAsRocheal(string p0)
        {
            memberPO.Firstname(ngWebDriver).SendKeys(p0);
        }

        [Then(@"I entered Last name as (.*)")]
        public void ThenIEnteredLastNameAsStew(string p0)
        {
            memberPO.Lastname(ngWebDriver).SendKeys(p0);
        }

        [Then(@"I selected profile Image")]
      //static extern bool SetForegroundWindow(IntPtr hWnd);

        public void ThenISelectedProfileImage()
        {
            //IntPtr p;
            //SetForegroundWindow(hWnd: p);
            memberPO.Btn_ProfileImage_Click(ngWebDriver);
            Thread.Sleep(2000);
            SendKeys.SendWait(@"E:\New folder\G.jpg");
            Thread.Sleep(3000);
            SendKeys.SendWait(@"{Enter}");
            Thread.Sleep(3000);
        }

        [Then(@"I entered DOB  as (.*)")]
        public void ThenIEnteredDOBAs(string p0)
        {
            memberPO.DOB(ngWebDriver).SendKeys(p0);
        }

        [Then(@"I selected martial Status")]
        public void ThenISelectedMartialStatus()
        {
            memberPO.Click_Status(ngWebDriver).Click();
            Thread.Sleep(3000);
            memberPO.MartialStatus(ngWebDriver).Click();
        }
        [Then(@"I have enter NIC as (.*)")]
        public void ThenIHaveEnterNICAs(string p0)
        {
            memberPO.NIC(ngWebDriver).SendKeys(p0);
        }

        [Then(@"I Clicked on the NEXT button and display the error message")]
        public void ThenIClickedOnTheNEXTButtonAndDisplayTheErrorMessage()
        {
            memberPO.Btn_NextPersonal_Click(ngWebDriver);
            Thread.Sleep(4000);
        }

        [Then(@"I clicked on logout button")]
        public void ThenIClickedOnLogoutButton()
        {
            loginPO.Btn_Logout_Click(ngWebDriver);
        }
        //Invalid data
        [Given(@"The Application Page load")]
        public void GivenTheApplicationPageLoad()
        {
            loginPage.LoginPageLoad(ngWebDriver);
        }

        [Then(@"User entered un & pw ""(.*)"" and ""(.*)""")]
        public void ThenUserEnteredUnPwAnd(string p0, string p1)
        {
            loginPage.login(p0, p1);
        }

        [Then(@"Clicks on Submit btn")]
        public void ThenClicksOnSubmitBtn()
        {
            loginPO.Btn_SignIn_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"is navigated to Homepage page")]
        public void ThenIsNavigatedToHomepagePage()
        {
            navPage.Load_Dashboard();
            Thread.Sleep(4000);
        }

        [Then(@"User navigates to Members Section")]
        public void ThenUserNavigatesToMembersSection()
        {
            navPage.Btn_Member_click(ngWebDriver);
            Thread.Sleep(5000);
        }

        [Then(@"I Click on Add New Member button")]
        public void ThenIClickOnAddNewMemberButton()
        {
            memberPO.Btn_NewMember_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"I entered Firstname as (.*)")]
        public void ThenIEnteredFirstnameAs(string p0)
        {
            memberPO.Firstname(ngWebDriver).SendKeys(p0);
        }

        [Then(@"I entered Lastname as (.*)")]
        public void ThenIEnteredLastnameAs(string p0)
        {
            memberPO.Lastname(ngWebDriver).SendKeys(p0);
        }

        [Then(@"User selected profile Image")]
        public void ThenUserSelectedProfileImage()
        {
            memberPO.Btn_ProfileImage_Click(ngWebDriver);
            Thread.Sleep(2000);
            SendKeys.SendWait(@"E:\New folder\G.jpg");
            Thread.Sleep(3000);
            SendKeys.SendWait(@"{Enter}");
            Thread.Sleep(3000);
        }

        [Then(@"User entered DOB  as (.*)")]
        public void ThenUserEnteredDOBAs(string p0)
        {
            memberPO.DOB(ngWebDriver).SendKeys(p0);
        }

        [Then(@"User selected martial Status")]
        public void ThenUserSelectedMartialStatus()
        {
            memberPO.Click_Status(ngWebDriver).Click();
            Thread.Sleep(3000);
            memberPO.MartialStatus(ngWebDriver).Click();
        }

        [Then(@"User have enter NIC as (.*)")]
        public void ThenUserHaveEnterNICAs(string p0)
        {
            memberPO.NIC(ngWebDriver).SendKeys(p0);
        }
        [Then(@"User Clicked on the NEXT button")]
        public void ThenUserClickedOnTheNEXTButton()
        {
            memberPO.Btn_NextPersonal_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        
        [Then(@"I selected a Country")]
        public void ThenISelectedACountry()
        {
            memberPO.Country(ngWebDriver).Click();
        }

        [Then(@"User enter City as (.*)")]
        public void ThenUserEnterCityAsJabriya(string p0)
        {
            memberPO.City(ngWebDriver).SendKeys(p0);
            Thread.Sleep(2000);
        }

        [Then(@"User enter Postal as (.*)")]
        public void ThenUserEnterPostalAs(string p0)
        {
            memberPO.Postal(ngWebDriver).SendKeys(p0);
        }

        [Then(@"User enter Address as (.*)")]
        public void ThenUserEnterAddressAsBayanStreeet(string p0)
        {
            memberPO.Address(ngWebDriver).SendKeys(p0);
        }

        [Then(@"User enters Mob as (.*)")]
        public void ThenUserEntersMobAs(string p0)
        {
            memberPO.Mobile(ngWebDriver).SendKeys(p0);
        }

        [Then(@"User enter Telephone as (.*)")]
        public void ThenUserEnterTelephoneAs(string p0)
        {
            memberPO.Telephone(ngWebDriver).SendKeys(p0);
        }

        [Then(@"User enter Email as (.*)")]
        public void ThenUserEnterEmailAsSalvatoryGmal_(string p0)
        {
            memberPO.Email(ngWebDriver).SendKeys(p0);
        }
        [Then(@"I Clicked on NEXT button and display error msg")]
        public void ThenIClickedOnNEXTButtonAndDisplayErrorMsg()
        {
            memberPO.Btn_ContactNextButton_Click(ngWebDriver);
        }

                  
        [Then(@"User clicked on logout button")]
        public void ThenUserClickedOnLogoutButton()
        {
            loginPO.Btn_Logout_Click(ngWebDriver);
        }

    }
}
